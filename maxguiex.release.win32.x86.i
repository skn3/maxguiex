ModuleInfo "History: 1.06"
ModuleInfo "History: added CreatePanelEx() function which creates an extended panel that can have new cool stuff"
ModuleInfo "History: added SetPanelExGradient() function to set gradient backgrounds for extended panels"
ModuleInfo "History: 1.05"
ModuleInfo "History: added PointOverGadget() function"
ModuleInfo "History: 1.04"
ModuleInfo "History: fixed broken GadgetWindow() function"
ModuleInfo "History: 1.03"
ModuleInfo "History: Added GetAppResourcesPath() and tweaked h files"
ModuleInfo "History: 1.02"
ModuleInfo "History: Added ScrollTextAreaToTop() ScrollTextAreaToBottom() ScrollTextAreaToCursor() functions"
ModuleInfo "History: 1.01"
ModuleInfo "History: Added SetTextAreaLineSpacing() function"
ModuleInfo "History: 1.00"
ModuleInfo "History: Initial Release To Public"
import brl.blitz
import brl.map
import brl.linkedlist
import maxgui.drivers
import skn3.systemex
skn3_clientToScreen%(hwnd%,point%% Var)S="ClientToScreen@8"
skn3_loadCursorFromFile%(path$w)S="LoadCursorFromFileW@4"
skn3_destroyCursor%(hcursor%)S="DestroyCursor@4"
skn3_addFontResourceEx%(path$w,fl%,pdv%)S="AddFontResourceExW@12"
skn3_addFontMemResourceEx%(pbFont@*,cbFont%,pdv%,pcFonts@*)S="AddFontMemResourceEx@16"
skn3_sendMessagePtr%(hwnd%,MSG%,wParam@*,lParam@*)S="SendMessageW@16"
BCM_GETIDEALSIZE%=5633
BCM_GETTEXTMARGIN%=5637
FR_PRIVATE%=16
Skn3CustomPointer^Object{
all:TMap&=mem:p("_skn3_maxguiex_Skn3CustomPointer_all")
.path$&
.pointer%&
.refCount%&
-New%()="_skn3_maxguiex_Skn3CustomPointer_New"
-Delete%()="_skn3_maxguiex_Skn3CustomPointer_Delete"
}="skn3_maxguiex_Skn3CustomPointer"
RequestScrollbarSize%()="skn3_maxguiex_RequestScrollbarSize"
SetComboBoxHeight%(comboBox:TGadget,Height%)="skn3_maxguiex_SetComboBoxHeight"
GadgetScreenPosition%&[](gadget:TGadget,client%=0)="skn3_maxguiex_GadgetScreenPosition"
PointOverGadget%(pointX%,pointY%,targetGadget:TGadget,sourceGadget:TGadget="bbNullObject")="skn3_maxguiex_PointOverGadget"
DisableGadgetRedraw%(gadget:TGadget)="skn3_maxguiex_DisableGadgetRedraw"
EnableGadgetRedraw%(gadget:TGadget)="skn3_maxguiex_EnableGadgetRedraw"
MessageBox%(title$,message$,parent:TGadget="bbNullObject")="skn3_maxguiex_MessageBox"
GadgetSizeForString%&[](gadget:TGadget,text$,maxWidth%=0)="skn3_maxguiex_GadgetSizeForString"
GetCreationGroup:TGadget(Gadget:TGadget)="skn3_maxguiex_GetCreationGroup"
SetGadgetReadOnly%(gadget:TGadget,yes%)="skn3_maxguiex_SetGadgetReadOnly"
SetGadgetMaxLength%(gadget:TGadget,length%=0)="skn3_maxguiex_SetGadgetMaxLength"
GetGadgetMaxLength%(gadget:TGadget)="skn3_maxguiex_GetGadgetMaxLength"
LoadCustomPointer:Skn3CustomPointer(path$)="skn3_maxguiex_LoadCustomPointer"
SetCustomPointer%(pointer:Skn3CustomPointer)="skn3_maxguiex_SetCustomPointer"
FreeCustomPointer%(pointer:Skn3CustomPointer)="skn3_maxguiex_FreeCustomPointer"
ExtractCursorHotspot%&[](path$,index%=0)="skn3_maxguiex_ExtractCursorHotspot"
ListBatchLock%(Gadget:TGadget)="skn3_maxguiex_ListBatchLock"
ListBatchAdd%(Gadget:TGadget,text$,flags%,icon%,tip$,extra:Object="bbNullObject")="skn3_maxguiex_ListBatchAdd"
ListBatchUnlock%(Gadget:TGadget)="skn3_maxguiex_ListBatchUnlock"
GadgetWindow:TGadget(Gadget:TGadget)="skn3_maxguiex_GadgetWindow"
SetWindowAlwaysOnTop%(Window:TGadget,State%=0)="skn3_maxguiex_SetWindowAlwaysOnTop"
BringWindowToTop%(Window:TGadget)="skn3_maxguiex_BringWindowToTop"
FocusWindow%(Window:TGadget)="skn3_maxguiex_FocusWindow"
GadgetToInt%(Gadget:TGadget)="skn3_maxguiex_GadgetToInt"
SetColorPickerCustomColors%(colors%&[])="skn3_maxguiex_SetColorPickerCustomColors"
ClearColorPickerCustomColors%()="skn3_maxguiex_ClearColorPickerCustomColors"
RedrawGadgetFrame%(Gadget:TGadget)="skn3_maxguiex_RedrawGadgetFrame"
HideGadgetBorder%(Gadget:TGadget)="skn3_maxguiex_HideGadgetBorder"
InstallGuiFont%(path$)="skn3_maxguiex_InstallGuiFont"
SetTextareaLineSpacing%(Gadget:TGadget,lineSpacing#)="skn3_maxguiex_SetTextareaLineSpacing"
ScrollTextAreaToTop%(Gadget:TGadget)="skn3_maxguiex_ScrollTextAreaToTop"
ScrollTextAreaToBottom%(Gadget:TGadget)="skn3_maxguiex_ScrollTextAreaToBottom"
ScrollTextAreaToCursor%(Gadget:TGadget)="skn3_maxguiex_ScrollTextAreaToCursor"
GetAppResourcesPath$()="skn3_maxguiex_GetAppResourcesPath"
AssignGadgetClassId%()="skn3_maxguiex_AssignGadgetClassId"
Skn3PanelEx^TWindowsPanel{
.gradientOn%&
.gradientVertical%&
.gradientStartR%&
.gradientStartG%&
.gradientStartB%&
.gradientEndR%&
.gradientEndG%&
.gradientEndB%&
-New%()="_skn3_maxguiex_Skn3PanelEx_New"
-Delete%()="_skn3_maxguiex_Skn3PanelEx_Delete"
-WndProc%(hwnd%,MSG%,wp%,lp%)="_skn3_maxguiex_Skn3PanelEx_WndProc"
-SetGradient%(on%,r1%=0,b1%=0,g1%=0,r2%=0,b2%=0,g2%=0,vertical%=1)="_skn3_maxguiex_Skn3PanelEx_SetGradient"
}="skn3_maxguiex_Skn3PanelEx"
CreatePanelEx:TGadget(x%,y%,Width%,Height%,group:TGadget,Style%=0,text$=$"")="skn3_maxguiex_CreatePanelEx"
SetPanelExGradient%(Gadget:TGadget,on%,r1%=0,b1%=0,g1%=0,r2%=0,b2%=0,g2%=0,vertical%=1)="skn3_maxguiex_SetPanelExGradient"
