	format	MS COFF
	extrn	_AddFontMemResourceEx@16
	extrn	_AddFontResourceExW@12
	extrn	_AlphaBlend@44
	extrn	_BitBlt@36
	extrn	_ClientToScreen@8
	extrn	_CreateCompatibleBitmap@12
	extrn	_CreateCompatibleDC@4
	extrn	_CreateSolidBrush@4
	extrn	_DeleteDC@4
	extrn	_DeleteObject@4
	extrn	_DestroyCursor@4
	extrn	_DrawTextW@20
	extrn	_FillRect@12
	extrn	_GetClientRect@8
	extrn	_GetClipBox@8
	extrn	_GetDC@4
	extrn	_GetUpdateRect@12
	extrn	_GetWindowLongW@8
	extrn	_GetWindowRect@8
	extrn	_IsRectEmpty@4
	extrn	_LoadCursorFromFileW@4
	extrn	_RedrawWindow@16
	extrn	_ReleaseDC@8
	extrn	_SelectObject@8
	extrn	_SendMessageA@16
	extrn	_SendMessageW@16
	extrn	_SetCursor@4
	extrn	_SetFocus@4
	extrn	_SetStretchBltMode@8
	extrn	_SetWindowLongW@12
	extrn	_SetWindowPos@28
	extrn	_StretchBlt@44
	extrn	___bb_blitz_blitz
	extrn	___bb_drivers_drivers
	extrn	___bb_linkedlist_linkedlist
	extrn	___bb_map_map
	extrn	___bb_systemex_systemex
	extrn	__maxgui_maxgui_TGadget_AddText
	extrn	__maxgui_maxgui_TGadget_AreaLen
	extrn	__maxgui_maxgui_TGadget_AreaText
	extrn	__maxgui_maxgui_TGadget_CharAt
	extrn	__maxgui_maxgui_TGadget_CharX
	extrn	__maxgui_maxgui_TGadget_CharY
	extrn	__maxgui_maxgui_TGadget_CleanUp
	extrn	__maxgui_maxgui_TGadget_Clear
	extrn	__maxgui_maxgui_TGadget_ClearListItems
	extrn	__maxgui_maxgui_TGadget_CountKids
	extrn	__maxgui_maxgui_TGadget_DoLayout
	extrn	__maxgui_maxgui_TGadget_GetCursorPos
	extrn	__maxgui_maxgui_TGadget_GetGroup
	extrn	__maxgui_maxgui_TGadget_GetHeight
	extrn	__maxgui_maxgui_TGadget_GetMenu
	extrn	__maxgui_maxgui_TGadget_GetProp
	extrn	__maxgui_maxgui_TGadget_GetSelectionLength
	extrn	__maxgui_maxgui_TGadget_GetSensitivity
	extrn	__maxgui_maxgui_TGadget_GetStatusText
	extrn	__maxgui_maxgui_TGadget_GetWatch
	extrn	__maxgui_maxgui_TGadget_GetWidth
	extrn	__maxgui_maxgui_TGadget_GetXPos
	extrn	__maxgui_maxgui_TGadget_GetYPos
	extrn	__maxgui_maxgui_TGadget_Handle
	extrn	__maxgui_maxgui_TGadget_HasDescendant
	extrn	__maxgui_maxgui_TGadget_Insert
	extrn	__maxgui_maxgui_TGadget_InsertItem
	extrn	__maxgui_maxgui_TGadget_InsertItemFromKey
	extrn	__maxgui_maxgui_TGadget_InsertListItem
	extrn	__maxgui_maxgui_TGadget_InsertNode
	extrn	__maxgui_maxgui_TGadget_ItemCount
	extrn	__maxgui_maxgui_TGadget_ItemExtra
	extrn	__maxgui_maxgui_TGadget_ItemFlags
	extrn	__maxgui_maxgui_TGadget_ItemIcon
	extrn	__maxgui_maxgui_TGadget_ItemState
	extrn	__maxgui_maxgui_TGadget_ItemText
	extrn	__maxgui_maxgui_TGadget_ItemTip
	extrn	__maxgui_maxgui_TGadget_KeysFromList
	extrn	__maxgui_maxgui_TGadget_KeysFromObjectArray
	extrn	__maxgui_maxgui_TGadget_LineAt
	extrn	__maxgui_maxgui_TGadget_ListItemState
	extrn	__maxgui_maxgui_TGadget_LockLayout
	extrn	__maxgui_maxgui_TGadget_LockText
	extrn	__maxgui_maxgui_TGadget_ModifyNode
	extrn	__maxgui_maxgui_TGadget_PopupMenu
	extrn	__maxgui_maxgui_TGadget_RemoveItem
	extrn	__maxgui_maxgui_TGadget_RemoveListItem
	extrn	__maxgui_maxgui_TGadget_ReplaceText
	extrn	__maxgui_maxgui_TGadget_RootNode
	extrn	__maxgui_maxgui_TGadget_Run
	extrn	__maxgui_maxgui_TGadget_SelectItem
	extrn	__maxgui_maxgui_TGadget_SelectedItem
	extrn	__maxgui_maxgui_TGadget_SelectedItems
	extrn	__maxgui_maxgui_TGadget_SelectedNode
	extrn	__maxgui_maxgui_TGadget_SelectionChanged
	extrn	__maxgui_maxgui_TGadget_SetDataSource
	extrn	__maxgui_maxgui_TGadget_SetFilter
	extrn	__maxgui_maxgui_TGadget_SetIconStrip
	extrn	__maxgui_maxgui_TGadget_SetItem
	extrn	__maxgui_maxgui_TGadget_SetItemState
	extrn	__maxgui_maxgui_TGadget_SetLayout
	extrn	__maxgui_maxgui_TGadget_SetListItem
	extrn	__maxgui_maxgui_TGadget_SetListItemState
	extrn	__maxgui_maxgui_TGadget_SetMaximumSize
	extrn	__maxgui_maxgui_TGadget_SetMinimumSize
	extrn	__maxgui_maxgui_TGadget_SetProp
	extrn	__maxgui_maxgui_TGadget_SetRange
	extrn	__maxgui_maxgui_TGadget_SetRect
	extrn	__maxgui_maxgui_TGadget_SetSelected
	extrn	__maxgui_maxgui_TGadget_SetSelection
	extrn	__maxgui_maxgui_TGadget_SetSensitivity
	extrn	__maxgui_maxgui_TGadget_SetShape
	extrn	__maxgui_maxgui_TGadget_SetStatusText
	extrn	__maxgui_maxgui_TGadget_SetStep
	extrn	__maxgui_maxgui_TGadget_SetStyle
	extrn	__maxgui_maxgui_TGadget_SetTabs
	extrn	__maxgui_maxgui_TGadget_SetValue
	extrn	__maxgui_maxgui_TGadget_SetWatch
	extrn	__maxgui_maxgui_TGadget_SyncData
	extrn	__maxgui_maxgui_TGadget_SyncDataSource
	extrn	__maxgui_maxgui_TGadget_UnlockText
	extrn	__maxgui_maxgui_TGadget_UpdateMenu
	extrn	__maxgui_maxgui_TGadget__setparent
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__cursor
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	extrn	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	extrn	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Query
	extrn	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Register
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	extrn	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_State
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isControl
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Activate
	extrn	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Class
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Create
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Delete
	extrn	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Free
	extrn	__maxgui_win32maxguiex_TWindowsPanel_New
	extrn	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	extrn	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	extrn	__maxgui_win32maxguiex_TWindowsTextArea__oldCursor
	extrn	_bbAppTitle
	extrn	_bbArrayNew1D
	extrn	_bbArraySlice
	extrn	_bbEmptyString
	extrn	_bbFloatToInt
	extrn	_bbGCFree
	extrn	_bbIncbinLen
	extrn	_bbIncbinPtr
	extrn	_bbIntAbs
	extrn	_bbMemFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbStringAsc
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_bbStringReplace
	extrn	_bbStringSlice
	extrn	_bbStringToInt
	extrn	_bbStringToLower
	extrn	_bbStringToWString
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_DeleteFile
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_ReadFile
	extrn	_brl_filesystem_StripDir
	extrn	_brl_linkedlist_CreateList
	extrn	_brl_map_CreateMap
	extrn	_brl_stream_CloseStream
	extrn	_brl_stream_CopyStream
	extrn	_brl_stream_ReadStream
	extrn	_brl_stream_WriteStream
	extrn	_brl_system_Notify
	extrn	_maxgui_localization_LocalizationMode
	extrn	_maxgui_maxgui_AddGadgetItem
	extrn	_maxgui_maxgui_GadgetClass
	extrn	_maxgui_maxgui_GadgetGroup
	extrn	_maxgui_maxgui_GadgetHeight
	extrn	_maxgui_maxgui_GadgetHidden
	extrn	_maxgui_maxgui_GadgetText
	extrn	_maxgui_maxgui_GadgetWidth
	extrn	_maxgui_maxgui_LocalizeGadget
	extrn	_maxgui_maxgui_QueryGadget
	extrn	_maxgui_maxgui_RedrawGadget
	extrn	_maxgui_maxgui_SetGadgetText
	extrn	_maxgui_maxgui_SetPointer
	extrn	_maxgui_maxgui_TGadgetItem
	extrn	_maxgui_maxgui_TProxyGadget
	extrn	_maxgui_maxgui_lastPointer
	extrn	_maxgui_win32maxguiex_TWindowsGadget
	extrn	_maxgui_win32maxguiex_TWindowsListBox
	extrn	_maxgui_win32maxguiex_TWindowsPanel
	extrn	_pub_win32_LVITEMW
	extrn	_skn3_systemex_GetTempDirectory
	public	___bb_maxguiex_maxguiex
	public	__skn3_maxguiex_PARAFORMAT2_Delete
	public	__skn3_maxguiex_PARAFORMAT2_New
	public	__skn3_maxguiex_Skn3CustomPointer_Delete
	public	__skn3_maxguiex_Skn3CustomPointer_New
	public	__skn3_maxguiex_Skn3CustomPointer_all
	public	__skn3_maxguiex_Skn3ListBatchLock_Delete
	public	__skn3_maxguiex_Skn3ListBatchLock_Find
	public	__skn3_maxguiex_Skn3ListBatchLock_New
	public	__skn3_maxguiex_Skn3ListBatchLock_add
	public	__skn3_maxguiex_Skn3ListBatchLock_all
	public	__skn3_maxguiex_Skn3ListBatchLock_remove
	public	__skn3_maxguiex_Skn3PanelEx_Delete
	public	__skn3_maxguiex_Skn3PanelEx_New
	public	__skn3_maxguiex_Skn3PanelEx_SetGradient
	public	__skn3_maxguiex_Skn3PanelEx_WndProc
	public	_skn3_maxguiex_AssignGadgetClassId
	public	_skn3_maxguiex_BringWindowToTop
	public	_skn3_maxguiex_ClearColorPickerCustomColors
	public	_skn3_maxguiex_CreatePanelEx
	public	_skn3_maxguiex_DisableGadgetRedraw
	public	_skn3_maxguiex_EnableGadgetRedraw
	public	_skn3_maxguiex_ExtractCursorHotspot
	public	_skn3_maxguiex_FocusWindow
	public	_skn3_maxguiex_FreeCustomPointer
	public	_skn3_maxguiex_GadgetScreenPosition
	public	_skn3_maxguiex_GadgetSizeForString
	public	_skn3_maxguiex_GadgetToInt
	public	_skn3_maxguiex_GadgetWindow
	public	_skn3_maxguiex_GetAppResourcesPath
	public	_skn3_maxguiex_GetCreationGroup
	public	_skn3_maxguiex_GetGadgetMaxLength
	public	_skn3_maxguiex_HideGadgetBorder
	public	_skn3_maxguiex_InstallGuiFont
	public	_skn3_maxguiex_ListBatchAdd
	public	_skn3_maxguiex_ListBatchLock
	public	_skn3_maxguiex_ListBatchUnlock
	public	_skn3_maxguiex_LoadCustomPointer
	public	_skn3_maxguiex_MessageBox
	public	_skn3_maxguiex_PointOverGadget
	public	_skn3_maxguiex_RedrawGadgetFrame
	public	_skn3_maxguiex_RequestScrollbarSize
	public	_skn3_maxguiex_ScrollTextAreaToBottom
	public	_skn3_maxguiex_ScrollTextAreaToCursor
	public	_skn3_maxguiex_ScrollTextAreaToTop
	public	_skn3_maxguiex_SetColorPickerCustomColors
	public	_skn3_maxguiex_SetComboBoxHeight
	public	_skn3_maxguiex_SetCustomPointer
	public	_skn3_maxguiex_SetGadgetMaxLength
	public	_skn3_maxguiex_SetGadgetReadOnly
	public	_skn3_maxguiex_SetPanelExGradient
	public	_skn3_maxguiex_SetTextareaLineSpacing
	public	_skn3_maxguiex_SetWindowAlwaysOnTop
	public	_skn3_maxguiex_Skn3CustomPointer
	public	_skn3_maxguiex_Skn3PanelEx
	section	"code" code
___bb_maxguiex_maxguiex:
	push	ebp
	mov	ebp,esp
	cmp	dword [_366],0
	je	_367
	mov	eax,0
	mov	esp,ebp
	pop	ebp
	ret
_367:
	mov	dword [_366],1
	call	___bb_blitz_blitz
	call	___bb_map_map
	call	___bb_linkedlist_linkedlist
	call	___bb_drivers_drivers
	call	___bb_systemex_systemex
	push	_16
	call	_bbObjectRegisterType
	add	esp,4
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectRegisterType
	add	esp,4
	push	_20
	call	_bbObjectRegisterType
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectRegisterType
	add	esp,4
	mov	eax,0
	jmp	_157
_157:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_16
	mov	dword [ebx+8],0
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	dword [ebx+16],0
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	dword [ebx+28],0
	mov	eax,0
	jmp	_160
_160:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_163:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_373
	push	eax
	call	_bbGCFree
	add	esp,4
_373:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_375
	push	eax
	call	_bbGCFree
	add	esp,4
_375:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_377
	push	eax
	call	_bbGCFree
	add	esp,4
_377:
	mov	eax,0
	jmp	_371
_371:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_Find:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	esi,dword [ebp+8]
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_378
	mov	eax,_bbNullObject
	jmp	_166
_378:
	mov	eax,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+88]
	add	esp,4
	mov	ebx,eax
	jmp	_17
_19:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	dword [eax+12],esi
	jne	_383
	jmp	_166
_383:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
_17:
	cmp	ebx,_bbNullObject
	jne	_19
_18:
	mov	eax,_bbNullObject
	jmp	_166
_166:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_add:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_385
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	dec	dword [eax+4]
	jnz	_389
	push	eax
	call	_bbGCFree
	add	esp,4
_389:
	mov	dword [__skn3_maxguiex_Skn3ListBatchLock_all],esi
_385:
	mov	eax,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_394
	push	eax
	call	_bbGCFree
	add	esp,4
_394:
	mov	dword [ebx+20],esi
	mov	eax,0
	jmp	_169
_169:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_remove:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	esi,dword [ebp+8]
	cmp	esi,_bbNullObject
	je	_395
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_400
	push	eax
	call	_bbGCFree
	add	esp,4
_400:
	mov	dword [esi+12],ebx
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_404
	push	eax
	call	_bbGCFree
	add	esp,4
_404:
	mov	dword [esi+24],ebx
_395:
	mov	eax,0
	jmp	_172
_172:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3CustomPointer_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_skn3_maxguiex_Skn3CustomPointer
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	dword [ebx+12],0
	mov	dword [ebx+16],0
	mov	eax,0
	jmp	_175
_175:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3CustomPointer_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_178:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_408
	push	eax
	call	_bbGCFree
	add	esp,4
_408:
	mov	eax,0
	jmp	_406
_406:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_PARAFORMAT2_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_20
	mov	dword [ebx+8],0
	mov	dword [ebx+12],0
	mov	word [ebx+16],0
	mov	word [ebx+18],0
	mov	dword [ebx+20],0
	mov	dword [ebx+24],0
	mov	dword [ebx+28],0
	mov	word [ebx+32],0
	mov	word [ebx+34],32
	mov	dword [ebx+36],0
	mov	dword [ebx+40],0
	mov	dword [ebx+44],0
	mov	dword [ebx+48],0
	mov	dword [ebx+52],0
	mov	dword [ebx+56],0
	mov	dword [ebx+60],0
	mov	dword [ebx+64],0
	mov	dword [ebx+68],0
	mov	dword [ebx+72],0
	mov	dword [ebx+76],0
	mov	dword [ebx+80],0
	mov	dword [ebx+84],0
	mov	dword [ebx+88],0
	mov	dword [ebx+92],0
	mov	dword [ebx+96],0
	mov	dword [ebx+100],0
	mov	dword [ebx+104],0
	mov	dword [ebx+108],0
	mov	dword [ebx+112],0
	mov	dword [ebx+116],0
	mov	dword [ebx+120],0
	mov	dword [ebx+124],0
	mov	dword [ebx+128],0
	mov	dword [ebx+132],0
	mov	dword [ebx+136],0
	mov	dword [ebx+140],0
	mov	dword [ebx+144],0
	mov	dword [ebx+148],0
	mov	dword [ebx+152],0
	mov	dword [ebx+156],0
	mov	dword [ebx+160],0
	mov	dword [ebx+164],0
	mov	dword [ebx+168],0
	mov	dword [ebx+172],0
	mov	word [ebx+176],0
	mov	byte [ebx+178],0
	mov	byte [ebx+179],0
	mov	word [ebx+180],0
	mov	word [ebx+182],0
	mov	word [ebx+184],0
	mov	word [ebx+186],0
	mov	word [ebx+188],0
	mov	word [ebx+190],0
	mov	word [ebx+192],0
	mov	word [ebx+194],0
	mov	eax,0
	jmp	_181
_181:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_PARAFORMAT2_Delete:
	push	ebp
	mov	ebp,esp
_184:
	mov	eax,0
	jmp	_409
_409:
	mov	esp,ebp
	pop	ebp
	ret
_22:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	cmp	dword [ebx+8],0
	jne	_410
	mov	eax,_1
	jmp	_189
_410:
	push	_21
	push	_23
	push	ebx
	call	_bbStringReplace
	add	esp,12
	mov	ebx,eax
	push	_21
	push	dword [ebp+12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_411
	push	dword [ebp+12]
	push	_21
	push	ebx
	call	_bbStringReplace
	add	esp,12
	mov	ebx,eax
_411:
	mov	edi,0
	mov	dword [ebp-4],0
	push	dword [ebp+12]
	call	_bbStringAsc
	add	esp,4
	mov	edx,eax
	mov	ecx,0
	mov	esi,dword [ebx+8]
	jmp	_416
_26:
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_418
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,edx
	setne	al
	movzx	eax,al
_418:
	cmp	eax,0
	je	_420
	jmp	_25
_420:
	mov	eax,ecx
	add	eax,1
	mov	edi,eax
	mov	dword [ebp-4],1
_24:
	add	ecx,1
_416:
	cmp	ecx,esi
	jl	_26
_25:
	mov	eax,dword [ebx+8]
	sub	eax,edi
	mov	esi,eax
	mov	eax,dword [ebx+8]
	sub	eax,1
	mov	ecx,eax
	jmp	_422
_29:
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_423
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,edx
	setne	al
	movzx	eax,al
_423:
	cmp	eax,0
	je	_425
	jmp	_28
_425:
	sub	esi,1
_27:
	add	ecx,-1
_422:
	cmp	ecx,0
	jge	_29
_28:
	cmp	esi,0
	jg	_426
	mov	eax,_1
	jmp	_189
_426:
	mov	eax,edi
	add	eax,esi
	push	eax
	push	edi
	push	ebx
	call	_bbStringSlice
	add	esp,12
	mov	ebx,eax
	mov	eax,dword [ebp-4]
	cmp	eax,0
	je	_427
	mov	eax,dword [ebp+16]
_427:
	cmp	eax,0
	je	_429
	push	ebx
	push	dword [ebp+12]
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
_429:
	mov	eax,ebx
	jmp	_189
_189:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_30:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	push	_31
	push	8
	push	0
	push	dword [ebp+8]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_430
	push	_21
	push	1
	push	_21
	push	0
	call	_skn3_systemex_GetTempDirectory
	add	esp,4
	push	eax
	call	_22
	add	esp,12
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
	mov	ebx,_1
	mov	eax,dword [ebp+8]
	push	dword [eax+8]
	push	8
	push	dword [ebp+8]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_brl_filesystem_StripDir
	add	esp,4
	mov	edi,eax
	jmp	_32
_34:
	push	ebx
	call	_bbStringToInt
	add	esp,4
	add	eax,1
	push	eax
	call	_bbStringFromInt
	add	esp,4
	mov	ebx,eax
_32:
	push	edi
	push	ebx
	push	esi
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,0
	jne	_34
_33:
	push	edi
	push	ebx
	push	esi
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	edi,eax
	push	dword [ebp+8]
	call	_brl_stream_ReadStream
	add	esp,4
	mov	esi,eax
	cmp	esi,_bbNullObject
	jne	_436
	mov	eax,_1
	jmp	_192
_436:
	push	edi
	call	_brl_stream_WriteStream
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_438
	push	esi
	call	_brl_stream_CloseStream
	add	esp,4
	mov	eax,_1
	jmp	_192
_438:
	push	4096
	push	ebx
	push	esi
	call	_brl_stream_CopyStream
	add	esp,12
	push	ebx
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	esi
	call	_brl_stream_CloseStream
	add	esp,4
	mov	eax,edi
	jmp	_192
_430:
	mov	eax,_1
	jmp	_192
_192:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_35:
	push	ebp
	mov	ebp,esp
	fld	dword [ebp+8]
	fld	dword [ebp+12]
	fld	dword [ebp+16]
	fld	dword [ebp+20]
	fld	dword [ebp+24]
	fld	dword [ebp+28]
	fxch	st5
	fucom	st3
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	jne	_812
	fstp	st0
	fstp	st0
	fxch	st1
	fstp	st0
	jmp	_439
_812:
	fxch	st1
	faddp	st3,st0
	fucomp	st2
	fxch	st1
	fstp	st0
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_439:
	cmp	eax,0
	je	_441
	fxch	st1
	fucom	st1
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	fxch	st1
_441:
	cmp	eax,0
	jne	_813
	fstp	st0
	fstp	st0
	fstp	st0
	jmp	_443
_813:
	faddp	st2,st0
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_443:
	jmp	_200
_200:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RequestScrollbarSize:
	push	ebp
	mov	ebp,esp
	mov	eax,18
	jmp	_202
_202:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetComboBoxHeight:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	sub	eax,6
	push	eax
	push	-1
	push	339
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageA@16
	push	ebx
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	mov	eax,0
	jmp	_206
_206:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetScreenPosition:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	edx,dword [ebp+12]
	mov	dword [ebp-8],0
	mov	dword [ebp-4],0
	cmp	edx,0
	je	_446
	lea	edx,dword [ebp-8]
	push	edx
	push	2
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
	jmp	_448
_446:
	lea	edx,dword [ebp-8]
	push	edx
	push	1
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
_448:
	push	2
	push	_450
	call	_bbArrayNew1D
	add	esp,8
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx]
	mov	dword [eax+24],edx
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx+4]
	mov	dword [eax+4+24],edx
	jmp	_210
_210:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_PointOverGadget:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	mov	ebx,dword [ebp+16]
	mov	esi,dword [ebp+20]
	cmp	ebx,_bbNullObject
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_452
	push	0
	push	ebx
	call	_maxgui_maxgui_GadgetHidden
	add	esp,8
_452:
	cmp	eax,0
	je	_454
	mov	eax,0
	jmp	_216
_454:
	cmp	esi,_bbNullObject
	je	_455
	push	0
	push	esi
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	edx,dword [eax+24]
	add	edx,edi
	mov	edi,edx
	mov	eax,dword [eax+4+24]
	add	eax,dword [ebp+12]
	mov	dword [ebp+12],eax
_455:
	push	0
	push	ebx
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	esi,eax
	push	ebx
	call	_maxgui_maxgui_GadgetHeight
	add	esp,4
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	push	ebx
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [esi+4+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [esi+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp+12]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	dword [ebp+-4],edi
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	call	_35
	add	esp,24
	jmp	_216
_216:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_DisableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	0
	push	0
	push	11
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	mov	eax,1
	jmp	_219
_219:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_EnableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	0
	push	1
	push	11
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	mov	eax,1
	jmp	_222
_222:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_MessageBox:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	esi,dword [ebp+12]
	mov	edi,dword [_bbAppTitle]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [_bbAppTitle]
	dec	dword [eax+4]
	jnz	_462
	push	eax
	call	_bbGCFree
	add	esp,4
_462:
	mov	dword [_bbAppTitle],ebx
	push	0
	push	esi
	call	_brl_system_Notify
	add	esp,8
	mov	ebx,edi
	inc	dword [ebx+4]
	mov	eax,dword [_bbAppTitle]
	dec	dword [eax+4]
	jnz	_466
	push	eax
	call	_bbGCFree
	add	esp,4
_466:
	mov	dword [_bbAppTitle],ebx
	mov	eax,0
	jmp	_227
_227:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetSizeForString:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebp+16]
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,2
	je	_470
	push	dword [ebp-12]
	call	_GetDC@4
	mov	edi,eax
	push	0
	push	0
	push	49
	push	dword [ebp-12]
	call	_SendMessageW@16
	push	eax
	push	edi
	call	_SelectObject@8
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],0
	mov	dword [eax+28],0
	mov	dword [eax+32],esi
	mov	dword [eax+36],0
	mov	dword [ebp-4],eax
	mov	ebx,1280
	cmp	esi,0
	jle	_476
	or	ebx,16
_476:
	push	dword [ebp+12]
	call	_bbStringToWString
	add	esp,4
	mov	esi,eax
	push	ebx
	mov	eax,dword [ebp-4]
	lea	eax,dword [eax+24]
	push	eax
	push	-1
	push	esi
	push	edi
	call	_DrawTextW@20
	push	esi
	call	_bbMemFree
	add	esp,4
	push	edi
	push	dword [ebp-12]
	call	_ReleaseDC@8
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edx,dword [ebp-4]
	mov	edx,dword [edx+8+24]
	mov	dword [eax+24],edx
	mov	edx,dword [ebp-4]
	mov	edx,dword [edx+12+24]
	mov	dword [eax+28],edx
	jmp	_232
_470:
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],esi
	mov	dword [eax+28],0
	mov	dword [ebp-8],eax
	push	0
	push	0
	push	11
	push	dword [ebp-12]
	call	_SendMessageW@16
	push	ebx
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	esi,eax
	push	dword [ebp+12]
	push	ebx
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	eax,dword [ebp-8]
	lea	eax,byte [eax+24]
	push	eax
	push	0
	push	5633
	push	dword [ebp-12]
	call	_SendMessageW@16
	push	esi
	push	ebx
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	0
	push	1
	push	11
	push	dword [ebp-12]
	call	_SendMessageW@16
	mov	eax,dword [ebp-8]
	jmp	_232
_232:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetCreationGroup:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	_maxgui_maxgui_TProxyGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_484
	push	dword [eax+140]
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	jmp	_235
_484:
	mov	eax,ebx
	jmp	_235
_235:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetReadOnly:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	esi
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_487
	cmp	eax,4
	je	_487
	jmp	_486
_487:
	push	1
	push	esi
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	0
	push	ebx
	push	207
	push	eax
	call	_SendMessageW@16
	jmp	_486
_486:
	mov	eax,0
	jmp	_239
_239:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	esi
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_491
	cmp	eax,5
	je	_491
	jmp	_490
_491:
	cmp	ebx,0
	jge	_492
	mov	ebx,0
_492:
	push	0
	push	ebx
	push	197
	push	1
	push	esi
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	jmp	_490
_490:
	mov	eax,0
	jmp	_243
_243:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_495
	cmp	eax,5
	je	_495
	mov	eax,0
	jmp	_246
_495:
	push	0
	push	0
	push	213
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	jmp	_246
_246:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_LoadCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	edi,_bbNullObject
	push	esi
	call	_30
	add	esp,4
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	eax,dword [ebp-4]
	cmp	dword [eax+8],0
	jne	_499
	mov	dword [ebp-4],esi
	jmp	_500
_499:
	mov	dword [ebp-8],1
_500:
	cmp	dword [__skn3_maxguiex_Skn3CustomPointer_all],_bbNullObject
	jne	_501
	call	_brl_map_CreateMap
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	dec	dword [eax+4]
	jnz	_505
	push	eax
	call	_bbGCFree
	add	esp,4
_505:
	mov	dword [__skn3_maxguiex_Skn3CustomPointer_all],ebx
	jmp	_506
_501:
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	push	_skn3_maxguiex_Skn3CustomPointer
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,8
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
_506:
	cmp	edi,_bbNullObject
	jne	_508
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectNew
	add	esp,4
	mov	edi,eax
	mov	eax,esi
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+8]
	dec	dword [eax+4]
	jnz	_512
	push	eax
	call	_bbGCFree
	add	esp,4
_512:
	mov	dword [edi+8],ebx
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	push	edi
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,12
	push	dword [ebp-4]
	call	_bbStringToWString
	add	esp,4
	mov	esi,eax
	push	esi
	call	_LoadCursorFromFileW@4
	mov	ebx,eax
	push	esi
	call	_bbMemFree
	add	esp,4
	mov	dword [edi+12],ebx
_508:
	cmp	dword [ebp-8],0
	je	_516
	push	dword [ebp-4]
	call	_brl_filesystem_DeleteFile
	add	esp,4
_516:
	cmp	dword [edi+12],0
	je	_517
	add	dword [edi+16],1
	mov	eax,edi
	jmp	_249
_517:
	mov	eax,_bbNullObject
	jmp	_249
_249:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetCustomPointer:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	cmp	ebx,_bbNullObject
	je	_518
	mov	dword [_maxgui_maxgui_lastPointer],-1
	push	dword [ebx+12]
	call	_SetCursor@4
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	cmp	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],0
	je	_519
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],eax
_519:
_518:
	mov	eax,0
	jmp	_252
_252:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FreeCustomPointer:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	cmp	ebx,_bbNullObject
	je	_520
	sub	dword [ebx+16],1
	cmp	dword [ebx+16],0
	jne	_521
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	push	dword [ebx+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	eax,dword [ebx+12]
	cmp	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	jne	_523
	push	0
	call	_maxgui_maxgui_SetPointer
	add	esp,4
_523:
	push	dword [ebx+12]
	call	_DestroyCursor@4
	mov	dword [ebx+12],0
_521:
_520:
	mov	eax,0
	jmp	_255
_255:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ExtractCursorHotspot:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	edi,dword [ebp+12]
	push	2
	push	_524
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-4],eax
	push	ebx
	push	_36
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_ReadFile
	add	esp,4
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_527
	mov	eax,esi
	push	2
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,8
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,2
	jne	_531
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	ebx,eax
	cmp	edi,ebx
	jge	_533
	mov	eax,edi
	imul	eax,12
	add	eax,6
	add	eax,4
	mov	ebx,eax
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	ebx,eax
	jge	_535
	mov	eax,esi
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,8
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	edx,dword [ebp-4]
	mov	dword [edx+24],eax
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	edx,dword [ebp-4]
	mov	dword [edx+4+24],eax
_535:
_533:
_531:
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,4
_527:
	mov	eax,dword [ebp-4]
	jmp	_259
_259:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchLock:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	push	esi
	call	dword [_16+48]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	je	_541
	add	dword [ebx+8],1
	mov	eax,0
	jmp	_262
_541:
	push	_maxgui_win32maxguiex_TWindowsListBox
	push	esi
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
	cmp	edi,_bbNullObject
	jne	_543
	mov	eax,0
	jmp	_262
_543:
	push	_16
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	mov	dword [ebx+8],1
	mov	eax,edi
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_547
	push	eax
	call	_bbGCFree
	add	esp,4
_547:
	mov	dword [ebx+12],esi
	mov	eax,dword [edi+124]
	mov	eax,dword [eax+20]
	mov	dword [ebx+16],eax
	push	_pub_win32_LVITEMW
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_551
	push	eax
	call	_bbGCFree
	add	esp,4
_551:
	mov	dword [ebx+24],esi
	push	1
	push	edi
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebx+28],eax
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+552]
	add	esp,4
	push	ebx
	call	dword [_16+52]
	add	esp,4
	mov	eax,0
	jmp	_262
_262:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchAdd:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+16]
	mov	esi,dword [ebp+20]
	push	dword [ebp+8]
	call	dword [_16+48]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_554
	push	dword [ebp+28]
	push	dword [ebp+24]
	push	esi
	push	edi
	push	dword [ebp+12]
	push	dword [ebp+8]
	call	_maxgui_maxgui_AddGadgetItem
	add	esp,24
	mov	eax,0
	jmp	_270
_554:
	push	_maxgui_maxgui_TGadgetItem
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp-4]
	push	edi
	push	dword [ebp+28]
	push	esi
	push	dword [ebp+24]
	push	dword [ebp+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,24
	mov	eax,dword [ebx+16]
	add	eax,1
	push	eax
	push	0
	mov	eax,dword [ebp+8]
	push	dword [eax+124]
	push	_557
	call	_bbArraySlice
	add	esp,16
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+124]
	dec	dword [eax+4]
	jnz	_561
	push	eax
	call	_bbGCFree
	add	esp,4
_561:
	mov	eax,dword [ebp+8]
	mov	dword [eax+124],esi
	mov	esi,dword [ebp-4]
	inc	dword [esi+4]
	mov	eax,dword [ebp+8]
	mov	edx,dword [eax+124]
	mov	eax,dword [ebx+16]
	mov	eax,dword [edx+eax*4+24]
	dec	dword [eax+4]
	jnz	_565
	push	eax
	call	_bbGCFree
	add	esp,4
_565:
	mov	eax,dword [ebp+8]
	mov	edx,dword [eax+124]
	mov	eax,dword [ebx+16]
	mov	dword [edx+eax*4+24],esi
	mov	eax,dword [ebx+24]
	mov	dword [eax+8],4097
	mov	edx,dword [ebx+24]
	mov	eax,dword [ebx+16]
	mov	dword [edx+12],eax
	mov	esi,dword [ebx+24]
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	call	_bbStringToWString
	add	esp,4
	mov	dword [esi+28],eax
	mov	edx,dword [ebx+24]
	mov	eax,dword [ebx+24]
	mov	eax,dword [eax+8]
	or	eax,2
	mov	dword [edx+8],eax
	mov	edx,dword [ebx+24]
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+16]
	mov	dword [edx+36],eax
	mov	eax,dword [ebx+24]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	4173
	push	dword [ebx+28]
	call	_SendMessageW@16
	mov	eax,dword [ebx+24]
	push	dword [eax+28]
	call	_bbMemFree
	add	esp,4
	add	dword [ebx+16],1
	mov	eax,0
	jmp	_270
_270:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchUnlock:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	eax
	call	dword [_16+48]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_567
	mov	eax,0
	jmp	_273
_567:
	sub	dword [ebx+8],1
	cmp	dword [ebx+8],0
	jne	_568
	push	-2
	push	0
	push	4126
	push	dword [ebx+28]
	call	_SendMessageW@16
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+580]
	add	esp,4
	cmp	eax,0
	jne	_570
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_570:
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+548]
	add	esp,4
	push	ebx
	call	dword [_16+56]
	add	esp,4
_568:
	mov	eax,0
	jmp	_273
_273:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetWindow:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	eax,dword [ebp+8]
	push	eax
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	ebx,eax
	jmp	_37
_39:
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,1
	jne	_574
	mov	eax,ebx
	jmp	_276
_574:
	push	ebx
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	ebx,eax
_37:
	cmp	ebx,_bbNullObject
	jne	_39
_38:
	mov	eax,_bbNullObject
	jmp	_276
_276:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetWindowAlwaysOnTop:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_576
	cmp	ebx,0
	je	_577
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-1
	push	eax
	call	_SetWindowPos@28
	jmp	_578
_577:
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-2
	push	eax
	call	_SetWindowPos@28
_578:
_576:
	mov	eax,0
	jmp	_280
_280:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_BringWindowToTop:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_580
	push	19
	push	0
	push	0
	push	0
	push	0
	push	0
	push	eax
	call	_SetWindowPos@28
_580:
	mov	eax,0
	jmp	_283
_283:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FocusWindow:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_582
	push	eax
	call	_SetFocus@4
_582:
	mov	eax,0
	jmp	_286
_286:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetToInt:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	jmp	_289
_289:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	eax,dword [ebp+8]
	cmp	dword [eax+20],16
	jge	_583
	mov	ebx,dword [eax+20]
	push	16
	push	0
	push	eax
	push	_62
	call	_bbArraySlice
	add	esp,16
	jmp	_585
_42:
	mov	dword [eax+ebx*4+24],16777215
_40:
	add	ebx,1
_585:
	cmp	ebx,16
	jl	_42
_41:
_583:
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	dec	dword [eax+4]
	jnz	_589
	push	eax
	call	_bbGCFree
	add	esp,4
_589:
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors],ebx
	mov	eax,0
	jmp	_292
_292:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ClearColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	mov	edx,0
	jmp	_591
_45:
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	mov	dword [eax+edx*4+24],16777215
_43:
	add	edx,1
_591:
	cmp	edx,16
	jl	_45
_44:
	mov	eax,0
	jmp	_294
_294:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RedrawGadgetFrame:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	ebx,eax
	cmp	ebx,0
	je	_593
	push	55
	push	0
	push	0
	push	0
	push	0
	push	0
	push	ebx
	call	_SetWindowPos@28
	push	1345
	push	0
	push	0
	push	ebx
	call	_RedrawWindow@16
_593:
	mov	eax,0
	jmp	_297
_297:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_HideGadgetBorder:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	push	dword [ebp+8]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_596
	cmp	eax,4
	je	_596
	cmp	eax,7
	je	_596
	jmp	_595
_596:
	push	1
	push	dword [ebp+8]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	esi,eax
	cmp	esi,0
	je	_598
	push	-16
	push	esi
	call	_GetWindowLongW@8
	mov	ebx,eax
	push	-20
	push	esi
	call	_GetWindowLongW@8
	mov	edi,eax
	mov	eax,0
	mov	edx,ebx
	and	edx,8388608
	cmp	edx,0
	je	_602
	mov	eax,ebx
	and	eax,-8388609
	push	eax
	push	-16
	push	esi
	call	_SetWindowLongW@12
	mov	eax,1
_602:
	mov	edx,edi
	and	edx,512
	cmp	edx,0
	je	_603
	mov	eax,edi
	and	eax,-513
	push	eax
	push	-20
	push	esi
	call	_SetWindowLongW@12
	mov	eax,1
_603:
	cmp	eax,0
	je	_604
	push	dword [ebp+8]
	call	_skn3_maxguiex_RedrawGadgetFrame
	add	esp,4
_604:
_598:
	jmp	_595
_595:
	mov	eax,0
	jmp	_300
_300:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_InstallGuiFont:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	_31
	push	8
	push	0
	push	ebx
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_605
	push	dword [ebx+8]
	push	8
	push	ebx
	call	_bbStringSlice
	add	esp,12
	mov	ebx,eax
	mov	dword [ebp-4],0
	lea	eax,dword [ebp-4]
	push	eax
	push	0
	push	ebx
	call	_bbIncbinLen
	add	esp,4
	push	eax
	push	ebx
	call	_bbIncbinPtr
	add	esp,4
	push	eax
	call	_AddFontMemResourceEx@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	jmp	_303
_605:
	push	ebx
	call	_bbStringToWString
	add	esp,4
	mov	ebx,eax
	push	0
	push	16
	push	ebx
	call	_AddFontResourceExW@12
	mov	esi,eax
	push	ebx
	call	_bbMemFree
	add	esp,4
	cmp	esi,0
	setne	al
	movzx	eax,al
	jmp	_303
_303:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetTextareaLineSpacing:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_610
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	esi,eax
	cmp	esi,0
	je	_612
	push	_20
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],188
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],256
	mov	eax,dword [ebp-4]
	mov	byte [eax+178],5
	mov	ebx,dword [ebp-4]
	fld	dword [ebp+12]
	fmul	dword [_934]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+172],eax
	mov	eax,dword [ebp-4]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	1095
	push	esi
	call	_SendMessageW@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	jmp	_307
_612:
_610:
	mov	eax,0
	jmp	_307
_307:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToTop:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_614
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_616
	push	0
	push	6
	push	181
	push	eax
	call	_SendMessageW@16
_616:
_614:
	mov	eax,0
	jmp	_310
_310:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToBottom:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_617
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_619
	push	0
	push	7
	push	181
	push	eax
	call	_SendMessageW@16
_619:
_617:
	mov	eax,0
	jmp	_313
_313:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToCursor:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_620
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_622
	push	0
	push	0
	push	183
	push	eax
	call	_SendMessageW@16
_622:
_620:
	mov	eax,0
	jmp	_316
_316:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetAppResourcesPath:
	push	ebp
	mov	ebp,esp
	mov	eax,_31
	jmp	_318
_318:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_AssignGadgetClassId:
	push	ebp
	mov	ebp,esp
	sub	dword [_623],1
	mov	eax,dword [_623]
	jmp	_320
_320:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	__maxgui_win32maxguiex_TWindowsPanel_New
	add	esp,4
	mov	dword [ebx],_skn3_maxguiex_Skn3PanelEx
	mov	dword [ebx+272],0
	mov	dword [ebx+276],1
	mov	dword [ebx+280],255
	mov	dword [ebx+284],255
	mov	dword [ebx+288],255
	mov	dword [ebx+292],0
	mov	dword [ebx+296],0
	mov	dword [ebx+300],0
	mov	eax,0
	jmp	_323
_323:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_326:
	mov	dword [eax],_maxgui_win32maxguiex_TWindowsPanel
	push	eax
	call	__maxgui_win32maxguiex_TWindowsPanel_Delete
	add	esp,4
	mov	eax,0
	jmp	_624
_624:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_WndProc:
	push	ebp
	mov	ebp,esp
	sub	esp,84
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+16]
	mov	ecx,dword [ebp+24]
	cmp	eax,20
	je	_627
	jmp	_626
_627:
	mov	edx,dword [ebp+8]
	cmp	dword [edx+272],0
	jne	_628
	push	ecx
	push	dword [ebp+20]
	push	eax
	push	dword [ebp+12]
	push	dword [ebp+8]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	jmp	_333
_628:
	mov	eax,dword [ebp+8]
	cmp	dword [eax+244],2
	jne	_630
	mov	eax,1
	jmp	_333
_630:
	mov	eax,dword [ebp+20]
	mov	dword [ebp-76],eax
	mov	dword [ebp-80],0
	mov	dword [ebp-72],0
	push	4
	push	_640
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-12],eax
	push	4
	push	_642
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-16],eax
	push	4
	push	_644
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-4],eax
	push	4
	push	_646
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-4]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-76]
	call	_GetClipBox@8
	mov	eax,dword [ebp-8]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp+12]
	call	_GetWindowRect@8
	mov	eax,dword [ebp-12]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp+12]
	call	_GetClientRect@8
	push	0
	mov	eax,dword [ebp-16]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp+12]
	call	_GetUpdateRect@12
	cmp	eax,0
	jne	_648
	mov	eax,dword [ebp-4]
	mov	dword [ebp-16],eax
_648:
	mov	eax,dword [ebp-16]
	lea	eax,dword [eax+24]
	push	eax
	call	_IsRectEmpty@4
	cmp	eax,0
	je	_649
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],0
	mov	dword [eax+28],0
	mov	edx,dword [ebp-8]
	mov	ecx,dword [edx+8+24]
	mov	edx,dword [ebp-8]
	sub	ecx,dword [edx+24]
	mov	dword [eax+32],ecx
	mov	edx,dword [ebp-8]
	mov	ecx,dword [edx+12+24]
	mov	edx,dword [ebp-8]
	sub	ecx,dword [edx+4+24]
	mov	dword [eax+36],ecx
	mov	dword [ebp-16],eax
_649:
	mov	edx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	cmp	eax,dword [edx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_657
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+200]
	cmp	eax,0
	je	_651
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+252]
_651:
	cmp	eax,0
	je	_653
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+256]
_653:
	cmp	eax,0
	jne	_655
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_655:
_657:
	cmp	eax,0
	je	_659
	push	dword [ebp+20]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-76],eax
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	dword [ebp+20]
	call	_CreateCompatibleBitmap@12
	mov	dword [ebp-80],eax
	push	dword [ebp-80]
	push	dword [ebp-76]
	call	_SelectObject@8
_659:
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+24]
	mov	dword [eax+24],edx
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+4+24]
	mov	dword [eax+28],edx
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+8+24]
	mov	dword [eax+32],edx
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+12+24]
	mov	dword [eax+36],edx
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+8]
	cmp	dword [eax+276],0
	je	_672
	mov	eax,dword [ebp-12]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+292]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+280]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-44]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+296]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+284]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-40]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+300]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+288]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-48]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+280]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+4+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-44]
	faddp	st1,st0
	fstp	dword [ebp-28]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+284]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+4+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-40]
	faddp	st1,st0
	fstp	dword [ebp-24]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+288]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+4+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-48]
	faddp	st1,st0
	fstp	dword [ebp-32]
	mov	eax,dword [ebp-16]
	mov	edi,dword [eax+4+24]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax+12+24]
	mov	dword [ebp-56],eax
	jmp	_673
_48:
	fld	dword [ebp-32]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-24]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-28]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	ebx,eax
	push	ebx
	push	dword [ebp-76]
	call	_SelectObject@8
	mov	esi,eax
	mov	eax,dword [ebp-20]
	mov	dword [eax+4+24],edi
	mov	edx,dword [ebp-20]
	mov	eax,edi
	add	eax,1
	mov	dword [edx+12+24],eax
	push	ebx
	mov	eax,dword [ebp-20]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-76]
	call	_FillRect@12
	push	esi
	push	dword [ebp-76]
	call	_SelectObject@8
	push	ebx
	call	_DeleteObject@4
	fld	dword [ebp-28]
	fadd	dword [ebp-44]
	fstp	dword [ebp-28]
	fld	dword [ebp-24]
	fadd	dword [ebp-40]
	fstp	dword [ebp-24]
	fld	dword [ebp-32]
	fadd	dword [ebp-48]
	fstp	dword [ebp-32]
_46:
	add	edi,1
_673:
	cmp	edi,dword [ebp-56]
	jl	_48
_47:
	jmp	_675
_672:
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+292]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+280]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-44]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+296]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+284]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-40]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+300]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+288]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-48]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+280]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-44]
	faddp	st1,st0
	fstp	dword [ebp-28]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+284]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-40]
	faddp	st1,st0
	fstp	dword [ebp-24]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+288]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-48]
	faddp	st1,st0
	fstp	dword [ebp-32]
	mov	eax,dword [ebp-16]
	mov	edi,dword [eax+24]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax+8+24]
	mov	dword [ebp-52],eax
	jmp	_676
_51:
	fld	dword [ebp-32]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-24]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-28]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	ebx,eax
	push	ebx
	push	dword [ebp-76]
	call	_SelectObject@8
	mov	esi,eax
	mov	eax,dword [ebp-20]
	mov	dword [eax+24],edi
	mov	edx,dword [ebp-20]
	mov	eax,edi
	add	eax,1
	mov	dword [edx+8+24],eax
	push	ebx
	mov	eax,dword [ebp-20]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-76]
	call	_FillRect@12
	push	esi
	push	dword [ebp-76]
	call	_SelectObject@8
	push	ebx
	call	_DeleteObject@4
	fld	dword [ebp-28]
	fadd	dword [ebp-44]
	fstp	dword [ebp-28]
	fld	dword [ebp-24]
	fadd	dword [ebp-40]
	fstp	dword [ebp-24]
	fld	dword [ebp-32]
	fadd	dword [ebp-48]
	fstp	dword [ebp-32]
_49:
	add	edi,1
_676:
	cmp	edi,dword [ebp-52]
	jl	_51
_50:
_675:
	mov	edx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	cmp	eax,dword [edx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_684
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+200]
	cmp	eax,0
	je	_678
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+252]
_678:
	cmp	eax,0
	je	_680
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+256]
_680:
	cmp	eax,0
	jne	_682
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_682:
_684:
	cmp	eax,0
	jne	_686
	mov	eax,1
	jmp	_333
_686:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+200]
	cmp	eax,0
	je	_687
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+252]
_687:
	cmp	eax,0
	je	_689
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+256]
_689:
	cmp	eax,0
	je	_691
	push	dword [ebp-76]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-36],eax
	mov	eax,dword [ebp+8]
	push	dword [eax+200]
	push	dword [ebp-36]
	call	_SelectObject@8
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+252]
	mov	eax,dword [ebp+8]
	mov	edi,dword [eax+256]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+260]
	and	eax,7
	cmp	eax,0
	je	_694
	cmp	eax,1
	je	_695
	cmp	eax,2
	je	_696
	cmp	eax,4
	je	_696
	cmp	eax,3
	je	_697
	jmp	_693
_694:
	jmp	_52
_54:
	mov	ebx,0
	jmp	_55
_57:
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_698
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_699
_698:
	push	13369376
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_BitBlt@36
_699:
	add	ebx,esi
_55:
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	cmp	ebx,edx
	jl	_57
_56:
	add	dword [ebp-72],edi
_52:
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	cmp	dword [ebp-72],edx
	jl	_54
_53:
	jmp	_693
_695:
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+24]
	sub	eax,esi
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+12+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+4+24]
	sub	eax,edi
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-72],eax
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_700
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_701
_700:
	push	13369376
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_BitBlt@36
_701:
	jmp	_693
_696:
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],esi
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-64]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],edi
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fld	dword [ebp-64]
	fucomp	st1
	fnstsw	ax
	sahf
	setbe	al
	movzx	eax,al
	cmp	eax,0
	je	_1003
	fstp	st0
	jmp	_704
_1003:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+260]
	and	eax,7
	cmp	eax,2
	je	_1004
	fstp	st0
	jmp	_705
_1004:
	fstp	dword [ebp-64]
	jmp	_706
_705:
_706:
_704:
	fld	dword [ebp-64]
	mov	dword [ebp+-84],esi
	fild	dword [ebp+-84]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-60],eax
	fld	dword [ebp-64]
	mov	dword [ebp+-84],edi
	fild	dword [ebp+-84]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-68],eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+24]
	sub	eax,dword [ebp-60]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+12+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+4+24]
	sub	eax,dword [ebp-68]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-72],eax
	push	3
	push	dword [ebp-76]
	call	_SetStretchBltMode@8
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_709
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	dword [ebp-68]
	push	dword [ebp-60]
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_710
_709:
	push	13369376
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	dword [ebp-68]
	push	dword [ebp-60]
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_StretchBlt@44
_710:
	jmp	_693
_697:
	push	3
	push	dword [ebp-76]
	call	_SetStretchBltMode@8
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_711
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_712
_711:
	push	13369376
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-76]
	call	_StretchBlt@44
_712:
	jmp	_693
_693:
	push	dword [ebp-36]
	call	_DeleteDC@4
_691:
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	jne	_713
	push	0
	push	dword [ebp+12]
	push	dword [ebp+20]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax]
	call	dword [eax+544]
	add	esp,12
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fmul	dword [_967]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	and	eax,255
	shl	eax,16
	push	eax
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+24]
	push	edx
	mov	eax,dword [ebp-16]
	push	dword [eax+4+24]
	mov	eax,dword [ebp-16]
	push	dword [eax+24]
	push	dword [ebp-76]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+24]
	push	edx
	mov	eax,dword [ebp-16]
	push	dword [eax+4+24]
	mov	eax,dword [ebp-16]
	push	dword [eax+24]
	push	dword [ebp+20]
	call	_AlphaBlend@44
	jmp	_715
_713:
	push	13369376
	push	0
	push	0
	push	dword [ebp-76]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	0
	push	0
	push	dword [ebp+20]
	call	_BitBlt@36
_715:
	push	dword [ebp-80]
	call	_DeleteObject@4
	push	dword [ebp-76]
	call	_DeleteDC@4
	mov	eax,1
	jmp	_333
_626:
	push	ecx
	push	dword [ebp+20]
	push	eax
	push	dword [ebp+12]
	push	dword [ebp+8]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	jmp	_333
_333:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_SetGradient:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	eax,dword [ebp+12]
	mov	ebx,dword [ebp+16]
	mov	ecx,dword [ebp+20]
	mov	edx,dword [ebp+24]
	mov	edi,dword [ebp+28]
	cmp	eax,0
	jne	_716
	cmp	dword [esi+272],0
	je	_717
	mov	dword [esi+272],0
	push	esi
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
_717:
	jmp	_718
_716:
	cmp	ebx,edi
	sete	al
	movzx	eax,al
	cmp	eax,0
	je	_719
	cmp	edx,dword [ebp+36]
	sete	al
	movzx	eax,al
_719:
	cmp	eax,0
	je	_721
	cmp	ecx,dword [ebp+32]
	sete	al
	movzx	eax,al
_721:
	cmp	eax,0
	je	_723
	mov	dword [esi+272],0
	mov	eax,esi
	push	ecx
	push	edx
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+248]
	add	esp,16
	jmp	_725
_723:
	mov	eax,dword [esi+272]
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_726
	cmp	ebx,dword [esi+280]
	setne	al
	movzx	eax,al
_726:
	cmp	eax,0
	jne	_728
	cmp	edx,dword [esi+284]
	setne	al
	movzx	eax,al
_728:
	cmp	eax,0
	jne	_730
	cmp	ecx,dword [esi+288]
	setne	al
	movzx	eax,al
_730:
	cmp	eax,0
	jne	_732
	cmp	edi,dword [esi+292]
	setne	al
	movzx	eax,al
_732:
	cmp	eax,0
	jne	_734
	mov	eax,dword [ebp+36]
	cmp	eax,dword [esi+296]
	setne	al
	movzx	eax,al
_734:
	cmp	eax,0
	jne	_736
	mov	eax,dword [ebp+32]
	cmp	eax,dword [esi+300]
	setne	al
	movzx	eax,al
_736:
	cmp	eax,0
	jne	_738
	mov	eax,dword [ebp+40]
	cmp	eax,dword [esi+276]
	setne	al
	movzx	eax,al
_738:
	cmp	eax,0
	je	_740
	mov	dword [esi+272],1
	mov	dword [esi+280],ebx
	mov	dword [esi+284],edx
	mov	dword [esi+288],ecx
	mov	dword [esi+292],edi
	mov	eax,dword [ebp+36]
	mov	dword [esi+296],eax
	mov	eax,dword [ebp+32]
	mov	dword [esi+300],eax
	mov	eax,dword [ebp+40]
	mov	dword [esi+276],eax
	push	esi
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
_740:
_725:
_718:
	mov	eax,0
	jmp	_344
_344:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_CreatePanelEx:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+24]
	mov	ebx,dword [ebp+28]
	mov	esi,dword [ebp+32]
	push	edi
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	mov	edi,eax
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectNew
	add	esp,4
	push	_1
	push	ebx
	push	edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+480]
	add	esp,16
	mov	ebx,eax
	call	_maxgui_localization_LocalizationMode
	and	eax,2
	cmp	eax,0
	je	_743
	push	_1
	push	esi
	push	ebx
	call	_maxgui_maxgui_LocalizeGadget
	add	esp,12
	jmp	_744
_743:
	mov	eax,ebx
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+236]
	add	esp,8
_744:
	cmp	edi,_bbNullObject
	je	_746
	mov	eax,ebx
	push	-1
	push	edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,12
_746:
	mov	eax,ebx
	push	dword [ebp+20]
	push	dword [ebp+16]
	push	dword [ebp+12]
	push	dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+88]
	add	esp,20
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_749
	mov	eax,ebx
	push	dword [__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+244]
	add	esp,8
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	edi
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_751
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	edi
	call	_bbObjectDowncast
	add	esp,8
	mov	eax,dword [eax+232]
	cmp	eax,0
	je	_753
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	edi
	call	_bbObjectDowncast
	add	esp,8
	mov	eax,dword [eax+236]
	cmp	eax,0
	sete	al
	movzx	eax,al
_753:
	cmp	eax,0
	sete	al
	movzx	eax,al
	mov	dword [esi+236],eax
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+232]
	add	esp,4
	and	eax,4
	cmp	eax,0
	sete	al
	movzx	eax,al
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+284]
	add	esp,8
_751:
	mov	eax,ebx
	push	0
	push	1
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+280]
	add	esp,12
_749:
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_758
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+548]
	add	esp,4
_758:
	mov	eax,ebx
	jmp	_353
_353:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetPanelExGradient:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	esi,dword [ebp+12]
	mov	ebx,dword [ebp+16]
	mov	edi,dword [ebp+20]
	push	_skn3_maxguiex_Skn3PanelEx
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_761
	push	dword [ebp+40]
	push	dword [ebp+32]
	push	dword [ebp+36]
	push	dword [ebp+28]
	push	edi
	push	dword [ebp+24]
	push	ebx
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+580]
	add	esp,36
_761:
	mov	eax,0
	jmp	_364
_364:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_366:
	dd	0
	align	4
__skn3_maxguiex_Skn3ListBatchLock_all:
	dd	_bbNullObject
_60:
	db	"Skn3ListBatchLock",0
_61:
	db	"refCount",0
_62:
	db	"i",0
_63:
	db	"listBox",0
_64:
	db	":TWindowsListBox",0
_65:
	db	"index",0
_66:
	db	"link",0
_67:
	db	":TLink",0
_68:
	db	"it",0
_69:
	db	":LVITEMW",0
_70:
	db	"hwnd",0
_71:
	db	"New",0
_72:
	db	"()i",0
_73:
	db	"Delete",0
_74:
	db	"Find",0
_75:
	db	"(:TGadget):Skn3ListBatchLock",0
_76:
	db	"add",0
_77:
	db	"(:Skn3ListBatchLock)i",0
_78:
	db	"remove",0
	align	4
_59:
	dd	2
	dd	_60
	dd	3
	dd	_61
	dd	_62
	dd	8
	dd	3
	dd	_63
	dd	_64
	dd	12
	dd	3
	dd	_65
	dd	_62
	dd	16
	dd	3
	dd	_66
	dd	_67
	dd	20
	dd	3
	dd	_68
	dd	_69
	dd	24
	dd	3
	dd	_70
	dd	_62
	dd	28
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	7
	dd	_74
	dd	_75
	dd	48
	dd	7
	dd	_76
	dd	_77
	dd	52
	dd	7
	dd	_78
	dd	_77
	dd	56
	dd	0
	align	4
_16:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_59
	dd	32
	dd	__skn3_maxguiex_Skn3ListBatchLock_New
	dd	__skn3_maxguiex_Skn3ListBatchLock_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__skn3_maxguiex_Skn3ListBatchLock_Find
	dd	__skn3_maxguiex_Skn3ListBatchLock_add
	dd	__skn3_maxguiex_Skn3ListBatchLock_remove
	align	4
__skn3_maxguiex_Skn3CustomPointer_all:
	dd	_bbNullObject
_80:
	db	"Skn3CustomPointer",0
_81:
	db	"path",0
_82:
	db	"$",0
_83:
	db	"pointer",0
	align	4
_79:
	dd	2
	dd	_80
	dd	3
	dd	_81
	dd	_82
	dd	8
	dd	3
	dd	_83
	dd	_62
	dd	12
	dd	3
	dd	_61
	dd	_62
	dd	16
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	0
	align	4
_skn3_maxguiex_Skn3CustomPointer:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_79
	dd	20
	dd	__skn3_maxguiex_Skn3CustomPointer_New
	dd	__skn3_maxguiex_Skn3CustomPointer_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_85:
	db	"PARAFORMAT2",0
_86:
	db	"cbSize",0
_87:
	db	"dwMask",0
_88:
	db	"wNumbering",0
_89:
	db	"s",0
_90:
	db	"wEffects",0
_91:
	db	"dxStartIndent",0
_92:
	db	"dxRightIndent",0
_93:
	db	"dxOffset",0
_94:
	db	"wAlignment",0
_95:
	db	"cTabCount",0
_96:
	db	"rgxTabs00",0
_97:
	db	"rgxTabs01",0
_98:
	db	"rgxTabs02",0
_99:
	db	"rgxTabs03",0
_100:
	db	"rgxTabs10",0
_101:
	db	"rgxTabs11",0
_102:
	db	"rgxTabs12",0
_103:
	db	"rgxTabs13",0
_104:
	db	"rgxTabs20",0
_105:
	db	"rgxTabs21",0
_106:
	db	"rgxTabs22",0
_107:
	db	"rgxTabs23",0
_108:
	db	"rgxTabs30",0
_109:
	db	"rgxTabs31",0
_110:
	db	"rgxTabs32",0
_111:
	db	"rgxTabs33",0
_112:
	db	"rgxTabs40",0
_113:
	db	"rgxTabs41",0
_114:
	db	"rgxTabs42",0
_115:
	db	"rgxTabs43",0
_116:
	db	"rgxTabs50",0
_117:
	db	"rgxTabs51",0
_118:
	db	"rgxTabs52",0
_119:
	db	"rgxTabs53",0
_120:
	db	"rgxTabs60",0
_121:
	db	"rgxTabs61",0
_122:
	db	"rgxTabs62",0
_123:
	db	"rgxTabs63",0
_124:
	db	"rgxTabs70",0
_125:
	db	"rgxTabs71",0
_126:
	db	"rgxTabs72",0
_127:
	db	"rgxTabs73",0
_128:
	db	"dySpaceBefore",0
_129:
	db	"dySpaceAfter",0
_130:
	db	"dyLineSpacing",0
_131:
	db	"sStyle",0
_132:
	db	"bLineSpacingRule",0
_133:
	db	"b",0
_134:
	db	"bOutlineLevel",0
_135:
	db	"wShadingWeight",0
_136:
	db	"wShadingStyle",0
_137:
	db	"wNumberingStart",0
_138:
	db	"wNumberingStyle",0
_139:
	db	"wNumberingTab",0
_140:
	db	"wBorderSpace",0
_141:
	db	"wBorderWidth",0
_142:
	db	"wBorders",0
	align	4
_84:
	dd	2
	dd	_85
	dd	3
	dd	_86
	dd	_62
	dd	8
	dd	3
	dd	_87
	dd	_62
	dd	12
	dd	3
	dd	_88
	dd	_89
	dd	16
	dd	3
	dd	_90
	dd	_89
	dd	18
	dd	3
	dd	_91
	dd	_62
	dd	20
	dd	3
	dd	_92
	dd	_62
	dd	24
	dd	3
	dd	_93
	dd	_62
	dd	28
	dd	3
	dd	_94
	dd	_89
	dd	32
	dd	3
	dd	_95
	dd	_89
	dd	34
	dd	3
	dd	_96
	dd	_62
	dd	36
	dd	3
	dd	_97
	dd	_62
	dd	40
	dd	3
	dd	_98
	dd	_62
	dd	44
	dd	3
	dd	_99
	dd	_62
	dd	48
	dd	3
	dd	_100
	dd	_62
	dd	52
	dd	3
	dd	_101
	dd	_62
	dd	56
	dd	3
	dd	_102
	dd	_62
	dd	60
	dd	3
	dd	_103
	dd	_62
	dd	64
	dd	3
	dd	_104
	dd	_62
	dd	68
	dd	3
	dd	_105
	dd	_62
	dd	72
	dd	3
	dd	_106
	dd	_62
	dd	76
	dd	3
	dd	_107
	dd	_62
	dd	80
	dd	3
	dd	_108
	dd	_62
	dd	84
	dd	3
	dd	_109
	dd	_62
	dd	88
	dd	3
	dd	_110
	dd	_62
	dd	92
	dd	3
	dd	_111
	dd	_62
	dd	96
	dd	3
	dd	_112
	dd	_62
	dd	100
	dd	3
	dd	_113
	dd	_62
	dd	104
	dd	3
	dd	_114
	dd	_62
	dd	108
	dd	3
	dd	_115
	dd	_62
	dd	112
	dd	3
	dd	_116
	dd	_62
	dd	116
	dd	3
	dd	_117
	dd	_62
	dd	120
	dd	3
	dd	_118
	dd	_62
	dd	124
	dd	3
	dd	_119
	dd	_62
	dd	128
	dd	3
	dd	_120
	dd	_62
	dd	132
	dd	3
	dd	_121
	dd	_62
	dd	136
	dd	3
	dd	_122
	dd	_62
	dd	140
	dd	3
	dd	_123
	dd	_62
	dd	144
	dd	3
	dd	_124
	dd	_62
	dd	148
	dd	3
	dd	_125
	dd	_62
	dd	152
	dd	3
	dd	_126
	dd	_62
	dd	156
	dd	3
	dd	_127
	dd	_62
	dd	160
	dd	3
	dd	_128
	dd	_62
	dd	164
	dd	3
	dd	_129
	dd	_62
	dd	168
	dd	3
	dd	_130
	dd	_62
	dd	172
	dd	3
	dd	_131
	dd	_89
	dd	176
	dd	3
	dd	_132
	dd	_133
	dd	178
	dd	3
	dd	_134
	dd	_133
	dd	179
	dd	3
	dd	_135
	dd	_89
	dd	180
	dd	3
	dd	_136
	dd	_89
	dd	182
	dd	3
	dd	_137
	dd	_89
	dd	184
	dd	3
	dd	_138
	dd	_89
	dd	186
	dd	3
	dd	_139
	dd	_89
	dd	188
	dd	3
	dd	_140
	dd	_89
	dd	190
	dd	3
	dd	_141
	dd	_89
	dd	192
	dd	3
	dd	_142
	dd	_89
	dd	194
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	0
	align	4
_20:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_84
	dd	196
	dd	__skn3_maxguiex_PARAFORMAT2_New
	dd	__skn3_maxguiex_PARAFORMAT2_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_144:
	db	"Skn3PanelEx",0
_145:
	db	"gradientOn",0
_146:
	db	"gradientVertical",0
_147:
	db	"gradientStartR",0
_148:
	db	"gradientStartG",0
_149:
	db	"gradientStartB",0
_150:
	db	"gradientEndR",0
_151:
	db	"gradientEndG",0
_152:
	db	"gradientEndB",0
_153:
	db	"WndProc",0
_154:
	db	"(i,i,i,i)i",0
_155:
	db	"SetGradient",0
_156:
	db	"(i,i,i,i,i,i,i,i)i",0
	align	4
_143:
	dd	2
	dd	_144
	dd	3
	dd	_145
	dd	_62
	dd	272
	dd	3
	dd	_146
	dd	_62
	dd	276
	dd	3
	dd	_147
	dd	_62
	dd	280
	dd	3
	dd	_148
	dd	_62
	dd	284
	dd	3
	dd	_149
	dd	_62
	dd	288
	dd	3
	dd	_150
	dd	_62
	dd	292
	dd	3
	dd	_151
	dd	_62
	dd	296
	dd	3
	dd	_152
	dd	_62
	dd	300
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	6
	dd	_153
	dd	_154
	dd	520
	dd	6
	dd	_155
	dd	_156
	dd	580
	dd	0
	align	4
_skn3_maxguiex_Skn3PanelEx:
	dd	_maxgui_win32maxguiex_TWindowsPanel
	dd	_bbObjectFree
	dd	_143
	dd	304
	dd	__skn3_maxguiex_Skn3PanelEx_New
	dd	__skn3_maxguiex_Skn3PanelEx_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__maxgui_maxgui_TGadget_SetFilter
	dd	__maxgui_maxgui_TGadget_HasDescendant
	dd	__maxgui_maxgui_TGadget__setparent
	dd	__maxgui_maxgui_TGadget_SelectionChanged
	dd	__maxgui_maxgui_TGadget_Handle
	dd	__maxgui_maxgui_TGadget_GetXPos
	dd	__maxgui_maxgui_TGadget_GetYPos
	dd	__maxgui_maxgui_TGadget_GetWidth
	dd	__maxgui_maxgui_TGadget_GetHeight
	dd	__maxgui_maxgui_TGadget_GetGroup
	dd	__maxgui_maxgui_TGadget_SetShape
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	dd	__maxgui_maxgui_TGadget_SetRect
	dd	__maxgui_maxgui_TGadget_LockLayout
	dd	__maxgui_maxgui_TGadget_SetLayout
	dd	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	dd	__maxgui_maxgui_TGadget_DoLayout
	dd	__maxgui_maxgui_TGadget_SetDataSource
	dd	__maxgui_maxgui_TGadget_KeysFromList
	dd	__maxgui_maxgui_TGadget_KeysFromObjectArray
	dd	__maxgui_maxgui_TGadget_SyncDataSource
	dd	__maxgui_maxgui_TGadget_SyncData
	dd	__maxgui_maxgui_TGadget_InsertItemFromKey
	dd	__maxgui_maxgui_TGadget_Clear
	dd	__maxgui_maxgui_TGadget_InsertItem
	dd	__maxgui_maxgui_TGadget_SetItem
	dd	__maxgui_maxgui_TGadget_RemoveItem
	dd	__maxgui_maxgui_TGadget_ItemCount
	dd	__maxgui_maxgui_TGadget_ItemText
	dd	__maxgui_maxgui_TGadget_ItemTip
	dd	__maxgui_maxgui_TGadget_ItemFlags
	dd	__maxgui_maxgui_TGadget_ItemIcon
	dd	__maxgui_maxgui_TGadget_ItemExtra
	dd	__maxgui_maxgui_TGadget_SetItemState
	dd	__maxgui_maxgui_TGadget_ItemState
	dd	__maxgui_maxgui_TGadget_SelectItem
	dd	__maxgui_maxgui_TGadget_SelectedItem
	dd	__maxgui_maxgui_TGadget_SelectedItems
	dd	__maxgui_maxgui_TGadget_Insert
	dd	__maxgui_win32maxguiex_TWindowsGadget_Query
	dd	__maxgui_maxgui_TGadget_CleanUp
	dd	__maxgui_win32maxguiex_TWindowsPanel_Free
	dd	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	dd	__maxgui_win32maxguiex_TWindowsPanel_Activate
	dd	__maxgui_win32maxguiex_TWindowsGadget_State
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	dd	__maxgui_maxgui_TGadget_SetIconStrip
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	dd	__maxgui_maxgui_TGadget_SetSelected
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	dd	__maxgui_maxgui_TGadget_SetSensitivity
	dd	__maxgui_maxgui_TGadget_GetSensitivity
	dd	__maxgui_maxgui_TGadget_SetWatch
	dd	__maxgui_maxgui_TGadget_GetWatch
	dd	__maxgui_win32maxguiex_TWindowsPanel_Class
	dd	__maxgui_maxgui_TGadget_GetStatusText
	dd	__maxgui_maxgui_TGadget_SetStatusText
	dd	__maxgui_maxgui_TGadget_GetMenu
	dd	__maxgui_maxgui_TGadget_PopupMenu
	dd	__maxgui_maxgui_TGadget_UpdateMenu
	dd	__maxgui_maxgui_TGadget_SetMinimumSize
	dd	__maxgui_maxgui_TGadget_SetMaximumSize
	dd	__maxgui_maxgui_TGadget_ClearListItems
	dd	__maxgui_maxgui_TGadget_InsertListItem
	dd	__maxgui_maxgui_TGadget_SetListItem
	dd	__maxgui_maxgui_TGadget_RemoveListItem
	dd	__maxgui_maxgui_TGadget_SetListItemState
	dd	__maxgui_maxgui_TGadget_ListItemState
	dd	__maxgui_maxgui_TGadget_RootNode
	dd	__maxgui_maxgui_TGadget_InsertNode
	dd	__maxgui_maxgui_TGadget_ModifyNode
	dd	__maxgui_maxgui_TGadget_SelectedNode
	dd	__maxgui_maxgui_TGadget_CountKids
	dd	__maxgui_maxgui_TGadget_ReplaceText
	dd	__maxgui_maxgui_TGadget_AddText
	dd	__maxgui_maxgui_TGadget_AreaText
	dd	__maxgui_maxgui_TGadget_AreaLen
	dd	__maxgui_maxgui_TGadget_LockText
	dd	__maxgui_maxgui_TGadget_UnlockText
	dd	__maxgui_maxgui_TGadget_SetTabs
	dd	__maxgui_maxgui_TGadget_GetCursorPos
	dd	__maxgui_maxgui_TGadget_GetSelectionLength
	dd	__maxgui_maxgui_TGadget_SetStyle
	dd	__maxgui_maxgui_TGadget_SetSelection
	dd	__maxgui_maxgui_TGadget_CharX
	dd	__maxgui_maxgui_TGadget_CharY
	dd	__maxgui_maxgui_TGadget_CharAt
	dd	__maxgui_maxgui_TGadget_LineAt
	dd	__maxgui_maxgui_TGadget_SetValue
	dd	__maxgui_maxgui_TGadget_SetRange
	dd	__maxgui_maxgui_TGadget_SetStep
	dd	__maxgui_maxgui_TGadget_SetProp
	dd	__maxgui_maxgui_TGadget_GetProp
	dd	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	dd	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	dd	__maxgui_maxgui_TGadget_Run
	dd	__maxgui_win32maxguiex_TWindowsPanel_Create
	dd	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	dd	__maxgui_win32maxguiex_TWindowsGadget_Register
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	dd	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	dd	__maxgui_win32maxguiex_TWindowsGadget_isControl
	dd	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	dd	__skn3_maxguiex_Skn3PanelEx_WndProc
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	dd	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	dd	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	dd	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	dd	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	dd	__skn3_maxguiex_Skn3PanelEx_SetGradient
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_21:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_23:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	92
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	105,110,99,98,105,110,58,58
_450:
	db	"i",0
_524:
	db	"i",0
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	108,105,116,116,108,101,101,110,100,105,97,110,58,58
_557:
	db	":TGadgetItem",0
	align	4
_934:
	dd	0x41a00000
	align	4
_623:
	dd	0
_640:
	db	"i",0
_642:
	db	"i",0
_644:
	db	"i",0
_646:
	db	"i",0
	align	4
_967:
	dd	0x437f0000
