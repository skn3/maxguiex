	format	MS COFF
	extrn	_AddFontMemResourceEx@16
	extrn	_AddFontResourceExW@12
	extrn	_AlphaBlend@44
	extrn	_BitBlt@36
	extrn	_ClientToScreen@8
	extrn	_CreateCompatibleBitmap@12
	extrn	_CreateCompatibleDC@4
	extrn	_CreateSolidBrush@4
	extrn	_DeleteDC@4
	extrn	_DeleteObject@4
	extrn	_DestroyCursor@4
	extrn	_DrawTextW@20
	extrn	_FillRect@12
	extrn	_GetClientRect@8
	extrn	_GetClipBox@8
	extrn	_GetDC@4
	extrn	_GetUpdateRect@12
	extrn	_GetWindowLongW@8
	extrn	_GetWindowRect@8
	extrn	_IsRectEmpty@4
	extrn	_LoadCursorFromFileW@4
	extrn	_RedrawWindow@16
	extrn	_ReleaseDC@8
	extrn	_SelectObject@8
	extrn	_SendMessageA@16
	extrn	_SendMessageW@16
	extrn	_SetCursor@4
	extrn	_SetFocus@4
	extrn	_SetStretchBltMode@8
	extrn	_SetWindowLongW@12
	extrn	_SetWindowPos@28
	extrn	_StretchBlt@44
	extrn	___bb_blitz_blitz
	extrn	___bb_drivers_drivers
	extrn	___bb_linkedlist_linkedlist
	extrn	___bb_map_map
	extrn	___bb_systemex_systemex
	extrn	__maxgui_maxgui_TGadget_AddText
	extrn	__maxgui_maxgui_TGadget_AreaLen
	extrn	__maxgui_maxgui_TGadget_AreaText
	extrn	__maxgui_maxgui_TGadget_CharAt
	extrn	__maxgui_maxgui_TGadget_CharX
	extrn	__maxgui_maxgui_TGadget_CharY
	extrn	__maxgui_maxgui_TGadget_CleanUp
	extrn	__maxgui_maxgui_TGadget_Clear
	extrn	__maxgui_maxgui_TGadget_ClearListItems
	extrn	__maxgui_maxgui_TGadget_CountKids
	extrn	__maxgui_maxgui_TGadget_DoLayout
	extrn	__maxgui_maxgui_TGadget_GetCursorPos
	extrn	__maxgui_maxgui_TGadget_GetGroup
	extrn	__maxgui_maxgui_TGadget_GetHeight
	extrn	__maxgui_maxgui_TGadget_GetMenu
	extrn	__maxgui_maxgui_TGadget_GetProp
	extrn	__maxgui_maxgui_TGadget_GetSelectionLength
	extrn	__maxgui_maxgui_TGadget_GetSensitivity
	extrn	__maxgui_maxgui_TGadget_GetStatusText
	extrn	__maxgui_maxgui_TGadget_GetWatch
	extrn	__maxgui_maxgui_TGadget_GetWidth
	extrn	__maxgui_maxgui_TGadget_GetXPos
	extrn	__maxgui_maxgui_TGadget_GetYPos
	extrn	__maxgui_maxgui_TGadget_Handle
	extrn	__maxgui_maxgui_TGadget_HasDescendant
	extrn	__maxgui_maxgui_TGadget_Insert
	extrn	__maxgui_maxgui_TGadget_InsertItem
	extrn	__maxgui_maxgui_TGadget_InsertItemFromKey
	extrn	__maxgui_maxgui_TGadget_InsertListItem
	extrn	__maxgui_maxgui_TGadget_InsertNode
	extrn	__maxgui_maxgui_TGadget_ItemCount
	extrn	__maxgui_maxgui_TGadget_ItemExtra
	extrn	__maxgui_maxgui_TGadget_ItemFlags
	extrn	__maxgui_maxgui_TGadget_ItemIcon
	extrn	__maxgui_maxgui_TGadget_ItemState
	extrn	__maxgui_maxgui_TGadget_ItemText
	extrn	__maxgui_maxgui_TGadget_ItemTip
	extrn	__maxgui_maxgui_TGadget_KeysFromList
	extrn	__maxgui_maxgui_TGadget_KeysFromObjectArray
	extrn	__maxgui_maxgui_TGadget_LineAt
	extrn	__maxgui_maxgui_TGadget_ListItemState
	extrn	__maxgui_maxgui_TGadget_LockLayout
	extrn	__maxgui_maxgui_TGadget_LockText
	extrn	__maxgui_maxgui_TGadget_ModifyNode
	extrn	__maxgui_maxgui_TGadget_PopupMenu
	extrn	__maxgui_maxgui_TGadget_RemoveItem
	extrn	__maxgui_maxgui_TGadget_RemoveListItem
	extrn	__maxgui_maxgui_TGadget_ReplaceText
	extrn	__maxgui_maxgui_TGadget_RootNode
	extrn	__maxgui_maxgui_TGadget_Run
	extrn	__maxgui_maxgui_TGadget_SelectItem
	extrn	__maxgui_maxgui_TGadget_SelectedItem
	extrn	__maxgui_maxgui_TGadget_SelectedItems
	extrn	__maxgui_maxgui_TGadget_SelectedNode
	extrn	__maxgui_maxgui_TGadget_SelectionChanged
	extrn	__maxgui_maxgui_TGadget_SetDataSource
	extrn	__maxgui_maxgui_TGadget_SetFilter
	extrn	__maxgui_maxgui_TGadget_SetIconStrip
	extrn	__maxgui_maxgui_TGadget_SetItem
	extrn	__maxgui_maxgui_TGadget_SetItemState
	extrn	__maxgui_maxgui_TGadget_SetLayout
	extrn	__maxgui_maxgui_TGadget_SetListItem
	extrn	__maxgui_maxgui_TGadget_SetListItemState
	extrn	__maxgui_maxgui_TGadget_SetMaximumSize
	extrn	__maxgui_maxgui_TGadget_SetMinimumSize
	extrn	__maxgui_maxgui_TGadget_SetProp
	extrn	__maxgui_maxgui_TGadget_SetRange
	extrn	__maxgui_maxgui_TGadget_SetRect
	extrn	__maxgui_maxgui_TGadget_SetSelected
	extrn	__maxgui_maxgui_TGadget_SetSelection
	extrn	__maxgui_maxgui_TGadget_SetSensitivity
	extrn	__maxgui_maxgui_TGadget_SetShape
	extrn	__maxgui_maxgui_TGadget_SetStatusText
	extrn	__maxgui_maxgui_TGadget_SetStep
	extrn	__maxgui_maxgui_TGadget_SetStyle
	extrn	__maxgui_maxgui_TGadget_SetTabs
	extrn	__maxgui_maxgui_TGadget_SetValue
	extrn	__maxgui_maxgui_TGadget_SetWatch
	extrn	__maxgui_maxgui_TGadget_SyncData
	extrn	__maxgui_maxgui_TGadget_SyncDataSource
	extrn	__maxgui_maxgui_TGadget_UnlockText
	extrn	__maxgui_maxgui_TGadget_UpdateMenu
	extrn	__maxgui_maxgui_TGadget__setparent
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__cursor
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	extrn	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	extrn	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Query
	extrn	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Register
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	extrn	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_State
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isControl
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Activate
	extrn	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Class
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Create
	extrn	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Free
	extrn	__maxgui_win32maxguiex_TWindowsPanel_New
	extrn	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	extrn	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	extrn	__maxgui_win32maxguiex_TWindowsTextArea__oldCursor
	extrn	_bbAppTitle
	extrn	_bbArrayNew1D
	extrn	_bbArraySlice
	extrn	_bbEmptyString
	extrn	_bbFloatToInt
	extrn	_bbIncbinLen
	extrn	_bbIncbinPtr
	extrn	_bbIntAbs
	extrn	_bbMemFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectDtor
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbStringAsc
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_bbStringReplace
	extrn	_bbStringSlice
	extrn	_bbStringToInt
	extrn	_bbStringToLower
	extrn	_bbStringToWString
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_DeleteFile
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_ReadFile
	extrn	_brl_filesystem_StripDir
	extrn	_brl_linkedlist_CreateList
	extrn	_brl_map_CreateMap
	extrn	_brl_stream_CloseStream
	extrn	_brl_stream_CopyStream
	extrn	_brl_stream_ReadStream
	extrn	_brl_stream_WriteStream
	extrn	_brl_system_Notify
	extrn	_maxgui_localization_LocalizationMode
	extrn	_maxgui_maxgui_AddGadgetItem
	extrn	_maxgui_maxgui_GadgetClass
	extrn	_maxgui_maxgui_GadgetGroup
	extrn	_maxgui_maxgui_GadgetHeight
	extrn	_maxgui_maxgui_GadgetHidden
	extrn	_maxgui_maxgui_GadgetText
	extrn	_maxgui_maxgui_GadgetWidth
	extrn	_maxgui_maxgui_LocalizeGadget
	extrn	_maxgui_maxgui_QueryGadget
	extrn	_maxgui_maxgui_RedrawGadget
	extrn	_maxgui_maxgui_SetGadgetText
	extrn	_maxgui_maxgui_SetPointer
	extrn	_maxgui_maxgui_TGadgetItem
	extrn	_maxgui_maxgui_TProxyGadget
	extrn	_maxgui_maxgui_lastPointer
	extrn	_maxgui_win32maxguiex_TWindowsGadget
	extrn	_maxgui_win32maxguiex_TWindowsListBox
	extrn	_maxgui_win32maxguiex_TWindowsPanel
	extrn	_pub_win32_LVITEMW
	extrn	_skn3_systemex_GetTempDirectory
	public	___bb_maxguiex_maxguiex
	public	__skn3_maxguiex_PARAFORMAT2_New
	public	__skn3_maxguiex_Skn3CustomPointer_New
	public	__skn3_maxguiex_Skn3CustomPointer_all
	public	__skn3_maxguiex_Skn3ListBatchLock_Find
	public	__skn3_maxguiex_Skn3ListBatchLock_New
	public	__skn3_maxguiex_Skn3ListBatchLock_add
	public	__skn3_maxguiex_Skn3ListBatchLock_all
	public	__skn3_maxguiex_Skn3ListBatchLock_remove
	public	__skn3_maxguiex_Skn3PanelEx_New
	public	__skn3_maxguiex_Skn3PanelEx_SetGradient
	public	__skn3_maxguiex_Skn3PanelEx_WndProc
	public	_skn3_maxguiex_AssignGadgetClassId
	public	_skn3_maxguiex_BringWindowToTop
	public	_skn3_maxguiex_ClearColorPickerCustomColors
	public	_skn3_maxguiex_CreatePanelEx
	public	_skn3_maxguiex_DisableGadgetRedraw
	public	_skn3_maxguiex_EnableGadgetRedraw
	public	_skn3_maxguiex_ExtractCursorHotspot
	public	_skn3_maxguiex_FocusWindow
	public	_skn3_maxguiex_FreeCustomPointer
	public	_skn3_maxguiex_GadgetScreenPosition
	public	_skn3_maxguiex_GadgetSizeForString
	public	_skn3_maxguiex_GadgetToInt
	public	_skn3_maxguiex_GadgetWindow
	public	_skn3_maxguiex_GetAppResourcesPath
	public	_skn3_maxguiex_GetCreationGroup
	public	_skn3_maxguiex_GetGadgetMaxLength
	public	_skn3_maxguiex_HideGadgetBorder
	public	_skn3_maxguiex_InstallGuiFont
	public	_skn3_maxguiex_ListBatchAdd
	public	_skn3_maxguiex_ListBatchLock
	public	_skn3_maxguiex_ListBatchUnlock
	public	_skn3_maxguiex_LoadCustomPointer
	public	_skn3_maxguiex_MessageBox
	public	_skn3_maxguiex_PointOverGadget
	public	_skn3_maxguiex_RedrawGadgetFrame
	public	_skn3_maxguiex_RequestScrollbarSize
	public	_skn3_maxguiex_ScrollTextAreaToBottom
	public	_skn3_maxguiex_ScrollTextAreaToCursor
	public	_skn3_maxguiex_ScrollTextAreaToTop
	public	_skn3_maxguiex_SetColorPickerCustomColors
	public	_skn3_maxguiex_SetComboBoxHeight
	public	_skn3_maxguiex_SetCustomPointer
	public	_skn3_maxguiex_SetGadgetMaxLength
	public	_skn3_maxguiex_SetGadgetReadOnly
	public	_skn3_maxguiex_SetPanelExGradient
	public	_skn3_maxguiex_SetTextareaLineSpacing
	public	_skn3_maxguiex_SetWindowAlwaysOnTop
	public	_skn3_maxguiex_Skn3CustomPointer
	public	_skn3_maxguiex_Skn3PanelEx
	section	"code" code
___bb_maxguiex_maxguiex:
	push	ebp
	mov	ebp,esp
	cmp	dword [_353],0
	je	_354
	mov	eax,0
	mov	esp,ebp
	pop	ebp
	ret
_354:
	mov	dword [_353],1
	call	___bb_blitz_blitz
	call	___bb_map_map
	call	___bb_linkedlist_linkedlist
	call	___bb_drivers_drivers
	call	___bb_systemex_systemex
	push	_16
	call	_bbObjectRegisterType
	add	esp,4
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectRegisterType
	add	esp,4
	push	_20
	call	_bbObjectRegisterType
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectRegisterType
	add	esp,4
	mov	eax,0
	jmp	_156
_156:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_16
	mov	dword [ebx+8],0
	mov	dword [ebx+12],_bbNullObject
	mov	dword [ebx+16],0
	mov	dword [ebx+20],_bbNullObject
	mov	dword [ebx+24],_bbNullObject
	mov	dword [ebx+28],0
	mov	eax,0
	jmp	_159
_159:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_Find:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	esi,dword [ebp+8]
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_355
	mov	eax,_bbNullObject
	jmp	_162
_355:
	mov	eax,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+88]
	add	esp,4
	mov	ebx,eax
	jmp	_17
_19:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	dword [eax+12],esi
	jne	_360
	jmp	_162
_360:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
_17:
	cmp	ebx,_bbNullObject
	jne	_19
_18:
	mov	eax,_bbNullObject
	jmp	_162
_162:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_add:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_362
	call	_brl_linkedlist_CreateList
	mov	dword [__skn3_maxguiex_Skn3ListBatchLock_all],eax
_362:
	mov	eax,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebx+20],eax
	mov	eax,0
	jmp	_165
_165:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_remove:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	cmp	ebx,_bbNullObject
	je	_364
	mov	eax,dword [ebx+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	mov	dword [ebx+12],_bbNullObject
	mov	dword [ebx+24],_bbNullObject
_364:
	mov	eax,0
	jmp	_168
_168:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3CustomPointer_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_skn3_maxguiex_Skn3CustomPointer
	mov	dword [ebx+8],_bbEmptyString
	mov	dword [ebx+12],0
	mov	dword [ebx+16],0
	mov	eax,0
	jmp	_171
_171:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_PARAFORMAT2_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_20
	mov	dword [ebx+8],0
	mov	dword [ebx+12],0
	mov	word [ebx+16],0
	mov	word [ebx+18],0
	mov	dword [ebx+20],0
	mov	dword [ebx+24],0
	mov	dword [ebx+28],0
	mov	word [ebx+32],0
	mov	word [ebx+34],32
	mov	dword [ebx+36],0
	mov	dword [ebx+40],0
	mov	dword [ebx+44],0
	mov	dword [ebx+48],0
	mov	dword [ebx+52],0
	mov	dword [ebx+56],0
	mov	dword [ebx+60],0
	mov	dword [ebx+64],0
	mov	dword [ebx+68],0
	mov	dword [ebx+72],0
	mov	dword [ebx+76],0
	mov	dword [ebx+80],0
	mov	dword [ebx+84],0
	mov	dword [ebx+88],0
	mov	dword [ebx+92],0
	mov	dword [ebx+96],0
	mov	dword [ebx+100],0
	mov	dword [ebx+104],0
	mov	dword [ebx+108],0
	mov	dword [ebx+112],0
	mov	dword [ebx+116],0
	mov	dword [ebx+120],0
	mov	dword [ebx+124],0
	mov	dword [ebx+128],0
	mov	dword [ebx+132],0
	mov	dword [ebx+136],0
	mov	dword [ebx+140],0
	mov	dword [ebx+144],0
	mov	dword [ebx+148],0
	mov	dword [ebx+152],0
	mov	dword [ebx+156],0
	mov	dword [ebx+160],0
	mov	dword [ebx+164],0
	mov	dword [ebx+168],0
	mov	dword [ebx+172],0
	mov	word [ebx+176],0
	mov	byte [ebx+178],0
	mov	byte [ebx+179],0
	mov	word [ebx+180],0
	mov	word [ebx+182],0
	mov	word [ebx+184],0
	mov	word [ebx+186],0
	mov	word [ebx+188],0
	mov	word [ebx+190],0
	mov	word [ebx+192],0
	mov	word [ebx+194],0
	mov	eax,0
	jmp	_174
_174:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_22:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	cmp	dword [ebx+8],0
	jne	_366
	mov	eax,_1
	jmp	_179
_366:
	push	_21
	push	_23
	push	ebx
	call	_bbStringReplace
	add	esp,12
	mov	ebx,eax
	push	_21
	push	dword [ebp+12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_367
	push	dword [ebp+12]
	push	_21
	push	ebx
	call	_bbStringReplace
	add	esp,12
	mov	ebx,eax
_367:
	mov	edi,0
	mov	dword [ebp-4],0
	push	dword [ebp+12]
	call	_bbStringAsc
	add	esp,4
	mov	edx,eax
	mov	ecx,0
	mov	esi,dword [ebx+8]
	jmp	_372
_26:
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_374
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,edx
	setne	al
	movzx	eax,al
_374:
	cmp	eax,0
	je	_376
	jmp	_25
_376:
	mov	eax,ecx
	add	eax,1
	mov	edi,eax
	mov	dword [ebp-4],1
_24:
	add	ecx,1
_372:
	cmp	ecx,esi
	jl	_26
_25:
	mov	eax,dword [ebx+8]
	sub	eax,edi
	mov	esi,eax
	mov	eax,dword [ebx+8]
	sub	eax,1
	mov	ecx,eax
	jmp	_378
_29:
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_379
	movzx	eax,word [ebx+ecx*2+12]
	mov	eax,eax
	cmp	eax,edx
	setne	al
	movzx	eax,al
_379:
	cmp	eax,0
	je	_381
	jmp	_28
_381:
	sub	esi,1
_27:
	add	ecx,-1
_378:
	cmp	ecx,0
	jge	_29
_28:
	cmp	esi,0
	jg	_382
	mov	eax,_1
	jmp	_179
_382:
	mov	eax,edi
	add	eax,esi
	push	eax
	push	edi
	push	ebx
	call	_bbStringSlice
	add	esp,12
	mov	ebx,eax
	mov	eax,dword [ebp-4]
	cmp	eax,0
	je	_383
	mov	eax,dword [ebp+16]
_383:
	cmp	eax,0
	je	_385
	push	ebx
	push	dword [ebp+12]
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
_385:
	mov	eax,ebx
	jmp	_179
_179:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_30:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	push	_31
	push	8
	push	0
	push	dword [ebp+8]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_386
	push	_21
	push	1
	push	_21
	push	0
	call	_skn3_systemex_GetTempDirectory
	add	esp,4
	push	eax
	call	_22
	add	esp,12
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
	mov	ebx,_1
	mov	eax,dword [ebp+8]
	push	dword [eax+8]
	push	8
	push	dword [ebp+8]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_brl_filesystem_StripDir
	add	esp,4
	mov	edi,eax
	jmp	_32
_34:
	push	ebx
	call	_bbStringToInt
	add	esp,4
	add	eax,1
	push	eax
	call	_bbStringFromInt
	add	esp,4
	mov	ebx,eax
_32:
	push	edi
	push	ebx
	push	esi
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,0
	jne	_34
_33:
	push	edi
	push	ebx
	push	esi
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	edi,eax
	push	dword [ebp+8]
	call	_brl_stream_ReadStream
	add	esp,4
	mov	esi,eax
	cmp	esi,_bbNullObject
	jne	_392
	mov	eax,_1
	jmp	_182
_392:
	push	edi
	call	_brl_stream_WriteStream
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_394
	push	esi
	call	_brl_stream_CloseStream
	add	esp,4
	mov	eax,_1
	jmp	_182
_394:
	push	4096
	push	ebx
	push	esi
	call	_brl_stream_CopyStream
	add	esp,12
	push	ebx
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	esi
	call	_brl_stream_CloseStream
	add	esp,4
	mov	eax,edi
	jmp	_182
_386:
	mov	eax,_1
	jmp	_182
_182:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_35:
	push	ebp
	mov	ebp,esp
	fld	dword [ebp+8]
	fld	dword [ebp+12]
	fld	dword [ebp+16]
	fld	dword [ebp+20]
	fld	dword [ebp+24]
	fld	dword [ebp+28]
	fxch	st5
	fucom	st3
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	jne	_725
	fstp	st0
	fstp	st0
	fxch	st1
	fstp	st0
	jmp	_395
_725:
	fxch	st1
	faddp	st3,st0
	fucomp	st2
	fxch	st1
	fstp	st0
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_395:
	cmp	eax,0
	je	_397
	fxch	st1
	fucom	st1
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	fxch	st1
_397:
	cmp	eax,0
	jne	_726
	fstp	st0
	fstp	st0
	fstp	st0
	jmp	_399
_726:
	faddp	st2,st0
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_399:
	jmp	_190
_190:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RequestScrollbarSize:
	push	ebp
	mov	ebp,esp
	mov	eax,18
	jmp	_192
_192:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetComboBoxHeight:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	sub	eax,6
	push	eax
	push	-1
	push	339
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageA@16
	push	ebx
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	mov	eax,0
	jmp	_196
_196:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetScreenPosition:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	edx,dword [ebp+12]
	mov	dword [ebp-8],0
	mov	dword [ebp-4],0
	cmp	edx,0
	je	_402
	lea	edx,dword [ebp-8]
	push	edx
	push	2
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
	jmp	_404
_402:
	lea	edx,dword [ebp-8]
	push	edx
	push	1
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
_404:
	push	2
	push	_406
	call	_bbArrayNew1D
	add	esp,8
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx]
	mov	dword [eax+24],edx
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx+4]
	mov	dword [eax+4+24],edx
	jmp	_200
_200:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_PointOverGadget:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	mov	ebx,dword [ebp+16]
	mov	esi,dword [ebp+20]
	cmp	ebx,_bbNullObject
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_408
	push	0
	push	ebx
	call	_maxgui_maxgui_GadgetHidden
	add	esp,8
_408:
	cmp	eax,0
	je	_410
	mov	eax,0
	jmp	_206
_410:
	cmp	esi,_bbNullObject
	je	_411
	push	0
	push	esi
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	edx,dword [eax+24]
	add	edx,edi
	mov	edi,edx
	mov	eax,dword [eax+4+24]
	add	eax,dword [ebp+12]
	mov	dword [ebp+12],eax
_411:
	push	0
	push	ebx
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	esi,eax
	push	ebx
	call	_maxgui_maxgui_GadgetHeight
	add	esp,4
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	push	ebx
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [esi+4+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [esi+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp+12]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	dword [ebp+-4],edi
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	call	_35
	add	esp,24
	jmp	_206
_206:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_DisableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	0
	push	0
	push	11
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	mov	eax,1
	jmp	_209
_209:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_EnableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	0
	push	1
	push	11
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	mov	eax,1
	jmp	_212
_212:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_MessageBox:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	edx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	mov	ebx,dword [_bbAppTitle]
	mov	dword [_bbAppTitle],edx
	push	0
	push	eax
	call	_brl_system_Notify
	add	esp,8
	mov	dword [_bbAppTitle],ebx
	mov	eax,0
	jmp	_217
_217:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetSizeForString:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebp+16]
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,2
	je	_418
	push	dword [ebp-12]
	call	_GetDC@4
	mov	edi,eax
	push	0
	push	0
	push	49
	push	dword [ebp-12]
	call	_SendMessageW@16
	push	eax
	push	edi
	call	_SelectObject@8
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],0
	mov	dword [eax+28],0
	mov	dword [eax+32],esi
	mov	dword [eax+36],0
	mov	dword [ebp-4],eax
	mov	ebx,1280
	cmp	esi,0
	jle	_424
	or	ebx,16
_424:
	push	dword [ebp+12]
	call	_bbStringToWString
	add	esp,4
	mov	esi,eax
	push	ebx
	mov	eax,dword [ebp-4]
	lea	eax,dword [eax+24]
	push	eax
	push	-1
	push	esi
	push	edi
	call	_DrawTextW@20
	push	esi
	call	_bbMemFree
	add	esp,4
	push	edi
	push	dword [ebp-12]
	call	_ReleaseDC@8
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edx,dword [ebp-4]
	mov	edx,dword [edx+8+24]
	mov	dword [eax+24],edx
	mov	edx,dword [ebp-4]
	mov	edx,dword [edx+12+24]
	mov	dword [eax+28],edx
	jmp	_222
_418:
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],esi
	mov	dword [eax+28],0
	mov	dword [ebp-8],eax
	push	0
	push	0
	push	11
	push	dword [ebp-12]
	call	_SendMessageW@16
	push	ebx
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	esi,eax
	push	dword [ebp+12]
	push	ebx
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	eax,dword [ebp-8]
	lea	eax,byte [eax+24]
	push	eax
	push	0
	push	5633
	push	dword [ebp-12]
	call	_SendMessageW@16
	push	esi
	push	ebx
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	0
	push	1
	push	11
	push	dword [ebp-12]
	call	_SendMessageW@16
	mov	eax,dword [ebp-8]
	jmp	_222
_222:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetCreationGroup:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	_maxgui_maxgui_TProxyGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_432
	push	dword [eax+140]
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	jmp	_225
_432:
	mov	eax,ebx
	jmp	_225
_225:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetReadOnly:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	esi
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_435
	cmp	eax,4
	je	_435
	jmp	_434
_435:
	push	1
	push	esi
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	0
	push	ebx
	push	207
	push	eax
	call	_SendMessageW@16
	jmp	_434
_434:
	mov	eax,0
	jmp	_229
_229:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	esi
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_439
	cmp	eax,5
	je	_439
	jmp	_438
_439:
	cmp	ebx,0
	jge	_440
	mov	ebx,0
_440:
	push	0
	push	ebx
	push	197
	push	1
	push	esi
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	jmp	_438
_438:
	mov	eax,0
	jmp	_233
_233:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_443
	cmp	eax,5
	je	_443
	mov	eax,0
	jmp	_236
_443:
	push	0
	push	0
	push	213
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	jmp	_236
_236:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_LoadCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	edi,_bbNullObject
	push	ebx
	call	_30
	add	esp,4
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	eax,dword [ebp-4]
	cmp	dword [eax+8],0
	jne	_447
	mov	dword [ebp-4],ebx
	jmp	_448
_447:
	mov	dword [ebp-8],1
_448:
	cmp	dword [__skn3_maxguiex_Skn3CustomPointer_all],_bbNullObject
	jne	_449
	call	_brl_map_CreateMap
	mov	dword [__skn3_maxguiex_Skn3CustomPointer_all],eax
	jmp	_450
_449:
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	push	_skn3_maxguiex_Skn3CustomPointer
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,8
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
_450:
	cmp	edi,_bbNullObject
	jne	_452
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectNew
	add	esp,4
	mov	edi,eax
	mov	dword [edi+8],ebx
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	push	edi
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,12
	push	dword [ebp-4]
	call	_bbStringToWString
	add	esp,4
	mov	esi,eax
	push	esi
	call	_LoadCursorFromFileW@4
	mov	ebx,eax
	push	esi
	call	_bbMemFree
	add	esp,4
	mov	dword [edi+12],ebx
_452:
	cmp	dword [ebp-8],0
	je	_456
	push	dword [ebp-4]
	call	_brl_filesystem_DeleteFile
	add	esp,4
_456:
	cmp	dword [edi+12],0
	je	_457
	add	dword [edi+16],1
	mov	eax,edi
	jmp	_239
_457:
	mov	eax,_bbNullObject
	jmp	_239
_239:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetCustomPointer:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	cmp	ebx,_bbNullObject
	je	_458
	mov	dword [_maxgui_maxgui_lastPointer],-1
	push	dword [ebx+12]
	call	_SetCursor@4
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	cmp	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],0
	je	_459
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],eax
_459:
_458:
	mov	eax,0
	jmp	_242
_242:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FreeCustomPointer:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	cmp	ebx,_bbNullObject
	je	_460
	sub	dword [ebx+16],1
	cmp	dword [ebx+16],0
	jne	_461
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	push	dword [ebx+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	eax,dword [ebx+12]
	cmp	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	jne	_463
	push	0
	call	_maxgui_maxgui_SetPointer
	add	esp,4
_463:
	push	dword [ebx+12]
	call	_DestroyCursor@4
	mov	dword [ebx+12],0
_461:
_460:
	mov	eax,0
	jmp	_245
_245:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ExtractCursorHotspot:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	edi,dword [ebp+12]
	push	2
	push	_464
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-4],eax
	push	ebx
	push	_36
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_ReadFile
	add	esp,4
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_467
	mov	eax,esi
	push	2
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,8
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,2
	jne	_471
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	ebx,eax
	cmp	edi,ebx
	jge	_473
	mov	eax,edi
	imul	eax,12
	add	eax,6
	add	eax,4
	mov	ebx,eax
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	ebx,eax
	jge	_475
	mov	eax,esi
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,8
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	edx,dword [ebp-4]
	mov	dword [edx+24],eax
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,4
	mov	edx,dword [ebp-4]
	mov	dword [edx+4+24],eax
_475:
_473:
_471:
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,4
_467:
	mov	eax,dword [ebp-4]
	jmp	_249
_249:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchLock:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	dword [_16+48]
	add	esp,4
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_481
	add	dword [esi+8],1
	mov	eax,0
	jmp	_252
_481:
	push	_maxgui_win32maxguiex_TWindowsListBox
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_483
	mov	eax,0
	jmp	_252
_483:
	push	_16
	call	_bbObjectNew
	add	esp,4
	mov	esi,eax
	mov	dword [esi+8],1
	mov	dword [esi+12],ebx
	mov	eax,dword [ebx+124]
	mov	eax,dword [eax+20]
	mov	dword [esi+16],eax
	push	_pub_win32_LVITEMW
	call	_bbObjectNew
	add	esp,4
	mov	dword [esi+24],eax
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [esi+28],eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+552]
	add	esp,4
	push	esi
	call	dword [_16+52]
	add	esp,4
	mov	eax,0
	jmp	_252
_252:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchAdd:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	push	esi
	call	dword [_16+48]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_486
	push	dword [ebp+28]
	push	dword [ebp+24]
	push	dword [ebp+20]
	push	dword [ebp+16]
	push	dword [ebp+12]
	push	esi
	call	_maxgui_maxgui_AddGadgetItem
	add	esp,24
	mov	eax,0
	jmp	_260
_486:
	push	_maxgui_maxgui_TGadgetItem
	call	_bbObjectNew
	add	esp,4
	mov	edi,eax
	mov	eax,edi
	push	dword [ebp+16]
	push	dword [ebp+28]
	push	dword [ebp+20]
	push	dword [ebp+24]
	push	dword [ebp+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,24
	mov	eax,dword [ebx+16]
	add	eax,1
	push	eax
	push	0
	push	dword [esi+124]
	push	_489
	call	_bbArraySlice
	add	esp,16
	mov	dword [esi+124],eax
	mov	edx,dword [esi+124]
	mov	eax,dword [ebx+16]
	mov	dword [edx+eax*4+24],edi
	mov	eax,dword [ebx+24]
	mov	dword [eax+8],4097
	mov	edx,dword [ebx+24]
	mov	eax,dword [ebx+16]
	mov	dword [edx+12],eax
	mov	esi,dword [ebx+24]
	push	dword [edi+8]
	call	_bbStringToWString
	add	esp,4
	mov	dword [esi+28],eax
	mov	edx,dword [ebx+24]
	mov	eax,dword [ebx+24]
	mov	eax,dword [eax+8]
	or	eax,2
	mov	dword [edx+8],eax
	mov	edx,dword [ebx+24]
	mov	eax,dword [edi+16]
	mov	dword [edx+36],eax
	mov	eax,dword [ebx+24]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	4173
	push	dword [ebx+28]
	call	_SendMessageW@16
	mov	eax,dword [ebx+24]
	push	dword [eax+28]
	call	_bbMemFree
	add	esp,4
	add	dword [ebx+16],1
	mov	eax,0
	jmp	_260
_260:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchUnlock:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	eax
	call	dword [_16+48]
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_491
	mov	eax,0
	jmp	_263
_491:
	sub	dword [ebx+8],1
	cmp	dword [ebx+8],0
	jne	_492
	push	-2
	push	0
	push	4126
	push	dword [ebx+28]
	call	_SendMessageW@16
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+580]
	add	esp,4
	cmp	eax,0
	jne	_494
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_494:
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+548]
	add	esp,4
	push	ebx
	call	dword [_16+56]
	add	esp,4
_492:
	mov	eax,0
	jmp	_263
_263:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetWindow:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	eax,dword [ebp+8]
	push	eax
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	ebx,eax
	jmp	_37
_39:
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,1
	jne	_498
	mov	eax,ebx
	jmp	_266
_498:
	push	ebx
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	ebx,eax
_37:
	cmp	ebx,_bbNullObject
	jne	_39
_38:
	mov	eax,_bbNullObject
	jmp	_266
_266:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetWindowAlwaysOnTop:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_500
	cmp	ebx,0
	je	_501
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-1
	push	eax
	call	_SetWindowPos@28
	jmp	_502
_501:
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-2
	push	eax
	call	_SetWindowPos@28
_502:
_500:
	mov	eax,0
	jmp	_270
_270:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_BringWindowToTop:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_504
	push	19
	push	0
	push	0
	push	0
	push	0
	push	0
	push	eax
	call	_SetWindowPos@28
_504:
	mov	eax,0
	jmp	_273
_273:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FocusWindow:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_506
	push	eax
	call	_SetFocus@4
_506:
	mov	eax,0
	jmp	_276
_276:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetToInt:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	jmp	_279
_279:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	eax,dword [ebp+8]
	cmp	dword [eax+20],16
	jge	_507
	mov	ebx,dword [eax+20]
	push	16
	push	0
	push	eax
	push	_62
	call	_bbArraySlice
	add	esp,16
	jmp	_509
_42:
	mov	dword [eax+ebx*4+24],16777215
_40:
	add	ebx,1
_509:
	cmp	ebx,16
	jl	_42
_41:
_507:
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors],eax
	mov	eax,0
	jmp	_282
_282:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ClearColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	mov	edx,0
	jmp	_511
_45:
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	mov	dword [eax+edx*4+24],16777215
_43:
	add	edx,1
_511:
	cmp	edx,16
	jl	_45
_44:
	mov	eax,0
	jmp	_284
_284:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RedrawGadgetFrame:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	1
	push	eax
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	ebx,eax
	cmp	ebx,0
	je	_513
	push	55
	push	0
	push	0
	push	0
	push	0
	push	0
	push	ebx
	call	_SetWindowPos@28
	push	1345
	push	0
	push	0
	push	ebx
	call	_RedrawWindow@16
_513:
	mov	eax,0
	jmp	_287
_287:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_HideGadgetBorder:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	push	dword [ebp+8]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_516
	cmp	eax,4
	je	_516
	cmp	eax,7
	je	_516
	jmp	_515
_516:
	push	1
	push	dword [ebp+8]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	esi,eax
	cmp	esi,0
	je	_518
	push	-16
	push	esi
	call	_GetWindowLongW@8
	mov	ebx,eax
	push	-20
	push	esi
	call	_GetWindowLongW@8
	mov	edi,eax
	mov	eax,0
	mov	edx,ebx
	and	edx,8388608
	cmp	edx,0
	je	_522
	mov	eax,ebx
	and	eax,-8388609
	push	eax
	push	-16
	push	esi
	call	_SetWindowLongW@12
	mov	eax,1
_522:
	mov	edx,edi
	and	edx,512
	cmp	edx,0
	je	_523
	mov	eax,edi
	and	eax,-513
	push	eax
	push	-20
	push	esi
	call	_SetWindowLongW@12
	mov	eax,1
_523:
	cmp	eax,0
	je	_524
	push	dword [ebp+8]
	call	_skn3_maxguiex_RedrawGadgetFrame
	add	esp,4
_524:
_518:
	jmp	_515
_515:
	mov	eax,0
	jmp	_290
_290:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_InstallGuiFont:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	_31
	push	8
	push	0
	push	ebx
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_525
	push	dword [ebx+8]
	push	8
	push	ebx
	call	_bbStringSlice
	add	esp,12
	mov	ebx,eax
	mov	dword [ebp-4],0
	lea	eax,dword [ebp-4]
	push	eax
	push	0
	push	ebx
	call	_bbIncbinLen
	add	esp,4
	push	eax
	push	ebx
	call	_bbIncbinPtr
	add	esp,4
	push	eax
	call	_AddFontMemResourceEx@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	jmp	_293
_525:
	push	ebx
	call	_bbStringToWString
	add	esp,4
	mov	ebx,eax
	push	0
	push	16
	push	ebx
	call	_AddFontResourceExW@12
	mov	esi,eax
	push	ebx
	call	_bbMemFree
	add	esp,4
	cmp	esi,0
	setne	al
	movzx	eax,al
	jmp	_293
_293:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetTextareaLineSpacing:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_530
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	esi,eax
	cmp	esi,0
	je	_532
	push	_20
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],188
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],256
	mov	eax,dword [ebp-4]
	mov	byte [eax+178],5
	mov	ebx,dword [ebp-4]
	fld	dword [ebp+12]
	fmul	dword [_847]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+172],eax
	mov	eax,dword [ebp-4]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	1095
	push	esi
	call	_SendMessageW@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	jmp	_297
_532:
_530:
	mov	eax,0
	jmp	_297
_297:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToTop:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_534
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_536
	push	0
	push	6
	push	181
	push	eax
	call	_SendMessageW@16
_536:
_534:
	mov	eax,0
	jmp	_300
_300:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToBottom:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_537
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_539
	push	0
	push	7
	push	181
	push	eax
	call	_SendMessageW@16
_539:
_537:
	mov	eax,0
	jmp	_303
_303:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToCursor:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_540
	push	1
	push	ebx
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	cmp	eax,0
	je	_542
	push	0
	push	0
	push	183
	push	eax
	call	_SendMessageW@16
_542:
_540:
	mov	eax,0
	jmp	_306
_306:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetAppResourcesPath:
	push	ebp
	mov	ebp,esp
	mov	eax,_31
	jmp	_308
_308:
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_AssignGadgetClassId:
	push	ebp
	mov	ebp,esp
	sub	dword [_543],1
	mov	eax,dword [_543]
	jmp	_310
_310:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	__maxgui_win32maxguiex_TWindowsPanel_New
	add	esp,4
	mov	dword [ebx],_skn3_maxguiex_Skn3PanelEx
	mov	dword [ebx+272],0
	mov	dword [ebx+276],1
	mov	dword [ebx+280],255
	mov	dword [ebx+284],255
	mov	dword [ebx+288],255
	mov	dword [ebx+292],0
	mov	dword [ebx+296],0
	mov	dword [ebx+300],0
	mov	eax,0
	jmp	_313
_313:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_WndProc:
	push	ebp
	mov	ebp,esp
	sub	esp,84
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+16]
	mov	ecx,dword [ebp+24]
	cmp	eax,20
	je	_546
	jmp	_545
_546:
	mov	edx,dword [ebp+8]
	cmp	dword [edx+272],0
	jne	_547
	push	ecx
	push	dword [ebp+20]
	push	eax
	push	dword [ebp+12]
	push	dword [ebp+8]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	jmp	_320
_547:
	mov	eax,dword [ebp+8]
	cmp	dword [eax+244],2
	jne	_549
	mov	eax,1
	jmp	_320
_549:
	mov	eax,dword [ebp+20]
	mov	dword [ebp-76],eax
	mov	dword [ebp-80],0
	mov	dword [ebp-72],0
	push	4
	push	_559
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-12],eax
	push	4
	push	_561
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-16],eax
	push	4
	push	_563
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-4],eax
	push	4
	push	_565
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-4]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-76]
	call	_GetClipBox@8
	mov	eax,dword [ebp-8]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp+12]
	call	_GetWindowRect@8
	mov	eax,dword [ebp-12]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp+12]
	call	_GetClientRect@8
	push	0
	mov	eax,dword [ebp-16]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp+12]
	call	_GetUpdateRect@12
	cmp	eax,0
	jne	_567
	mov	eax,dword [ebp-4]
	mov	dword [ebp-16],eax
_567:
	mov	eax,dword [ebp-16]
	lea	eax,dword [eax+24]
	push	eax
	call	_IsRectEmpty@4
	cmp	eax,0
	je	_568
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],0
	mov	dword [eax+28],0
	mov	edx,dword [ebp-8]
	mov	ecx,dword [edx+8+24]
	mov	edx,dword [ebp-8]
	sub	ecx,dword [edx+24]
	mov	dword [eax+32],ecx
	mov	edx,dword [ebp-8]
	mov	ecx,dword [edx+12+24]
	mov	edx,dword [ebp-8]
	sub	ecx,dword [edx+4+24]
	mov	dword [eax+36],ecx
	mov	dword [ebp-16],eax
_568:
	mov	edx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	cmp	eax,dword [edx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_576
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+200]
	cmp	eax,0
	je	_570
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+252]
_570:
	cmp	eax,0
	je	_572
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+256]
_572:
	cmp	eax,0
	jne	_574
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_574:
_576:
	cmp	eax,0
	je	_578
	push	dword [ebp+20]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-76],eax
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	dword [ebp+20]
	call	_CreateCompatibleBitmap@12
	mov	dword [ebp-80],eax
	push	dword [ebp-80]
	push	dword [ebp-76]
	call	_SelectObject@8
_578:
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+24]
	mov	dword [eax+24],edx
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+4+24]
	mov	dword [eax+28],edx
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+8+24]
	mov	dword [eax+32],edx
	mov	edx,dword [ebp-16]
	mov	edx,dword [edx+12+24]
	mov	dword [eax+36],edx
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+8]
	cmp	dword [eax+276],0
	je	_591
	mov	eax,dword [ebp-12]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+292]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+280]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-44]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+296]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+284]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-40]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+300]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+288]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-48]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+280]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+4+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-44]
	faddp	st1,st0
	fstp	dword [ebp-28]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+284]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+4+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-40]
	faddp	st1,st0
	fstp	dword [ebp-24]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+288]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+4+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-48]
	faddp	st1,st0
	fstp	dword [ebp-32]
	mov	eax,dword [ebp-16]
	mov	edi,dword [eax+4+24]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax+12+24]
	mov	dword [ebp-56],eax
	jmp	_592
_48:
	fld	dword [ebp-32]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-24]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-28]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	ebx,eax
	push	ebx
	push	dword [ebp-76]
	call	_SelectObject@8
	mov	esi,eax
	mov	eax,dword [ebp-20]
	mov	dword [eax+4+24],edi
	mov	edx,dword [ebp-20]
	mov	eax,edi
	add	eax,1
	mov	dword [edx+12+24],eax
	push	ebx
	mov	eax,dword [ebp-20]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-76]
	call	_FillRect@12
	push	esi
	push	dword [ebp-76]
	call	_SelectObject@8
	push	ebx
	call	_DeleteObject@4
	fld	dword [ebp-28]
	fadd	dword [ebp-44]
	fstp	dword [ebp-28]
	fld	dword [ebp-24]
	fadd	dword [ebp-40]
	fstp	dword [ebp-24]
	fld	dword [ebp-32]
	fadd	dword [ebp-48]
	fstp	dword [ebp-32]
_46:
	add	edi,1
_592:
	cmp	edi,dword [ebp-56]
	jl	_48
_47:
	jmp	_594
_591:
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+292]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+280]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-44]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+296]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+284]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-40]
	mov	edx,dword [ebp+8]
	mov	ecx,dword [edx+300]
	mov	edx,dword [ebp+8]
	sub	ecx,dword [edx+288]
	mov	dword [ebp+-84],ecx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-48]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+280]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-44]
	faddp	st1,st0
	fstp	dword [ebp-28]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+284]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-40]
	faddp	st1,st0
	fstp	dword [ebp-24]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+288]
	mov	dword [ebp+-84],eax
	fild	dword [ebp+-84]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+24]
	mov	eax,dword [ebp-12]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	fmul	dword [ebp-48]
	faddp	st1,st0
	fstp	dword [ebp-32]
	mov	eax,dword [ebp-16]
	mov	edi,dword [eax+24]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax+8+24]
	mov	dword [ebp-52],eax
	jmp	_595
_51:
	fld	dword [ebp-32]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-24]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-28]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	ebx,eax
	push	ebx
	push	dword [ebp-76]
	call	_SelectObject@8
	mov	esi,eax
	mov	eax,dword [ebp-20]
	mov	dword [eax+24],edi
	mov	edx,dword [ebp-20]
	mov	eax,edi
	add	eax,1
	mov	dword [edx+8+24],eax
	push	ebx
	mov	eax,dword [ebp-20]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-76]
	call	_FillRect@12
	push	esi
	push	dword [ebp-76]
	call	_SelectObject@8
	push	ebx
	call	_DeleteObject@4
	fld	dword [ebp-28]
	fadd	dword [ebp-44]
	fstp	dword [ebp-28]
	fld	dword [ebp-24]
	fadd	dword [ebp-40]
	fstp	dword [ebp-24]
	fld	dword [ebp-32]
	fadd	dword [ebp-48]
	fstp	dword [ebp-32]
_49:
	add	edi,1
_595:
	cmp	edi,dword [ebp-52]
	jl	_51
_50:
_594:
	mov	edx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	cmp	eax,dword [edx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_603
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+200]
	cmp	eax,0
	je	_597
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+252]
_597:
	cmp	eax,0
	je	_599
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+256]
_599:
	cmp	eax,0
	jne	_601
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_601:
_603:
	cmp	eax,0
	jne	_605
	mov	eax,1
	jmp	_320
_605:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+200]
	cmp	eax,0
	je	_606
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+252]
_606:
	cmp	eax,0
	je	_608
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+256]
_608:
	cmp	eax,0
	je	_610
	push	dword [ebp-76]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-36],eax
	mov	eax,dword [ebp+8]
	push	dword [eax+200]
	push	dword [ebp-36]
	call	_SelectObject@8
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+252]
	mov	eax,dword [ebp+8]
	mov	edi,dword [eax+256]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+260]
	and	eax,7
	cmp	eax,0
	je	_613
	cmp	eax,1
	je	_614
	cmp	eax,2
	je	_615
	cmp	eax,4
	je	_615
	cmp	eax,3
	je	_616
	jmp	_612
_613:
	jmp	_52
_54:
	mov	ebx,0
	jmp	_55
_57:
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_617
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_618
_617:
	push	13369376
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_BitBlt@36
_618:
	add	ebx,esi
_55:
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	cmp	ebx,edx
	jl	_57
_56:
	add	dword [ebp-72],edi
_52:
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	cmp	dword [ebp-72],edx
	jl	_54
_53:
	jmp	_612
_614:
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+24]
	sub	eax,esi
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+12+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+4+24]
	sub	eax,edi
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-72],eax
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_619
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_620
_619:
	push	13369376
	push	0
	push	0
	push	dword [ebp-36]
	push	edi
	push	esi
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_BitBlt@36
_620:
	jmp	_612
_615:
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],esi
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fstp	dword [ebp-64]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	mov	dword [ebp+-84],edx
	fild	dword [ebp+-84]
	mov	dword [ebp+-84],edi
	fild	dword [ebp+-84]
	fdivp	st1,st0
	fld	dword [ebp-64]
	fucomp	st1
	fnstsw	ax
	sahf
	setbe	al
	movzx	eax,al
	cmp	eax,0
	je	_914
	fstp	st0
	jmp	_623
_914:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+260]
	and	eax,7
	cmp	eax,2
	je	_915
	fstp	st0
	jmp	_624
_915:
	fstp	dword [ebp-64]
	jmp	_625
_624:
_625:
_623:
	fld	dword [ebp-64]
	mov	dword [ebp+-84],esi
	fild	dword [ebp+-84]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-60],eax
	fld	dword [ebp-64]
	mov	dword [ebp+-84],edi
	fild	dword [ebp+-84]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-68],eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+24]
	sub	eax,dword [ebp-60]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+12+24]
	mov	edx,dword [ebp-8]
	sub	eax,dword [edx+4+24]
	sub	eax,dword [ebp-68]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-72],eax
	push	3
	push	dword [ebp-76]
	call	_SetStretchBltMode@8
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_628
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	dword [ebp-68]
	push	dword [ebp-60]
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_629
_628:
	push	13369376
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	push	dword [ebp-68]
	push	dword [ebp-60]
	push	dword [ebp-72]
	push	ebx
	push	dword [ebp-76]
	call	_StretchBlt@44
_629:
	jmp	_612
_616:
	push	3
	push	dword [ebp-76]
	call	_SetStretchBltMode@8
	mov	eax,dword [ebp+8]
	cmp	dword [eax+268],0
	je	_630
	push	33488896
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-76]
	call	_AlphaBlend@44
	jmp	_631
_630:
	push	13369376
	push	edi
	push	esi
	push	0
	push	0
	push	dword [ebp-36]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-76]
	call	_StretchBlt@44
_631:
	jmp	_612
_612:
	push	dword [ebp-36]
	call	_DeleteDC@4
_610:
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	jne	_632
	push	0
	push	dword [ebp+12]
	push	dword [ebp+20]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax]
	call	dword [eax+544]
	add	esp,12
	mov	eax,dword [ebp+8]
	fld	dword [eax+248]
	fmul	dword [_878]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	and	eax,255
	shl	eax,16
	push	eax
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+24]
	push	edx
	mov	eax,dword [ebp-16]
	push	dword [eax+4+24]
	mov	eax,dword [ebp-16]
	push	dword [eax+24]
	push	dword [ebp-76]
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-16]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-16]
	sub	edx,dword [eax+24]
	push	edx
	mov	eax,dword [ebp-16]
	push	dword [eax+4+24]
	mov	eax,dword [ebp-16]
	push	dword [eax+24]
	push	dword [ebp+20]
	call	_AlphaBlend@44
	jmp	_634
_632:
	push	13369376
	push	0
	push	0
	push	dword [ebp-76]
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+12+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+4+24]
	push	edx
	mov	eax,dword [ebp-8]
	mov	edx,dword [eax+8+24]
	mov	eax,dword [ebp-8]
	sub	edx,dword [eax+24]
	push	edx
	push	0
	push	0
	push	dword [ebp+20]
	call	_BitBlt@36
_634:
	push	dword [ebp-80]
	call	_DeleteObject@4
	push	dword [ebp-76]
	call	_DeleteDC@4
	mov	eax,1
	jmp	_320
_545:
	push	ecx
	push	dword [ebp+20]
	push	eax
	push	dword [ebp+12]
	push	dword [ebp+8]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	jmp	_320
_320:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_SetGradient:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	eax,dword [ebp+12]
	mov	ebx,dword [ebp+16]
	mov	ecx,dword [ebp+20]
	mov	edx,dword [ebp+24]
	mov	edi,dword [ebp+28]
	cmp	eax,0
	jne	_635
	cmp	dword [esi+272],0
	je	_636
	mov	dword [esi+272],0
	push	esi
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
_636:
	jmp	_637
_635:
	cmp	ebx,edi
	sete	al
	movzx	eax,al
	cmp	eax,0
	je	_638
	cmp	edx,dword [ebp+36]
	sete	al
	movzx	eax,al
_638:
	cmp	eax,0
	je	_640
	cmp	ecx,dword [ebp+32]
	sete	al
	movzx	eax,al
_640:
	cmp	eax,0
	je	_642
	mov	dword [esi+272],0
	mov	eax,esi
	push	ecx
	push	edx
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+248]
	add	esp,16
	jmp	_644
_642:
	mov	eax,dword [esi+272]
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_645
	cmp	ebx,dword [esi+280]
	setne	al
	movzx	eax,al
_645:
	cmp	eax,0
	jne	_647
	cmp	edx,dword [esi+284]
	setne	al
	movzx	eax,al
_647:
	cmp	eax,0
	jne	_649
	cmp	ecx,dword [esi+288]
	setne	al
	movzx	eax,al
_649:
	cmp	eax,0
	jne	_651
	cmp	edi,dword [esi+292]
	setne	al
	movzx	eax,al
_651:
	cmp	eax,0
	jne	_653
	mov	eax,dword [ebp+36]
	cmp	eax,dword [esi+296]
	setne	al
	movzx	eax,al
_653:
	cmp	eax,0
	jne	_655
	mov	eax,dword [ebp+32]
	cmp	eax,dword [esi+300]
	setne	al
	movzx	eax,al
_655:
	cmp	eax,0
	jne	_657
	mov	eax,dword [ebp+40]
	cmp	eax,dword [esi+276]
	setne	al
	movzx	eax,al
_657:
	cmp	eax,0
	je	_659
	mov	dword [esi+272],1
	mov	dword [esi+280],ebx
	mov	dword [esi+284],edx
	mov	dword [esi+288],ecx
	mov	dword [esi+292],edi
	mov	eax,dword [ebp+36]
	mov	dword [esi+296],eax
	mov	eax,dword [ebp+32]
	mov	dword [esi+300],eax
	mov	eax,dword [ebp+40]
	mov	dword [esi+276],eax
	push	esi
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
_659:
_644:
_637:
	mov	eax,0
	jmp	_331
_331:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_CreatePanelEx:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+24]
	mov	ebx,dword [ebp+28]
	mov	esi,dword [ebp+32]
	push	edi
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	mov	edi,eax
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectNew
	add	esp,4
	push	_1
	push	ebx
	push	edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+480]
	add	esp,16
	mov	ebx,eax
	call	_maxgui_localization_LocalizationMode
	and	eax,2
	cmp	eax,0
	je	_662
	push	_1
	push	esi
	push	ebx
	call	_maxgui_maxgui_LocalizeGadget
	add	esp,12
	jmp	_663
_662:
	mov	eax,ebx
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+236]
	add	esp,8
_663:
	cmp	edi,_bbNullObject
	je	_665
	mov	eax,ebx
	push	-1
	push	edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,12
_665:
	mov	eax,ebx
	push	dword [ebp+20]
	push	dword [ebp+16]
	push	dword [ebp+12]
	push	dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+88]
	add	esp,20
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_668
	mov	eax,ebx
	push	dword [__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+244]
	add	esp,8
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	edi
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_670
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	edi
	call	_bbObjectDowncast
	add	esp,8
	mov	eax,dword [eax+232]
	cmp	eax,0
	je	_672
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	edi
	call	_bbObjectDowncast
	add	esp,8
	mov	eax,dword [eax+236]
	cmp	eax,0
	sete	al
	movzx	eax,al
_672:
	cmp	eax,0
	sete	al
	movzx	eax,al
	mov	dword [esi+236],eax
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+232]
	add	esp,4
	and	eax,4
	cmp	eax,0
	sete	al
	movzx	eax,al
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+284]
	add	esp,8
_670:
	mov	eax,ebx
	push	0
	push	1
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+280]
	add	esp,12
_668:
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_677
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	ebx
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+548]
	add	esp,4
_677:
	mov	eax,ebx
	jmp	_340
_340:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetPanelExGradient:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	esi,dword [ebp+12]
	mov	ebx,dword [ebp+16]
	mov	edi,dword [ebp+20]
	push	_skn3_maxguiex_Skn3PanelEx
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_680
	push	dword [ebp+40]
	push	dword [ebp+32]
	push	dword [ebp+36]
	push	dword [ebp+28]
	push	edi
	push	dword [ebp+24]
	push	ebx
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+580]
	add	esp,36
_680:
	mov	eax,0
	jmp	_351
_351:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_353:
	dd	0
	align	4
__skn3_maxguiex_Skn3ListBatchLock_all:
	dd	_bbNullObject
_60:
	db	"Skn3ListBatchLock",0
_61:
	db	"refCount",0
_62:
	db	"i",0
_63:
	db	"listBox",0
_64:
	db	":maxgui.win32maxguiex.TWindowsListBox",0
_65:
	db	"index",0
_66:
	db	"link",0
_67:
	db	":brl.linkedlist.TLink",0
_68:
	db	"it",0
_69:
	db	":pub.win32.LVITEMW",0
_70:
	db	"hwnd",0
_71:
	db	"New",0
_72:
	db	"()i",0
_73:
	db	"Find",0
_74:
	db	"(:maxgui.maxgui.TGadget):Skn3ListBatchLock",0
_75:
	db	"add",0
_76:
	db	"(:Skn3ListBatchLock)i",0
_77:
	db	"remove",0
	align	4
_59:
	dd	2
	dd	_60
	dd	3
	dd	_61
	dd	_62
	dd	8
	dd	3
	dd	_63
	dd	_64
	dd	12
	dd	3
	dd	_65
	dd	_62
	dd	16
	dd	3
	dd	_66
	dd	_67
	dd	20
	dd	3
	dd	_68
	dd	_69
	dd	24
	dd	3
	dd	_70
	dd	_62
	dd	28
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	7
	dd	_73
	dd	_74
	dd	48
	dd	7
	dd	_75
	dd	_76
	dd	52
	dd	7
	dd	_77
	dd	_76
	dd	56
	dd	0
	align	4
_16:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_59
	dd	32
	dd	__skn3_maxguiex_Skn3ListBatchLock_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__skn3_maxguiex_Skn3ListBatchLock_Find
	dd	__skn3_maxguiex_Skn3ListBatchLock_add
	dd	__skn3_maxguiex_Skn3ListBatchLock_remove
	align	4
__skn3_maxguiex_Skn3CustomPointer_all:
	dd	_bbNullObject
_79:
	db	"Skn3CustomPointer",0
_80:
	db	"path",0
_81:
	db	"$",0
_82:
	db	"pointer",0
	align	4
_78:
	dd	2
	dd	_79
	dd	3
	dd	_80
	dd	_81
	dd	8
	dd	3
	dd	_82
	dd	_62
	dd	12
	dd	3
	dd	_61
	dd	_62
	dd	16
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	0
	align	4
_skn3_maxguiex_Skn3CustomPointer:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_78
	dd	20
	dd	__skn3_maxguiex_Skn3CustomPointer_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_84:
	db	"PARAFORMAT2",0
_85:
	db	"cbSize",0
_86:
	db	"dwMask",0
_87:
	db	"wNumbering",0
_88:
	db	"s",0
_89:
	db	"wEffects",0
_90:
	db	"dxStartIndent",0
_91:
	db	"dxRightIndent",0
_92:
	db	"dxOffset",0
_93:
	db	"wAlignment",0
_94:
	db	"cTabCount",0
_95:
	db	"rgxTabs00",0
_96:
	db	"rgxTabs01",0
_97:
	db	"rgxTabs02",0
_98:
	db	"rgxTabs03",0
_99:
	db	"rgxTabs10",0
_100:
	db	"rgxTabs11",0
_101:
	db	"rgxTabs12",0
_102:
	db	"rgxTabs13",0
_103:
	db	"rgxTabs20",0
_104:
	db	"rgxTabs21",0
_105:
	db	"rgxTabs22",0
_106:
	db	"rgxTabs23",0
_107:
	db	"rgxTabs30",0
_108:
	db	"rgxTabs31",0
_109:
	db	"rgxTabs32",0
_110:
	db	"rgxTabs33",0
_111:
	db	"rgxTabs40",0
_112:
	db	"rgxTabs41",0
_113:
	db	"rgxTabs42",0
_114:
	db	"rgxTabs43",0
_115:
	db	"rgxTabs50",0
_116:
	db	"rgxTabs51",0
_117:
	db	"rgxTabs52",0
_118:
	db	"rgxTabs53",0
_119:
	db	"rgxTabs60",0
_120:
	db	"rgxTabs61",0
_121:
	db	"rgxTabs62",0
_122:
	db	"rgxTabs63",0
_123:
	db	"rgxTabs70",0
_124:
	db	"rgxTabs71",0
_125:
	db	"rgxTabs72",0
_126:
	db	"rgxTabs73",0
_127:
	db	"dySpaceBefore",0
_128:
	db	"dySpaceAfter",0
_129:
	db	"dyLineSpacing",0
_130:
	db	"sStyle",0
_131:
	db	"bLineSpacingRule",0
_132:
	db	"b",0
_133:
	db	"bOutlineLevel",0
_134:
	db	"wShadingWeight",0
_135:
	db	"wShadingStyle",0
_136:
	db	"wNumberingStart",0
_137:
	db	"wNumberingStyle",0
_138:
	db	"wNumberingTab",0
_139:
	db	"wBorderSpace",0
_140:
	db	"wBorderWidth",0
_141:
	db	"wBorders",0
	align	4
_83:
	dd	2
	dd	_84
	dd	3
	dd	_85
	dd	_62
	dd	8
	dd	3
	dd	_86
	dd	_62
	dd	12
	dd	3
	dd	_87
	dd	_88
	dd	16
	dd	3
	dd	_89
	dd	_88
	dd	18
	dd	3
	dd	_90
	dd	_62
	dd	20
	dd	3
	dd	_91
	dd	_62
	dd	24
	dd	3
	dd	_92
	dd	_62
	dd	28
	dd	3
	dd	_93
	dd	_88
	dd	32
	dd	3
	dd	_94
	dd	_88
	dd	34
	dd	3
	dd	_95
	dd	_62
	dd	36
	dd	3
	dd	_96
	dd	_62
	dd	40
	dd	3
	dd	_97
	dd	_62
	dd	44
	dd	3
	dd	_98
	dd	_62
	dd	48
	dd	3
	dd	_99
	dd	_62
	dd	52
	dd	3
	dd	_100
	dd	_62
	dd	56
	dd	3
	dd	_101
	dd	_62
	dd	60
	dd	3
	dd	_102
	dd	_62
	dd	64
	dd	3
	dd	_103
	dd	_62
	dd	68
	dd	3
	dd	_104
	dd	_62
	dd	72
	dd	3
	dd	_105
	dd	_62
	dd	76
	dd	3
	dd	_106
	dd	_62
	dd	80
	dd	3
	dd	_107
	dd	_62
	dd	84
	dd	3
	dd	_108
	dd	_62
	dd	88
	dd	3
	dd	_109
	dd	_62
	dd	92
	dd	3
	dd	_110
	dd	_62
	dd	96
	dd	3
	dd	_111
	dd	_62
	dd	100
	dd	3
	dd	_112
	dd	_62
	dd	104
	dd	3
	dd	_113
	dd	_62
	dd	108
	dd	3
	dd	_114
	dd	_62
	dd	112
	dd	3
	dd	_115
	dd	_62
	dd	116
	dd	3
	dd	_116
	dd	_62
	dd	120
	dd	3
	dd	_117
	dd	_62
	dd	124
	dd	3
	dd	_118
	dd	_62
	dd	128
	dd	3
	dd	_119
	dd	_62
	dd	132
	dd	3
	dd	_120
	dd	_62
	dd	136
	dd	3
	dd	_121
	dd	_62
	dd	140
	dd	3
	dd	_122
	dd	_62
	dd	144
	dd	3
	dd	_123
	dd	_62
	dd	148
	dd	3
	dd	_124
	dd	_62
	dd	152
	dd	3
	dd	_125
	dd	_62
	dd	156
	dd	3
	dd	_126
	dd	_62
	dd	160
	dd	3
	dd	_127
	dd	_62
	dd	164
	dd	3
	dd	_128
	dd	_62
	dd	168
	dd	3
	dd	_129
	dd	_62
	dd	172
	dd	3
	dd	_130
	dd	_88
	dd	176
	dd	3
	dd	_131
	dd	_132
	dd	178
	dd	3
	dd	_133
	dd	_132
	dd	179
	dd	3
	dd	_134
	dd	_88
	dd	180
	dd	3
	dd	_135
	dd	_88
	dd	182
	dd	3
	dd	_136
	dd	_88
	dd	184
	dd	3
	dd	_137
	dd	_88
	dd	186
	dd	3
	dd	_138
	dd	_88
	dd	188
	dd	3
	dd	_139
	dd	_88
	dd	190
	dd	3
	dd	_140
	dd	_88
	dd	192
	dd	3
	dd	_141
	dd	_88
	dd	194
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	0
	align	4
_20:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_83
	dd	196
	dd	__skn3_maxguiex_PARAFORMAT2_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_143:
	db	"Skn3PanelEx",0
_144:
	db	"gradientOn",0
_145:
	db	"gradientVertical",0
_146:
	db	"gradientStartR",0
_147:
	db	"gradientStartG",0
_148:
	db	"gradientStartB",0
_149:
	db	"gradientEndR",0
_150:
	db	"gradientEndG",0
_151:
	db	"gradientEndB",0
_152:
	db	"WndProc",0
_153:
	db	"(i,i,i,i)i",0
_154:
	db	"SetGradient",0
_155:
	db	"(i,i,i,i,i,i,i,i)i",0
	align	4
_142:
	dd	2
	dd	_143
	dd	3
	dd	_144
	dd	_62
	dd	272
	dd	3
	dd	_145
	dd	_62
	dd	276
	dd	3
	dd	_146
	dd	_62
	dd	280
	dd	3
	dd	_147
	dd	_62
	dd	284
	dd	3
	dd	_148
	dd	_62
	dd	288
	dd	3
	dd	_149
	dd	_62
	dd	292
	dd	3
	dd	_150
	dd	_62
	dd	296
	dd	3
	dd	_151
	dd	_62
	dd	300
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_152
	dd	_153
	dd	520
	dd	6
	dd	_154
	dd	_155
	dd	580
	dd	0
	align	4
_skn3_maxguiex_Skn3PanelEx:
	dd	_maxgui_win32maxguiex_TWindowsPanel
	dd	_bbObjectFree
	dd	_142
	dd	304
	dd	__skn3_maxguiex_Skn3PanelEx_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__maxgui_maxgui_TGadget_SetFilter
	dd	__maxgui_maxgui_TGadget_HasDescendant
	dd	__maxgui_maxgui_TGadget__setparent
	dd	__maxgui_maxgui_TGadget_SelectionChanged
	dd	__maxgui_maxgui_TGadget_Handle
	dd	__maxgui_maxgui_TGadget_GetXPos
	dd	__maxgui_maxgui_TGadget_GetYPos
	dd	__maxgui_maxgui_TGadget_GetWidth
	dd	__maxgui_maxgui_TGadget_GetHeight
	dd	__maxgui_maxgui_TGadget_GetGroup
	dd	__maxgui_maxgui_TGadget_SetShape
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	dd	__maxgui_maxgui_TGadget_SetRect
	dd	__maxgui_maxgui_TGadget_LockLayout
	dd	__maxgui_maxgui_TGadget_SetLayout
	dd	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	dd	__maxgui_maxgui_TGadget_DoLayout
	dd	__maxgui_maxgui_TGadget_SetDataSource
	dd	__maxgui_maxgui_TGadget_KeysFromList
	dd	__maxgui_maxgui_TGadget_KeysFromObjectArray
	dd	__maxgui_maxgui_TGadget_SyncDataSource
	dd	__maxgui_maxgui_TGadget_SyncData
	dd	__maxgui_maxgui_TGadget_InsertItemFromKey
	dd	__maxgui_maxgui_TGadget_Clear
	dd	__maxgui_maxgui_TGadget_InsertItem
	dd	__maxgui_maxgui_TGadget_SetItem
	dd	__maxgui_maxgui_TGadget_RemoveItem
	dd	__maxgui_maxgui_TGadget_ItemCount
	dd	__maxgui_maxgui_TGadget_ItemText
	dd	__maxgui_maxgui_TGadget_ItemTip
	dd	__maxgui_maxgui_TGadget_ItemFlags
	dd	__maxgui_maxgui_TGadget_ItemIcon
	dd	__maxgui_maxgui_TGadget_ItemExtra
	dd	__maxgui_maxgui_TGadget_SetItemState
	dd	__maxgui_maxgui_TGadget_ItemState
	dd	__maxgui_maxgui_TGadget_SelectItem
	dd	__maxgui_maxgui_TGadget_SelectedItem
	dd	__maxgui_maxgui_TGadget_SelectedItems
	dd	__maxgui_maxgui_TGadget_Insert
	dd	__maxgui_win32maxguiex_TWindowsGadget_Query
	dd	__maxgui_maxgui_TGadget_CleanUp
	dd	__maxgui_win32maxguiex_TWindowsPanel_Free
	dd	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	dd	__maxgui_win32maxguiex_TWindowsPanel_Activate
	dd	__maxgui_win32maxguiex_TWindowsGadget_State
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	dd	__maxgui_maxgui_TGadget_SetIconStrip
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	dd	__maxgui_maxgui_TGadget_SetSelected
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	dd	__maxgui_maxgui_TGadget_SetSensitivity
	dd	__maxgui_maxgui_TGadget_GetSensitivity
	dd	__maxgui_maxgui_TGadget_SetWatch
	dd	__maxgui_maxgui_TGadget_GetWatch
	dd	__maxgui_win32maxguiex_TWindowsPanel_Class
	dd	__maxgui_maxgui_TGadget_GetStatusText
	dd	__maxgui_maxgui_TGadget_SetStatusText
	dd	__maxgui_maxgui_TGadget_GetMenu
	dd	__maxgui_maxgui_TGadget_PopupMenu
	dd	__maxgui_maxgui_TGadget_UpdateMenu
	dd	__maxgui_maxgui_TGadget_SetMinimumSize
	dd	__maxgui_maxgui_TGadget_SetMaximumSize
	dd	__maxgui_maxgui_TGadget_ClearListItems
	dd	__maxgui_maxgui_TGadget_InsertListItem
	dd	__maxgui_maxgui_TGadget_SetListItem
	dd	__maxgui_maxgui_TGadget_RemoveListItem
	dd	__maxgui_maxgui_TGadget_SetListItemState
	dd	__maxgui_maxgui_TGadget_ListItemState
	dd	__maxgui_maxgui_TGadget_RootNode
	dd	__maxgui_maxgui_TGadget_InsertNode
	dd	__maxgui_maxgui_TGadget_ModifyNode
	dd	__maxgui_maxgui_TGadget_SelectedNode
	dd	__maxgui_maxgui_TGadget_CountKids
	dd	__maxgui_maxgui_TGadget_ReplaceText
	dd	__maxgui_maxgui_TGadget_AddText
	dd	__maxgui_maxgui_TGadget_AreaText
	dd	__maxgui_maxgui_TGadget_AreaLen
	dd	__maxgui_maxgui_TGadget_LockText
	dd	__maxgui_maxgui_TGadget_UnlockText
	dd	__maxgui_maxgui_TGadget_SetTabs
	dd	__maxgui_maxgui_TGadget_GetCursorPos
	dd	__maxgui_maxgui_TGadget_GetSelectionLength
	dd	__maxgui_maxgui_TGadget_SetStyle
	dd	__maxgui_maxgui_TGadget_SetSelection
	dd	__maxgui_maxgui_TGadget_CharX
	dd	__maxgui_maxgui_TGadget_CharY
	dd	__maxgui_maxgui_TGadget_CharAt
	dd	__maxgui_maxgui_TGadget_LineAt
	dd	__maxgui_maxgui_TGadget_SetValue
	dd	__maxgui_maxgui_TGadget_SetRange
	dd	__maxgui_maxgui_TGadget_SetStep
	dd	__maxgui_maxgui_TGadget_SetProp
	dd	__maxgui_maxgui_TGadget_GetProp
	dd	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	dd	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	dd	__maxgui_maxgui_TGadget_Run
	dd	__maxgui_win32maxguiex_TWindowsPanel_Create
	dd	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	dd	__maxgui_win32maxguiex_TWindowsGadget_Register
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	dd	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	dd	__maxgui_win32maxguiex_TWindowsGadget_isControl
	dd	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	dd	__skn3_maxguiex_Skn3PanelEx_WndProc
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	dd	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	dd	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	dd	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	dd	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	dd	__skn3_maxguiex_Skn3PanelEx_SetGradient
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_21:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_23:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	92
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	105,110,99,98,105,110,58,58
_406:
	db	"i",0
_464:
	db	"i",0
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	108,105,116,116,108,101,101,110,100,105,97,110,58,58
_489:
	db	":TGadgetItem",0
	align	4
_847:
	dd	0x41a00000
	align	4
_543:
	dd	0
_559:
	db	"i",0
_561:
	db	"i",0
_563:
	db	"i",0
_565:
	db	"i",0
	align	4
_878:
	dd	0x437f0000
