	format	MS COFF
	extrn	_AddFontMemResourceEx@16
	extrn	_AddFontResourceExW@12
	extrn	_AlphaBlend@44
	extrn	_BitBlt@36
	extrn	_ClientToScreen@8
	extrn	_CreateCompatibleBitmap@12
	extrn	_CreateCompatibleDC@4
	extrn	_CreateSolidBrush@4
	extrn	_DeleteDC@4
	extrn	_DeleteObject@4
	extrn	_DestroyCursor@4
	extrn	_DrawTextW@20
	extrn	_FillRect@12
	extrn	_GetClientRect@8
	extrn	_GetClipBox@8
	extrn	_GetDC@4
	extrn	_GetUpdateRect@12
	extrn	_GetWindowLongW@8
	extrn	_GetWindowRect@8
	extrn	_IsRectEmpty@4
	extrn	_LoadCursorFromFileW@4
	extrn	_RedrawWindow@16
	extrn	_ReleaseDC@8
	extrn	_SelectObject@8
	extrn	_SendMessageA@16
	extrn	_SendMessageW@16
	extrn	_SetCursor@4
	extrn	_SetFocus@4
	extrn	_SetStretchBltMode@8
	extrn	_SetWindowLongW@12
	extrn	_SetWindowPos@28
	extrn	_StretchBlt@44
	extrn	___bb_blitz_blitz
	extrn	___bb_drivers_drivers
	extrn	___bb_linkedlist_linkedlist
	extrn	___bb_map_map
	extrn	___bb_systemex_systemex
	extrn	__maxgui_maxgui_TGadget_AddText
	extrn	__maxgui_maxgui_TGadget_AreaLen
	extrn	__maxgui_maxgui_TGadget_AreaText
	extrn	__maxgui_maxgui_TGadget_CharAt
	extrn	__maxgui_maxgui_TGadget_CharX
	extrn	__maxgui_maxgui_TGadget_CharY
	extrn	__maxgui_maxgui_TGadget_CleanUp
	extrn	__maxgui_maxgui_TGadget_Clear
	extrn	__maxgui_maxgui_TGadget_ClearListItems
	extrn	__maxgui_maxgui_TGadget_CountKids
	extrn	__maxgui_maxgui_TGadget_DoLayout
	extrn	__maxgui_maxgui_TGadget_GetCursorPos
	extrn	__maxgui_maxgui_TGadget_GetGroup
	extrn	__maxgui_maxgui_TGadget_GetHeight
	extrn	__maxgui_maxgui_TGadget_GetMenu
	extrn	__maxgui_maxgui_TGadget_GetProp
	extrn	__maxgui_maxgui_TGadget_GetSelectionLength
	extrn	__maxgui_maxgui_TGadget_GetSensitivity
	extrn	__maxgui_maxgui_TGadget_GetStatusText
	extrn	__maxgui_maxgui_TGadget_GetWatch
	extrn	__maxgui_maxgui_TGadget_GetWidth
	extrn	__maxgui_maxgui_TGadget_GetXPos
	extrn	__maxgui_maxgui_TGadget_GetYPos
	extrn	__maxgui_maxgui_TGadget_Handle
	extrn	__maxgui_maxgui_TGadget_HasDescendant
	extrn	__maxgui_maxgui_TGadget_Insert
	extrn	__maxgui_maxgui_TGadget_InsertItem
	extrn	__maxgui_maxgui_TGadget_InsertItemFromKey
	extrn	__maxgui_maxgui_TGadget_InsertListItem
	extrn	__maxgui_maxgui_TGadget_InsertNode
	extrn	__maxgui_maxgui_TGadget_ItemCount
	extrn	__maxgui_maxgui_TGadget_ItemExtra
	extrn	__maxgui_maxgui_TGadget_ItemFlags
	extrn	__maxgui_maxgui_TGadget_ItemIcon
	extrn	__maxgui_maxgui_TGadget_ItemState
	extrn	__maxgui_maxgui_TGadget_ItemText
	extrn	__maxgui_maxgui_TGadget_ItemTip
	extrn	__maxgui_maxgui_TGadget_KeysFromList
	extrn	__maxgui_maxgui_TGadget_KeysFromObjectArray
	extrn	__maxgui_maxgui_TGadget_LineAt
	extrn	__maxgui_maxgui_TGadget_ListItemState
	extrn	__maxgui_maxgui_TGadget_LockLayout
	extrn	__maxgui_maxgui_TGadget_LockText
	extrn	__maxgui_maxgui_TGadget_ModifyNode
	extrn	__maxgui_maxgui_TGadget_PopupMenu
	extrn	__maxgui_maxgui_TGadget_RemoveItem
	extrn	__maxgui_maxgui_TGadget_RemoveListItem
	extrn	__maxgui_maxgui_TGadget_ReplaceText
	extrn	__maxgui_maxgui_TGadget_RootNode
	extrn	__maxgui_maxgui_TGadget_Run
	extrn	__maxgui_maxgui_TGadget_SelectItem
	extrn	__maxgui_maxgui_TGadget_SelectedItem
	extrn	__maxgui_maxgui_TGadget_SelectedItems
	extrn	__maxgui_maxgui_TGadget_SelectedNode
	extrn	__maxgui_maxgui_TGadget_SelectionChanged
	extrn	__maxgui_maxgui_TGadget_SetDataSource
	extrn	__maxgui_maxgui_TGadget_SetFilter
	extrn	__maxgui_maxgui_TGadget_SetIconStrip
	extrn	__maxgui_maxgui_TGadget_SetItem
	extrn	__maxgui_maxgui_TGadget_SetItemState
	extrn	__maxgui_maxgui_TGadget_SetLayout
	extrn	__maxgui_maxgui_TGadget_SetListItem
	extrn	__maxgui_maxgui_TGadget_SetListItemState
	extrn	__maxgui_maxgui_TGadget_SetMaximumSize
	extrn	__maxgui_maxgui_TGadget_SetMinimumSize
	extrn	__maxgui_maxgui_TGadget_SetProp
	extrn	__maxgui_maxgui_TGadget_SetRange
	extrn	__maxgui_maxgui_TGadget_SetRect
	extrn	__maxgui_maxgui_TGadget_SetSelected
	extrn	__maxgui_maxgui_TGadget_SetSelection
	extrn	__maxgui_maxgui_TGadget_SetSensitivity
	extrn	__maxgui_maxgui_TGadget_SetShape
	extrn	__maxgui_maxgui_TGadget_SetStatusText
	extrn	__maxgui_maxgui_TGadget_SetStep
	extrn	__maxgui_maxgui_TGadget_SetStyle
	extrn	__maxgui_maxgui_TGadget_SetTabs
	extrn	__maxgui_maxgui_TGadget_SetValue
	extrn	__maxgui_maxgui_TGadget_SetWatch
	extrn	__maxgui_maxgui_TGadget_SyncData
	extrn	__maxgui_maxgui_TGadget_SyncDataSource
	extrn	__maxgui_maxgui_TGadget_UnlockText
	extrn	__maxgui_maxgui_TGadget_UpdateMenu
	extrn	__maxgui_maxgui_TGadget__setparent
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__cursor
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	extrn	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	extrn	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Query
	extrn	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Register
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	extrn	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_State
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isControl
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Activate
	extrn	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Class
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Create
	extrn	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Free
	extrn	__maxgui_win32maxguiex_TWindowsPanel_New
	extrn	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	extrn	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	extrn	__maxgui_win32maxguiex_TWindowsTextArea__oldCursor
	extrn	_bbAppTitle
	extrn	_bbArrayNew1D
	extrn	_bbArraySlice
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbFloatToInt
	extrn	_bbIncbinLen
	extrn	_bbIncbinPtr
	extrn	_bbIntAbs
	extrn	_bbMemFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectDtor
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringAsc
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_bbStringReplace
	extrn	_bbStringSlice
	extrn	_bbStringToInt
	extrn	_bbStringToLower
	extrn	_bbStringToWString
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_blitz_RuntimeError
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_DeleteFile
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_ReadFile
	extrn	_brl_filesystem_StripDir
	extrn	_brl_linkedlist_CreateList
	extrn	_brl_map_CreateMap
	extrn	_brl_stream_CloseStream
	extrn	_brl_stream_CopyStream
	extrn	_brl_stream_ReadStream
	extrn	_brl_stream_WriteStream
	extrn	_brl_system_Notify
	extrn	_maxgui_localization_LocalizationMode
	extrn	_maxgui_maxgui_AddGadgetItem
	extrn	_maxgui_maxgui_GadgetClass
	extrn	_maxgui_maxgui_GadgetGroup
	extrn	_maxgui_maxgui_GadgetHeight
	extrn	_maxgui_maxgui_GadgetHidden
	extrn	_maxgui_maxgui_GadgetText
	extrn	_maxgui_maxgui_GadgetWidth
	extrn	_maxgui_maxgui_LocalizeGadget
	extrn	_maxgui_maxgui_QueryGadget
	extrn	_maxgui_maxgui_RedrawGadget
	extrn	_maxgui_maxgui_SetGadgetText
	extrn	_maxgui_maxgui_SetPointer
	extrn	_maxgui_maxgui_TGadgetItem
	extrn	_maxgui_maxgui_TProxyGadget
	extrn	_maxgui_maxgui_lastPointer
	extrn	_maxgui_win32maxguiex_TWindowsGadget
	extrn	_maxgui_win32maxguiex_TWindowsListBox
	extrn	_maxgui_win32maxguiex_TWindowsPanel
	extrn	_pub_win32_LVITEMW
	extrn	_skn3_systemex_GetTempDirectory
	public	___bb_maxguiex_maxguiex
	public	__skn3_maxguiex_PARAFORMAT2_New
	public	__skn3_maxguiex_Skn3CustomPointer_New
	public	__skn3_maxguiex_Skn3CustomPointer_all
	public	__skn3_maxguiex_Skn3ListBatchLock_Find
	public	__skn3_maxguiex_Skn3ListBatchLock_New
	public	__skn3_maxguiex_Skn3ListBatchLock_add
	public	__skn3_maxguiex_Skn3ListBatchLock_all
	public	__skn3_maxguiex_Skn3ListBatchLock_remove
	public	__skn3_maxguiex_Skn3PanelEx_New
	public	__skn3_maxguiex_Skn3PanelEx_SetGradient
	public	__skn3_maxguiex_Skn3PanelEx_WndProc
	public	_skn3_maxguiex_AssignGadgetClassId
	public	_skn3_maxguiex_BringWindowToTop
	public	_skn3_maxguiex_ClearColorPickerCustomColors
	public	_skn3_maxguiex_CreatePanelEx
	public	_skn3_maxguiex_DisableGadgetRedraw
	public	_skn3_maxguiex_EnableGadgetRedraw
	public	_skn3_maxguiex_ExtractCursorHotspot
	public	_skn3_maxguiex_FocusWindow
	public	_skn3_maxguiex_FreeCustomPointer
	public	_skn3_maxguiex_GadgetScreenPosition
	public	_skn3_maxguiex_GadgetSizeForString
	public	_skn3_maxguiex_GadgetToInt
	public	_skn3_maxguiex_GadgetWindow
	public	_skn3_maxguiex_GetAppResourcesPath
	public	_skn3_maxguiex_GetCreationGroup
	public	_skn3_maxguiex_GetGadgetMaxLength
	public	_skn3_maxguiex_HideGadgetBorder
	public	_skn3_maxguiex_InstallGuiFont
	public	_skn3_maxguiex_ListBatchAdd
	public	_skn3_maxguiex_ListBatchLock
	public	_skn3_maxguiex_ListBatchUnlock
	public	_skn3_maxguiex_LoadCustomPointer
	public	_skn3_maxguiex_MessageBox
	public	_skn3_maxguiex_PointOverGadget
	public	_skn3_maxguiex_RedrawGadgetFrame
	public	_skn3_maxguiex_RequestScrollbarSize
	public	_skn3_maxguiex_ScrollTextAreaToBottom
	public	_skn3_maxguiex_ScrollTextAreaToCursor
	public	_skn3_maxguiex_ScrollTextAreaToTop
	public	_skn3_maxguiex_SetColorPickerCustomColors
	public	_skn3_maxguiex_SetComboBoxHeight
	public	_skn3_maxguiex_SetCustomPointer
	public	_skn3_maxguiex_SetGadgetMaxLength
	public	_skn3_maxguiex_SetGadgetReadOnly
	public	_skn3_maxguiex_SetPanelExGradient
	public	_skn3_maxguiex_SetTextareaLineSpacing
	public	_skn3_maxguiex_SetWindowAlwaysOnTop
	public	_skn3_maxguiex_Skn3CustomPointer
	public	_skn3_maxguiex_Skn3PanelEx
	section	"code" code
___bb_maxguiex_maxguiex:
	push	ebp
	mov	ebp,esp
	push	ebx
	cmp	dword [_364],0
	je	_365
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_365:
	mov	dword [_364],1
	push	ebp
	push	_356
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_map_map
	call	___bb_linkedlist_linkedlist
	call	___bb_drivers_drivers
	call	___bb_systemex_systemex
	push	_353
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_16
	call	_bbObjectRegisterType
	add	esp,4
	push	_355
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectRegisterType
	add	esp,4
	push	_20
	call	_bbObjectRegisterType
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectRegisterType
	add	esp,4
	mov	ebx,0
	jmp	_156
_156:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_367
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_16
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],_bbNullObject
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],_bbNullObject
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],_bbNullObject
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	push	ebp
	push	_366
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_159
_159:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_Find:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	push	ebp
	push	_394
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_370
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_371
	push	ebp
	push	_373
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_372
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_bbNullObject
	call	dword [_bbOnDebugLeaveScope]
	jmp	_162
_371:
	push	_374
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	push	_376
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	cmp	ebx,_bbNullObject
	jne	_378
	call	_brl_blitz_NullObjectError
_378:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+88]
	add	esp,4
	mov	dword [ebp-12],eax
	push	_380
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_17
_19:
	push	ebp
	push	_393
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_381
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_383
	call	_brl_blitz_NullObjectError
_383:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	push	_384
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_386
	call	_brl_blitz_NullObjectError
_386:
	mov	eax,dword [ebp-4]
	cmp	dword [ebx+12],eax
	jne	_387
	push	ebp
	push	_389
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_388
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_162
_387:
	push	_390
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_392
	call	_brl_blitz_NullObjectError
_392:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_17:
	cmp	dword [ebp-12],_bbNullObject
	jne	_19
_18:
	mov	ebx,_bbNullObject
	jmp	_162
_162:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_add:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_409
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_399
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_400
	push	ebp
	push	_402
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_401
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_linkedlist_CreateList
	mov	dword [__skn3_maxguiex_Skn3ListBatchLock_all],eax
	call	dword [_bbOnDebugLeaveScope]
_400:
	push	_403
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_405
	call	_brl_blitz_NullObjectError
_405:
	mov	esi,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	cmp	esi,_bbNullObject
	jne	_408
	call	_brl_blitz_NullObjectError
_408:
	push	dword [ebp-4]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebx+20],eax
	mov	ebx,0
	jmp	_165
_165:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_remove:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_427
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_411
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_412
	push	ebp
	push	_426
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_415
	call	_brl_blitz_NullObjectError
_415:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_417
	call	_brl_blitz_NullObjectError
_417:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	push	_418
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_420
	call	_brl_blitz_NullObjectError
_420:
	mov	dword [ebx+12],_bbNullObject
	push	_422
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_424
	call	_brl_blitz_NullObjectError
_424:
	mov	dword [ebx+24],_bbNullObject
	call	dword [_bbOnDebugLeaveScope]
_412:
	mov	ebx,0
	jmp	_168
_168:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3CustomPointer_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_429
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_skn3_maxguiex_Skn3CustomPointer
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],_bbEmptyString
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],0
	push	ebp
	push	_428
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_171
_171:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_PARAFORMAT2_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_432
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_20
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	eax,dword [ebp-4]
	mov	word [eax+16],0
	mov	eax,dword [ebp-4]
	mov	word [eax+18],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	mov	eax,dword [ebp-4]
	mov	word [eax+32],0
	mov	eax,dword [ebp-4]
	mov	word [eax+34],32
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+52],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+56],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+60],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+64],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+68],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+72],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+76],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+80],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+84],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+88],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+92],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+96],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+100],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+104],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+108],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+112],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+116],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+120],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+124],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+128],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+132],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+136],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+140],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+144],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+148],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+152],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+156],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+160],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+164],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+168],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+172],0
	mov	eax,dword [ebp-4]
	mov	word [eax+176],0
	mov	eax,dword [ebp-4]
	mov	byte [eax+178],0
	mov	eax,dword [ebp-4]
	mov	byte [eax+179],0
	mov	eax,dword [ebp-4]
	mov	word [eax+180],0
	mov	eax,dword [ebp-4]
	mov	word [eax+182],0
	mov	eax,dword [ebp-4]
	mov	word [eax+184],0
	mov	eax,dword [ebp-4]
	mov	word [eax+186],0
	mov	eax,dword [ebp-4]
	mov	word [eax+188],0
	mov	eax,dword [ebp-4]
	mov	word [eax+190],0
	mov	eax,dword [ebp-4]
	mov	word [eax+192],0
	mov	eax,dword [ebp-4]
	mov	word [eax+194],0
	push	ebp
	push	_431
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_174
_174:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_22:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	eax,ebp
	push	eax
	push	_499
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_434
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	cmp	dword [eax+8],0
	jne	_435
	mov	eax,ebp
	push	eax
	push	_437
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_436
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_179
_435:
	push	_438
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	_23
	push	dword [ebp-4]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-4],eax
	push	_439
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	dword [ebp-8]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_440
	mov	eax,ebp
	push	eax
	push	_442
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_441
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	_21
	push	dword [ebp-4]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_440:
	push	_443
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_445
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	push	_447
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],0
	push	_449
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bbStringAsc
	add	esp,4
	mov	dword [ebp-28],eax
	push	_451
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	mov	eax,dword [ebp-4]
	mov	edi,dword [eax+8]
	jmp	_452
_26:
	mov	eax,ebp
	push	eax
	push	_468
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_454
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_457
	call	_brl_blitz_ArrayBoundsError
_457:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_461
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_460
	call	_brl_blitz_ArrayBoundsError
_460:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,dword [ebp-28]
	setne	al
	movzx	eax,al
_461:
	cmp	eax,0
	je	_463
	mov	eax,ebp
	push	eax
	push	_465
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_464
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_25
_463:
	push	_466
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	add	eax,1
	mov	dword [ebp-20],eax
	push	_467
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],1
	call	dword [_bbOnDebugLeaveScope]
_24:
	add	dword [ebp-16],1
_452:
	cmp	dword [ebp-16],edi
	jl	_26
_25:
	push	_469
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	sub	eax,dword [ebp-20]
	mov	dword [ebp-32],eax
	push	_471
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	sub	eax,1
	mov	dword [ebp-16],eax
	jmp	_472
_29:
	mov	eax,ebp
	push	eax
	push	_486
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_473
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_476
	call	_brl_blitz_ArrayBoundsError
_476:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_480
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_479
	call	_brl_blitz_ArrayBoundsError
_479:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,dword [ebp-28]
	setne	al
	movzx	eax,al
_480:
	cmp	eax,0
	je	_482
	mov	eax,ebp
	push	eax
	push	_484
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_483
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_28
_482:
	push	_485
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [ebp-32],1
	call	dword [_bbOnDebugLeaveScope]
_27:
	add	dword [ebp-16],-1
_472:
	cmp	dword [ebp-16],0
	jge	_29
_28:
	push	_487
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],0
	jg	_488
	mov	eax,ebp
	push	eax
	push	_490
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_489
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_179
_488:
	push	_491
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	add	eax,dword [ebp-32]
	push	eax
	push	dword [ebp-20]
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	mov	dword [ebp-4],eax
	push	_492
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-24]
	cmp	eax,0
	je	_493
	mov	eax,dword [ebp-12]
_493:
	cmp	eax,0
	je	_495
	mov	eax,ebp
	push	eax
	push	_497
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_496
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_495:
	push	_498
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_179
_179:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_30:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyString
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],_bbEmptyString
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-28],_bbNullObject
	push	ebp
	push	_546
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_507
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_31
	push	8
	push	0
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_508
	push	ebp
	push	_537
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_509
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	1
	push	_21
	push	0
	call	_skn3_systemex_GetTempDirectory
	add	esp,4
	push	eax
	call	_22
	add	esp,12
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	push	_511
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_1
	push	_513
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	push	8
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_brl_filesystem_StripDir
	add	esp,4
	mov	dword [ebp-16],eax
	push	_515
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_32
_34:
	push	ebp
	push	_517
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_516
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bbStringToInt
	add	esp,4
	add	eax,1
	push	eax
	call	_bbStringFromInt
	add	esp,4
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
_32:
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,0
	jne	_34
_33:
	push	_518
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-20],eax
	push	_520
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadStream
	add	esp,4
	mov	dword [ebp-24],eax
	push	_522
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-24],_bbNullObject
	jne	_523
	push	ebp
	push	_525
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_524
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_182
_523:
	push	_526
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_stream_WriteStream
	add	esp,4
	mov	dword [ebp-28],eax
	push	_528
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],_bbNullObject
	jne	_529
	push	ebp
	push	_532
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_530
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_brl_stream_CloseStream
	add	esp,4
	push	_531
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_182
_529:
	push	_533
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4096
	push	dword [ebp-28]
	push	dword [ebp-24]
	call	_brl_stream_CopyStream
	add	esp,12
	push	_534
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	_535
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_brl_stream_CloseStream
	add	esp,4
	push	_536
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_182
_508:
	push	_545
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	jmp	_182
_182:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_35:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	fld	dword [ebp+8]
	fstp	dword [ebp-4]
	fld	dword [ebp+12]
	fstp	dword [ebp-8]
	fld	dword [ebp+16]
	fstp	dword [ebp-12]
	fld	dword [ebp+20]
	fstp	dword [ebp-16]
	fld	dword [ebp+24]
	fstp	dword [ebp-20]
	fld	dword [ebp+28]
	fstp	dword [ebp-24]
	push	ebp
	push	_555
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_548
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-4]
	fld	dword [ebp-12]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	je	_549
	fld	dword [ebp-4]
	fld	dword [ebp-12]
	fadd	dword [ebp-20]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_549:
	cmp	eax,0
	je	_551
	fld	dword [ebp-8]
	fld	dword [ebp-16]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
_551:
	cmp	eax,0
	je	_553
	fld	dword [ebp-8]
	fld	dword [ebp-16]
	fadd	dword [ebp-24]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_553:
	mov	ebx,eax
	jmp	_190
_190:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RequestScrollbarSize:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_565
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_564
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,18
	jmp	_192
_192:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetComboBoxHeight:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_569
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_567
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	sub	eax,6
	push	eax
	push	-1
	push	339
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageA@16
	push	_568
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	mov	ebx,0
	jmp	_196
_196:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetScreenPosition:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-16],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-4],0
	mov	dword [ebp-20],_bbEmptyArray
	push	ebp
	push	_598
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_573
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	mov	dword [ebp-4],0
	push	_575
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],0
	je	_576
	push	ebp
	push	_580
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_577
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_579
	call	_brl_blitz_NullObjectError
_579:
	lea	eax,dword [ebp-8]
	push	eax
	push	2
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_581
_576:
	push	ebp
	push	_585
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_582
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_584
	call	_brl_blitz_NullObjectError
_584:
	lea	eax,dword [ebp-8]
	push	eax
	push	1
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
	call	dword [_bbOnDebugLeaveScope]
_581:
	push	_586
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_587
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-20],eax
	push	_589
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-20]
	cmp	ebx,dword [eax+20]
	jb	_591
	call	_brl_blitz_ArrayBoundsError
_591:
	mov	eax,dword [ebp-20]
	shl	ebx,2
	add	eax,ebx
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx]
	mov	dword [eax+24],edx
	push	_593
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-20]
	cmp	ebx,dword [eax+20]
	jb	_595
	call	_brl_blitz_ArrayBoundsError
_595:
	mov	eax,dword [ebp-20]
	shl	ebx,2
	add	eax,ebx
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx+4]
	mov	dword [eax+24],edx
	push	_597
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	jmp	_200
_200:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_PointOverGadget:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	dword [ebp-24],_bbEmptyArray
	mov	dword [ebp-20],_bbEmptyArray
	push	ebp
	push	_631
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_606
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,_bbNullObject
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_607
	push	0
	push	dword [ebp-12]
	call	_maxgui_maxgui_GadgetHidden
	add	esp,8
_607:
	cmp	eax,0
	je	_609
	push	ebp
	push	_611
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_610
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_206
_609:
	push	_612
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],_bbNullObject
	je	_613
	push	ebp
	push	_622
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_614
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-16]
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	dword [ebp-24],eax
	push	_616
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-24]
	cmp	ebx,dword [eax+20]
	jb	_618
	call	_brl_blitz_ArrayBoundsError
_618:
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+ebx*4+24]
	add	eax,dword [ebp-4]
	mov	dword [ebp-4],eax
	push	_619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-24]
	cmp	ebx,dword [eax+20]
	jb	_621
	call	_brl_blitz_ArrayBoundsError
_621:
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+ebx*4+24]
	add	eax,dword [ebp-8]
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_613:
	push	_624
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-12]
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	dword [ebp-20],eax
	push	_626
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,0
	mov	eax,dword [ebp-20]
	cmp	esi,dword [eax+20]
	jb	_628
	call	_brl_blitz_ArrayBoundsError
_628:
	mov	ebx,1
	mov	eax,dword [ebp-20]
	cmp	ebx,dword [eax+20]
	jb	_630
	call	_brl_blitz_ArrayBoundsError
_630:
	push	dword [ebp-12]
	call	_maxgui_maxgui_GadgetHeight
	add	esp,4
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	push	dword [ebp-12]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-8]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-4]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	call	_35
	add	esp,24
	mov	ebx,eax
	jmp	_206
_206:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_DisableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_638
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_636
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	11
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	push	_637
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	jmp	_209
_209:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_EnableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_642
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_640
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	11
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	push	_641
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	jmp	_212
_212:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_MessageBox:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	dword [ebp-16],_bbEmptyString
	push	ebp
	push	_649
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_644
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bbAppTitle]
	mov	dword [ebp-16],eax
	push	_646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [_bbAppTitle],eax
	push	_647
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	call	_brl_system_Notify
	add	esp,8
	push	_648
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	mov	dword [_bbAppTitle],eax
	mov	ebx,0
	jmp	_217
_217:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetSizeForString:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],_bbEmptyArray
	mov	dword [ebp-32],0
	mov	dword [ebp-36],_bbEmptyArray
	mov	dword [ebp-40],_bbEmptyString
	push	ebp
	push	_704
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_655
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-16],eax
	push	_657
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,2
	je	_660
	push	ebp
	push	_685
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_661
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_GetDC@4
	mov	dword [ebp-20],eax
	push	_663
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	49
	push	dword [ebp-16]
	call	_SendMessageW@16
	mov	dword [ebp-24],eax
	push	_665
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	push	dword [ebp-20]
	call	_SelectObject@8
	push	_666
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],0
	mov	dword [eax+28],0
	mov	edx,dword [ebp-12]
	mov	dword [eax+32],edx
	mov	dword [eax+36],0
	mov	dword [ebp-28],eax
	push	_669
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],1280
	push	_671
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	jle	_672
	push	ebp
	push	_674
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_673
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	or	dword [ebp-32],16
	call	dword [_bbOnDebugLeaveScope]
_672:
	push	_675
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bbStringToWString
	add	esp,4
	mov	ebx,eax
	push	dword [ebp-32]
	mov	eax,dword [ebp-28]
	lea	eax,dword [eax+24]
	push	eax
	push	-1
	push	ebx
	push	dword [ebp-20]
	call	_DrawTextW@20
	push	ebx
	call	_bbMemFree
	add	esp,4
	push	_678
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-16]
	call	_ReleaseDC@8
	push	_679
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	ebx,eax
	mov	esi,2
	mov	eax,dword [ebp-28]
	cmp	esi,dword [eax+20]
	jb	_681
	call	_brl_blitz_ArrayBoundsError
_681:
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+24],eax
	mov	esi,3
	mov	eax,dword [ebp-28]
	cmp	esi,dword [eax+20]
	jb	_683
	call	_brl_blitz_ArrayBoundsError
_683:
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+28],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_222
_660:
	push	ebp
	push	_701
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_690
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edx,dword [ebp-12]
	mov	dword [eax+24],edx
	mov	dword [eax+28],0
	mov	dword [ebp-36],eax
	push	_693
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	11
	push	dword [ebp-16]
	call	_SendMessageW@16
	push	_694
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	dword [ebp-40],eax
	push	_696
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_697
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-36]
	lea	eax,byte [eax+24]
	push	eax
	push	0
	push	5633
	push	dword [ebp-16]
	call	_SendMessageW@16
	push	_698
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-40]
	push	dword [ebp-4]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_699
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	11
	push	dword [ebp-16]
	call	_SendMessageW@16
	push	_700
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-36]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_222
_222:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetCreationGroup:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_717
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_708
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_maxgui_TProxyGadget
	push	dword [ebp-4]
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	push	_710
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	je	_711
	push	ebp
	push	_715
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_712
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_714
	call	_brl_blitz_NullObjectError
_714:
	push	dword [ebx+140]
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_225
_711:
	push	_716
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_225
_225:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetReadOnly:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],0
	push	ebp
	push	_729
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_721
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_724
	cmp	eax,4
	je	_724
	jmp	_723
_724:
	push	ebp
	push	_728
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_725
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	_727
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	push	207
	push	dword [ebp-12]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
	jmp	_723
_723:
	mov	ebx,0
	jmp	_229
_229:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_742
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_732
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_735
	cmp	eax,5
	je	_735
	jmp	_734
_735:
	push	ebp
	push	_741
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_736
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	jge	_737
	push	ebp
	push	_739
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_738
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	call	dword [_bbOnDebugLeaveScope]
_737:
	push	_740
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	push	197
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
	jmp	_734
_734:
	mov	ebx,0
	jmp	_233
_233:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_752
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_744
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_747
	cmp	eax,5
	je	_747
	push	ebp
	push	_749
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_748
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_236
_747:
	push	ebp
	push	_751
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_750
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	213
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_236
_236:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_LoadCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],0
	mov	eax,ebp
	push	eax
	push	_807
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_754
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	push	_756
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_30
	add	esp,4
	mov	dword [ebp-12],eax
	push	_758
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_760
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	dword [eax+8],0
	jne	_761
	mov	eax,ebp
	push	eax
	push	_763
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_762
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_764
_761:
	mov	eax,ebp
	push	eax
	push	_766
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_765
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_764:
	push	_767
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__skn3_maxguiex_Skn3CustomPointer_all],_bbNullObject
	jne	_768
	mov	eax,ebp
	push	eax
	push	_770
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_769
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_map_CreateMap
	mov	dword [__skn3_maxguiex_Skn3CustomPointer_all],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_771
_768:
	mov	eax,ebp
	push	eax
	push	_775
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_772
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	cmp	ebx,_bbNullObject
	jne	_774
	call	_brl_blitz_NullObjectError
_774:
	push	_skn3_maxguiex_Skn3CustomPointer
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,8
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_771:
	push	_776
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	jne	_777
	mov	eax,ebp
	push	eax
	push	_792
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_778
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_779
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_781
	call	_brl_blitz_NullObjectError
_781:
	mov	eax,dword [ebp-4]
	mov	dword [ebx+8],eax
	push	_783
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	cmp	ebx,_bbNullObject
	jne	_785
	call	_brl_blitz_NullObjectError
_785:
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	push	_786
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_788
	call	_brl_blitz_NullObjectError
_788:
	mov	edi,ebx
	push	dword [ebp-12]
	call	_bbStringToWString
	add	esp,4
	mov	esi,eax
	push	esi
	call	_LoadCursorFromFileW@4
	mov	ebx,eax
	push	esi
	call	_bbMemFree
	add	esp,4
	mov	dword [edi+12],ebx
	call	dword [_bbOnDebugLeaveScope]
_777:
	push	_793
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],0
	je	_794
	mov	eax,ebp
	push	eax
	push	_796
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_795
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_brl_filesystem_DeleteFile
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_794:
	push	_797
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_799
	call	_brl_blitz_NullObjectError
_799:
	cmp	dword [ebx+12],0
	je	_800
	mov	eax,ebp
	push	eax
	push	_806
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_801
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_803
	call	_brl_blitz_NullObjectError
_803:
	add	dword [ebx+16],1
	push	_805
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_239
_800:
	mov	ebx,_bbNullObject
	jmp	_239
_239:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_826
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_810
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_811
	push	ebp
	push	_825
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_812
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_maxgui_maxgui_lastPointer],-1
	push	_813
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_815
	call	_brl_blitz_NullObjectError
_815:
	push	dword [ebx+12]
	call	_SetCursor@4
	push	_816
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_818
	call	_brl_blitz_NullObjectError
_818:
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	push	_819
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],0
	je	_820
	push	ebp
	push	_824
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_821
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_823
	call	_brl_blitz_NullObjectError
_823:
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],eax
	call	dword [_bbOnDebugLeaveScope]
_820:
	call	dword [_bbOnDebugLeaveScope]
_811:
	mov	ebx,0
	jmp	_242
_242:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FreeCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_858
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_828
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_829
	push	ebp
	push	_857
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_830
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_832
	call	_brl_blitz_NullObjectError
_832:
	sub	dword [ebx+16],1
	push	_834
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_836
	call	_brl_blitz_NullObjectError
_836:
	cmp	dword [ebx+16],0
	jne	_837
	push	ebp
	push	_856
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_838
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	cmp	esi,_bbNullObject
	jne	_840
	call	_brl_blitz_NullObjectError
_840:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_842
	call	_brl_blitz_NullObjectError
_842:
	push	dword [ebx+8]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+68]
	add	esp,8
	push	_843
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_845
	call	_brl_blitz_NullObjectError
_845:
	mov	eax,dword [ebx+12]
	cmp	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	jne	_846
	push	ebp
	push	_848
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_847
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	call	_maxgui_maxgui_SetPointer
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_846:
	push	_849
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_851
	call	_brl_blitz_NullObjectError
_851:
	push	dword [ebx+12]
	call	_DestroyCursor@4
	push	_852
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_854
	call	_brl_blitz_NullObjectError
_854:
	mov	dword [ebx+12],0
	call	dword [_bbOnDebugLeaveScope]
_837:
	call	dword [_bbOnDebugLeaveScope]
_829:
	mov	ebx,0
	jmp	_245
_245:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ExtractCursorHotspot:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbEmptyArray
	mov	dword [ebp-16],_bbNullObject
	mov	dword [ebp-20],0
	push	ebp
	push	_910
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_860
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_861
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-12],eax
	push	_863
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	_36
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_ReadFile
	add	esp,4
	mov	dword [ebp-16],eax
	push	_865
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],_bbNullObject
	je	_866
	push	ebp
	push	_907
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_867
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_869
	call	_brl_blitz_NullObjectError
_869:
	push	2
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,8
	push	_870
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_872
	call	_brl_blitz_NullObjectError
_872:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebp-20],eax
	push	_874
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],2
	jne	_875
	push	ebp
	push	_903
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_876
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_878
	call	_brl_blitz_NullObjectError
_878:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebp-20],eax
	push	_879
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	cmp	dword [ebp-8],eax
	jge	_880
	push	ebp
	push	_902
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_881
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	imul	eax,12
	add	eax,6
	add	eax,4
	mov	dword [ebp-20],eax
	push	_882
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_884
	call	_brl_blitz_NullObjectError
_884:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	dword [ebp-20],eax
	jge	_885
	push	ebp
	push	_901
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_886
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_888
	call	_brl_blitz_NullObjectError
_888:
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,8
	push	_889
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,0
	mov	eax,dword [ebp-12]
	cmp	esi,dword [eax+20]
	jb	_891
	call	_brl_blitz_ArrayBoundsError
_891:
	mov	ebx,dword [ebp-12]
	shl	esi,2
	add	ebx,esi
	mov	esi,dword [ebp-16]
	cmp	esi,_bbNullObject
	jne	_894
	call	_brl_blitz_NullObjectError
_894:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebx+24],eax
	push	_895
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,1
	mov	eax,dword [ebp-12]
	cmp	esi,dword [eax+20]
	jb	_897
	call	_brl_blitz_ArrayBoundsError
_897:
	mov	ebx,dword [ebp-12]
	shl	esi,2
	add	ebx,esi
	mov	esi,dword [ebp-16]
	cmp	esi,_bbNullObject
	jne	_900
	call	_brl_blitz_NullObjectError
_900:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebx+24],eax
	call	dword [_bbOnDebugLeaveScope]
_885:
	call	dword [_bbOnDebugLeaveScope]
_880:
	call	dword [_bbOnDebugLeaveScope]
_875:
	push	_904
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_906
	call	_brl_blitz_NullObjectError
_906:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_866:
	push	_909
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	jmp	_249
_249:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchLock:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	push	ebp
	push	_957
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_914
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	dword [_16+48]
	add	esp,4
	mov	dword [ebp-8],eax
	push	_916
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	je	_917
	push	ebp
	push	_923
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_918
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_920
	call	_brl_blitz_NullObjectError
_920:
	add	dword [ebx+8],1
	push	_922
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_252
_917:
	push	_924
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsListBox
	push	dword [ebp-4]
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	push	_926
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],_bbNullObject
	jne	_927
	push	ebp
	push	_929
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_928
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_252
_927:
	push	_930
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_16
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_931
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_933
	call	_brl_blitz_NullObjectError
_933:
	mov	dword [ebx+8],1
	push	_935
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_937
	call	_brl_blitz_NullObjectError
_937:
	mov	eax,dword [ebp-12]
	mov	dword [ebx+12],eax
	push	_939
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_941
	call	_brl_blitz_NullObjectError
_941:
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_944
	call	_brl_blitz_NullObjectError
_944:
	mov	eax,dword [esi+124]
	mov	eax,dword [eax+20]
	mov	dword [ebx+16],eax
	push	_945
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_947
	call	_brl_blitz_NullObjectError
_947:
	push	_pub_win32_LVITEMW
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebx+24],eax
	push	_949
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_951
	call	_brl_blitz_NullObjectError
_951:
	push	1
	push	dword [ebp-12]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebx+28],eax
	push	_953
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_955
	call	_brl_blitz_NullObjectError
_955:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+552]
	add	esp,4
	push	_956
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	dword [_16+52]
	add	esp,4
	mov	ebx,0
	jmp	_252
_252:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchAdd:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],_bbNullObject
	mov	dword [ebp-32],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1040
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_959
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	dword [_16+48]
	add	esp,4
	mov	dword [ebp-28],eax
	push	_961
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],_bbNullObject
	jne	_962
	mov	eax,ebp
	push	eax
	push	_965
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_963
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_maxgui_maxgui_AddGadgetItem
	add	esp,24
	push	_964
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_260
_962:
	push	_966
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_maxgui_TGadgetItem
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-32],eax
	push	_968
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_970
	call	_brl_blitz_NullObjectError
_970:
	push	dword [ebp-12]
	push	dword [ebp-24]
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	dword [ebp-8]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,24
	push	_971
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_973
	call	_brl_blitz_NullObjectError
_973:
	mov	edi,ebx
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_976
	call	_brl_blitz_NullObjectError
_976:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_978
	call	_brl_blitz_NullObjectError
_978:
	mov	eax,dword [ebx+16]
	add	eax,1
	push	eax
	push	0
	push	dword [esi+124]
	push	_979
	call	_bbArraySlice
	add	esp,16
	mov	dword [edi+124],eax
	push	_980
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_982
	call	_brl_blitz_NullObjectError
_982:
	mov	esi,dword [ebx+124]
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_985
	call	_brl_blitz_NullObjectError
_985:
	mov	ebx,dword [ebx+16]
	cmp	ebx,dword [esi+20]
	jb	_987
	call	_brl_blitz_ArrayBoundsError
_987:
	shl	ebx,2
	add	esi,ebx
	mov	eax,dword [ebp-32]
	mov	dword [esi+24],eax
	push	_989
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_991
	call	_brl_blitz_NullObjectError
_991:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_993
	call	_brl_blitz_NullObjectError
_993:
	mov	dword [ebx+8],4097
	push	_995
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_997
	call	_brl_blitz_NullObjectError
_997:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_999
	call	_brl_blitz_NullObjectError
_999:
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_1002
	call	_brl_blitz_NullObjectError
_1002:
	mov	eax,dword [esi+16]
	mov	dword [ebx+12],eax
	push	_1003
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1005
	call	_brl_blitz_NullObjectError
_1005:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1007
	call	_brl_blitz_NullObjectError
_1007:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_1010
	call	_brl_blitz_NullObjectError
_1010:
	push	dword [esi+8]
	call	_bbStringToWString
	add	esp,4
	mov	dword [ebx+28],eax
	push	_1011
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1013
	call	_brl_blitz_NullObjectError
_1013:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1015
	call	_brl_blitz_NullObjectError
_1015:
	or	dword [ebx+8],2
	push	_1017
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1019
	call	_brl_blitz_NullObjectError
_1019:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1021
	call	_brl_blitz_NullObjectError
_1021:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_1024
	call	_brl_blitz_NullObjectError
_1024:
	mov	eax,dword [esi+16]
	mov	dword [ebx+36],eax
	push	_1025
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1027
	call	_brl_blitz_NullObjectError
_1027:
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_1029
	call	_brl_blitz_NullObjectError
_1029:
	mov	eax,dword [esi+24]
	mov	dword [ebp-36],eax
	mov	eax,dword [ebp-36]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	4173
	push	dword [ebx+28]
	call	_SendMessageW@16
	push	_1031
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1033
	call	_brl_blitz_NullObjectError
_1033:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1035
	call	_brl_blitz_NullObjectError
_1035:
	push	dword [ebx+28]
	call	_bbMemFree
	add	esp,4
	push	_1036
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1038
	call	_brl_blitz_NullObjectError
_1038:
	add	dword [ebx+16],1
	mov	ebx,0
	jmp	_260
_260:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchUnlock:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_1084
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1048
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	dword [_16+48]
	add	esp,4
	mov	dword [ebp-8],eax
	push	_1050
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	jne	_1051
	push	ebp
	push	_1053
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1052
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_263
_1051:
	push	_1054
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1056
	call	_brl_blitz_NullObjectError
_1056:
	sub	dword [ebx+8],1
	push	_1058
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1060
	call	_brl_blitz_NullObjectError
_1060:
	cmp	dword [ebx+8],0
	jne	_1061
	push	ebp
	push	_1083
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1062
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1064
	call	_brl_blitz_NullObjectError
_1064:
	push	-2
	push	0
	push	4126
	push	dword [ebx+28]
	call	_SendMessageW@16
	push	_1065
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1067
	call	_brl_blitz_NullObjectError
_1067:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_1069
	call	_brl_blitz_NullObjectError
_1069:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+580]
	add	esp,4
	cmp	eax,0
	jne	_1070
	push	ebp
	push	_1076
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1071
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1073
	call	_brl_blitz_NullObjectError
_1073:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_1075
	call	_brl_blitz_NullObjectError
_1075:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1070:
	push	_1077
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1079
	call	_brl_blitz_NullObjectError
_1079:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_1081
	call	_brl_blitz_NullObjectError
_1081:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+548]
	add	esp,4
	push	_1082
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	dword [_16+56]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1061:
	mov	ebx,0
	jmp	_263
_263:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetWindow:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_1095
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1086
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	dword [ebp-8],eax
	push	_1088
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_37
_39:
	push	ebp
	push	_1094
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1089
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,1
	jne	_1090
	push	ebp
	push	_1092
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1091
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_266
_1090:
	push	_1093
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_37:
	cmp	dword [ebp-8],_bbNullObject
	jne	_39
_38:
	mov	ebx,_bbNullObject
	jmp	_266
_266:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetWindowAlwaysOnTop:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],0
	push	ebp
	push	_1109
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1097
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	_1099
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	je	_1100
	push	ebp
	push	_1108
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1101
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1102
	push	ebp
	push	_1104
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1103
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-1
	push	dword [ebp-12]
	call	_SetWindowPos@28
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1105
_1102:
	push	ebp
	push	_1107
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1106
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-2
	push	dword [ebp-12]
	call	_SetWindowPos@28
	call	dword [_bbOnDebugLeaveScope]
_1105:
	call	dword [_bbOnDebugLeaveScope]
_1100:
	mov	ebx,0
	jmp	_270
_270:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_BringWindowToTop:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1119
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1113
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1115
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1116
	push	ebp
	push	_1118
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1117
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	19
	push	0
	push	0
	push	0
	push	0
	push	0
	push	dword [ebp-8]
	call	_SetWindowPos@28
	call	dword [_bbOnDebugLeaveScope]
_1116:
	mov	ebx,0
	jmp	_273
_273:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FocusWindow:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1127
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1121
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1123
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1124
	push	ebp
	push	_1126
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1125
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_SetFocus@4
	call	dword [_bbOnDebugLeaveScope]
_1124:
	mov	ebx,0
	jmp	_276
_276:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetToInt:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1130
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1129
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	ebx,eax
	jmp	_279
_279:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1146
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1132
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	cmp	dword [eax+20],16
	jge	_1133
	push	ebp
	push	_1144
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1134
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+20]
	mov	dword [ebp-8],eax
	push	_1136
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	16
	push	0
	push	dword [ebp-4]
	push	_62
	call	_bbArraySlice
	add	esp,16
	mov	dword [ebp-4],eax
	push	_1137
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_1138
_42:
	push	ebp
	push	_1143
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1139
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	mov	eax,dword [ebp-4]
	cmp	ebx,dword [eax+20]
	jb	_1141
	call	_brl_blitz_ArrayBoundsError
_1141:
	mov	eax,dword [ebp-4]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],16777215
	call	dword [_bbOnDebugLeaveScope]
_40:
	add	dword [ebp-8],1
_1138:
	cmp	dword [ebp-8],16
	jl	_42
_41:
	call	dword [_bbOnDebugLeaveScope]
_1133:
	push	_1145
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors],eax
	mov	ebx,0
	jmp	_282
_282:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ClearColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	dword [ebp-4],0
	push	ebp
	push	_1157
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1149
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-4],0
	mov	dword [ebp-4],0
	jmp	_1151
_45:
	push	ebp
	push	_1156
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1152
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	cmp	ebx,dword [eax+20]
	jb	_1154
	call	_brl_blitz_ArrayBoundsError
_1154:
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],16777215
	call	dword [_bbOnDebugLeaveScope]
_43:
	add	dword [ebp-4],1
_1151:
	cmp	dword [ebp-4],16
	jl	_45
_44:
	mov	ebx,0
	jmp	_284
_284:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RedrawGadgetFrame:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1166
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1159
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1161
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1162
	push	ebp
	push	_1165
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1163
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	55
	push	0
	push	0
	push	0
	push	0
	push	0
	push	dword [ebp-8]
	call	_SetWindowPos@28
	push	_1164
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1345
	push	0
	push	0
	push	dword [ebp-8]
	call	_RedrawWindow@16
	call	dword [_bbOnDebugLeaveScope]
_1162:
	mov	ebx,0
	jmp	_287
_287:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_HideGadgetBorder:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	push	ebp
	push	_1201
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1168
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_1171
	cmp	eax,4
	je	_1171
	cmp	eax,7
	je	_1171
	jmp	_1170
_1171:
	push	ebp
	push	_1200
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1172
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1174
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1175
	push	ebp
	push	_1196
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-16
	push	dword [ebp-8]
	call	_GetWindowLongW@8
	mov	dword [ebp-12],eax
	push	_1178
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-20
	push	dword [ebp-8]
	call	_GetWindowLongW@8
	mov	dword [ebp-16],eax
	push	_1180
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	push	_1182
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	and	eax,8388608
	cmp	eax,0
	je	_1183
	push	ebp
	push	_1186
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1184
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	and	eax,-8388609
	push	eax
	push	-16
	push	dword [ebp-8]
	call	_SetWindowLongW@12
	push	_1185
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	call	dword [_bbOnDebugLeaveScope]
_1183:
	push	_1187
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	and	eax,512
	cmp	eax,0
	je	_1188
	push	ebp
	push	_1191
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1189
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	and	eax,-513
	push	eax
	push	-20
	push	dword [ebp-8]
	call	_SetWindowLongW@12
	push	_1190
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	call	dword [_bbOnDebugLeaveScope]
_1188:
	push	_1192
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],0
	je	_1193
	push	ebp
	push	_1195
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1194
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_skn3_maxguiex_RedrawGadgetFrame
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1193:
	call	dword [_bbOnDebugLeaveScope]
_1175:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1170
_1170:
	mov	ebx,0
	jmp	_290
_290:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_InstallGuiFont:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1216
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1203
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_31
	push	8
	push	0
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1204
	push	ebp
	push	_1209
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1205
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	push	8
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	mov	dword [ebp-4],eax
	push	_1206
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	push	_1208
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	lea	eax,dword [ebp-8]
	push	eax
	push	0
	push	dword [ebp-4]
	call	_bbIncbinLen
	add	esp,4
	push	eax
	push	dword [ebp-4]
	call	_bbIncbinPtr
	add	esp,4
	push	eax
	call	_AddFontMemResourceEx@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_293
_1204:
	push	ebp
	push	_1215
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1212
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bbStringToWString
	add	esp,4
	mov	ebx,eax
	push	0
	push	16
	push	ebx
	call	_AddFontResourceExW@12
	mov	esi,eax
	push	ebx
	call	_bbMemFree
	add	esp,4
	cmp	esi,0
	setne	al
	movzx	eax,al
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_293
_293:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetTextareaLineSpacing:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	fld	dword [ebp+12]
	fstp	dword [ebp-8]
	mov	dword [ebp-12],0
	mov	dword [ebp-16],_bbNullObject
	push	ebp
	push	_1246
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1218
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1219
	push	ebp
	push	_1245
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1220
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	_1222
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	je	_1223
	push	ebp
	push	_1243
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1224
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_20
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1226
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1228
	call	_brl_blitz_NullObjectError
_1228:
	mov	dword [ebx+8],188
	push	_1230
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1232
	call	_brl_blitz_NullObjectError
_1232:
	mov	dword [ebx+12],256
	push	_1234
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1236
	call	_brl_blitz_NullObjectError
_1236:
	mov	byte [ebx+178],5
	push	_1238
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1240
	call	_brl_blitz_NullObjectError
_1240:
	fld	dword [ebp-8]
	fmul	dword [_2268]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+172],eax
	push	_1242
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	1095
	push	dword [ebp-12]
	call	_SendMessageW@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_297
_1223:
	call	dword [_bbOnDebugLeaveScope]
_1219:
	mov	ebx,0
	jmp	_297
_297:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToTop:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1258
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1249
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1250
	push	ebp
	push	_1257
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1251
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1253
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1254
	push	ebp
	push	_1256
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1255
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	6
	push	181
	push	dword [ebp-8]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
_1254:
	call	dword [_bbOnDebugLeaveScope]
_1250:
	mov	ebx,0
	jmp	_300
_300:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToBottom:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1269
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1260
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1261
	push	ebp
	push	_1268
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1262
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1264
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1265
	push	ebp
	push	_1267
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1266
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	7
	push	181
	push	dword [ebp-8]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
_1265:
	call	dword [_bbOnDebugLeaveScope]
_1261:
	mov	ebx,0
	jmp	_303
_303:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToCursor:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1280
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1271
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1272
	push	ebp
	push	_1279
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1273
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1275
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1276
	push	ebp
	push	_1278
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1277
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	183
	push	dword [ebp-8]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
_1276:
	call	dword [_bbOnDebugLeaveScope]
_1272:
	mov	ebx,0
	jmp	_306
_306:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetAppResourcesPath:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_1283
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1282
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_31
	jmp	_308
_308:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_AssignGadgetClassId:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_1289
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1285
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1287
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [_1286],1
	push	_1288
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_1286]
	jmp	_310
_310:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1293
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	__maxgui_win32maxguiex_TWindowsPanel_New
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_skn3_maxguiex_Skn3PanelEx
	mov	eax,dword [ebp-4]
	mov	dword [eax+272],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+276],1
	mov	eax,dword [ebp-4]
	mov	dword [eax+280],255
	mov	eax,dword [ebp-4]
	mov	dword [eax+284],255
	mov	eax,dword [ebp-4]
	mov	dword [eax+288],255
	mov	eax,dword [ebp-4]
	mov	dword [eax+292],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+296],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+300],0
	push	ebp
	push	_1292
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_313
_313:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_WndProc:
	push	ebp
	mov	ebp,esp
	sub	esp,192
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	mov	dword [ebp-60],_bbEmptyArray
	mov	dword [ebp-64],_bbEmptyArray
	mov	dword [ebp-68],_bbEmptyArray
	mov	dword [ebp-72],_bbEmptyArray
	mov	dword [ebp-76],0
	mov	dword [ebp-80],0
	mov	dword [ebp-84],0
	mov	dword [ebp-88],0
	mov	dword [ebp-92],_bbEmptyArray
	fldz
	fstp	dword [ebp-96]
	fldz
	fstp	dword [ebp-100]
	fldz
	fstp	dword [ebp-104]
	fldz
	fstp	dword [ebp-108]
	fldz
	fstp	dword [ebp-112]
	fldz
	fstp	dword [ebp-116]
	fldz
	fstp	dword [ebp-120]
	fldz
	fstp	dword [ebp-124]
	mov	dword [ebp-128],0
	mov	dword [ebp-132],0
	mov	dword [ebp-136],0
	mov	eax,ebp
	push	eax
	push	_1810
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1295
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,20
	je	_1298
	jmp	_1297
_1298:
	mov	eax,ebp
	push	eax
	push	_1808
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1299
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1301
	call	_brl_blitz_NullObjectError
_1301:
	cmp	dword [ebx+272],0
	jne	_1302
	mov	eax,ebp
	push	eax
	push	_1304
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1303
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_320
_1302:
	mov	eax,ebp
	push	eax
	push	_1783
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1306
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1308
	call	_brl_blitz_NullObjectError
_1308:
	cmp	dword [ebx+244],2
	jne	_1309
	mov	eax,ebp
	push	eax
	push	_1311
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1310
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_320
_1309:
	push	_1312
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	push	_1322
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_1323
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-60],eax
	push	4
	push	_1325
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-64],eax
	push	4
	push	_1327
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-68],eax
	push	4
	push	_1329
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-72],eax
	push	_1331
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-68]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-24]
	call	_GetClipBox@8
	push	_1332
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-72]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-8]
	call	_GetWindowRect@8
	push	_1333
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-60]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-8]
	call	_GetClientRect@8
	push	_1334
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	mov	eax,dword [ebp-64]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-8]
	call	_GetUpdateRect@12
	cmp	eax,0
	jne	_1335
	mov	eax,ebp
	push	eax
	push	_1337
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1336
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-68]
	mov	dword [ebp-64],eax
	call	dword [_bbOnDebugLeaveScope]
_1335:
	push	_1338
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-64]
	lea	eax,dword [eax+24]
	push	eax
	call	_IsRectEmpty@4
	cmp	eax,0
	je	_1339
	mov	eax,ebp
	push	eax
	push	_1350
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1340
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edi,eax
	mov	dword [edi+24],0
	mov	dword [edi+28],0
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1342
	call	_brl_blitz_ArrayBoundsError
_1342:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1344
	call	_brl_blitz_ArrayBoundsError
_1344:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [edi+32],edx
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1346
	call	_brl_blitz_ArrayBoundsError
_1346:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1348
	call	_brl_blitz_ArrayBoundsError
_1348:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [edi+36],edx
	mov	dword [ebp-64],edi
	call	dword [_bbOnDebugLeaveScope]
_1339:
	push	_1351
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1353
	call	_brl_blitz_NullObjectError
_1353:
	mov	eax,dword [ebp-8]
	cmp	eax,dword [ebx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_1368
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1355
	call	_brl_blitz_NullObjectError
_1355:
	mov	eax,dword [ebx+200]
	cmp	eax,0
	je	_1358
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1357
	call	_brl_blitz_NullObjectError
_1357:
	mov	eax,dword [ebx+252]
_1358:
	cmp	eax,0
	je	_1362
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1361
	call	_brl_blitz_NullObjectError
_1361:
	mov	eax,dword [ebx+256]
_1362:
	cmp	eax,0
	jne	_1366
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1365
	call	_brl_blitz_NullObjectError
_1365:
	fld	dword [ebx+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_1366:
_1368:
	cmp	eax,0
	je	_1370
	mov	eax,ebp
	push	eax
	push	_1382
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1371
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-24],eax
	push	_1372
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-140],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-140],eax
	jb	_1374
	call	_brl_blitz_ArrayBoundsError
_1374:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1376
	call	_brl_blitz_ArrayBoundsError
_1376:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1378
	call	_brl_blitz_ArrayBoundsError
_1378:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1380
	call	_brl_blitz_ArrayBoundsError
_1380:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-140]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	dword [ebp-16]
	call	_CreateCompatibleBitmap@12
	mov	dword [ebp-28],eax
	push	_1381
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	push	dword [ebp-24]
	call	_SelectObject@8
	call	dword [_bbOnDebugLeaveScope]
_1370:
	push	_1383
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-76],0
	push	_1385
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-80],0
	push	_1387
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-84],0
	push	_1389
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-88],0
	push	_1391
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	ebx,eax
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1393
	call	_brl_blitz_ArrayBoundsError
_1393:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+24],eax
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1395
	call	_brl_blitz_ArrayBoundsError
_1395:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+28],eax
	mov	esi,2
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1397
	call	_brl_blitz_ArrayBoundsError
_1397:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+32],eax
	mov	esi,3
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1399
	call	_brl_blitz_ArrayBoundsError
_1399:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+36],eax
	mov	dword [ebp-92],ebx
	push	_1402
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-96]
	push	_1404
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-100]
	push	_1406
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-104]
	push	_1408
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-108]
	push	_1410
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-112]
	push	_1412
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-116]
	push	_1414
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1416
	call	_brl_blitz_NullObjectError
_1416:
	cmp	dword [ebx+276],0
	je	_1417
	mov	eax,ebp
	push	eax
	push	_1483
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1418
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-60]
	cmp	esi,dword [eax+20]
	jb	_1420
	call	_brl_blitz_ArrayBoundsError
_1420:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1422
	call	_brl_blitz_ArrayBoundsError
_1422:
	mov	eax,dword [ebp-60]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	dword [ebp-80],eax
	push	_1423
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1425
	call	_brl_blitz_NullObjectError
_1425:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1427
	call	_brl_blitz_NullObjectError
_1427:
	mov	eax,dword [esi+292]
	sub	eax,dword [ebx+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-108]
	push	_1428
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1430
	call	_brl_blitz_NullObjectError
_1430:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1432
	call	_brl_blitz_NullObjectError
_1432:
	mov	eax,dword [esi+296]
	sub	eax,dword [ebx+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-112]
	push	_1433
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1435
	call	_brl_blitz_NullObjectError
_1435:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1437
	call	_brl_blitz_NullObjectError
_1437:
	mov	eax,dword [esi+300]
	sub	eax,dword [ebx+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-116]
	push	_1438
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1440
	call	_brl_blitz_NullObjectError
_1440:
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1442
	call	_brl_blitz_ArrayBoundsError
_1442:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1444
	call	_brl_blitz_ArrayBoundsError
_1444:
	mov	eax,dword [edi+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-108]
	faddp	st1,st0
	fstp	dword [ebp-96]
	push	_1445
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1447
	call	_brl_blitz_NullObjectError
_1447:
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1449
	call	_brl_blitz_ArrayBoundsError
_1449:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1451
	call	_brl_blitz_ArrayBoundsError
_1451:
	mov	eax,dword [edi+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-112]
	faddp	st1,st0
	fstp	dword [ebp-100]
	push	_1452
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1454
	call	_brl_blitz_NullObjectError
_1454:
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1456
	call	_brl_blitz_ArrayBoundsError
_1456:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1458
	call	_brl_blitz_ArrayBoundsError
_1458:
	mov	eax,dword [edi+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-116]
	faddp	st1,st0
	fstp	dword [ebp-104]
	push	_1459
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1461
	call	_brl_blitz_ArrayBoundsError
_1461:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp-84],eax
	mov	ebx,3
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1463
	call	_brl_blitz_ArrayBoundsError
_1463:
	mov	eax,dword [ebp-64]
	mov	esi,dword [eax+ebx*4+24]
	jmp	_1464
_48:
	mov	eax,ebp
	push	eax
	push	_1482
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1466
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-100]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-96]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	dword [ebp-88],eax
	push	_1467
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	push	dword [ebp-24]
	call	_SelectObject@8
	mov	dword [ebp-76],eax
	push	_1468
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1470
	call	_brl_blitz_ArrayBoundsError
_1470:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	mov	dword [eax+24],edx
	push	_1472
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,3
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1474
	call	_brl_blitz_ArrayBoundsError
_1474:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	add	edx,1
	mov	dword [eax+24],edx
	push	_1476
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	mov	eax,dword [ebp-92]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-24]
	call	_FillRect@12
	push	_1477
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-76]
	push	dword [ebp-24]
	call	_SelectObject@8
	push	_1478
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	call	_DeleteObject@4
	push	_1479
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-96]
	fadd	dword [ebp-108]
	fstp	dword [ebp-96]
	push	_1480
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-100]
	fadd	dword [ebp-112]
	fstp	dword [ebp-100]
	push	_1481
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	fadd	dword [ebp-116]
	fstp	dword [ebp-104]
	call	dword [_bbOnDebugLeaveScope]
_46:
	add	dword [ebp-84],1
_1464:
	cmp	dword [ebp-84],esi
	jl	_48
_47:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1484
_1417:
	mov	eax,ebp
	push	eax
	push	_1550
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1485
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1487
	call	_brl_blitz_ArrayBoundsError
_1487:
	mov	ebx,0
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1489
	call	_brl_blitz_ArrayBoundsError
_1489:
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-64]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	dword [ebp-80],eax
	push	_1490
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1492
	call	_brl_blitz_NullObjectError
_1492:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1494
	call	_brl_blitz_NullObjectError
_1494:
	mov	eax,dword [esi+292]
	sub	eax,dword [ebx+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-108]
	push	_1495
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1497
	call	_brl_blitz_NullObjectError
_1497:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1499
	call	_brl_blitz_NullObjectError
_1499:
	mov	eax,dword [esi+296]
	sub	eax,dword [ebx+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-112]
	push	_1500
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1502
	call	_brl_blitz_NullObjectError
_1502:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1504
	call	_brl_blitz_NullObjectError
_1504:
	mov	eax,dword [esi+300]
	sub	eax,dword [ebx+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-116]
	push	_1505
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1507
	call	_brl_blitz_NullObjectError
_1507:
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1509
	call	_brl_blitz_ArrayBoundsError
_1509:
	mov	ebx,0
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1511
	call	_brl_blitz_ArrayBoundsError
_1511:
	mov	eax,dword [edi+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-108]
	faddp	st1,st0
	fstp	dword [ebp-96]
	push	_1512
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1514
	call	_brl_blitz_NullObjectError
_1514:
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1516
	call	_brl_blitz_ArrayBoundsError
_1516:
	mov	ebx,0
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1518
	call	_brl_blitz_ArrayBoundsError
_1518:
	mov	eax,dword [edi+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-112]
	faddp	st1,st0
	fstp	dword [ebp-100]
	push	_1519
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1521
	call	_brl_blitz_NullObjectError
_1521:
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1523
	call	_brl_blitz_ArrayBoundsError
_1523:
	mov	ebx,0
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1525
	call	_brl_blitz_ArrayBoundsError
_1525:
	mov	eax,dword [edi+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-116]
	faddp	st1,st0
	fstp	dword [ebp-104]
	push	_1526
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1528
	call	_brl_blitz_ArrayBoundsError
_1528:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp-84],eax
	mov	ebx,2
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1530
	call	_brl_blitz_ArrayBoundsError
_1530:
	mov	eax,dword [ebp-64]
	mov	esi,dword [eax+ebx*4+24]
	jmp	_1531
_51:
	mov	eax,ebp
	push	eax
	push	_1549
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1533
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-100]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-96]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	dword [ebp-88],eax
	push	_1534
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	push	dword [ebp-24]
	call	_SelectObject@8
	mov	dword [ebp-76],eax
	push	_1535
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1537
	call	_brl_blitz_ArrayBoundsError
_1537:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	mov	dword [eax+24],edx
	push	_1539
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,2
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1541
	call	_brl_blitz_ArrayBoundsError
_1541:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	add	edx,1
	mov	dword [eax+24],edx
	push	_1543
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	mov	eax,dword [ebp-92]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-24]
	call	_FillRect@12
	push	_1544
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-76]
	push	dword [ebp-24]
	call	_SelectObject@8
	push	_1545
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	call	_DeleteObject@4
	push	_1546
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-96]
	fadd	dword [ebp-108]
	fstp	dword [ebp-96]
	push	_1547
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-100]
	fadd	dword [ebp-112]
	fstp	dword [ebp-100]
	push	_1548
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	fadd	dword [ebp-116]
	fstp	dword [ebp-104]
	call	dword [_bbOnDebugLeaveScope]
_49:
	add	dword [ebp-84],1
_1531:
	cmp	dword [ebp-84],esi
	jl	_51
_50:
	call	dword [_bbOnDebugLeaveScope]
_1484:
	push	_1551
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1553
	call	_brl_blitz_NullObjectError
_1553:
	mov	eax,dword [ebp-8]
	cmp	eax,dword [ebx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_1568
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1555
	call	_brl_blitz_NullObjectError
_1555:
	mov	eax,dword [ebx+200]
	cmp	eax,0
	je	_1558
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1557
	call	_brl_blitz_NullObjectError
_1557:
	mov	eax,dword [ebx+252]
_1558:
	cmp	eax,0
	je	_1562
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1561
	call	_brl_blitz_NullObjectError
_1561:
	mov	eax,dword [ebx+256]
_1562:
	cmp	eax,0
	jne	_1566
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1565
	call	_brl_blitz_NullObjectError
_1565:
	fld	dword [ebx+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_1566:
_1568:
	cmp	eax,0
	jne	_1570
	mov	eax,ebp
	push	eax
	push	_1572
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1571
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_320
_1570:
	push	_1573
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1575
	call	_brl_blitz_NullObjectError
_1575:
	mov	eax,dword [ebx+200]
	cmp	eax,0
	je	_1578
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1577
	call	_brl_blitz_NullObjectError
_1577:
	mov	eax,dword [ebx+252]
_1578:
	cmp	eax,0
	je	_1582
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1581
	call	_brl_blitz_NullObjectError
_1581:
	mov	eax,dword [ebx+256]
_1582:
	cmp	eax,0
	je	_1584
	mov	eax,ebp
	push	eax
	push	_1730
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1585
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-32],eax
	push	_1586
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1588
	call	_brl_blitz_NullObjectError
_1588:
	push	dword [ebx+200]
	push	dword [ebp-32]
	call	_SelectObject@8
	push	_1589
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1591
	call	_brl_blitz_NullObjectError
_1591:
	mov	eax,dword [ebx+252]
	mov	dword [ebp-36],eax
	push	_1592
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1594
	call	_brl_blitz_NullObjectError
_1594:
	mov	eax,dword [ebx+256]
	mov	dword [ebp-40],eax
	push	_1595
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1597
	call	_brl_blitz_NullObjectError
_1597:
	mov	eax,dword [ebx+260]
	and	eax,7
	cmp	eax,0
	je	_1600
	cmp	eax,1
	je	_1601
	cmp	eax,2
	je	_1602
	cmp	eax,4
	je	_1602
	cmp	eax,3
	je	_1603
	jmp	_1599
_1600:
	mov	eax,ebp
	push	eax
	push	_1628
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1604
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_52
_54:
	mov	eax,ebp
	push	eax
	push	_1627
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1609
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],0
	push	_1610
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_55
_57:
	mov	eax,ebp
	push	eax
	push	_1625
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1615
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1617
	call	_brl_blitz_NullObjectError
_1617:
	cmp	dword [ebx+268],0
	je	_1618
	mov	eax,ebp
	push	eax
	push	_1620
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1621
_1618:
	mov	eax,ebp
	push	eax
	push	_1623
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1622
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	13369376
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_BitBlt@36
	call	dword [_bbOnDebugLeaveScope]
_1621:
	push	_1624
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-36]
	add	dword [ebp-44],eax
	call	dword [_bbOnDebugLeaveScope]
_55:
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1612
	call	_brl_blitz_ArrayBoundsError
_1612:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1614
	call	_brl_blitz_ArrayBoundsError
_1614:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	cmp	dword [ebp-44],edx
	jl	_57
_56:
	push	_1626
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-40]
	add	dword [ebp-48],eax
	call	dword [_bbOnDebugLeaveScope]
_52:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1606
	call	_brl_blitz_ArrayBoundsError
_1606:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1608
	call	_brl_blitz_ArrayBoundsError
_1608:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	cmp	dword [ebp-48],edx
	jl	_54
_53:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1599
_1601:
	mov	eax,ebp
	push	eax
	push	_1648
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1629
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1631
	call	_brl_blitz_ArrayBoundsError
_1631:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1633
	call	_brl_blitz_ArrayBoundsError
_1633:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-36]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-44],eax
	push	_1634
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1636
	call	_brl_blitz_ArrayBoundsError
_1636:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1638
	call	_brl_blitz_ArrayBoundsError
_1638:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-40]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-48],eax
	push	_1639
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1641
	call	_brl_blitz_NullObjectError
_1641:
	cmp	dword [ebx+268],0
	je	_1642
	mov	eax,ebp
	push	eax
	push	_1644
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1643
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1645
_1642:
	mov	eax,ebp
	push	eax
	push	_1647
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	13369376
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_BitBlt@36
	call	dword [_bbOnDebugLeaveScope]
_1645:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1599
_1602:
	mov	eax,ebp
	push	eax
	push	_1697
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1649
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1651
	call	_brl_blitz_ArrayBoundsError
_1651:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1653
	call	_brl_blitz_ArrayBoundsError
_1653:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-36]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-120]
	push	_1655
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1657
	call	_brl_blitz_ArrayBoundsError
_1657:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1659
	call	_brl_blitz_ArrayBoundsError
_1659:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-40]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-124]
	push	_1661
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	fld	dword [ebp-124]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setbe	al
	movzx	eax,al
	cmp	eax,0
	jne	_1662
	mov	eax,ebp
	push	eax
	push	_1672
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1663
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1665
	call	_brl_blitz_NullObjectError
_1665:
	mov	eax,dword [ebx+260]
	and	eax,7
	cmp	eax,2
	jne	_1666
	mov	eax,ebp
	push	eax
	push	_1668
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1667
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-124]
	fstp	dword [ebp-120]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1669
_1666:
	mov	eax,ebp
	push	eax
	push	_1671
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1670
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	fstp	dword [ebp-124]
	call	dword [_bbOnDebugLeaveScope]
_1669:
	call	dword [_bbOnDebugLeaveScope]
_1662:
	push	_1673
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	mov	eax,dword [ebp-36]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-128],eax
	push	_1675
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	mov	eax,dword [ebp-40]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-132],eax
	push	_1677
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1679
	call	_brl_blitz_ArrayBoundsError
_1679:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1681
	call	_brl_blitz_ArrayBoundsError
_1681:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-128]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-44],eax
	push	_1682
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1684
	call	_brl_blitz_ArrayBoundsError
_1684:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1686
	call	_brl_blitz_ArrayBoundsError
_1686:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-132]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-48],eax
	push	_1687
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	3
	push	dword [ebp-24]
	call	_SetStretchBltMode@8
	push	_1688
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1690
	call	_brl_blitz_NullObjectError
_1690:
	cmp	dword [ebx+268],0
	je	_1691
	mov	eax,ebp
	push	eax
	push	_1693
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1692
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-132]
	push	dword [ebp-128]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1694
_1691:
	mov	eax,ebp
	push	eax
	push	_1696
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1695
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	13369376
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-132]
	push	dword [ebp-128]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_StretchBlt@44
	call	dword [_bbOnDebugLeaveScope]
_1694:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1599
_1603:
	mov	eax,ebp
	push	eax
	push	_1728
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1702
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	3
	push	dword [ebp-24]
	call	_SetStretchBltMode@8
	push	_1703
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1705
	call	_brl_blitz_NullObjectError
_1705:
	cmp	dword [ebx+268],0
	je	_1706
	mov	eax,ebp
	push	eax
	push	_1716
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1707
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-144],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-144],eax
	jb	_1709
	call	_brl_blitz_ArrayBoundsError
_1709:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1711
	call	_brl_blitz_ArrayBoundsError
_1711:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1713
	call	_brl_blitz_ArrayBoundsError
_1713:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1715
	call	_brl_blitz_ArrayBoundsError
_1715:
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-144]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1717
_1706:
	mov	eax,ebp
	push	eax
	push	_1727
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1718
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-148],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-148],eax
	jb	_1720
	call	_brl_blitz_ArrayBoundsError
_1720:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1722
	call	_brl_blitz_ArrayBoundsError
_1722:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1724
	call	_brl_blitz_ArrayBoundsError
_1724:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1726
	call	_brl_blitz_ArrayBoundsError
_1726:
	push	13369376
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-148]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-24]
	call	_StretchBlt@44
	call	dword [_bbOnDebugLeaveScope]
_1717:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1599
_1599:
	push	_1729
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-32]
	call	_DeleteDC@4
	call	dword [_bbOnDebugLeaveScope]
_1584:
	push	_1731
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1733
	call	_brl_blitz_NullObjectError
_1733:
	fld	dword [ebx+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	jne	_1734
	mov	eax,ebp
	push	eax
	push	_1765
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1735
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	push	dword [ebp-16]
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax]
	call	dword [eax+544]
	add	esp,12
	push	_1736
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1738
	call	_brl_blitz_NullObjectError
_1738:
	fld	dword [ebx+248]
	fmul	dword [_2298]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	and	eax,255
	shl	eax,16
	mov	dword [ebp-136],eax
	push	_1740
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-188],0
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-188],eax
	jb	_1742
	call	_brl_blitz_ArrayBoundsError
_1742:
	mov	dword [ebp-184],1
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-184],eax
	jb	_1744
	call	_brl_blitz_ArrayBoundsError
_1744:
	mov	dword [ebp-180],2
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-180],eax
	jb	_1746
	call	_brl_blitz_ArrayBoundsError
_1746:
	mov	dword [ebp-176],0
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-176],eax
	jb	_1748
	call	_brl_blitz_ArrayBoundsError
_1748:
	mov	dword [ebp-172],3
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-172],eax
	jb	_1750
	call	_brl_blitz_ArrayBoundsError
_1750:
	mov	dword [ebp-168],1
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-168],eax
	jb	_1752
	call	_brl_blitz_ArrayBoundsError
_1752:
	mov	dword [ebp-164],0
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-164],eax
	jb	_1754
	call	_brl_blitz_ArrayBoundsError
_1754:
	mov	dword [ebp-160],1
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-160],eax
	jb	_1756
	call	_brl_blitz_ArrayBoundsError
_1756:
	mov	dword [ebp-152],2
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-152],eax
	jb	_1758
	call	_brl_blitz_ArrayBoundsError
_1758:
	mov	edi,0
	mov	eax,dword [ebp-64]
	cmp	edi,dword [eax+20]
	jb	_1760
	call	_brl_blitz_ArrayBoundsError
_1760:
	mov	esi,3
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1762
	call	_brl_blitz_ArrayBoundsError
_1762:
	mov	ebx,1
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1764
	call	_brl_blitz_ArrayBoundsError
_1764:
	push	dword [ebp-136]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-64]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-152]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-64]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-160]
	push	dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-164]
	push	dword [edx+eax*4+24]
	push	dword [ebp-24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-172]
	mov	ecx,dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-168]
	sub	ecx,dword [edx+eax*4+24]
	push	ecx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-180]
	mov	ecx,dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-176]
	sub	ecx,dword [edx+eax*4+24]
	push	ecx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-184]
	push	dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-188]
	push	dword [edx+eax*4+24]
	push	dword [ebp-16]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1767
_1734:
	mov	eax,ebp
	push	eax
	push	_1777
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1768
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-156],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-156],eax
	jb	_1770
	call	_brl_blitz_ArrayBoundsError
_1770:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1772
	call	_brl_blitz_ArrayBoundsError
_1772:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1774
	call	_brl_blitz_ArrayBoundsError
_1774:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1776
	call	_brl_blitz_ArrayBoundsError
_1776:
	push	13369376
	push	0
	push	0
	push	dword [ebp-24]
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-156]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-16]
	call	_BitBlt@36
	call	dword [_bbOnDebugLeaveScope]
_1767:
	push	_1778
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	cmp	dword [ebp-24],eax
	jne	_1779
	push	_58
	call	_brl_blitz_RuntimeError
	add	esp,4
_1779:
	push	_1780
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	call	_DeleteObject@4
	push	_1781
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_DeleteDC@4
	push	_1782
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_320
_1297:
	push	_1809
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	mov	ebx,eax
	jmp	_320
_320:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_SetGradient:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp+36]
	mov	dword [ebp-32],eax
	mov	eax,dword [ebp+40]
	mov	dword [ebp-36],eax
	push	ebp
	push	_1911
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1814
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	jne	_1815
	push	ebp
	push	_1826
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1816
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1818
	call	_brl_blitz_NullObjectError
_1818:
	cmp	dword [ebx+272],0
	je	_1819
	push	ebp
	push	_1825
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1820
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1822
	call	_brl_blitz_NullObjectError
_1822:
	mov	dword [ebx+272],0
	push	_1824
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1819:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1827
_1815:
	push	ebp
	push	_1910
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1828
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,dword [ebp-24]
	sete	al
	movzx	eax,al
	cmp	eax,0
	je	_1829
	mov	eax,dword [ebp-20]
	cmp	eax,dword [ebp-32]
	sete	al
	movzx	eax,al
_1829:
	cmp	eax,0
	je	_1831
	mov	eax,dword [ebp-16]
	cmp	eax,dword [ebp-28]
	sete	al
	movzx	eax,al
_1831:
	cmp	eax,0
	je	_1833
	push	ebp
	push	_1841
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1834
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1836
	call	_brl_blitz_NullObjectError
_1836:
	mov	dword [ebx+272],0
	push	_1838
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1840
	call	_brl_blitz_NullObjectError
_1840:
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	dword [ebp-12]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+248]
	add	esp,16
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1842
_1833:
	push	ebp
	push	_1909
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1843
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1845
	call	_brl_blitz_NullObjectError
_1845:
	mov	eax,dword [ebx+272]
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_1848
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1847
	call	_brl_blitz_NullObjectError
_1847:
	mov	eax,dword [ebp-12]
	cmp	eax,dword [ebx+280]
	setne	al
	movzx	eax,al
_1848:
	cmp	eax,0
	jne	_1852
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1851
	call	_brl_blitz_NullObjectError
_1851:
	mov	eax,dword [ebp-20]
	cmp	eax,dword [ebx+284]
	setne	al
	movzx	eax,al
_1852:
	cmp	eax,0
	jne	_1856
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1855
	call	_brl_blitz_NullObjectError
_1855:
	mov	eax,dword [ebp-16]
	cmp	eax,dword [ebx+288]
	setne	al
	movzx	eax,al
_1856:
	cmp	eax,0
	jne	_1860
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1859
	call	_brl_blitz_NullObjectError
_1859:
	mov	eax,dword [ebp-24]
	cmp	eax,dword [ebx+292]
	setne	al
	movzx	eax,al
_1860:
	cmp	eax,0
	jne	_1864
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1863
	call	_brl_blitz_NullObjectError
_1863:
	mov	eax,dword [ebp-32]
	cmp	eax,dword [ebx+296]
	setne	al
	movzx	eax,al
_1864:
	cmp	eax,0
	jne	_1868
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1867
	call	_brl_blitz_NullObjectError
_1867:
	mov	eax,dword [ebp-28]
	cmp	eax,dword [ebx+300]
	setne	al
	movzx	eax,al
_1868:
	cmp	eax,0
	jne	_1872
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1871
	call	_brl_blitz_NullObjectError
_1871:
	mov	eax,dword [ebp-36]
	cmp	eax,dword [ebx+276]
	setne	al
	movzx	eax,al
_1872:
	cmp	eax,0
	je	_1874
	push	ebp
	push	_1908
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1875
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1877
	call	_brl_blitz_NullObjectError
_1877:
	mov	dword [ebx+272],1
	push	_1879
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1881
	call	_brl_blitz_NullObjectError
_1881:
	mov	eax,dword [ebp-12]
	mov	dword [ebx+280],eax
	push	_1883
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1885
	call	_brl_blitz_NullObjectError
_1885:
	mov	eax,dword [ebp-20]
	mov	dword [ebx+284],eax
	push	_1887
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1889
	call	_brl_blitz_NullObjectError
_1889:
	mov	eax,dword [ebp-16]
	mov	dword [ebx+288],eax
	push	_1891
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1893
	call	_brl_blitz_NullObjectError
_1893:
	mov	eax,dword [ebp-24]
	mov	dword [ebx+292],eax
	push	_1895
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1897
	call	_brl_blitz_NullObjectError
_1897:
	mov	eax,dword [ebp-32]
	mov	dword [ebx+296],eax
	push	_1899
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1901
	call	_brl_blitz_NullObjectError
_1901:
	mov	eax,dword [ebp-28]
	mov	dword [ebx+300],eax
	push	_1903
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1905
	call	_brl_blitz_NullObjectError
_1905:
	mov	eax,dword [ebp-36]
	mov	dword [ebx+276],eax
	push	_1907
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1874:
	call	dword [_bbOnDebugLeaveScope]
_1842:
	call	dword [_bbOnDebugLeaveScope]
_1827:
	mov	ebx,0
	jmp	_331
_331:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_CreatePanelEx:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	dword [ebp-32],_bbNullObject
	push	ebp
	push	_1977
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1920
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1921
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_1923
	call	_brl_blitz_NullObjectError
_1923:
	push	_1
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+480]
	add	esp,16
	mov	dword [ebp-32],eax
	push	_1925
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_maxgui_localization_LocalizationMode
	and	eax,2
	cmp	eax,0
	je	_1926
	push	ebp
	push	_1928
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1927
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-28]
	push	dword [ebp-32]
	call	_maxgui_maxgui_LocalizeGadget
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1929
_1926:
	push	ebp
	push	_1933
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1930
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1932
	call	_brl_blitz_NullObjectError
_1932:
	push	dword [ebp-28]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+236]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_1929:
	push	_1934
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],_bbNullObject
	je	_1935
	push	ebp
	push	_1939
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1936
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1938
	call	_brl_blitz_NullObjectError
_1938:
	push	-1
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
_1935:
	push	_1940
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1942
	call	_brl_blitz_NullObjectError
_1942:
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+88]
	add	esp,20
	push	_1943
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_1944
	push	ebp
	push	_1969
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1945
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1947
	call	_brl_blitz_NullObjectError
_1947:
	push	dword [__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+244]
	add	esp,8
	push	_1948
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-20]
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_1949
	push	ebp
	push	_1965
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1950
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_1952
	call	_brl_blitz_NullObjectError
_1952:
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-20]
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	jne	_1955
	call	_brl_blitz_NullObjectError
_1955:
	mov	eax,dword [esi+232]
	cmp	eax,0
	je	_1958
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-20]
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	jne	_1957
	call	_brl_blitz_NullObjectError
_1957:
	mov	eax,dword [esi+236]
	cmp	eax,0
	sete	al
	movzx	eax,al
_1958:
	cmp	eax,0
	sete	al
	movzx	eax,al
	mov	dword [ebx+236],eax
	push	_1960
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1962
	call	_brl_blitz_NullObjectError
_1962:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_1964
	call	_brl_blitz_NullObjectError
_1964:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+232]
	add	esp,4
	and	eax,4
	cmp	eax,0
	sete	al
	movzx	eax,al
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+284]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_1949:
	push	_1966
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1968
	call	_brl_blitz_NullObjectError
_1968:
	push	0
	push	1
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+280]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
_1944:
	push	_1970
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_1971
	push	ebp
	push	_1975
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1972
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_1974
	call	_brl_blitz_NullObjectError
_1974:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+548]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1971:
	push	_1976
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	jmp	_340
_340:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetPanelExGradient:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp+36]
	mov	dword [ebp-32],eax
	mov	eax,dword [ebp+40]
	mov	dword [ebp-36],eax
	mov	dword [ebp-40],_bbNullObject
	push	ebp
	push	_1990
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1982
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	push	dword [ebp-4]
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-40],eax
	push	_1984
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-40],_bbNullObject
	je	_1985
	push	ebp
	push	_1989
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1986
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_1988
	call	_brl_blitz_NullObjectError
_1988:
	push	dword [ebp-36]
	push	dword [ebp-28]
	push	dword [ebp-32]
	push	dword [ebp-24]
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+580]
	add	esp,36
	call	dword [_bbOnDebugLeaveScope]
_1985:
	mov	ebx,0
	jmp	_351
_351:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_364:
	dd	0
_357:
	db	"maxguiex",0
_358:
	db	"BCM_GETIDEALSIZE",0
_62:
	db	"i",0
	align	4
_359:
	dd	_bbStringClass
	dd	2147483646
	dd	4
	dw	53,54,51,51
_360:
	db	"BCM_GETTEXTMARGIN",0
	align	4
_361:
	dd	_bbStringClass
	dd	2147483646
	dd	4
	dw	53,54,51,55
_362:
	db	"FR_PRIVATE",0
	align	4
_363:
	dd	_bbStringClass
	dd	2147483646
	dd	2
	dw	49,54
	align	4
_356:
	dd	1
	dd	_357
	dd	1
	dd	_358
	dd	_62
	dd	_359
	dd	1
	dd	_360
	dd	_62
	dd	_361
	dd	1
	dd	_362
	dd	_62
	dd	_363
	dd	0
_354:
	db	"$BMXPATH/mod/skn3.mod/maxguiex.mod/maxguiex.bmx",0
	align	4
_353:
	dd	_354
	dd	58
	dd	2
	align	4
__skn3_maxguiex_Skn3ListBatchLock_all:
	dd	_bbNullObject
_60:
	db	"Skn3ListBatchLock",0
_61:
	db	"refCount",0
_63:
	db	"listBox",0
_64:
	db	":maxgui.win32maxguiex.TWindowsListBox",0
_65:
	db	"index",0
_66:
	db	"link",0
_67:
	db	":brl.linkedlist.TLink",0
_68:
	db	"it",0
_69:
	db	":pub.win32.LVITEMW",0
_70:
	db	"hwnd",0
_71:
	db	"New",0
_72:
	db	"()i",0
_73:
	db	"Find",0
_74:
	db	"(:maxgui.maxgui.TGadget):Skn3ListBatchLock",0
_75:
	db	"add",0
_76:
	db	"(:Skn3ListBatchLock)i",0
_77:
	db	"remove",0
	align	4
_59:
	dd	2
	dd	_60
	dd	3
	dd	_61
	dd	_62
	dd	8
	dd	3
	dd	_63
	dd	_64
	dd	12
	dd	3
	dd	_65
	dd	_62
	dd	16
	dd	3
	dd	_66
	dd	_67
	dd	20
	dd	3
	dd	_68
	dd	_69
	dd	24
	dd	3
	dd	_70
	dd	_62
	dd	28
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	7
	dd	_73
	dd	_74
	dd	48
	dd	7
	dd	_75
	dd	_76
	dd	52
	dd	7
	dd	_77
	dd	_76
	dd	56
	dd	0
	align	4
_16:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_59
	dd	32
	dd	__skn3_maxguiex_Skn3ListBatchLock_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__skn3_maxguiex_Skn3ListBatchLock_Find
	dd	__skn3_maxguiex_Skn3ListBatchLock_add
	dd	__skn3_maxguiex_Skn3ListBatchLock_remove
	align	4
_355:
	dd	_354
	dd	150
	dd	2
	align	4
__skn3_maxguiex_Skn3CustomPointer_all:
	dd	_bbNullObject
_79:
	db	"Skn3CustomPointer",0
_80:
	db	"path",0
_81:
	db	"$",0
_82:
	db	"pointer",0
	align	4
_78:
	dd	2
	dd	_79
	dd	3
	dd	_80
	dd	_81
	dd	8
	dd	3
	dd	_82
	dd	_62
	dd	12
	dd	3
	dd	_61
	dd	_62
	dd	16
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	0
	align	4
_skn3_maxguiex_Skn3CustomPointer:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_78
	dd	20
	dd	__skn3_maxguiex_Skn3CustomPointer_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_84:
	db	"PARAFORMAT2",0
_85:
	db	"cbSize",0
_86:
	db	"dwMask",0
_87:
	db	"wNumbering",0
_88:
	db	"s",0
_89:
	db	"wEffects",0
_90:
	db	"dxStartIndent",0
_91:
	db	"dxRightIndent",0
_92:
	db	"dxOffset",0
_93:
	db	"wAlignment",0
_94:
	db	"cTabCount",0
_95:
	db	"rgxTabs00",0
_96:
	db	"rgxTabs01",0
_97:
	db	"rgxTabs02",0
_98:
	db	"rgxTabs03",0
_99:
	db	"rgxTabs10",0
_100:
	db	"rgxTabs11",0
_101:
	db	"rgxTabs12",0
_102:
	db	"rgxTabs13",0
_103:
	db	"rgxTabs20",0
_104:
	db	"rgxTabs21",0
_105:
	db	"rgxTabs22",0
_106:
	db	"rgxTabs23",0
_107:
	db	"rgxTabs30",0
_108:
	db	"rgxTabs31",0
_109:
	db	"rgxTabs32",0
_110:
	db	"rgxTabs33",0
_111:
	db	"rgxTabs40",0
_112:
	db	"rgxTabs41",0
_113:
	db	"rgxTabs42",0
_114:
	db	"rgxTabs43",0
_115:
	db	"rgxTabs50",0
_116:
	db	"rgxTabs51",0
_117:
	db	"rgxTabs52",0
_118:
	db	"rgxTabs53",0
_119:
	db	"rgxTabs60",0
_120:
	db	"rgxTabs61",0
_121:
	db	"rgxTabs62",0
_122:
	db	"rgxTabs63",0
_123:
	db	"rgxTabs70",0
_124:
	db	"rgxTabs71",0
_125:
	db	"rgxTabs72",0
_126:
	db	"rgxTabs73",0
_127:
	db	"dySpaceBefore",0
_128:
	db	"dySpaceAfter",0
_129:
	db	"dyLineSpacing",0
_130:
	db	"sStyle",0
_131:
	db	"bLineSpacingRule",0
_132:
	db	"b",0
_133:
	db	"bOutlineLevel",0
_134:
	db	"wShadingWeight",0
_135:
	db	"wShadingStyle",0
_136:
	db	"wNumberingStart",0
_137:
	db	"wNumberingStyle",0
_138:
	db	"wNumberingTab",0
_139:
	db	"wBorderSpace",0
_140:
	db	"wBorderWidth",0
_141:
	db	"wBorders",0
	align	4
_83:
	dd	2
	dd	_84
	dd	3
	dd	_85
	dd	_62
	dd	8
	dd	3
	dd	_86
	dd	_62
	dd	12
	dd	3
	dd	_87
	dd	_88
	dd	16
	dd	3
	dd	_89
	dd	_88
	dd	18
	dd	3
	dd	_90
	dd	_62
	dd	20
	dd	3
	dd	_91
	dd	_62
	dd	24
	dd	3
	dd	_92
	dd	_62
	dd	28
	dd	3
	dd	_93
	dd	_88
	dd	32
	dd	3
	dd	_94
	dd	_88
	dd	34
	dd	3
	dd	_95
	dd	_62
	dd	36
	dd	3
	dd	_96
	dd	_62
	dd	40
	dd	3
	dd	_97
	dd	_62
	dd	44
	dd	3
	dd	_98
	dd	_62
	dd	48
	dd	3
	dd	_99
	dd	_62
	dd	52
	dd	3
	dd	_100
	dd	_62
	dd	56
	dd	3
	dd	_101
	dd	_62
	dd	60
	dd	3
	dd	_102
	dd	_62
	dd	64
	dd	3
	dd	_103
	dd	_62
	dd	68
	dd	3
	dd	_104
	dd	_62
	dd	72
	dd	3
	dd	_105
	dd	_62
	dd	76
	dd	3
	dd	_106
	dd	_62
	dd	80
	dd	3
	dd	_107
	dd	_62
	dd	84
	dd	3
	dd	_108
	dd	_62
	dd	88
	dd	3
	dd	_109
	dd	_62
	dd	92
	dd	3
	dd	_110
	dd	_62
	dd	96
	dd	3
	dd	_111
	dd	_62
	dd	100
	dd	3
	dd	_112
	dd	_62
	dd	104
	dd	3
	dd	_113
	dd	_62
	dd	108
	dd	3
	dd	_114
	dd	_62
	dd	112
	dd	3
	dd	_115
	dd	_62
	dd	116
	dd	3
	dd	_116
	dd	_62
	dd	120
	dd	3
	dd	_117
	dd	_62
	dd	124
	dd	3
	dd	_118
	dd	_62
	dd	128
	dd	3
	dd	_119
	dd	_62
	dd	132
	dd	3
	dd	_120
	dd	_62
	dd	136
	dd	3
	dd	_121
	dd	_62
	dd	140
	dd	3
	dd	_122
	dd	_62
	dd	144
	dd	3
	dd	_123
	dd	_62
	dd	148
	dd	3
	dd	_124
	dd	_62
	dd	152
	dd	3
	dd	_125
	dd	_62
	dd	156
	dd	3
	dd	_126
	dd	_62
	dd	160
	dd	3
	dd	_127
	dd	_62
	dd	164
	dd	3
	dd	_128
	dd	_62
	dd	168
	dd	3
	dd	_129
	dd	_62
	dd	172
	dd	3
	dd	_130
	dd	_88
	dd	176
	dd	3
	dd	_131
	dd	_132
	dd	178
	dd	3
	dd	_133
	dd	_132
	dd	179
	dd	3
	dd	_134
	dd	_88
	dd	180
	dd	3
	dd	_135
	dd	_88
	dd	182
	dd	3
	dd	_136
	dd	_88
	dd	184
	dd	3
	dd	_137
	dd	_88
	dd	186
	dd	3
	dd	_138
	dd	_88
	dd	188
	dd	3
	dd	_139
	dd	_88
	dd	190
	dd	3
	dd	_140
	dd	_88
	dd	192
	dd	3
	dd	_141
	dd	_88
	dd	194
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	0
	align	4
_20:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_83
	dd	196
	dd	__skn3_maxguiex_PARAFORMAT2_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_143:
	db	"Skn3PanelEx",0
_144:
	db	"gradientOn",0
_145:
	db	"gradientVertical",0
_146:
	db	"gradientStartR",0
_147:
	db	"gradientStartG",0
_148:
	db	"gradientStartB",0
_149:
	db	"gradientEndR",0
_150:
	db	"gradientEndG",0
_151:
	db	"gradientEndB",0
_152:
	db	"WndProc",0
_153:
	db	"(i,i,i,i)i",0
_154:
	db	"SetGradient",0
_155:
	db	"(i,i,i,i,i,i,i,i)i",0
	align	4
_142:
	dd	2
	dd	_143
	dd	3
	dd	_144
	dd	_62
	dd	272
	dd	3
	dd	_145
	dd	_62
	dd	276
	dd	3
	dd	_146
	dd	_62
	dd	280
	dd	3
	dd	_147
	dd	_62
	dd	284
	dd	3
	dd	_148
	dd	_62
	dd	288
	dd	3
	dd	_149
	dd	_62
	dd	292
	dd	3
	dd	_150
	dd	_62
	dd	296
	dd	3
	dd	_151
	dd	_62
	dd	300
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_152
	dd	_153
	dd	520
	dd	6
	dd	_154
	dd	_155
	dd	580
	dd	0
	align	4
_skn3_maxguiex_Skn3PanelEx:
	dd	_maxgui_win32maxguiex_TWindowsPanel
	dd	_bbObjectFree
	dd	_142
	dd	304
	dd	__skn3_maxguiex_Skn3PanelEx_New
	dd	_bbObjectDtor
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__maxgui_maxgui_TGadget_SetFilter
	dd	__maxgui_maxgui_TGadget_HasDescendant
	dd	__maxgui_maxgui_TGadget__setparent
	dd	__maxgui_maxgui_TGadget_SelectionChanged
	dd	__maxgui_maxgui_TGadget_Handle
	dd	__maxgui_maxgui_TGadget_GetXPos
	dd	__maxgui_maxgui_TGadget_GetYPos
	dd	__maxgui_maxgui_TGadget_GetWidth
	dd	__maxgui_maxgui_TGadget_GetHeight
	dd	__maxgui_maxgui_TGadget_GetGroup
	dd	__maxgui_maxgui_TGadget_SetShape
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	dd	__maxgui_maxgui_TGadget_SetRect
	dd	__maxgui_maxgui_TGadget_LockLayout
	dd	__maxgui_maxgui_TGadget_SetLayout
	dd	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	dd	__maxgui_maxgui_TGadget_DoLayout
	dd	__maxgui_maxgui_TGadget_SetDataSource
	dd	__maxgui_maxgui_TGadget_KeysFromList
	dd	__maxgui_maxgui_TGadget_KeysFromObjectArray
	dd	__maxgui_maxgui_TGadget_SyncDataSource
	dd	__maxgui_maxgui_TGadget_SyncData
	dd	__maxgui_maxgui_TGadget_InsertItemFromKey
	dd	__maxgui_maxgui_TGadget_Clear
	dd	__maxgui_maxgui_TGadget_InsertItem
	dd	__maxgui_maxgui_TGadget_SetItem
	dd	__maxgui_maxgui_TGadget_RemoveItem
	dd	__maxgui_maxgui_TGadget_ItemCount
	dd	__maxgui_maxgui_TGadget_ItemText
	dd	__maxgui_maxgui_TGadget_ItemTip
	dd	__maxgui_maxgui_TGadget_ItemFlags
	dd	__maxgui_maxgui_TGadget_ItemIcon
	dd	__maxgui_maxgui_TGadget_ItemExtra
	dd	__maxgui_maxgui_TGadget_SetItemState
	dd	__maxgui_maxgui_TGadget_ItemState
	dd	__maxgui_maxgui_TGadget_SelectItem
	dd	__maxgui_maxgui_TGadget_SelectedItem
	dd	__maxgui_maxgui_TGadget_SelectedItems
	dd	__maxgui_maxgui_TGadget_Insert
	dd	__maxgui_win32maxguiex_TWindowsGadget_Query
	dd	__maxgui_maxgui_TGadget_CleanUp
	dd	__maxgui_win32maxguiex_TWindowsPanel_Free
	dd	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	dd	__maxgui_win32maxguiex_TWindowsPanel_Activate
	dd	__maxgui_win32maxguiex_TWindowsGadget_State
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	dd	__maxgui_maxgui_TGadget_SetIconStrip
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	dd	__maxgui_maxgui_TGadget_SetSelected
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	dd	__maxgui_maxgui_TGadget_SetSensitivity
	dd	__maxgui_maxgui_TGadget_GetSensitivity
	dd	__maxgui_maxgui_TGadget_SetWatch
	dd	__maxgui_maxgui_TGadget_GetWatch
	dd	__maxgui_win32maxguiex_TWindowsPanel_Class
	dd	__maxgui_maxgui_TGadget_GetStatusText
	dd	__maxgui_maxgui_TGadget_SetStatusText
	dd	__maxgui_maxgui_TGadget_GetMenu
	dd	__maxgui_maxgui_TGadget_PopupMenu
	dd	__maxgui_maxgui_TGadget_UpdateMenu
	dd	__maxgui_maxgui_TGadget_SetMinimumSize
	dd	__maxgui_maxgui_TGadget_SetMaximumSize
	dd	__maxgui_maxgui_TGadget_ClearListItems
	dd	__maxgui_maxgui_TGadget_InsertListItem
	dd	__maxgui_maxgui_TGadget_SetListItem
	dd	__maxgui_maxgui_TGadget_RemoveListItem
	dd	__maxgui_maxgui_TGadget_SetListItemState
	dd	__maxgui_maxgui_TGadget_ListItemState
	dd	__maxgui_maxgui_TGadget_RootNode
	dd	__maxgui_maxgui_TGadget_InsertNode
	dd	__maxgui_maxgui_TGadget_ModifyNode
	dd	__maxgui_maxgui_TGadget_SelectedNode
	dd	__maxgui_maxgui_TGadget_CountKids
	dd	__maxgui_maxgui_TGadget_ReplaceText
	dd	__maxgui_maxgui_TGadget_AddText
	dd	__maxgui_maxgui_TGadget_AreaText
	dd	__maxgui_maxgui_TGadget_AreaLen
	dd	__maxgui_maxgui_TGadget_LockText
	dd	__maxgui_maxgui_TGadget_UnlockText
	dd	__maxgui_maxgui_TGadget_SetTabs
	dd	__maxgui_maxgui_TGadget_GetCursorPos
	dd	__maxgui_maxgui_TGadget_GetSelectionLength
	dd	__maxgui_maxgui_TGadget_SetStyle
	dd	__maxgui_maxgui_TGadget_SetSelection
	dd	__maxgui_maxgui_TGadget_CharX
	dd	__maxgui_maxgui_TGadget_CharY
	dd	__maxgui_maxgui_TGadget_CharAt
	dd	__maxgui_maxgui_TGadget_LineAt
	dd	__maxgui_maxgui_TGadget_SetValue
	dd	__maxgui_maxgui_TGadget_SetRange
	dd	__maxgui_maxgui_TGadget_SetStep
	dd	__maxgui_maxgui_TGadget_SetProp
	dd	__maxgui_maxgui_TGadget_GetProp
	dd	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	dd	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	dd	__maxgui_maxgui_TGadget_Run
	dd	__maxgui_win32maxguiex_TWindowsPanel_Create
	dd	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	dd	__maxgui_win32maxguiex_TWindowsGadget_Register
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	dd	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	dd	__maxgui_win32maxguiex_TWindowsGadget_isControl
	dd	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	dd	__skn3_maxguiex_Skn3PanelEx_WndProc
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	dd	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	dd	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	dd	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	dd	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	dd	__skn3_maxguiex_Skn3PanelEx_SetGradient
_368:
	db	"Self",0
_369:
	db	":Skn3ListBatchLock",0
	align	4
_367:
	dd	1
	dd	_71
	dd	2
	dd	_368
	dd	_369
	dd	-4
	dd	0
	align	4
_366:
	dd	3
	dd	0
	dd	0
_395:
	db	"Gadget",0
_396:
	db	":maxgui.maxgui.TGadget",0
_397:
	db	"listBoxLock",0
_398:
	db	"listBoxLockLink",0
	align	4
_394:
	dd	1
	dd	_73
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_397
	dd	_369
	dd	-8
	dd	2
	dd	_398
	dd	_67
	dd	-12
	dd	0
	align	4
_370:
	dd	_354
	dd	69
	dd	3
	align	4
_373:
	dd	3
	dd	0
	dd	0
	align	4
_372:
	dd	_354
	dd	69
	dd	17
	align	4
_374:
	dd	_354
	dd	71
	dd	3
	align	4
_376:
	dd	_354
	dd	72
	dd	3
	align	4
_380:
	dd	_354
	dd	73
	dd	3
	align	4
_393:
	dd	3
	dd	0
	dd	0
	align	4
_381:
	dd	_354
	dd	75
	dd	4
	align	4
_384:
	dd	_354
	dd	78
	dd	4
	align	4
_389:
	dd	3
	dd	0
	dd	0
	align	4
_388:
	dd	_354
	dd	78
	dd	36
	align	4
_390:
	dd	_354
	dd	81
	dd	4
_410:
	db	"Lock",0
	align	4
_409:
	dd	1
	dd	_75
	dd	2
	dd	_410
	dd	_369
	dd	-4
	dd	0
	align	4
_399:
	dd	_354
	dd	87
	dd	3
	align	4
_402:
	dd	3
	dd	0
	dd	0
	align	4
_401:
	dd	_354
	dd	87
	dd	17
	align	4
_403:
	dd	_354
	dd	88
	dd	3
	align	4
_427:
	dd	1
	dd	_77
	dd	2
	dd	_410
	dd	_369
	dd	-4
	dd	0
	align	4
_411:
	dd	_354
	dd	93
	dd	3
	align	4
_426:
	dd	3
	dd	0
	dd	0
	align	4
_413:
	dd	_354
	dd	94
	dd	4
	align	4
_418:
	dd	_354
	dd	95
	dd	4
	align	4
_422:
	dd	_354
	dd	96
	dd	4
_430:
	db	":Skn3CustomPointer",0
	align	4
_429:
	dd	1
	dd	_71
	dd	2
	dd	_368
	dd	_430
	dd	-4
	dd	0
	align	4
_428:
	dd	3
	dd	0
	dd	0
_433:
	db	":PARAFORMAT2",0
	align	4
_432:
	dd	1
	dd	_71
	dd	2
	dd	_368
	dd	_433
	dd	-4
	dd	0
	align	4
_431:
	dd	3
	dd	0
	dd	0
_500:
	db	"TrimAndFixPath",0
_501:
	db	"slash",0
_502:
	db	"keepRootSlash",0
_503:
	db	"startIndex",0
_504:
	db	"hasRootSlash",0
_505:
	db	"slashAsc",0
_506:
	db	"length",0
	align	4
_499:
	dd	1
	dd	_500
	dd	2
	dd	_80
	dd	_81
	dd	-4
	dd	2
	dd	_501
	dd	_81
	dd	-8
	dd	2
	dd	_502
	dd	_62
	dd	-12
	dd	2
	dd	_65
	dd	_62
	dd	-16
	dd	2
	dd	_503
	dd	_62
	dd	-20
	dd	2
	dd	_504
	dd	_62
	dd	-24
	dd	2
	dd	_505
	dd	_62
	dd	-28
	dd	2
	dd	_506
	dd	_62
	dd	-32
	dd	0
	align	4
_434:
	dd	_354
	dd	207
	dd	2
	align	4
_437:
	dd	3
	dd	0
	dd	0
	align	4
_436:
	dd	_354
	dd	207
	dd	21
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_438:
	dd	_354
	dd	210
	dd	2
	align	4
_21:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_23:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	92
	align	4
_439:
	dd	_354
	dd	211
	dd	2
	align	4
_442:
	dd	3
	dd	0
	dd	0
	align	4
_441:
	dd	_354
	dd	211
	dd	18
	align	4
_443:
	dd	_354
	dd	214
	dd	2
	align	4
_445:
	dd	_354
	dd	215
	dd	2
	align	4
_447:
	dd	_354
	dd	216
	dd	2
	align	4
_449:
	dd	_354
	dd	217
	dd	2
	align	4
_451:
	dd	_354
	dd	219
	dd	2
	align	4
_468:
	dd	3
	dd	0
	dd	0
	align	4
_454:
	dd	_354
	dd	220
	dd	3
	align	4
_465:
	dd	3
	dd	0
	dd	0
	align	4
_464:
	dd	_354
	dd	220
	dd	52
	align	4
_466:
	dd	_354
	dd	221
	dd	3
	align	4
_467:
	dd	_354
	dd	222
	dd	3
	align	4
_469:
	dd	_354
	dd	225
	dd	2
	align	4
_471:
	dd	_354
	dd	226
	dd	2
	align	4
_486:
	dd	3
	dd	0
	dd	0
	align	4
_473:
	dd	_354
	dd	227
	dd	3
	align	4
_484:
	dd	3
	dd	0
	dd	0
	align	4
_483:
	dd	_354
	dd	227
	dd	52
	align	4
_485:
	dd	_354
	dd	228
	dd	3
	align	4
_487:
	dd	_354
	dd	230
	dd	2
	align	4
_490:
	dd	3
	dd	0
	dd	0
	align	4
_489:
	dd	_354
	dd	230
	dd	17
	align	4
_491:
	dd	_354
	dd	233
	dd	2
	align	4
_492:
	dd	_354
	dd	234
	dd	2
	align	4
_497:
	dd	3
	dd	0
	dd	0
	align	4
_496:
	dd	_354
	dd	234
	dd	36
	align	4
_498:
	dd	_354
	dd	237
	dd	2
_547:
	db	"IncBinToDisk",0
	align	4
_546:
	dd	1
	dd	_547
	dd	2
	dd	_80
	dd	_81
	dd	-4
	dd	0
	align	4
_507:
	dd	_354
	dd	242
	dd	2
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	105,110,99,98,105,110,58,58
_538:
	db	"pathBase",0
_539:
	db	"pathCount",0
_540:
	db	"pathFile",0
_541:
	db	"path2",0
_542:
	db	"in",0
_543:
	db	":brl.stream.TStream",0
_544:
	db	"out",0
	align	4
_537:
	dd	3
	dd	0
	dd	2
	dd	_538
	dd	_81
	dd	-8
	dd	2
	dd	_539
	dd	_81
	dd	-12
	dd	2
	dd	_540
	dd	_81
	dd	-16
	dd	2
	dd	_541
	dd	_81
	dd	-20
	dd	2
	dd	_542
	dd	_543
	dd	-24
	dd	2
	dd	_544
	dd	_543
	dd	-28
	dd	0
	align	4
_509:
	dd	_354
	dd	244
	dd	3
	align	4
_511:
	dd	_354
	dd	245
	dd	3
	align	4
_513:
	dd	_354
	dd	246
	dd	3
	align	4
_515:
	dd	_354
	dd	248
	dd	3
	align	4
_517:
	dd	3
	dd	0
	dd	0
	align	4
_516:
	dd	_354
	dd	249
	dd	4
	align	4
_518:
	dd	_354
	dd	253
	dd	3
	align	4
_520:
	dd	_354
	dd	256
	dd	3
	align	4
_522:
	dd	_354
	dd	257
	dd	3
	align	4
_525:
	dd	3
	dd	0
	dd	0
	align	4
_524:
	dd	_354
	dd	257
	dd	16
	align	4
_526:
	dd	_354
	dd	258
	dd	3
	align	4
_528:
	dd	_354
	dd	259
	dd	3
	align	4
_532:
	dd	3
	dd	0
	dd	0
	align	4
_530:
	dd	_354
	dd	260
	dd	4
	align	4
_531:
	dd	_354
	dd	261
	dd	4
	align	4
_533:
	dd	_354
	dd	263
	dd	3
	align	4
_534:
	dd	_354
	dd	264
	dd	3
	align	4
_535:
	dd	_354
	dd	265
	dd	3
	align	4
_536:
	dd	_354
	dd	268
	dd	3
	align	4
_545:
	dd	_354
	dd	272
	dd	2
_556:
	db	"PointInRect",0
_557:
	db	"pointX",0
_558:
	db	"f",0
_559:
	db	"pointY",0
_560:
	db	"rectX",0
_561:
	db	"rectY",0
_562:
	db	"rectWidth",0
_563:
	db	"rectHeight",0
	align	4
_555:
	dd	1
	dd	_556
	dd	2
	dd	_557
	dd	_558
	dd	-4
	dd	2
	dd	_559
	dd	_558
	dd	-8
	dd	2
	dd	_560
	dd	_558
	dd	-12
	dd	2
	dd	_561
	dd	_558
	dd	-16
	dd	2
	dd	_562
	dd	_558
	dd	-20
	dd	2
	dd	_563
	dd	_558
	dd	-24
	dd	0
	align	4
_548:
	dd	_354
	dd	277
	dd	2
_566:
	db	"RequestScrollbarSize",0
	align	4
_565:
	dd	1
	dd	_566
	dd	0
	align	4
_564:
	dd	_354
	dd	299
	dd	2
_570:
	db	"SetComboBoxHeight",0
_571:
	db	"comboBox",0
_572:
	db	"Height",0
	align	4
_569:
	dd	1
	dd	_570
	dd	2
	dd	_571
	dd	_396
	dd	-4
	dd	2
	dd	_572
	dd	_62
	dd	-8
	dd	0
	align	4
_567:
	dd	_354
	dd	316
	dd	3
	align	4
_568:
	dd	_354
	dd	317
	dd	3
_599:
	db	"GadgetScreenPosition",0
_600:
	db	"gadget",0
_601:
	db	"client",0
_602:
	db	"point",0
_603:
	db	"l",0
_604:
	db	"Position",0
_605:
	db	"[]i",0
	align	4
_598:
	dd	1
	dd	_599
	dd	2
	dd	_600
	dd	_396
	dd	-12
	dd	2
	dd	_601
	dd	_62
	dd	-16
	dd	2
	dd	_602
	dd	_603
	dd	-8
	dd	2
	dd	_604
	dd	_605
	dd	-20
	dd	0
	align	4
_573:
	dd	_354
	dd	336
	dd	3
	align	4
_575:
	dd	_354
	dd	337
	dd	3
	align	4
_580:
	dd	3
	dd	0
	dd	0
	align	4
_577:
	dd	_354
	dd	338
	dd	4
	align	4
_585:
	dd	3
	dd	0
	dd	0
	align	4
_582:
	dd	_354
	dd	340
	dd	4
	align	4
_586:
	dd	_354
	dd	343
	dd	3
_587:
	db	"i",0
	align	4
_589:
	dd	_354
	dd	344
	dd	3
	align	4
_593:
	dd	_354
	dd	345
	dd	3
	align	4
_597:
	dd	_354
	dd	349
	dd	2
_632:
	db	"PointOverGadget",0
_633:
	db	"targetGadget",0
_634:
	db	"sourceGadget",0
_635:
	db	"targetPosition",0
	align	4
_631:
	dd	1
	dd	_632
	dd	2
	dd	_557
	dd	_62
	dd	-4
	dd	2
	dd	_559
	dd	_62
	dd	-8
	dd	2
	dd	_633
	dd	_396
	dd	-12
	dd	2
	dd	_634
	dd	_396
	dd	-16
	dd	2
	dd	_635
	dd	_605
	dd	-20
	dd	0
	align	4
_606:
	dd	_354
	dd	368
	dd	2
	align	4
_611:
	dd	3
	dd	0
	dd	0
	align	4
_610:
	dd	_354
	dd	368
	dd	55
	align	4
_612:
	dd	_354
	dd	371
	dd	2
_623:
	db	"sourcePosition",0
	align	4
_622:
	dd	3
	dd	0
	dd	2
	dd	_623
	dd	_605
	dd	-24
	dd	0
	align	4
_614:
	dd	_354
	dd	372
	dd	3
	align	4
_616:
	dd	_354
	dd	373
	dd	3
	align	4
_619:
	dd	_354
	dd	374
	dd	3
	align	4
_624:
	dd	_354
	dd	378
	dd	2
	align	4
_626:
	dd	_354
	dd	380
	dd	2
_639:
	db	"DisableGadgetRedraw",0
	align	4
_638:
	dd	1
	dd	_639
	dd	2
	dd	_600
	dd	_396
	dd	-4
	dd	0
	align	4
_636:
	dd	_354
	dd	399
	dd	3
	align	4
_637:
	dd	_354
	dd	400
	dd	3
_643:
	db	"EnableGadgetRedraw",0
	align	4
_642:
	dd	1
	dd	_643
	dd	2
	dd	_600
	dd	_396
	dd	-4
	dd	0
	align	4
_640:
	dd	_354
	dd	419
	dd	3
	align	4
_641:
	dd	_354
	dd	420
	dd	3
_650:
	db	"MessageBox",0
_651:
	db	"title",0
_652:
	db	"message",0
_653:
	db	"parent",0
_654:
	db	"oldTitle",0
	align	4
_649:
	dd	1
	dd	_650
	dd	2
	dd	_651
	dd	_81
	dd	-4
	dd	2
	dd	_652
	dd	_81
	dd	-8
	dd	2
	dd	_653
	dd	_396
	dd	-12
	dd	2
	dd	_654
	dd	_81
	dd	-16
	dd	0
	align	4
_644:
	dd	_354
	dd	439
	dd	2
	align	4
_646:
	dd	_354
	dd	440
	dd	2
	align	4
_647:
	dd	_354
	dd	441
	dd	2
	align	4
_648:
	dd	_354
	dd	442
	dd	2
_705:
	db	"GadgetSizeForString",0
_706:
	db	"text",0
_707:
	db	"maxWidth",0
	align	4
_704:
	dd	1
	dd	_705
	dd	2
	dd	_600
	dd	_396
	dd	-4
	dd	2
	dd	_706
	dd	_81
	dd	-8
	dd	2
	dd	_707
	dd	_62
	dd	-12
	dd	2
	dd	_70
	dd	_62
	dd	-16
	dd	0
	align	4
_655:
	dd	_354
	dd	462
	dd	3
	align	4
_657:
	dd	_354
	dd	465
	dd	3
_686:
	db	"dc",0
_687:
	db	"font",0
_688:
	db	"rect",0
_689:
	db	"flags",0
	align	4
_685:
	dd	3
	dd	0
	dd	2
	dd	_686
	dd	_62
	dd	-20
	dd	2
	dd	_687
	dd	_62
	dd	-24
	dd	2
	dd	_688
	dd	_605
	dd	-28
	dd	2
	dd	_689
	dd	_62
	dd	-32
	dd	0
	align	4
_661:
	dd	_354
	dd	488
	dd	5
	align	4
_663:
	dd	_354
	dd	491
	dd	5
	align	4
_665:
	dd	_354
	dd	492
	dd	5
	align	4
_666:
	dd	_354
	dd	495
	dd	5
	align	4
_669:
	dd	_354
	dd	497
	dd	5
	align	4
_671:
	dd	_354
	dd	498
	dd	5
	align	4
_674:
	dd	3
	dd	0
	dd	0
	align	4
_673:
	dd	_354
	dd	498
	dd	21
	align	4
_675:
	dd	_354
	dd	501
	dd	5
	align	4
_678:
	dd	_354
	dd	504
	dd	5
	align	4
_679:
	dd	_354
	dd	507
	dd	5
_702:
	db	"size",0
_703:
	db	"oldText",0
	align	4
_701:
	dd	3
	dd	0
	dd	2
	dd	_702
	dd	_605
	dd	-36
	dd	2
	dd	_703
	dd	_81
	dd	-40
	dd	0
	align	4
_690:
	dd	_354
	dd	468
	dd	5
	align	4
_693:
	dd	_354
	dd	471
	dd	5
	align	4
_694:
	dd	_354
	dd	474
	dd	5
	align	4
_696:
	dd	_354
	dd	475
	dd	5
	align	4
_697:
	dd	_354
	dd	478
	dd	5
	align	4
_698:
	dd	_354
	dd	481
	dd	5
	align	4
_699:
	dd	_354
	dd	482
	dd	5
	align	4
_700:
	dd	_354
	dd	485
	dd	5
_718:
	db	"GetCreationGroup",0
_719:
	db	"tmpProxy",0
_720:
	db	":maxgui.maxgui.TProxyGadget",0
	align	4
_717:
	dd	1
	dd	_718
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_719
	dd	_720
	dd	-8
	dd	0
	align	4
_708:
	dd	_354
	dd	530
	dd	2
	align	4
_710:
	dd	_354
	dd	531
	dd	2
	align	4
_715:
	dd	3
	dd	0
	dd	0
	align	4
_712:
	dd	_354
	dd	531
	dd	19
	align	4
_716:
	dd	_354
	dd	532
	dd	2
_730:
	db	"SetGadgetReadOnly",0
_731:
	db	"yes",0
	align	4
_729:
	dd	1
	dd	_730
	dd	2
	dd	_600
	dd	_396
	dd	-4
	dd	2
	dd	_731
	dd	_62
	dd	-8
	dd	0
	align	4
_721:
	dd	_354
	dd	549
	dd	2
	align	4
_728:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-12
	dd	0
	align	4
_725:
	dd	_354
	dd	552
	dd	5
	align	4
_727:
	dd	_354
	dd	553
	dd	5
_743:
	db	"SetGadgetMaxLength",0
	align	4
_742:
	dd	1
	dd	_743
	dd	2
	dd	_600
	dd	_396
	dd	-4
	dd	2
	dd	_506
	dd	_62
	dd	-8
	dd	0
	align	4
_732:
	dd	_354
	dd	573
	dd	2
	align	4
_741:
	dd	3
	dd	0
	dd	0
	align	4
_736:
	dd	_354
	dd	576
	dd	5
	align	4
_739:
	dd	3
	dd	0
	dd	0
	align	4
_738:
	dd	_354
	dd	576
	dd	19
	align	4
_740:
	dd	_354
	dd	577
	dd	5
_753:
	db	"GetGadgetMaxLength",0
	align	4
_752:
	dd	1
	dd	_753
	dd	2
	dd	_600
	dd	_396
	dd	-4
	dd	0
	align	4
_744:
	dd	_354
	dd	599
	dd	2
	align	4
_749:
	dd	3
	dd	0
	dd	0
	align	4
_748:
	dd	_354
	dd	607
	dd	4
	align	4
_751:
	dd	3
	dd	0
	dd	0
	align	4
_750:
	dd	_354
	dd	602
	dd	5
_808:
	db	"LoadCustomPointer",0
_809:
	db	"deletePath2",0
	align	4
_807:
	dd	1
	dd	_808
	dd	2
	dd	_80
	dd	_81
	dd	-4
	dd	2
	dd	_82
	dd	_430
	dd	-8
	dd	2
	dd	_541
	dd	_81
	dd	-12
	dd	2
	dd	_809
	dd	_62
	dd	-16
	dd	0
	align	4
_754:
	dd	_354
	dd	625
	dd	2
	align	4
_756:
	dd	_354
	dd	626
	dd	2
	align	4
_758:
	dd	_354
	dd	627
	dd	2
	align	4
_760:
	dd	_354
	dd	628
	dd	2
	align	4
_763:
	dd	3
	dd	0
	dd	0
	align	4
_762:
	dd	_354
	dd	629
	dd	3
	align	4
_766:
	dd	3
	dd	0
	dd	0
	align	4
_765:
	dd	_354
	dd	631
	dd	3
	align	4
_767:
	dd	_354
	dd	635
	dd	2
	align	4
_770:
	dd	3
	dd	0
	dd	0
	align	4
_769:
	dd	_354
	dd	637
	dd	3
	align	4
_775:
	dd	3
	dd	0
	dd	0
	align	4
_772:
	dd	_354
	dd	640
	dd	3
	align	4
_776:
	dd	_354
	dd	644
	dd	2
	align	4
_792:
	dd	3
	dd	0
	dd	0
	align	4
_778:
	dd	_354
	dd	646
	dd	3
	align	4
_779:
	dd	_354
	dd	647
	dd	3
	align	4
_783:
	dd	_354
	dd	648
	dd	3
	align	4
_786:
	dd	_354
	dd	652
	dd	3
	align	4
_793:
	dd	_354
	dd	662
	dd	2
	align	4
_796:
	dd	3
	dd	0
	dd	0
	align	4
_795:
	dd	_354
	dd	662
	dd	17
	align	4
_797:
	dd	_354
	dd	665
	dd	2
	align	4
_806:
	dd	3
	dd	0
	dd	0
	align	4
_801:
	dd	_354
	dd	667
	dd	3
	align	4
_805:
	dd	_354
	dd	668
	dd	3
_827:
	db	"SetCustomPointer",0
	align	4
_826:
	dd	1
	dd	_827
	dd	2
	dd	_82
	dd	_430
	dd	-4
	dd	0
	align	4
_810:
	dd	_354
	dd	685
	dd	2
	align	4
_825:
	dd	3
	dd	0
	dd	0
	align	4
_812:
	dd	_354
	dd	686
	dd	3
	align	4
_813:
	dd	_354
	dd	688
	dd	3
	align	4
_816:
	dd	_354
	dd	689
	dd	3
	align	4
_819:
	dd	_354
	dd	690
	dd	3
	align	4
_824:
	dd	3
	dd	0
	dd	0
	align	4
_821:
	dd	_354
	dd	690
	dd	39
_859:
	db	"FreeCustomPointer",0
	align	4
_858:
	dd	1
	dd	_859
	dd	2
	dd	_82
	dd	_430
	dd	-4
	dd	0
	align	4
_828:
	dd	_354
	dd	710
	dd	2
	align	4
_857:
	dd	3
	dd	0
	dd	0
	align	4
_830:
	dd	_354
	dd	712
	dd	3
	align	4
_834:
	dd	_354
	dd	713
	dd	3
	align	4
_856:
	dd	3
	dd	0
	dd	0
	align	4
_838:
	dd	_354
	dd	715
	dd	4
	align	4
_843:
	dd	_354
	dd	720
	dd	4
	align	4
_848:
	dd	3
	dd	0
	dd	0
	align	4
_847:
	dd	_354
	dd	720
	dd	51
	align	4
_849:
	dd	_354
	dd	722
	dd	4
	align	4
_852:
	dd	_354
	dd	723
	dd	4
_911:
	db	"ExtractCursorHotspot",0
_912:
	db	"result",0
_913:
	db	"file",0
	align	4
_910:
	dd	1
	dd	_911
	dd	2
	dd	_80
	dd	_81
	dd	-4
	dd	2
	dd	_65
	dd	_62
	dd	-8
	dd	2
	dd	_912
	dd	_605
	dd	-12
	dd	2
	dd	_913
	dd	_543
	dd	-16
	dd	0
	align	4
_860:
	dd	_354
	dd	747
	dd	2
_861:
	db	"i",0
	align	4
_863:
	dd	_354
	dd	749
	dd	2
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	108,105,116,116,108,101,101,110,100,105,97,110,58,58
	align	4
_865:
	dd	_354
	dd	750
	dd	2
_908:
	db	"temp",0
	align	4
_907:
	dd	3
	dd	0
	dd	2
	dd	_908
	dd	_62
	dd	-20
	dd	0
	align	4
_867:
	dd	_354
	dd	752
	dd	3
	align	4
_870:
	dd	_354
	dd	755
	dd	3
	align	4
_874:
	dd	_354
	dd	756
	dd	3
	align	4
_903:
	dd	3
	dd	0
	dd	0
	align	4
_876:
	dd	_354
	dd	758
	dd	4
	align	4
_879:
	dd	_354
	dd	759
	dd	4
	align	4
_902:
	dd	3
	dd	0
	dd	0
	align	4
_881:
	dd	_354
	dd	761
	dd	5
	align	4
_882:
	dd	_354
	dd	762
	dd	5
	align	4
_901:
	dd	3
	dd	0
	dd	0
	align	4
_886:
	dd	_354
	dd	763
	dd	6
	align	4
_889:
	dd	_354
	dd	764
	dd	6
	align	4
_895:
	dd	_354
	dd	765
	dd	6
	align	4
_904:
	dd	_354
	dd	771
	dd	3
	align	4
_909:
	dd	_354
	dd	775
	dd	2
_958:
	db	"ListBatchLock",0
	align	4
_957:
	dd	1
	dd	_958
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_410
	dd	_369
	dd	-8
	dd	2
	dd	_63
	dd	_64
	dd	-12
	dd	0
	align	4
_914:
	dd	_354
	dd	792
	dd	3
	align	4
_916:
	dd	_354
	dd	793
	dd	3
	align	4
_923:
	dd	3
	dd	0
	dd	0
	align	4
_918:
	dd	_354
	dd	795
	dd	4
	align	4
_922:
	dd	_354
	dd	796
	dd	4
	align	4
_924:
	dd	_354
	dd	800
	dd	3
	align	4
_926:
	dd	_354
	dd	801
	dd	3
	align	4
_929:
	dd	3
	dd	0
	dd	0
	align	4
_928:
	dd	_354
	dd	801
	dd	21
	align	4
_930:
	dd	_354
	dd	804
	dd	3
	align	4
_931:
	dd	_354
	dd	805
	dd	3
	align	4
_935:
	dd	_354
	dd	806
	dd	3
	align	4
_939:
	dd	_354
	dd	807
	dd	3
	align	4
_945:
	dd	_354
	dd	808
	dd	3
	align	4
_949:
	dd	_354
	dd	809
	dd	3
	align	4
_953:
	dd	_354
	dd	812
	dd	3
	align	4
_956:
	dd	_354
	dd	815
	dd	3
_1041:
	db	"ListBatchAdd",0
_1042:
	db	"icon",0
_1043:
	db	"tip",0
_1044:
	db	"extra",0
_1045:
	db	":Object",0
_1046:
	db	"item",0
_1047:
	db	":maxgui.maxgui.TGadgetItem",0
	align	4
_1040:
	dd	1
	dd	_1041
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_706
	dd	_81
	dd	-8
	dd	2
	dd	_689
	dd	_62
	dd	-12
	dd	2
	dd	_1042
	dd	_62
	dd	-16
	dd	2
	dd	_1043
	dd	_81
	dd	-20
	dd	2
	dd	_1044
	dd	_1045
	dd	-24
	dd	2
	dd	_410
	dd	_369
	dd	-28
	dd	2
	dd	_1046
	dd	_1047
	dd	-32
	dd	0
	align	4
_959:
	dd	_354
	dd	834
	dd	3
	align	4
_961:
	dd	_354
	dd	837
	dd	3
	align	4
_965:
	dd	3
	dd	0
	dd	0
	align	4
_963:
	dd	_354
	dd	838
	dd	4
	align	4
_964:
	dd	_354
	dd	839
	dd	4
	align	4
_966:
	dd	_354
	dd	844
	dd	3
	align	4
_968:
	dd	_354
	dd	845
	dd	3
	align	4
_971:
	dd	_354
	dd	848
	dd	3
_979:
	db	":TGadgetItem",0
	align	4
_980:
	dd	_354
	dd	849
	dd	3
	align	4
_989:
	dd	_354
	dd	852
	dd	3
	align	4
_995:
	dd	_354
	dd	853
	dd	3
	align	4
_1003:
	dd	_354
	dd	854
	dd	3
	align	4
_1011:
	dd	_354
	dd	857
	dd	4
	align	4
_1017:
	dd	_354
	dd	858
	dd	4
	align	4
_1025:
	dd	_354
	dd	861
	dd	3
	align	4
_1031:
	dd	_354
	dd	862
	dd	3
	align	4
_1036:
	dd	_354
	dd	865
	dd	3
_1085:
	db	"ListBatchUnlock",0
	align	4
_1084:
	dd	1
	dd	_1085
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_410
	dd	_369
	dd	-8
	dd	0
	align	4
_1048:
	dd	_354
	dd	887
	dd	3
	align	4
_1050:
	dd	_354
	dd	888
	dd	3
	align	4
_1053:
	dd	3
	dd	0
	dd	0
	align	4
_1052:
	dd	_354
	dd	888
	dd	18
	align	4
_1054:
	dd	_354
	dd	891
	dd	3
	align	4
_1058:
	dd	_354
	dd	894
	dd	3
	align	4
_1083:
	dd	3
	dd	0
	dd	0
	align	4
_1062:
	dd	_354
	dd	896
	dd	4
	align	4
_1065:
	dd	_354
	dd	897
	dd	4
	align	4
_1076:
	dd	3
	dd	0
	dd	0
	align	4
_1071:
	dd	_354
	dd	897
	dd	46
	align	4
_1077:
	dd	_354
	dd	898
	dd	4
	align	4
_1082:
	dd	_354
	dd	901
	dd	4
_1096:
	db	"GadgetWindow",0
	align	4
_1095:
	dd	1
	dd	_1096
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_653
	dd	_396
	dd	-8
	dd	0
	align	4
_1086:
	dd	_354
	dd	921
	dd	2
	align	4
_1088:
	dd	_354
	dd	922
	dd	2
	align	4
_1094:
	dd	3
	dd	0
	dd	0
	align	4
_1089:
	dd	_354
	dd	923
	dd	3
	align	4
_1092:
	dd	3
	dd	0
	dd	0
	align	4
_1091:
	dd	_354
	dd	923
	dd	42
	align	4
_1093:
	dd	_354
	dd	924
	dd	3
_1110:
	db	"SetWindowAlwaysOnTop",0
_1111:
	db	"Window",0
_1112:
	db	"State",0
	align	4
_1109:
	dd	1
	dd	_1110
	dd	2
	dd	_1111
	dd	_396
	dd	-4
	dd	2
	dd	_1112
	dd	_62
	dd	-8
	dd	2
	dd	_70
	dd	_62
	dd	-12
	dd	0
	align	4
_1097:
	dd	_354
	dd	942
	dd	3
	align	4
_1099:
	dd	_354
	dd	943
	dd	3
	align	4
_1108:
	dd	3
	dd	0
	dd	0
	align	4
_1101:
	dd	_354
	dd	944
	dd	4
	align	4
_1104:
	dd	3
	dd	0
	dd	0
	align	4
_1103:
	dd	_354
	dd	946
	dd	5
	align	4
_1107:
	dd	3
	dd	0
	dd	0
	align	4
_1106:
	dd	_354
	dd	948
	dd	5
_1120:
	db	"BringWindowToTop",0
	align	4
_1119:
	dd	1
	dd	_1120
	dd	2
	dd	_1111
	dd	_396
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1113:
	dd	_354
	dd	970
	dd	3
	align	4
_1115:
	dd	_354
	dd	971
	dd	3
	align	4
_1118:
	dd	3
	dd	0
	dd	0
	align	4
_1117:
	dd	_354
	dd	971
	dd	11
_1128:
	db	"FocusWindow",0
	align	4
_1127:
	dd	1
	dd	_1128
	dd	2
	dd	_1111
	dd	_396
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1121:
	dd	_354
	dd	991
	dd	3
	align	4
_1123:
	dd	_354
	dd	992
	dd	3
	align	4
_1126:
	dd	3
	dd	0
	dd	0
	align	4
_1125:
	dd	_354
	dd	992
	dd	11
_1131:
	db	"GadgetToInt",0
	align	4
_1130:
	dd	1
	dd	_1131
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	0
	align	4
_1129:
	dd	_354
	dd	1015
	dd	3
_1147:
	db	"SetColorPickerCustomColors",0
_1148:
	db	"colors",0
	align	4
_1146:
	dd	1
	dd	_1147
	dd	2
	dd	_1148
	dd	_605
	dd	-4
	dd	0
	align	4
_1132:
	dd	_354
	dd	1040
	dd	3
	align	4
_1144:
	dd	3
	dd	0
	dd	2
	dd	_65
	dd	_62
	dd	-8
	dd	0
	align	4
_1134:
	dd	_354
	dd	1041
	dd	4
	align	4
_1136:
	dd	_354
	dd	1042
	dd	4
	align	4
_1137:
	dd	_354
	dd	1043
	dd	4
	align	4
_1143:
	dd	3
	dd	0
	dd	0
	align	4
_1139:
	dd	_354
	dd	1044
	dd	5
	align	4
_1145:
	dd	_354
	dd	1048
	dd	3
_1158:
	db	"ClearColorPickerCustomColors",0
	align	4
_1157:
	dd	1
	dd	_1158
	dd	0
	align	4
_1149:
	dd	_354
	dd	1078
	dd	3
	align	4
_1156:
	dd	3
	dd	0
	dd	2
	dd	_65
	dd	_62
	dd	-4
	dd	0
	align	4
_1152:
	dd	_354
	dd	1079
	dd	4
_1167:
	db	"RedrawGadgetFrame",0
	align	4
_1166:
	dd	1
	dd	_1167
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1159:
	dd	_354
	dd	1098
	dd	3
	align	4
_1161:
	dd	_354
	dd	1099
	dd	3
	align	4
_1165:
	dd	3
	dd	0
	dd	0
	align	4
_1163:
	dd	_354
	dd	1100
	dd	4
	align	4
_1164:
	dd	_354
	dd	1101
	dd	4
_1202:
	db	"HideGadgetBorder",0
	align	4
_1201:
	dd	1
	dd	_1202
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	0
	align	4
_1168:
	dd	_354
	dd	1120
	dd	3
	align	4
_1200:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1172:
	dd	_354
	dd	1122
	dd	5
	align	4
_1174:
	dd	_354
	dd	1123
	dd	5
_1197:
	db	"Style",0
_1198:
	db	"styleEx",0
_1199:
	db	"changed",0
	align	4
_1196:
	dd	3
	dd	0
	dd	2
	dd	_1197
	dd	_62
	dd	-12
	dd	2
	dd	_1198
	dd	_62
	dd	-16
	dd	2
	dd	_1199
	dd	_62
	dd	-20
	dd	0
	align	4
_1176:
	dd	_354
	dd	1124
	dd	6
	align	4
_1178:
	dd	_354
	dd	1125
	dd	6
	align	4
_1180:
	dd	_354
	dd	1128
	dd	6
	align	4
_1182:
	dd	_354
	dd	1129
	dd	6
	align	4
_1186:
	dd	3
	dd	0
	dd	0
	align	4
_1184:
	dd	_354
	dd	1130
	dd	7
	align	4
_1185:
	dd	_354
	dd	1131
	dd	7
	align	4
_1187:
	dd	_354
	dd	1133
	dd	6
	align	4
_1191:
	dd	3
	dd	0
	dd	0
	align	4
_1189:
	dd	_354
	dd	1134
	dd	7
	align	4
_1190:
	dd	_354
	dd	1135
	dd	7
	align	4
_1192:
	dd	_354
	dd	1139
	dd	6
	align	4
_1195:
	dd	3
	dd	0
	dd	0
	align	4
_1194:
	dd	_354
	dd	1139
	dd	17
_1217:
	db	"InstallGuiFont",0
	align	4
_1216:
	dd	1
	dd	_1217
	dd	2
	dd	_80
	dd	_81
	dd	-4
	dd	0
	align	4
_1203:
	dd	_354
	dd	1168
	dd	3
_1210:
	db	"installed",0
	align	4
_1209:
	dd	3
	dd	0
	dd	2
	dd	_1210
	dd	_62
	dd	-8
	dd	0
	align	4
_1205:
	dd	_354
	dd	1170
	dd	4
	align	4
_1206:
	dd	_354
	dd	1171
	dd	4
	align	4
_1208:
	dd	_354
	dd	1172
	dd	4
	align	4
_1215:
	dd	3
	dd	0
	dd	0
	align	4
_1212:
	dd	_354
	dd	1175
	dd	4
_1247:
	db	"SetTextareaLineSpacing",0
_1248:
	db	"lineSpacing",0
	align	4
_1246:
	dd	1
	dd	_1247
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_1248
	dd	_558
	dd	-8
	dd	0
	align	4
_1218:
	dd	_354
	dd	1227
	dd	2
	align	4
_1245:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-12
	dd	0
	align	4
_1220:
	dd	_354
	dd	1230
	dd	4
	align	4
_1222:
	dd	_354
	dd	1231
	dd	4
_1244:
	db	"P",0
	align	4
_1243:
	dd	3
	dd	0
	dd	2
	dd	_1244
	dd	_433
	dd	-16
	dd	0
	align	4
_1224:
	dd	_354
	dd	1232
	dd	5
	align	4
_1226:
	dd	_354
	dd	1233
	dd	5
	align	4
_1230:
	dd	_354
	dd	1234
	dd	5
	align	4
_1234:
	dd	_354
	dd	1235
	dd	5
	align	4
_1238:
	dd	_354
	dd	1236
	dd	5
	align	4
_2268:
	dd	0x41a00000
	align	4
_1242:
	dd	_354
	dd	1237
	dd	5
_1259:
	db	"ScrollTextAreaToTop",0
	align	4
_1258:
	dd	1
	dd	_1259
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	0
	align	4
_1249:
	dd	_354
	dd	1264
	dd	2
	align	4
_1257:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1251:
	dd	_354
	dd	1266
	dd	4
	align	4
_1253:
	dd	_354
	dd	1267
	dd	4
	align	4
_1256:
	dd	3
	dd	0
	dd	0
	align	4
_1255:
	dd	_354
	dd	1267
	dd	12
_1270:
	db	"ScrollTextAreaToBottom",0
	align	4
_1269:
	dd	1
	dd	_1270
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	0
	align	4
_1260:
	dd	_354
	dd	1288
	dd	2
	align	4
_1268:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1262:
	dd	_354
	dd	1290
	dd	4
	align	4
_1264:
	dd	_354
	dd	1291
	dd	4
	align	4
_1267:
	dd	3
	dd	0
	dd	0
	align	4
_1266:
	dd	_354
	dd	1291
	dd	12
_1281:
	db	"ScrollTextAreaToCursor",0
	align	4
_1280:
	dd	1
	dd	_1281
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	0
	align	4
_1271:
	dd	_354
	dd	1312
	dd	2
	align	4
_1279:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1273:
	dd	_354
	dd	1314
	dd	4
	align	4
_1275:
	dd	_354
	dd	1315
	dd	4
	align	4
_1278:
	dd	3
	dd	0
	dd	0
	align	4
_1277:
	dd	_354
	dd	1315
	dd	12
_1284:
	db	"GetAppResourcesPath",0
	align	4
_1283:
	dd	1
	dd	_1284
	dd	0
	align	4
_1282:
	dd	_354
	dd	1339
	dd	3
_1290:
	db	"AssignGadgetClassId",0
_1291:
	db	"idCount",0
	align	4
_1286:
	dd	0
	align	4
_1289:
	dd	1
	dd	_1290
	dd	4
	dd	_1291
	dd	_62
	dd	_1286
	dd	0
	align	4
_1285:
	dd	_354
	dd	1360
	dd	2
	align	4
_1287:
	dd	_354
	dd	1361
	dd	2
	align	4
_1288:
	dd	_354
	dd	1362
	dd	2
_1294:
	db	":Skn3PanelEx",0
	align	4
_1293:
	dd	1
	dd	_71
	dd	2
	dd	_368
	dd	_1294
	dd	-4
	dd	0
	align	4
_1292:
	dd	3
	dd	0
	dd	0
_1811:
	db	"MSG",0
_1812:
	db	"wp",0
_1813:
	db	"lp",0
	align	4
_1810:
	dd	1
	dd	_152
	dd	2
	dd	_368
	dd	_1294
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	2
	dd	_1811
	dd	_62
	dd	-12
	dd	2
	dd	_1812
	dd	_62
	dd	-16
	dd	2
	dd	_1813
	dd	_62
	dd	-20
	dd	0
	align	4
_1295:
	dd	_354
	dd	1391
	dd	3
	align	4
_1808:
	dd	3
	dd	0
	dd	0
	align	4
_1299:
	dd	_354
	dd	1393
	dd	5
	align	4
_1304:
	dd	3
	dd	0
	dd	0
	align	4
_1303:
	dd	_354
	dd	1395
	dd	6
_1784:
	db	"hdc",0
_1785:
	db	"hdcCanvas",0
_1786:
	db	"hdcBitmap",0
_1787:
	db	"srcw",0
_1788:
	db	"srch",0
_1789:
	db	"x",0
_1790:
	db	"y",0
_1791:
	db	"xoffset",0
_1792:
	db	"yoffset",0
_1793:
	db	"clientRect",0
_1794:
	db	"UpdateRect",0
_1795:
	db	"clipRect",0
_1796:
	db	"windowRect",0
_1797:
	db	"previousBrush",0
_1798:
	db	"gradientSize",0
_1799:
	db	"gradientPosition",0
_1800:
	db	"gradientBrush",0
_1801:
	db	"gradientRect",0
_1802:
	db	"gradientR",0
_1803:
	db	"gradientG",0
_1804:
	db	"gradientB",0
_1805:
	db	"gradientStepR",0
_1806:
	db	"gradientStepG",0
_1807:
	db	"gradientStepB",0
	align	4
_1783:
	dd	3
	dd	0
	dd	2
	dd	_1784
	dd	_62
	dd	-24
	dd	2
	dd	_1785
	dd	_62
	dd	-28
	dd	2
	dd	_1786
	dd	_62
	dd	-32
	dd	2
	dd	_1787
	dd	_62
	dd	-36
	dd	2
	dd	_1788
	dd	_62
	dd	-40
	dd	2
	dd	_1789
	dd	_62
	dd	-44
	dd	2
	dd	_1790
	dd	_62
	dd	-48
	dd	2
	dd	_1791
	dd	_62
	dd	-52
	dd	2
	dd	_1792
	dd	_62
	dd	-56
	dd	2
	dd	_1793
	dd	_605
	dd	-60
	dd	2
	dd	_1794
	dd	_605
	dd	-64
	dd	2
	dd	_1795
	dd	_605
	dd	-68
	dd	2
	dd	_1796
	dd	_605
	dd	-72
	dd	2
	dd	_1797
	dd	_62
	dd	-76
	dd	2
	dd	_1798
	dd	_62
	dd	-80
	dd	2
	dd	_1799
	dd	_62
	dd	-84
	dd	2
	dd	_1800
	dd	_62
	dd	-88
	dd	2
	dd	_1801
	dd	_605
	dd	-92
	dd	2
	dd	_1802
	dd	_558
	dd	-96
	dd	2
	dd	_1803
	dd	_558
	dd	-100
	dd	2
	dd	_1804
	dd	_558
	dd	-104
	dd	2
	dd	_1805
	dd	_558
	dd	-108
	dd	2
	dd	_1806
	dd	_558
	dd	-112
	dd	2
	dd	_1807
	dd	_558
	dd	-116
	dd	0
	align	4
_1306:
	dd	_354
	dd	1398
	dd	6
	align	4
_1311:
	dd	3
	dd	0
	dd	0
	align	4
_1310:
	dd	_354
	dd	1398
	dd	34
	align	4
_1312:
	dd	_354
	dd	1399
	dd	6
	align	4
_1322:
	dd	_354
	dd	1400
	dd	6
_1323:
	db	"i",0
_1325:
	db	"i",0
_1327:
	db	"i",0
_1329:
	db	"i",0
	align	4
_1331:
	dd	_354
	dd	1401
	dd	6
	align	4
_1332:
	dd	_354
	dd	1402
	dd	6
	align	4
_1333:
	dd	_354
	dd	1403
	dd	6
	align	4
_1334:
	dd	_354
	dd	1404
	dd	6
	align	4
_1337:
	dd	3
	dd	0
	dd	0
	align	4
_1336:
	dd	_354
	dd	1404
	dd	58
	align	4
_1338:
	dd	_354
	dd	1405
	dd	6
	align	4
_1350:
	dd	3
	dd	0
	dd	0
	align	4
_1340:
	dd	_354
	dd	1405
	dd	38
	align	4
_1351:
	dd	_354
	dd	1408
	dd	6
	align	4
_1382:
	dd	3
	dd	0
	dd	0
	align	4
_1371:
	dd	_354
	dd	1409
	dd	7
	align	4
_1372:
	dd	_354
	dd	1410
	dd	7
	align	4
_1381:
	dd	_354
	dd	1411
	dd	7
	align	4
_1383:
	dd	_354
	dd	1415
	dd	6
	align	4
_1385:
	dd	_354
	dd	1416
	dd	6
	align	4
_1387:
	dd	_354
	dd	1417
	dd	6
	align	4
_1389:
	dd	_354
	dd	1418
	dd	6
	align	4
_1391:
	dd	_354
	dd	1419
	dd	6
	align	4
_1402:
	dd	_354
	dd	1420
	dd	6
	align	4
_1404:
	dd	_354
	dd	1421
	dd	6
	align	4
_1406:
	dd	_354
	dd	1422
	dd	6
	align	4
_1408:
	dd	_354
	dd	1423
	dd	6
	align	4
_1410:
	dd	_354
	dd	1424
	dd	6
	align	4
_1412:
	dd	_354
	dd	1425
	dd	6
	align	4
_1414:
	dd	_354
	dd	1428
	dd	6
	align	4
_1483:
	dd	3
	dd	0
	dd	0
	align	4
_1418:
	dd	_354
	dd	1429
	dd	7
	align	4
_1423:
	dd	_354
	dd	1430
	dd	7
	align	4
_1428:
	dd	_354
	dd	1431
	dd	7
	align	4
_1433:
	dd	_354
	dd	1432
	dd	7
	align	4
_1438:
	dd	_354
	dd	1436
	dd	7
	align	4
_1445:
	dd	_354
	dd	1437
	dd	7
	align	4
_1452:
	dd	_354
	dd	1438
	dd	7
	align	4
_1459:
	dd	_354
	dd	1441
	dd	7
	align	4
_1482:
	dd	3
	dd	0
	dd	0
	align	4
_1466:
	dd	_354
	dd	1443
	dd	8
	align	4
_1467:
	dd	_354
	dd	1444
	dd	8
	align	4
_1468:
	dd	_354
	dd	1447
	dd	8
	align	4
_1472:
	dd	_354
	dd	1448
	dd	8
	align	4
_1476:
	dd	_354
	dd	1451
	dd	8
	align	4
_1477:
	dd	_354
	dd	1454
	dd	8
	align	4
_1478:
	dd	_354
	dd	1455
	dd	8
	align	4
_1479:
	dd	_354
	dd	1458
	dd	8
	align	4
_1480:
	dd	_354
	dd	1459
	dd	8
	align	4
_1481:
	dd	_354
	dd	1460
	dd	8
	align	4
_1550:
	dd	3
	dd	0
	dd	0
	align	4
_1485:
	dd	_354
	dd	1463
	dd	7
	align	4
_1490:
	dd	_354
	dd	1464
	dd	7
	align	4
_1495:
	dd	_354
	dd	1465
	dd	7
	align	4
_1500:
	dd	_354
	dd	1466
	dd	7
	align	4
_1505:
	dd	_354
	dd	1470
	dd	7
	align	4
_1512:
	dd	_354
	dd	1471
	dd	7
	align	4
_1519:
	dd	_354
	dd	1472
	dd	7
	align	4
_1526:
	dd	_354
	dd	1474
	dd	7
	align	4
_1549:
	dd	3
	dd	0
	dd	0
	align	4
_1533:
	dd	_354
	dd	1476
	dd	8
	align	4
_1534:
	dd	_354
	dd	1477
	dd	8
	align	4
_1535:
	dd	_354
	dd	1480
	dd	8
	align	4
_1539:
	dd	_354
	dd	1481
	dd	8
	align	4
_1543:
	dd	_354
	dd	1484
	dd	8
	align	4
_1544:
	dd	_354
	dd	1487
	dd	8
	align	4
_1545:
	dd	_354
	dd	1488
	dd	8
	align	4
_1546:
	dd	_354
	dd	1491
	dd	8
	align	4
_1547:
	dd	_354
	dd	1492
	dd	8
	align	4
_1548:
	dd	_354
	dd	1493
	dd	8
	align	4
_1551:
	dd	_354
	dd	1498
	dd	6
	align	4
_1572:
	dd	3
	dd	0
	dd	0
	align	4
_1571:
	dd	_354
	dd	1498
	dd	107
	align	4
_1573:
	dd	_354
	dd	1501
	dd	6
	align	4
_1730:
	dd	3
	dd	0
	dd	0
	align	4
_1585:
	dd	_354
	dd	1502
	dd	7
	align	4
_1586:
	dd	_354
	dd	1503
	dd	7
	align	4
_1589:
	dd	_354
	dd	1504
	dd	7
	align	4
_1592:
	dd	_354
	dd	1505
	dd	7
	align	4
_1595:
	dd	_354
	dd	1506
	dd	7
	align	4
_1628:
	dd	3
	dd	0
	dd	0
	align	4
_1604:
	dd	_354
	dd	1508
	dd	9
	align	4
_1627:
	dd	3
	dd	0
	dd	0
	align	4
_1609:
	dd	_354
	dd	1509
	dd	10
	align	4
_1610:
	dd	_354
	dd	1510
	dd	10
	align	4
_1625:
	dd	3
	dd	0
	dd	0
	align	4
_1615:
	dd	_354
	dd	1511
	dd	11
	align	4
_1620:
	dd	3
	dd	0
	dd	0
	align	4
_1619:
	dd	_354
	dd	1512
	dd	12
	align	4
_1623:
	dd	3
	dd	0
	dd	0
	align	4
_1622:
	dd	_354
	dd	1514
	dd	12
	align	4
_1624:
	dd	_354
	dd	1516
	dd	11
	align	4
_1626:
	dd	_354
	dd	1518
	dd	10
	align	4
_1648:
	dd	3
	dd	0
	dd	0
	align	4
_1629:
	dd	_354
	dd	1521
	dd	9
	align	4
_1634:
	dd	_354
	dd	1522
	dd	9
	align	4
_1639:
	dd	_354
	dd	1523
	dd	9
	align	4
_1644:
	dd	3
	dd	0
	dd	0
	align	4
_1643:
	dd	_354
	dd	1524
	dd	10
	align	4
_1647:
	dd	3
	dd	0
	dd	0
	align	4
_1646:
	dd	_354
	dd	1526
	dd	10
_1698:
	db	"mx",0
_1699:
	db	"my",0
_1700:
	db	"w",0
_1701:
	db	"h",0
	align	4
_1697:
	dd	3
	dd	0
	dd	2
	dd	_1698
	dd	_558
	dd	-120
	dd	2
	dd	_1699
	dd	_558
	dd	-124
	dd	2
	dd	_1700
	dd	_62
	dd	-128
	dd	2
	dd	_1701
	dd	_62
	dd	-132
	dd	0
	align	4
_1649:
	dd	_354
	dd	1531
	dd	9
	align	4
_1655:
	dd	_354
	dd	1532
	dd	9
	align	4
_1661:
	dd	_354
	dd	1534
	dd	9
	align	4
_1672:
	dd	3
	dd	0
	dd	0
	align	4
_1663:
	dd	_354
	dd	1535
	dd	10
	align	4
_1668:
	dd	3
	dd	0
	dd	0
	align	4
_1667:
	dd	_354
	dd	1535
	dd	73
	align	4
_1671:
	dd	3
	dd	0
	dd	0
	align	4
_1670:
	dd	_354
	dd	1535
	dd	84
	align	4
_1673:
	dd	_354
	dd	1537
	dd	9
	align	4
_1675:
	dd	_354
	dd	1538
	dd	9
	align	4
_1677:
	dd	_354
	dd	1539
	dd	9
	align	4
_1682:
	dd	_354
	dd	1540
	dd	9
	align	4
_1687:
	dd	_354
	dd	1541
	dd	9
	align	4
_1688:
	dd	_354
	dd	1543
	dd	9
	align	4
_1693:
	dd	3
	dd	0
	dd	0
	align	4
_1692:
	dd	_354
	dd	1544
	dd	10
	align	4
_1696:
	dd	3
	dd	0
	dd	0
	align	4
_1695:
	dd	_354
	dd	1546
	dd	10
	align	4
_1728:
	dd	3
	dd	0
	dd	0
	align	4
_1702:
	dd	_354
	dd	1550
	dd	9
	align	4
_1703:
	dd	_354
	dd	1552
	dd	9
	align	4
_1716:
	dd	3
	dd	0
	dd	0
	align	4
_1707:
	dd	_354
	dd	1553
	dd	10
	align	4
_1727:
	dd	3
	dd	0
	dd	0
	align	4
_1718:
	dd	_354
	dd	1555
	dd	10
	align	4
_1729:
	dd	_354
	dd	1560
	dd	7
	align	4
_1731:
	dd	_354
	dd	1564
	dd	6
_1766:
	db	"blendfunction",0
	align	4
_1765:
	dd	3
	dd	0
	dd	2
	dd	_1766
	dd	_62
	dd	-136
	dd	0
	align	4
_1735:
	dd	_354
	dd	1565
	dd	7
	align	4
_1736:
	dd	_354
	dd	1566
	dd	7
	align	4
_2298:
	dd	0x437f0000
	align	4
_1740:
	dd	_354
	dd	1567
	dd	7
	align	4
_1777:
	dd	3
	dd	0
	dd	0
	align	4
_1768:
	dd	_354
	dd	1569
	dd	7
	align	4
_1778:
	dd	_354
	dd	1572
	dd	6
	align	4
_58:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	104,100,99,32,61,61,32,119,112,33,32,80,108,101,97,115
	dw	101,32,112,111,115,116,32,97,32,77,97,120,71,85,73,32
	dw	98,117,103,32,114,101,112,111,114,116,46
	align	4
_1780:
	dd	_354
	dd	1574
	dd	6
	align	4
_1781:
	dd	_354
	dd	1575
	dd	6
	align	4
_1782:
	dd	_354
	dd	1577
	dd	6
	align	4
_1809:
	dd	_354
	dd	1582
	dd	3
_1912:
	db	"on",0
_1913:
	db	"r1",0
_1914:
	db	"b1",0
_1915:
	db	"g1",0
_1916:
	db	"r2",0
_1917:
	db	"b2",0
_1918:
	db	"g2",0
_1919:
	db	"vertical",0
	align	4
_1911:
	dd	1
	dd	_154
	dd	2
	dd	_368
	dd	_1294
	dd	-4
	dd	2
	dd	_1912
	dd	_62
	dd	-8
	dd	2
	dd	_1913
	dd	_62
	dd	-12
	dd	2
	dd	_1914
	dd	_62
	dd	-16
	dd	2
	dd	_1915
	dd	_62
	dd	-20
	dd	2
	dd	_1916
	dd	_62
	dd	-24
	dd	2
	dd	_1917
	dd	_62
	dd	-28
	dd	2
	dd	_1918
	dd	_62
	dd	-32
	dd	2
	dd	_1919
	dd	_62
	dd	-36
	dd	0
	align	4
_1814:
	dd	_354
	dd	1588
	dd	3
	align	4
_1826:
	dd	3
	dd	0
	dd	0
	align	4
_1816:
	dd	_354
	dd	1589
	dd	4
	align	4
_1825:
	dd	3
	dd	0
	dd	0
	align	4
_1820:
	dd	_354
	dd	1590
	dd	5
	align	4
_1824:
	dd	_354
	dd	1591
	dd	5
	align	4
_1910:
	dd	3
	dd	0
	dd	0
	align	4
_1828:
	dd	_354
	dd	1594
	dd	4
	align	4
_1841:
	dd	3
	dd	0
	dd	0
	align	4
_1834:
	dd	_354
	dd	1596
	dd	5
	align	4
_1838:
	dd	_354
	dd	1597
	dd	5
	align	4
_1909:
	dd	3
	dd	0
	dd	0
	align	4
_1843:
	dd	_354
	dd	1600
	dd	5
	align	4
_1908:
	dd	3
	dd	0
	dd	0
	align	4
_1875:
	dd	_354
	dd	1601
	dd	6
	align	4
_1879:
	dd	_354
	dd	1602
	dd	6
	align	4
_1883:
	dd	_354
	dd	1603
	dd	6
	align	4
_1887:
	dd	_354
	dd	1604
	dd	6
	align	4
_1891:
	dd	_354
	dd	1605
	dd	6
	align	4
_1895:
	dd	_354
	dd	1606
	dd	6
	align	4
_1899:
	dd	_354
	dd	1607
	dd	6
	align	4
_1903:
	dd	_354
	dd	1608
	dd	6
	align	4
_1907:
	dd	_354
	dd	1609
	dd	6
_1978:
	db	"CreatePanelEx",0
_1979:
	db	"Width",0
_1980:
	db	"group",0
_1981:
	db	"panel",0
	align	4
_1977:
	dd	1
	dd	_1978
	dd	2
	dd	_1789
	dd	_62
	dd	-4
	dd	2
	dd	_1790
	dd	_62
	dd	-8
	dd	2
	dd	_1979
	dd	_62
	dd	-12
	dd	2
	dd	_572
	dd	_62
	dd	-16
	dd	2
	dd	_1980
	dd	_396
	dd	-20
	dd	2
	dd	_1197
	dd	_62
	dd	-24
	dd	2
	dd	_706
	dd	_81
	dd	-28
	dd	2
	dd	_1981
	dd	_396
	dd	-32
	dd	0
	align	4
_1920:
	dd	_354
	dd	1640
	dd	2
	align	4
_1921:
	dd	_354
	dd	1646
	dd	3
	align	4
_1925:
	dd	_354
	dd	1648
	dd	3
	align	4
_1928:
	dd	3
	dd	0
	dd	0
	align	4
_1927:
	dd	_354
	dd	1649
	dd	4
	align	4
_1933:
	dd	3
	dd	0
	dd	0
	align	4
_1930:
	dd	_354
	dd	1651
	dd	4
	align	4
_1934:
	dd	_354
	dd	1654
	dd	3
	align	4
_1939:
	dd	3
	dd	0
	dd	0
	align	4
_1936:
	dd	_354
	dd	1654
	dd	17
	align	4
_1940:
	dd	_354
	dd	1655
	dd	3
	align	4
_1943:
	dd	_354
	dd	1658
	dd	3
	align	4
_1969:
	dd	3
	dd	0
	dd	0
	align	4
_1945:
	dd	_354
	dd	1659
	dd	4
	align	4
_1948:
	dd	_354
	dd	1660
	dd	4
	align	4
_1965:
	dd	3
	dd	0
	dd	0
	align	4
_1950:
	dd	_354
	dd	1661
	dd	5
	align	4
_1960:
	dd	_354
	dd	1662
	dd	5
	align	4
_1966:
	dd	_354
	dd	1664
	dd	4
	align	4
_1970:
	dd	_354
	dd	1667
	dd	3
	align	4
_1975:
	dd	3
	dd	0
	dd	0
	align	4
_1972:
	dd	_354
	dd	1667
	dd	33
	align	4
_1976:
	dd	_354
	dd	1670
	dd	3
_1991:
	db	"SetPanelExGradient",0
	align	4
_1990:
	dd	1
	dd	_1991
	dd	2
	dd	_395
	dd	_396
	dd	-4
	dd	2
	dd	_1912
	dd	_62
	dd	-8
	dd	2
	dd	_1913
	dd	_62
	dd	-12
	dd	2
	dd	_1914
	dd	_62
	dd	-16
	dd	2
	dd	_1915
	dd	_62
	dd	-20
	dd	2
	dd	_1916
	dd	_62
	dd	-24
	dd	2
	dd	_1917
	dd	_62
	dd	-28
	dd	2
	dd	_1918
	dd	_62
	dd	-32
	dd	2
	dd	_1919
	dd	_62
	dd	-36
	dd	2
	dd	_1981
	dd	_1294
	dd	-40
	dd	0
	align	4
_1982:
	dd	_354
	dd	1732
	dd	2
	align	4
_1984:
	dd	_354
	dd	1733
	dd	2
	align	4
_1989:
	dd	3
	dd	0
	dd	0
	align	4
_1986:
	dd	_354
	dd	1733
	dd	11
