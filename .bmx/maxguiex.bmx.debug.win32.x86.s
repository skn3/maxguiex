	format	MS COFF
	extrn	_AddFontMemResourceEx@16
	extrn	_AddFontResourceExW@12
	extrn	_AlphaBlend@44
	extrn	_BitBlt@36
	extrn	_ClientToScreen@8
	extrn	_CreateCompatibleBitmap@12
	extrn	_CreateCompatibleDC@4
	extrn	_CreateSolidBrush@4
	extrn	_DeleteDC@4
	extrn	_DeleteObject@4
	extrn	_DestroyCursor@4
	extrn	_DrawTextW@20
	extrn	_FillRect@12
	extrn	_GetClientRect@8
	extrn	_GetClipBox@8
	extrn	_GetDC@4
	extrn	_GetUpdateRect@12
	extrn	_GetWindowLongW@8
	extrn	_GetWindowRect@8
	extrn	_IsRectEmpty@4
	extrn	_LoadCursorFromFileW@4
	extrn	_RedrawWindow@16
	extrn	_ReleaseDC@8
	extrn	_SelectObject@8
	extrn	_SendMessageA@16
	extrn	_SendMessageW@16
	extrn	_SetCursor@4
	extrn	_SetFocus@4
	extrn	_SetStretchBltMode@8
	extrn	_SetWindowLongW@12
	extrn	_SetWindowPos@28
	extrn	_StretchBlt@44
	extrn	___bb_blitz_blitz
	extrn	___bb_drivers_drivers
	extrn	___bb_linkedlist_linkedlist
	extrn	___bb_map_map
	extrn	___bb_systemex_systemex
	extrn	__maxgui_maxgui_TGadget_AddText
	extrn	__maxgui_maxgui_TGadget_AreaLen
	extrn	__maxgui_maxgui_TGadget_AreaText
	extrn	__maxgui_maxgui_TGadget_CharAt
	extrn	__maxgui_maxgui_TGadget_CharX
	extrn	__maxgui_maxgui_TGadget_CharY
	extrn	__maxgui_maxgui_TGadget_CleanUp
	extrn	__maxgui_maxgui_TGadget_Clear
	extrn	__maxgui_maxgui_TGadget_ClearListItems
	extrn	__maxgui_maxgui_TGadget_CountKids
	extrn	__maxgui_maxgui_TGadget_DoLayout
	extrn	__maxgui_maxgui_TGadget_GetCursorPos
	extrn	__maxgui_maxgui_TGadget_GetGroup
	extrn	__maxgui_maxgui_TGadget_GetHeight
	extrn	__maxgui_maxgui_TGadget_GetMenu
	extrn	__maxgui_maxgui_TGadget_GetProp
	extrn	__maxgui_maxgui_TGadget_GetSelectionLength
	extrn	__maxgui_maxgui_TGadget_GetSensitivity
	extrn	__maxgui_maxgui_TGadget_GetStatusText
	extrn	__maxgui_maxgui_TGadget_GetWatch
	extrn	__maxgui_maxgui_TGadget_GetWidth
	extrn	__maxgui_maxgui_TGadget_GetXPos
	extrn	__maxgui_maxgui_TGadget_GetYPos
	extrn	__maxgui_maxgui_TGadget_Handle
	extrn	__maxgui_maxgui_TGadget_HasDescendant
	extrn	__maxgui_maxgui_TGadget_Insert
	extrn	__maxgui_maxgui_TGadget_InsertItem
	extrn	__maxgui_maxgui_TGadget_InsertItemFromKey
	extrn	__maxgui_maxgui_TGadget_InsertListItem
	extrn	__maxgui_maxgui_TGadget_InsertNode
	extrn	__maxgui_maxgui_TGadget_ItemCount
	extrn	__maxgui_maxgui_TGadget_ItemExtra
	extrn	__maxgui_maxgui_TGadget_ItemFlags
	extrn	__maxgui_maxgui_TGadget_ItemIcon
	extrn	__maxgui_maxgui_TGadget_ItemState
	extrn	__maxgui_maxgui_TGadget_ItemText
	extrn	__maxgui_maxgui_TGadget_ItemTip
	extrn	__maxgui_maxgui_TGadget_KeysFromList
	extrn	__maxgui_maxgui_TGadget_KeysFromObjectArray
	extrn	__maxgui_maxgui_TGadget_LineAt
	extrn	__maxgui_maxgui_TGadget_ListItemState
	extrn	__maxgui_maxgui_TGadget_LockLayout
	extrn	__maxgui_maxgui_TGadget_LockText
	extrn	__maxgui_maxgui_TGadget_ModifyNode
	extrn	__maxgui_maxgui_TGadget_PopupMenu
	extrn	__maxgui_maxgui_TGadget_RemoveItem
	extrn	__maxgui_maxgui_TGadget_RemoveListItem
	extrn	__maxgui_maxgui_TGadget_ReplaceText
	extrn	__maxgui_maxgui_TGadget_RootNode
	extrn	__maxgui_maxgui_TGadget_Run
	extrn	__maxgui_maxgui_TGadget_SelectItem
	extrn	__maxgui_maxgui_TGadget_SelectedItem
	extrn	__maxgui_maxgui_TGadget_SelectedItems
	extrn	__maxgui_maxgui_TGadget_SelectedNode
	extrn	__maxgui_maxgui_TGadget_SelectionChanged
	extrn	__maxgui_maxgui_TGadget_SetDataSource
	extrn	__maxgui_maxgui_TGadget_SetFilter
	extrn	__maxgui_maxgui_TGadget_SetIconStrip
	extrn	__maxgui_maxgui_TGadget_SetItem
	extrn	__maxgui_maxgui_TGadget_SetItemState
	extrn	__maxgui_maxgui_TGadget_SetLayout
	extrn	__maxgui_maxgui_TGadget_SetListItem
	extrn	__maxgui_maxgui_TGadget_SetListItemState
	extrn	__maxgui_maxgui_TGadget_SetMaximumSize
	extrn	__maxgui_maxgui_TGadget_SetMinimumSize
	extrn	__maxgui_maxgui_TGadget_SetProp
	extrn	__maxgui_maxgui_TGadget_SetRange
	extrn	__maxgui_maxgui_TGadget_SetRect
	extrn	__maxgui_maxgui_TGadget_SetSelected
	extrn	__maxgui_maxgui_TGadget_SetSelection
	extrn	__maxgui_maxgui_TGadget_SetSensitivity
	extrn	__maxgui_maxgui_TGadget_SetShape
	extrn	__maxgui_maxgui_TGadget_SetStatusText
	extrn	__maxgui_maxgui_TGadget_SetStep
	extrn	__maxgui_maxgui_TGadget_SetStyle
	extrn	__maxgui_maxgui_TGadget_SetTabs
	extrn	__maxgui_maxgui_TGadget_SetValue
	extrn	__maxgui_maxgui_TGadget_SetWatch
	extrn	__maxgui_maxgui_TGadget_SyncData
	extrn	__maxgui_maxgui_TGadget_SyncDataSource
	extrn	__maxgui_maxgui_TGadget_UnlockText
	extrn	__maxgui_maxgui_TGadget_UpdateMenu
	extrn	__maxgui_maxgui_TGadget__setparent
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__cursor
	extrn	__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	extrn	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	extrn	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	extrn	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	extrn	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Query
	extrn	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Register
	extrn	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	extrn	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetText
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	extrn	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	extrn	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	extrn	__maxgui_win32maxguiex_TWindowsGadget_State
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isControl
	extrn	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Activate
	extrn	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Class
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	extrn	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Create
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Delete
	extrn	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	extrn	__maxgui_win32maxguiex_TWindowsPanel_Free
	extrn	__maxgui_win32maxguiex_TWindowsPanel_New
	extrn	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	extrn	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	extrn	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	extrn	__maxgui_win32maxguiex_TWindowsTextArea__oldCursor
	extrn	_bbAppTitle
	extrn	_bbArrayNew1D
	extrn	_bbArraySlice
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbFloatToInt
	extrn	_bbGCFree
	extrn	_bbIncbinLen
	extrn	_bbIncbinPtr
	extrn	_bbIntAbs
	extrn	_bbMemFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringAsc
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_bbStringReplace
	extrn	_bbStringSlice
	extrn	_bbStringToInt
	extrn	_bbStringToLower
	extrn	_bbStringToWString
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_blitz_RuntimeError
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_DeleteFile
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_ReadFile
	extrn	_brl_filesystem_StripDir
	extrn	_brl_linkedlist_CreateList
	extrn	_brl_map_CreateMap
	extrn	_brl_stream_CloseStream
	extrn	_brl_stream_CopyStream
	extrn	_brl_stream_ReadStream
	extrn	_brl_stream_WriteStream
	extrn	_brl_system_Notify
	extrn	_maxgui_localization_LocalizationMode
	extrn	_maxgui_maxgui_AddGadgetItem
	extrn	_maxgui_maxgui_GadgetClass
	extrn	_maxgui_maxgui_GadgetGroup
	extrn	_maxgui_maxgui_GadgetHeight
	extrn	_maxgui_maxgui_GadgetHidden
	extrn	_maxgui_maxgui_GadgetText
	extrn	_maxgui_maxgui_GadgetWidth
	extrn	_maxgui_maxgui_LocalizeGadget
	extrn	_maxgui_maxgui_QueryGadget
	extrn	_maxgui_maxgui_RedrawGadget
	extrn	_maxgui_maxgui_SetGadgetText
	extrn	_maxgui_maxgui_SetPointer
	extrn	_maxgui_maxgui_TGadgetItem
	extrn	_maxgui_maxgui_TProxyGadget
	extrn	_maxgui_maxgui_lastPointer
	extrn	_maxgui_win32maxguiex_TWindowsGadget
	extrn	_maxgui_win32maxguiex_TWindowsListBox
	extrn	_maxgui_win32maxguiex_TWindowsPanel
	extrn	_pub_win32_LVITEMW
	extrn	_skn3_systemex_GetTempDirectory
	public	___bb_maxguiex_maxguiex
	public	__skn3_maxguiex_PARAFORMAT2_Delete
	public	__skn3_maxguiex_PARAFORMAT2_New
	public	__skn3_maxguiex_Skn3CustomPointer_Delete
	public	__skn3_maxguiex_Skn3CustomPointer_New
	public	__skn3_maxguiex_Skn3CustomPointer_all
	public	__skn3_maxguiex_Skn3ListBatchLock_Delete
	public	__skn3_maxguiex_Skn3ListBatchLock_Find
	public	__skn3_maxguiex_Skn3ListBatchLock_New
	public	__skn3_maxguiex_Skn3ListBatchLock_add
	public	__skn3_maxguiex_Skn3ListBatchLock_all
	public	__skn3_maxguiex_Skn3ListBatchLock_remove
	public	__skn3_maxguiex_Skn3PanelEx_Delete
	public	__skn3_maxguiex_Skn3PanelEx_New
	public	__skn3_maxguiex_Skn3PanelEx_SetGradient
	public	__skn3_maxguiex_Skn3PanelEx_WndProc
	public	_skn3_maxguiex_AssignGadgetClassId
	public	_skn3_maxguiex_BringWindowToTop
	public	_skn3_maxguiex_ClearColorPickerCustomColors
	public	_skn3_maxguiex_CreatePanelEx
	public	_skn3_maxguiex_DisableGadgetRedraw
	public	_skn3_maxguiex_EnableGadgetRedraw
	public	_skn3_maxguiex_ExtractCursorHotspot
	public	_skn3_maxguiex_FocusWindow
	public	_skn3_maxguiex_FreeCustomPointer
	public	_skn3_maxguiex_GadgetScreenPosition
	public	_skn3_maxguiex_GadgetSizeForString
	public	_skn3_maxguiex_GadgetToInt
	public	_skn3_maxguiex_GadgetWindow
	public	_skn3_maxguiex_GetAppResourcesPath
	public	_skn3_maxguiex_GetCreationGroup
	public	_skn3_maxguiex_GetGadgetMaxLength
	public	_skn3_maxguiex_HideGadgetBorder
	public	_skn3_maxguiex_InstallGuiFont
	public	_skn3_maxguiex_ListBatchAdd
	public	_skn3_maxguiex_ListBatchLock
	public	_skn3_maxguiex_ListBatchUnlock
	public	_skn3_maxguiex_LoadCustomPointer
	public	_skn3_maxguiex_MessageBox
	public	_skn3_maxguiex_PointOverGadget
	public	_skn3_maxguiex_RedrawGadgetFrame
	public	_skn3_maxguiex_RequestScrollbarSize
	public	_skn3_maxguiex_ScrollTextAreaToBottom
	public	_skn3_maxguiex_ScrollTextAreaToCursor
	public	_skn3_maxguiex_ScrollTextAreaToTop
	public	_skn3_maxguiex_SetColorPickerCustomColors
	public	_skn3_maxguiex_SetComboBoxHeight
	public	_skn3_maxguiex_SetCustomPointer
	public	_skn3_maxguiex_SetGadgetMaxLength
	public	_skn3_maxguiex_SetGadgetReadOnly
	public	_skn3_maxguiex_SetPanelExGradient
	public	_skn3_maxguiex_SetTextareaLineSpacing
	public	_skn3_maxguiex_SetWindowAlwaysOnTop
	public	_skn3_maxguiex_Skn3CustomPointer
	public	_skn3_maxguiex_Skn3PanelEx
	section	"code" code
___bb_maxguiex_maxguiex:
	push	ebp
	mov	ebp,esp
	push	ebx
	cmp	dword [_377],0
	je	_378
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_378:
	mov	dword [_377],1
	push	ebp
	push	_369
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_map_map
	call	___bb_linkedlist_linkedlist
	call	___bb_drivers_drivers
	call	___bb_systemex_systemex
	push	_366
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_16
	call	_bbObjectRegisterType
	add	esp,4
	push	_368
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectRegisterType
	add	esp,4
	push	_20
	call	_bbObjectRegisterType
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectRegisterType
	add	esp,4
	mov	ebx,0
	jmp	_157
_157:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_383
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_16
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],0
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],0
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	push	ebp
	push	_382
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_160
_160:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_163:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_388
	push	eax
	call	_bbGCFree
	add	esp,4
_388:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_390
	push	eax
	call	_bbGCFree
	add	esp,4
_390:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_392
	push	eax
	call	_bbGCFree
	add	esp,4
_392:
	mov	eax,0
	jmp	_386
_386:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_Find:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	push	ebp
	push	_417
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_393
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_394
	push	ebp
	push	_396
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_395
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_bbNullObject
	call	dword [_bbOnDebugLeaveScope]
	jmp	_166
_394:
	push	_397
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	push	_399
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	cmp	ebx,_bbNullObject
	jne	_401
	call	_brl_blitz_NullObjectError
_401:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+88]
	add	esp,4
	mov	dword [ebp-12],eax
	push	_403
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_17
_19:
	push	ebp
	push	_416
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_404
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_406
	call	_brl_blitz_NullObjectError
_406:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	push	_407
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_409
	call	_brl_blitz_NullObjectError
_409:
	mov	eax,dword [ebp-4]
	cmp	dword [ebx+12],eax
	jne	_410
	push	ebp
	push	_412
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_411
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_166
_410:
	push	_413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_415
	call	_brl_blitz_NullObjectError
_415:
	push	_16
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_17:
	cmp	dword [ebp-12],_bbNullObject
	jne	_19
_18:
	mov	ebx,_bbNullObject
	jmp	_166
_166:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_add:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_440
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_422
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__skn3_maxguiex_Skn3ListBatchLock_all],_bbNullObject
	jne	_423
	push	ebp
	push	_429
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_424
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	dec	dword [eax+4]
	jnz	_428
	push	eax
	call	_bbGCFree
	add	esp,4
_428:
	mov	dword [__skn3_maxguiex_Skn3ListBatchLock_all],ebx
	call	dword [_bbOnDebugLeaveScope]
_423:
	push	_430
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_432
	call	_brl_blitz_NullObjectError
_432:
	mov	esi,dword [__skn3_maxguiex_Skn3ListBatchLock_all]
	cmp	esi,_bbNullObject
	jne	_435
	call	_brl_blitz_NullObjectError
_435:
	push	dword [ebp-4]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+68]
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_439
	push	eax
	call	_bbGCFree
	add	esp,4
_439:
	mov	dword [ebx+20],esi
	mov	ebx,0
	jmp	_169
_169:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3ListBatchLock_remove:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_466
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_442
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_443
	push	ebp
	push	_465
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_444
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_446
	call	_brl_blitz_NullObjectError
_446:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_448
	call	_brl_blitz_NullObjectError
_448:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	push	_449
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_451
	call	_brl_blitz_NullObjectError
_451:
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_456
	push	eax
	call	_bbGCFree
	add	esp,4
_456:
	mov	dword [esi+12],ebx
	push	_457
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_459
	call	_brl_blitz_NullObjectError
_459:
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_464
	push	eax
	call	_bbGCFree
	add	esp,4
_464:
	mov	dword [esi+24],ebx
	call	dword [_bbOnDebugLeaveScope]
_443:
	mov	ebx,0
	jmp	_172
_172:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3CustomPointer_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_469
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_skn3_maxguiex_Skn3CustomPointer
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],0
	push	ebp
	push	_468
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_175
_175:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3CustomPointer_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_178:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_473
	push	eax
	call	_bbGCFree
	add	esp,4
_473:
	mov	eax,0
	jmp	_471
_471:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_PARAFORMAT2_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_475
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_20
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	eax,dword [ebp-4]
	mov	word [eax+16],0
	mov	eax,dword [ebp-4]
	mov	word [eax+18],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	mov	eax,dword [ebp-4]
	mov	word [eax+32],0
	mov	eax,dword [ebp-4]
	mov	word [eax+34],32
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+52],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+56],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+60],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+64],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+68],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+72],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+76],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+80],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+84],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+88],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+92],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+96],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+100],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+104],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+108],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+112],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+116],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+120],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+124],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+128],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+132],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+136],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+140],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+144],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+148],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+152],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+156],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+160],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+164],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+168],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+172],0
	mov	eax,dword [ebp-4]
	mov	word [eax+176],0
	mov	eax,dword [ebp-4]
	mov	byte [eax+178],0
	mov	eax,dword [ebp-4]
	mov	byte [eax+179],0
	mov	eax,dword [ebp-4]
	mov	word [eax+180],0
	mov	eax,dword [ebp-4]
	mov	word [eax+182],0
	mov	eax,dword [ebp-4]
	mov	word [eax+184],0
	mov	eax,dword [ebp-4]
	mov	word [eax+186],0
	mov	eax,dword [ebp-4]
	mov	word [eax+188],0
	mov	eax,dword [ebp-4]
	mov	word [eax+190],0
	mov	eax,dword [ebp-4]
	mov	word [eax+192],0
	mov	eax,dword [ebp-4]
	mov	word [eax+194],0
	push	ebp
	push	_474
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_181
_181:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_PARAFORMAT2_Delete:
	push	ebp
	mov	ebp,esp
_184:
	mov	eax,0
	jmp	_477
_477:
	mov	esp,ebp
	pop	ebp
	ret
_22:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	eax,ebp
	push	eax
	push	_543
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_478
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	cmp	dword [eax+8],0
	jne	_479
	mov	eax,ebp
	push	eax
	push	_481
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_480
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_189
_479:
	push	_482
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	_23
	push	dword [ebp-4]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-4],eax
	push	_483
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	dword [ebp-8]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_484
	mov	eax,ebp
	push	eax
	push	_486
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_485
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	_21
	push	dword [ebp-4]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_484:
	push	_487
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_489
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	push	_491
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],0
	push	_493
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bbStringAsc
	add	esp,4
	mov	dword [ebp-28],eax
	push	_495
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	mov	eax,dword [ebp-4]
	mov	edi,dword [eax+8]
	jmp	_496
_26:
	mov	eax,ebp
	push	eax
	push	_512
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_498
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_501
	call	_brl_blitz_ArrayBoundsError
_501:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_505
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_504
	call	_brl_blitz_ArrayBoundsError
_504:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,dword [ebp-28]
	setne	al
	movzx	eax,al
_505:
	cmp	eax,0
	je	_507
	mov	eax,ebp
	push	eax
	push	_509
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_508
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_25
_507:
	push	_510
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	add	eax,1
	mov	dword [ebp-20],eax
	push	_511
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],1
	call	dword [_bbOnDebugLeaveScope]
_24:
	add	dword [ebp-16],1
_496:
	cmp	dword [ebp-16],edi
	jl	_26
_25:
	push	_513
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	sub	eax,dword [ebp-20]
	mov	dword [ebp-32],eax
	push	_515
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	sub	eax,1
	mov	dword [ebp-16],eax
	jmp	_516
_29:
	mov	eax,ebp
	push	eax
	push	_530
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_517
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_520
	call	_brl_blitz_ArrayBoundsError
_520:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,32
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_524
	mov	esi,dword [ebp-4]
	mov	ebx,dword [ebp-16]
	cmp	ebx,dword [esi+8]
	jb	_523
	call	_brl_blitz_ArrayBoundsError
_523:
	movzx	eax,word [esi+ebx*2+12]
	mov	eax,eax
	cmp	eax,dword [ebp-28]
	setne	al
	movzx	eax,al
_524:
	cmp	eax,0
	je	_526
	mov	eax,ebp
	push	eax
	push	_528
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_527
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_28
_526:
	push	_529
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [ebp-32],1
	call	dword [_bbOnDebugLeaveScope]
_27:
	add	dword [ebp-16],-1
_516:
	cmp	dword [ebp-16],0
	jge	_29
_28:
	push	_531
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],0
	jg	_532
	mov	eax,ebp
	push	eax
	push	_534
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_533
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_189
_532:
	push	_535
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	add	eax,dword [ebp-32]
	push	eax
	push	dword [ebp-20]
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	mov	dword [ebp-4],eax
	push	_536
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-24]
	cmp	eax,0
	je	_537
	mov	eax,dword [ebp-12]
_537:
	cmp	eax,0
	je	_539
	mov	eax,ebp
	push	eax
	push	_541
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_540
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_539:
	push	_542
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_189
_189:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_30:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyString
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],_bbEmptyString
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-28],_bbNullObject
	push	ebp
	push	_590
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_551
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_31
	push	8
	push	0
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_552
	push	ebp
	push	_581
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_553
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	1
	push	_21
	push	0
	call	_skn3_systemex_GetTempDirectory
	add	esp,4
	push	eax
	call	_22
	add	esp,12
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	push	_555
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_1
	push	_557
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	push	8
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_brl_filesystem_StripDir
	add	esp,4
	mov	dword [ebp-16],eax
	push	_559
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_32
_34:
	push	ebp
	push	_561
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bbStringToInt
	add	esp,4
	add	eax,1
	push	eax
	call	_bbStringFromInt
	add	esp,4
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
_32:
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,0
	jne	_34
_33:
	push	_562
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-20],eax
	push	_564
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadStream
	add	esp,4
	mov	dword [ebp-24],eax
	push	_566
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-24],_bbNullObject
	jne	_567
	push	ebp
	push	_569
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_568
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_192
_567:
	push	_570
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_stream_WriteStream
	add	esp,4
	mov	dword [ebp-28],eax
	push	_572
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],_bbNullObject
	jne	_573
	push	ebp
	push	_576
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_574
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_brl_stream_CloseStream
	add	esp,4
	push	_575
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_192
_573:
	push	_577
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4096
	push	dword [ebp-28]
	push	dword [ebp-24]
	call	_brl_stream_CopyStream
	add	esp,12
	push	_578
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	_579
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_brl_stream_CloseStream
	add	esp,4
	push	_580
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_192
_552:
	push	_589
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	jmp	_192
_192:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_35:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	fld	dword [ebp+8]
	fstp	dword [ebp-4]
	fld	dword [ebp+12]
	fstp	dword [ebp-8]
	fld	dword [ebp+16]
	fstp	dword [ebp-12]
	fld	dword [ebp+20]
	fstp	dword [ebp-16]
	fld	dword [ebp+24]
	fstp	dword [ebp-20]
	fld	dword [ebp+28]
	fstp	dword [ebp-24]
	push	ebp
	push	_599
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_592
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-4]
	fld	dword [ebp-12]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	je	_593
	fld	dword [ebp-4]
	fld	dword [ebp-12]
	fadd	dword [ebp-20]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_593:
	cmp	eax,0
	je	_595
	fld	dword [ebp-8]
	fld	dword [ebp-16]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
_595:
	cmp	eax,0
	je	_597
	fld	dword [ebp-8]
	fld	dword [ebp-16]
	fadd	dword [ebp-24]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_597:
	mov	ebx,eax
	jmp	_200
_200:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RequestScrollbarSize:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_609
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_608
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,18
	jmp	_202
_202:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetComboBoxHeight:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_613
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_611
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	sub	eax,6
	push	eax
	push	-1
	push	339
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageA@16
	push	_612
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	mov	ebx,0
	jmp	_206
_206:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetScreenPosition:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-16],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-4],0
	mov	dword [ebp-20],_bbEmptyArray
	push	ebp
	push	_642
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_617
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	mov	dword [ebp-4],0
	push	_619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],0
	je	_620
	push	ebp
	push	_624
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_621
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_623
	call	_brl_blitz_NullObjectError
_623:
	lea	eax,dword [ebp-8]
	push	eax
	push	2
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_625
_620:
	push	ebp
	push	_629
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_626
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_628
	call	_brl_blitz_NullObjectError
_628:
	lea	eax,dword [ebp-8]
	push	eax
	push	1
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+204]
	add	esp,8
	push	eax
	call	_ClientToScreen@8
	call	dword [_bbOnDebugLeaveScope]
_625:
	push	_630
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_631
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-20],eax
	push	_633
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-20]
	cmp	ebx,dword [eax+20]
	jb	_635
	call	_brl_blitz_ArrayBoundsError
_635:
	mov	eax,dword [ebp-20]
	shl	ebx,2
	add	eax,ebx
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx]
	mov	dword [eax+24],edx
	push	_637
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-20]
	cmp	ebx,dword [eax+20]
	jb	_639
	call	_brl_blitz_ArrayBoundsError
_639:
	mov	eax,dword [ebp-20]
	shl	ebx,2
	add	eax,ebx
	lea	edx,dword [ebp-8]
	mov	edx,dword [edx+4]
	mov	dword [eax+24],edx
	push	_641
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	jmp	_210
_210:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_PointOverGadget:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	dword [ebp-24],_bbEmptyArray
	mov	dword [ebp-20],_bbEmptyArray
	push	ebp
	push	_675
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_650
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,_bbNullObject
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_651
	push	0
	push	dword [ebp-12]
	call	_maxgui_maxgui_GadgetHidden
	add	esp,8
_651:
	cmp	eax,0
	je	_653
	push	ebp
	push	_655
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_654
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_216
_653:
	push	_656
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],_bbNullObject
	je	_657
	push	ebp
	push	_666
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_658
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-16]
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	dword [ebp-24],eax
	push	_660
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-24]
	cmp	ebx,dword [eax+20]
	jb	_662
	call	_brl_blitz_ArrayBoundsError
_662:
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+ebx*4+24]
	add	eax,dword [ebp-4]
	mov	dword [ebp-4],eax
	push	_663
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-24]
	cmp	ebx,dword [eax+20]
	jb	_665
	call	_brl_blitz_ArrayBoundsError
_665:
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+ebx*4+24]
	add	eax,dword [ebp-8]
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_657:
	push	_668
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-12]
	call	_skn3_maxguiex_GadgetScreenPosition
	add	esp,8
	mov	dword [ebp-20],eax
	push	_670
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,0
	mov	eax,dword [ebp-20]
	cmp	esi,dword [eax+20]
	jb	_672
	call	_brl_blitz_ArrayBoundsError
_672:
	mov	ebx,1
	mov	eax,dword [ebp-20]
	cmp	ebx,dword [eax+20]
	jb	_674
	call	_brl_blitz_ArrayBoundsError
_674:
	push	dword [ebp-12]
	call	_maxgui_maxgui_GadgetHeight
	add	esp,4
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	push	dword [ebp-12]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-8]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-4]
	mov	dword [ebp+-28],eax
	fild	dword [ebp+-28]
	sub	esp,4
	fstp	dword [esp]
	call	_35
	add	esp,24
	mov	ebx,eax
	jmp	_216
_216:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_DisableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_682
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_680
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	11
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	push	_681
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	jmp	_219
_219:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_EnableGadgetRedraw:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_686
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_684
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	11
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	push	_685
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	jmp	_222
_222:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_MessageBox:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	dword [ebp-16],_bbEmptyString
	push	ebp
	push	_701
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_688
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bbAppTitle]
	mov	dword [ebp-16],eax
	push	_690
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	inc	dword [ebx+4]
	mov	eax,dword [_bbAppTitle]
	dec	dword [eax+4]
	jnz	_694
	push	eax
	call	_bbGCFree
	add	esp,4
_694:
	mov	dword [_bbAppTitle],ebx
	push	_695
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	call	_brl_system_Notify
	add	esp,8
	push	_696
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	inc	dword [ebx+4]
	mov	eax,dword [_bbAppTitle]
	dec	dword [eax+4]
	jnz	_700
	push	eax
	call	_bbGCFree
	add	esp,4
_700:
	mov	dword [_bbAppTitle],ebx
	mov	ebx,0
	jmp	_227
_227:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetSizeForString:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],_bbEmptyArray
	mov	dword [ebp-32],0
	mov	dword [ebp-36],_bbEmptyArray
	mov	dword [ebp-40],_bbEmptyString
	push	ebp
	push	_756
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_707
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-16],eax
	push	_709
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,2
	je	_712
	push	ebp
	push	_737
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_713
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_GetDC@4
	mov	dword [ebp-20],eax
	push	_715
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	49
	push	dword [ebp-16]
	call	_SendMessageW@16
	mov	dword [ebp-24],eax
	push	_717
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	push	dword [ebp-20]
	call	_SelectObject@8
	push	_718
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [eax+24],0
	mov	dword [eax+28],0
	mov	edx,dword [ebp-12]
	mov	dword [eax+32],edx
	mov	dword [eax+36],0
	mov	dword [ebp-28],eax
	push	_721
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],1280
	push	_723
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	jle	_724
	push	ebp
	push	_726
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_725
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	or	dword [ebp-32],16
	call	dword [_bbOnDebugLeaveScope]
_724:
	push	_727
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bbStringToWString
	add	esp,4
	mov	ebx,eax
	push	dword [ebp-32]
	mov	eax,dword [ebp-28]
	lea	eax,dword [eax+24]
	push	eax
	push	-1
	push	ebx
	push	dword [ebp-20]
	call	_DrawTextW@20
	push	ebx
	call	_bbMemFree
	add	esp,4
	push	_730
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-16]
	call	_ReleaseDC@8
	push	_731
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	ebx,eax
	mov	esi,2
	mov	eax,dword [ebp-28]
	cmp	esi,dword [eax+20]
	jb	_733
	call	_brl_blitz_ArrayBoundsError
_733:
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+24],eax
	mov	esi,3
	mov	eax,dword [ebp-28]
	cmp	esi,dword [eax+20]
	jb	_735
	call	_brl_blitz_ArrayBoundsError
_735:
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+28],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_232
_712:
	push	ebp
	push	_753
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_742
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edx,dword [ebp-12]
	mov	dword [eax+24],edx
	mov	dword [eax+28],0
	mov	dword [ebp-36],eax
	push	_745
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	11
	push	dword [ebp-16]
	call	_SendMessageW@16
	push	_746
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	dword [ebp-40],eax
	push	_748
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_749
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-36]
	lea	eax,byte [eax+24]
	push	eax
	push	0
	push	5633
	push	dword [ebp-16]
	call	_SendMessageW@16
	push	_750
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-40]
	push	dword [ebp-4]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_751
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	11
	push	dword [ebp-16]
	call	_SendMessageW@16
	push	_752
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-36]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_232
_232:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetCreationGroup:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_769
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_760
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_maxgui_TProxyGadget
	push	dword [ebp-4]
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	push	_762
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	je	_763
	push	ebp
	push	_767
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_764
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_766
	call	_brl_blitz_NullObjectError
_766:
	push	dword [ebx+140]
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_235
_763:
	push	_768
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_235
_235:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetReadOnly:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],0
	push	ebp
	push	_781
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_773
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_776
	cmp	eax,4
	je	_776
	jmp	_775
_776:
	push	ebp
	push	_780
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_777
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	_779
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	push	207
	push	dword [ebp-12]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
	jmp	_775
_775:
	mov	ebx,0
	jmp	_239
_239:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_794
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_784
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_787
	cmp	eax,5
	je	_787
	jmp	_786
_787:
	push	ebp
	push	_793
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_788
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	jge	_789
	push	ebp
	push	_791
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_790
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	call	dword [_bbOnDebugLeaveScope]
_789:
	push	_792
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	push	197
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
	jmp	_786
_786:
	mov	ebx,0
	jmp	_243
_243:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetGadgetMaxLength:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_804
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_796
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,4
	je	_799
	cmp	eax,5
	je	_799
	push	ebp
	push	_801
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_800
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_246
_799:
	push	ebp
	push	_803
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_802
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	213
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	push	eax
	call	_SendMessageW@16
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_246
_246:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_LoadCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],0
	mov	eax,ebp
	push	eax
	push	_867
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_806
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	push	_808
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_30
	add	esp,4
	mov	dword [ebp-12],eax
	push	_810
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_812
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	dword [eax+8],0
	jne	_813
	mov	eax,ebp
	push	eax
	push	_815
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_814
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_816
_813:
	mov	eax,ebp
	push	eax
	push	_818
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_817
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_816:
	push	_819
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__skn3_maxguiex_Skn3CustomPointer_all],_bbNullObject
	jne	_820
	mov	eax,ebp
	push	eax
	push	_826
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_821
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_map_CreateMap
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	dec	dword [eax+4]
	jnz	_825
	push	eax
	call	_bbGCFree
	add	esp,4
_825:
	mov	dword [__skn3_maxguiex_Skn3CustomPointer_all],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_827
_820:
	mov	eax,ebp
	push	eax
	push	_831
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_828
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	cmp	ebx,_bbNullObject
	jne	_830
	call	_brl_blitz_NullObjectError
_830:
	push	_skn3_maxguiex_Skn3CustomPointer
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,8
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_827:
	push	_832
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	jne	_833
	mov	eax,ebp
	push	eax
	push	_852
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_834
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3CustomPointer
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_835
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_837
	call	_brl_blitz_NullObjectError
_837:
	mov	ebx,dword [ebp-4]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_842
	push	eax
	call	_bbGCFree
	add	esp,4
_842:
	mov	dword [esi+8],ebx
	push	_843
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	cmp	ebx,_bbNullObject
	jne	_845
	call	_brl_blitz_NullObjectError
_845:
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	push	_846
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_848
	call	_brl_blitz_NullObjectError
_848:
	mov	edi,ebx
	push	dword [ebp-12]
	call	_bbStringToWString
	add	esp,4
	mov	esi,eax
	push	esi
	call	_LoadCursorFromFileW@4
	mov	ebx,eax
	push	esi
	call	_bbMemFree
	add	esp,4
	mov	dword [edi+12],ebx
	call	dword [_bbOnDebugLeaveScope]
_833:
	push	_853
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],0
	je	_854
	mov	eax,ebp
	push	eax
	push	_856
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_855
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_brl_filesystem_DeleteFile
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_854:
	push	_857
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_859
	call	_brl_blitz_NullObjectError
_859:
	cmp	dword [ebx+12],0
	je	_860
	mov	eax,ebp
	push	eax
	push	_866
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_861
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_863
	call	_brl_blitz_NullObjectError
_863:
	add	dword [ebx+16],1
	push	_865
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_249
_860:
	mov	ebx,_bbNullObject
	jmp	_249
_249:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_886
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_870
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_871
	push	ebp
	push	_885
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_872
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_maxgui_maxgui_lastPointer],-1
	push	_873
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_875
	call	_brl_blitz_NullObjectError
_875:
	push	dword [ebx+12]
	call	_SetCursor@4
	push	_876
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_878
	call	_brl_blitz_NullObjectError
_878:
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	push	_879
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],0
	je	_880
	push	ebp
	push	_884
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_881
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_883
	call	_brl_blitz_NullObjectError
_883:
	mov	eax,dword [ebx+12]
	mov	dword [__maxgui_win32maxguiex_TWindowsTextArea__oldCursor],eax
	call	dword [_bbOnDebugLeaveScope]
_880:
	call	dword [_bbOnDebugLeaveScope]
_871:
	mov	ebx,0
	jmp	_252
_252:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FreeCustomPointer:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_918
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_888
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_889
	push	ebp
	push	_917
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_890
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_892
	call	_brl_blitz_NullObjectError
_892:
	sub	dword [ebx+16],1
	push	_894
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_896
	call	_brl_blitz_NullObjectError
_896:
	cmp	dword [ebx+16],0
	jne	_897
	push	ebp
	push	_916
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_898
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [__skn3_maxguiex_Skn3CustomPointer_all]
	cmp	esi,_bbNullObject
	jne	_900
	call	_brl_blitz_NullObjectError
_900:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_902
	call	_brl_blitz_NullObjectError
_902:
	push	dword [ebx+8]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+68]
	add	esp,8
	push	_903
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_905
	call	_brl_blitz_NullObjectError
_905:
	mov	eax,dword [ebx+12]
	cmp	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__cursor],eax
	jne	_906
	push	ebp
	push	_908
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_907
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	call	_maxgui_maxgui_SetPointer
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_906:
	push	_909
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_911
	call	_brl_blitz_NullObjectError
_911:
	push	dword [ebx+12]
	call	_DestroyCursor@4
	push	_912
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_914
	call	_brl_blitz_NullObjectError
_914:
	mov	dword [ebx+12],0
	call	dword [_bbOnDebugLeaveScope]
_897:
	call	dword [_bbOnDebugLeaveScope]
_889:
	mov	ebx,0
	jmp	_255
_255:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ExtractCursorHotspot:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbEmptyArray
	mov	dword [ebp-16],_bbNullObject
	mov	dword [ebp-20],0
	push	ebp
	push	_970
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_920
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_921
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-12],eax
	push	_923
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	_36
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_ReadFile
	add	esp,4
	mov	dword [ebp-16],eax
	push	_925
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],_bbNullObject
	je	_926
	push	ebp
	push	_967
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_927
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_929
	call	_brl_blitz_NullObjectError
_929:
	push	2
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,8
	push	_930
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_932
	call	_brl_blitz_NullObjectError
_932:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebp-20],eax
	push	_934
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],2
	jne	_935
	push	ebp
	push	_963
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_936
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_938
	call	_brl_blitz_NullObjectError
_938:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebp-20],eax
	push	_939
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	cmp	dword [ebp-8],eax
	jge	_940
	push	ebp
	push	_962
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_941
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	imul	eax,12
	add	eax,6
	add	eax,4
	mov	dword [ebp-20],eax
	push	_942
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_944
	call	_brl_blitz_NullObjectError
_944:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	dword [ebp-20],eax
	jge	_945
	push	ebp
	push	_961
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_946
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_948
	call	_brl_blitz_NullObjectError
_948:
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,8
	push	_949
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,0
	mov	eax,dword [ebp-12]
	cmp	esi,dword [eax+20]
	jb	_951
	call	_brl_blitz_ArrayBoundsError
_951:
	mov	ebx,dword [ebp-12]
	shl	esi,2
	add	ebx,esi
	mov	esi,dword [ebp-16]
	cmp	esi,_bbNullObject
	jne	_954
	call	_brl_blitz_NullObjectError
_954:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebx+24],eax
	push	_955
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,1
	mov	eax,dword [ebp-12]
	cmp	esi,dword [eax+20]
	jb	_957
	call	_brl_blitz_ArrayBoundsError
_957:
	mov	ebx,dword [ebp-12]
	shl	esi,2
	add	ebx,esi
	mov	esi,dword [ebp-16]
	cmp	esi,_bbNullObject
	jne	_960
	call	_brl_blitz_NullObjectError
_960:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+100]
	add	esp,4
	mov	dword [ebx+24],eax
	call	dword [_bbOnDebugLeaveScope]
_945:
	call	dword [_bbOnDebugLeaveScope]
_940:
	call	dword [_bbOnDebugLeaveScope]
_935:
	push	_964
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_966
	call	_brl_blitz_NullObjectError
_966:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_926:
	push	_969
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	jmp	_259
_259:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchLock:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	push	ebp
	push	_1025
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_974
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	dword [_16+48]
	add	esp,4
	mov	dword [ebp-8],eax
	push	_976
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	je	_977
	push	ebp
	push	_983
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_978
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_980
	call	_brl_blitz_NullObjectError
_980:
	add	dword [ebx+8],1
	push	_982
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_262
_977:
	push	_984
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsListBox
	push	dword [ebp-4]
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	push	_986
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],_bbNullObject
	jne	_987
	push	ebp
	push	_989
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_988
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_262
_987:
	push	_990
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_16
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_991
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_993
	call	_brl_blitz_NullObjectError
_993:
	mov	dword [ebx+8],1
	push	_995
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_997
	call	_brl_blitz_NullObjectError
_997:
	mov	ebx,dword [ebp-12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_1002
	push	eax
	call	_bbGCFree
	add	esp,4
_1002:
	mov	dword [esi+12],ebx
	push	_1003
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1005
	call	_brl_blitz_NullObjectError
_1005:
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_1008
	call	_brl_blitz_NullObjectError
_1008:
	mov	eax,dword [esi+124]
	mov	eax,dword [eax+20]
	mov	dword [ebx+16],eax
	push	_1009
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1011
	call	_brl_blitz_NullObjectError
_1011:
	push	_pub_win32_LVITEMW
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1016
	push	eax
	call	_bbGCFree
	add	esp,4
_1016:
	mov	dword [ebx+24],esi
	push	_1017
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1019
	call	_brl_blitz_NullObjectError
_1019:
	push	1
	push	dword [ebp-12]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebx+28],eax
	push	_1021
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_1023
	call	_brl_blitz_NullObjectError
_1023:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+552]
	add	esp,4
	push	_1024
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	dword [_16+52]
	add	esp,4
	mov	ebx,0
	jmp	_262
_262:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchAdd:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],_bbNullObject
	mov	dword [ebp-32],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1116
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1027
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	dword [_16+48]
	add	esp,4
	mov	dword [ebp-28],eax
	push	_1029
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],_bbNullObject
	jne	_1030
	mov	eax,ebp
	push	eax
	push	_1033
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1031
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_maxgui_maxgui_AddGadgetItem
	add	esp,24
	push	_1032
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_270
_1030:
	push	_1034
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_maxgui_TGadgetItem
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-32],eax
	push	_1036
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1038
	call	_brl_blitz_NullObjectError
_1038:
	push	dword [ebp-12]
	push	dword [ebp-24]
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	dword [ebp-8]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,24
	push	_1039
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1041
	call	_brl_blitz_NullObjectError
_1041:
	mov	edi,ebx
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1044
	call	_brl_blitz_NullObjectError
_1044:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1046
	call	_brl_blitz_NullObjectError
_1046:
	mov	eax,dword [ebx+16]
	add	eax,1
	push	eax
	push	0
	push	dword [esi+124]
	push	_1047
	call	_bbArraySlice
	add	esp,16
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+124]
	dec	dword [eax+4]
	jnz	_1051
	push	eax
	call	_bbGCFree
	add	esp,4
_1051:
	mov	dword [edi+124],ebx
	push	_1052
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1054
	call	_brl_blitz_NullObjectError
_1054:
	mov	esi,dword [ebx+124]
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1057
	call	_brl_blitz_NullObjectError
_1057:
	mov	ebx,dword [ebx+16]
	cmp	ebx,dword [esi+20]
	jb	_1059
	call	_brl_blitz_ArrayBoundsError
_1059:
	shl	ebx,2
	add	esi,ebx
	mov	ebx,dword [ebp-32]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1064
	push	eax
	call	_bbGCFree
	add	esp,4
_1064:
	mov	dword [esi+24],ebx
	push	_1065
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1067
	call	_brl_blitz_NullObjectError
_1067:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1069
	call	_brl_blitz_NullObjectError
_1069:
	mov	dword [ebx+8],4097
	push	_1071
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1073
	call	_brl_blitz_NullObjectError
_1073:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1075
	call	_brl_blitz_NullObjectError
_1075:
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_1078
	call	_brl_blitz_NullObjectError
_1078:
	mov	eax,dword [esi+16]
	mov	dword [ebx+12],eax
	push	_1079
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1081
	call	_brl_blitz_NullObjectError
_1081:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1083
	call	_brl_blitz_NullObjectError
_1083:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_1086
	call	_brl_blitz_NullObjectError
_1086:
	push	dword [esi+8]
	call	_bbStringToWString
	add	esp,4
	mov	dword [ebx+28],eax
	push	_1087
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1089
	call	_brl_blitz_NullObjectError
_1089:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1091
	call	_brl_blitz_NullObjectError
_1091:
	or	dword [ebx+8],2
	push	_1093
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1095
	call	_brl_blitz_NullObjectError
_1095:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1097
	call	_brl_blitz_NullObjectError
_1097:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_1100
	call	_brl_blitz_NullObjectError
_1100:
	mov	eax,dword [esi+16]
	mov	dword [ebx+36],eax
	push	_1101
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1103
	call	_brl_blitz_NullObjectError
_1103:
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_1105
	call	_brl_blitz_NullObjectError
_1105:
	mov	eax,dword [esi+24]
	mov	dword [ebp-36],eax
	mov	eax,dword [ebp-36]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	4173
	push	dword [ebx+28]
	call	_SendMessageW@16
	push	_1107
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1109
	call	_brl_blitz_NullObjectError
_1109:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_1111
	call	_brl_blitz_NullObjectError
_1111:
	push	dword [ebx+28]
	call	_bbMemFree
	add	esp,4
	push	_1112
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1114
	call	_brl_blitz_NullObjectError
_1114:
	add	dword [ebx+16],1
	mov	ebx,0
	jmp	_270
_270:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ListBatchUnlock:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_1159
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1123
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	dword [_16+48]
	add	esp,4
	mov	dword [ebp-8],eax
	push	_1125
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],_bbNullObject
	jne	_1126
	push	ebp
	push	_1128
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1127
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_273
_1126:
	push	_1129
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1131
	call	_brl_blitz_NullObjectError
_1131:
	sub	dword [ebx+8],1
	push	_1133
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1135
	call	_brl_blitz_NullObjectError
_1135:
	cmp	dword [ebx+8],0
	jne	_1136
	push	ebp
	push	_1158
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1137
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1139
	call	_brl_blitz_NullObjectError
_1139:
	push	-2
	push	0
	push	4126
	push	dword [ebx+28]
	call	_SendMessageW@16
	push	_1140
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1142
	call	_brl_blitz_NullObjectError
_1142:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_1144
	call	_brl_blitz_NullObjectError
_1144:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+580]
	add	esp,4
	cmp	eax,0
	jne	_1145
	push	ebp
	push	_1151
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1146
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1148
	call	_brl_blitz_NullObjectError
_1148:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_1150
	call	_brl_blitz_NullObjectError
_1150:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1145:
	push	_1152
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_1154
	call	_brl_blitz_NullObjectError
_1154:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_1156
	call	_brl_blitz_NullObjectError
_1156:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+548]
	add	esp,4
	push	_1157
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	dword [_16+56]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1136:
	mov	ebx,0
	jmp	_273
_273:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetWindow:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_1170
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1161
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	dword [ebp-8],eax
	push	_1163
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_37
_39:
	push	ebp
	push	_1169
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1164
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,1
	jne	_1165
	push	ebp
	push	_1167
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1166
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_276
_1165:
	push	_1168
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_GadgetGroup
	add	esp,4
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_37:
	cmp	dword [ebp-8],_bbNullObject
	jne	_39
_38:
	mov	ebx,_bbNullObject
	jmp	_276
_276:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetWindowAlwaysOnTop:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],0
	push	ebp
	push	_1184
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1172
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	_1174
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	je	_1175
	push	ebp
	push	_1183
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1177
	push	ebp
	push	_1179
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1178
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-1
	push	dword [ebp-12]
	call	_SetWindowPos@28
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1180
_1177:
	push	ebp
	push	_1182
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1181
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	19
	push	0
	push	0
	push	0
	push	0
	push	-2
	push	dword [ebp-12]
	call	_SetWindowPos@28
	call	dword [_bbOnDebugLeaveScope]
_1180:
	call	dword [_bbOnDebugLeaveScope]
_1175:
	mov	ebx,0
	jmp	_280
_280:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_BringWindowToTop:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1194
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1188
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1190
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1191
	push	ebp
	push	_1193
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1192
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	19
	push	0
	push	0
	push	0
	push	0
	push	0
	push	dword [ebp-8]
	call	_SetWindowPos@28
	call	dword [_bbOnDebugLeaveScope]
_1191:
	mov	ebx,0
	jmp	_283
_283:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_FocusWindow:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1202
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1196
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1198
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1199
	push	ebp
	push	_1201
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1200
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_SetFocus@4
	call	dword [_bbOnDebugLeaveScope]
_1199:
	mov	ebx,0
	jmp	_286
_286:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GadgetToInt:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1205
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1204
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	ebx,eax
	jmp	_289
_289:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1225
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1207
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	cmp	dword [eax+20],16
	jge	_1208
	push	ebp
	push	_1219
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1209
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+20]
	mov	dword [ebp-8],eax
	push	_1211
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	16
	push	0
	push	dword [ebp-4]
	push	_62
	call	_bbArraySlice
	add	esp,16
	mov	dword [ebp-4],eax
	push	_1212
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_1213
_42:
	push	ebp
	push	_1218
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1214
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	mov	eax,dword [ebp-4]
	cmp	ebx,dword [eax+20]
	jb	_1216
	call	_brl_blitz_ArrayBoundsError
_1216:
	mov	eax,dword [ebp-4]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],16777215
	call	dword [_bbOnDebugLeaveScope]
_40:
	add	dword [ebp-8],1
_1213:
	cmp	dword [ebp-8],16
	jl	_42
_41:
	call	dword [_bbOnDebugLeaveScope]
_1208:
	push	_1220
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	inc	dword [ebx+4]
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	dec	dword [eax+4]
	jnz	_1224
	push	eax
	call	_bbGCFree
	add	esp,4
_1224:
	mov	dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors],ebx
	mov	ebx,0
	jmp	_292
_292:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ClearColorPickerCustomColors:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	dword [ebp-4],0
	push	ebp
	push	_1236
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1228
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-4],0
	mov	dword [ebp-4],0
	jmp	_1230
_45:
	push	ebp
	push	_1235
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1231
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	cmp	ebx,dword [eax+20]
	jb	_1233
	call	_brl_blitz_ArrayBoundsError
_1233:
	mov	eax,dword [__maxgui_win32maxguiex_TWindowsGUIDriver__customcolors]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],16777215
	call	dword [_bbOnDebugLeaveScope]
_43:
	add	dword [ebp-4],1
_1230:
	cmp	dword [ebp-4],16
	jl	_45
_44:
	mov	ebx,0
	jmp	_294
_294:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_RedrawGadgetFrame:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1245
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1238
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1240
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1241
	push	ebp
	push	_1244
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1242
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	55
	push	0
	push	0
	push	0
	push	0
	push	0
	push	dword [ebp-8]
	call	_SetWindowPos@28
	push	_1243
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1345
	push	0
	push	0
	push	dword [ebp-8]
	call	_RedrawWindow@16
	call	dword [_bbOnDebugLeaveScope]
_1241:
	mov	ebx,0
	jmp	_297
_297:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_HideGadgetBorder:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	push	ebp
	push	_1280
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1247
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	je	_1250
	cmp	eax,4
	je	_1250
	cmp	eax,7
	je	_1250
	jmp	_1249
_1250:
	push	ebp
	push	_1279
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1251
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1253
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1254
	push	ebp
	push	_1275
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1255
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-16
	push	dword [ebp-8]
	call	_GetWindowLongW@8
	mov	dword [ebp-12],eax
	push	_1257
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-20
	push	dword [ebp-8]
	call	_GetWindowLongW@8
	mov	dword [ebp-16],eax
	push	_1259
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	push	_1261
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	and	eax,8388608
	cmp	eax,0
	je	_1262
	push	ebp
	push	_1265
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1263
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	and	eax,-8388609
	push	eax
	push	-16
	push	dword [ebp-8]
	call	_SetWindowLongW@12
	push	_1264
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	call	dword [_bbOnDebugLeaveScope]
_1262:
	push	_1266
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	and	eax,512
	cmp	eax,0
	je	_1267
	push	ebp
	push	_1270
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1268
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	and	eax,-513
	push	eax
	push	-20
	push	dword [ebp-8]
	call	_SetWindowLongW@12
	push	_1269
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	call	dword [_bbOnDebugLeaveScope]
_1267:
	push	_1271
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],0
	je	_1272
	push	ebp
	push	_1274
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1273
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_skn3_maxguiex_RedrawGadgetFrame
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1272:
	call	dword [_bbOnDebugLeaveScope]
_1254:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1249
_1249:
	mov	ebx,0
	jmp	_300
_300:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_InstallGuiFont:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1295
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1282
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_31
	push	8
	push	0
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1283
	push	ebp
	push	_1288
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1284
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	push	8
	push	dword [ebp-4]
	call	_bbStringSlice
	add	esp,12
	mov	dword [ebp-4],eax
	push	_1285
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	push	_1287
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	lea	eax,dword [ebp-8]
	push	eax
	push	0
	push	dword [ebp-4]
	call	_bbIncbinLen
	add	esp,4
	push	eax
	push	dword [ebp-4]
	call	_bbIncbinPtr
	add	esp,4
	push	eax
	call	_AddFontMemResourceEx@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_303
_1283:
	push	ebp
	push	_1294
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1291
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bbStringToWString
	add	esp,4
	mov	ebx,eax
	push	0
	push	16
	push	ebx
	call	_AddFontResourceExW@12
	mov	esi,eax
	push	ebx
	call	_bbMemFree
	add	esp,4
	cmp	esi,0
	setne	al
	movzx	eax,al
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_303
_303:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetTextareaLineSpacing:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	fld	dword [ebp+12]
	fstp	dword [ebp-8]
	mov	dword [ebp-12],0
	mov	dword [ebp-16],_bbNullObject
	push	ebp
	push	_1325
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1297
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1298
	push	ebp
	push	_1324
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1299
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-12],eax
	push	_1301
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	je	_1302
	push	ebp
	push	_1322
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1303
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_20
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1305
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1307
	call	_brl_blitz_NullObjectError
_1307:
	mov	dword [ebx+8],188
	push	_1309
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1311
	call	_brl_blitz_NullObjectError
_1311:
	mov	dword [ebx+12],256
	push	_1313
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1315
	call	_brl_blitz_NullObjectError
_1315:
	mov	byte [ebx+178],5
	push	_1317
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1319
	call	_brl_blitz_NullObjectError
_1319:
	fld	dword [ebp-8]
	fmul	dword [_2354]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+172],eax
	push	_1321
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	lea	eax,dword [eax+8]
	push	eax
	push	0
	push	1095
	push	dword [ebp-12]
	call	_SendMessageW@16
	cmp	eax,0
	setne	al
	movzx	eax,al
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_307
_1302:
	call	dword [_bbOnDebugLeaveScope]
_1298:
	mov	ebx,0
	jmp	_307
_307:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToTop:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1337
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1328
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1329
	push	ebp
	push	_1336
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1330
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1332
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1333
	push	ebp
	push	_1335
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1334
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	6
	push	181
	push	dword [ebp-8]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
_1333:
	call	dword [_bbOnDebugLeaveScope]
_1329:
	mov	ebx,0
	jmp	_310
_310:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToBottom:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1348
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1339
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1340
	push	ebp
	push	_1347
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1341
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1343
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1344
	push	ebp
	push	_1346
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1345
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	7
	push	181
	push	dword [ebp-8]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
_1344:
	call	dword [_bbOnDebugLeaveScope]
_1340:
	mov	ebx,0
	jmp	_313
_313:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_ScrollTextAreaToCursor:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_1359
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1350
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_GadgetClass
	add	esp,4
	cmp	eax,5
	jne	_1351
	push	ebp
	push	_1358
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1352
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_maxgui_maxgui_QueryGadget
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1354
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1355
	push	ebp
	push	_1357
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1356
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	183
	push	dword [ebp-8]
	call	_SendMessageW@16
	call	dword [_bbOnDebugLeaveScope]
_1355:
	call	dword [_bbOnDebugLeaveScope]
_1351:
	mov	ebx,0
	jmp	_316
_316:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_GetAppResourcesPath:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_1362
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1361
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_31
	jmp	_318
_318:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_AssignGadgetClassId:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_1368
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1364
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1366
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [_1365],1
	push	_1367
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_1365]
	jmp	_320
_320:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1372
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	__maxgui_win32maxguiex_TWindowsPanel_New
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_skn3_maxguiex_Skn3PanelEx
	mov	eax,dword [ebp-4]
	mov	dword [eax+272],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+276],1
	mov	eax,dword [ebp-4]
	mov	dword [eax+280],255
	mov	eax,dword [ebp-4]
	mov	dword [eax+284],255
	mov	eax,dword [ebp-4]
	mov	dword [eax+288],255
	mov	eax,dword [ebp-4]
	mov	dword [eax+292],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+296],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+300],0
	push	ebp
	push	_1371
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_323
_323:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_326:
	mov	dword [eax],_maxgui_win32maxguiex_TWindowsPanel
	push	eax
	call	__maxgui_win32maxguiex_TWindowsPanel_Delete
	add	esp,4
	mov	eax,0
	jmp	_1374
_1374:
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_WndProc:
	push	ebp
	mov	ebp,esp
	sub	esp,192
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	mov	dword [ebp-60],_bbEmptyArray
	mov	dword [ebp-64],_bbEmptyArray
	mov	dword [ebp-68],_bbEmptyArray
	mov	dword [ebp-72],_bbEmptyArray
	mov	dword [ebp-76],0
	mov	dword [ebp-80],0
	mov	dword [ebp-84],0
	mov	dword [ebp-88],0
	mov	dword [ebp-92],_bbEmptyArray
	fldz
	fstp	dword [ebp-96]
	fldz
	fstp	dword [ebp-100]
	fldz
	fstp	dword [ebp-104]
	fldz
	fstp	dword [ebp-108]
	fldz
	fstp	dword [ebp-112]
	fldz
	fstp	dword [ebp-116]
	fldz
	fstp	dword [ebp-120]
	fldz
	fstp	dword [ebp-124]
	mov	dword [ebp-128],0
	mov	dword [ebp-132],0
	mov	dword [ebp-136],0
	mov	eax,ebp
	push	eax
	push	_1890
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1375
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,20
	je	_1378
	jmp	_1377
_1378:
	mov	eax,ebp
	push	eax
	push	_1888
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1379
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1381
	call	_brl_blitz_NullObjectError
_1381:
	cmp	dword [ebx+272],0
	jne	_1382
	mov	eax,ebp
	push	eax
	push	_1384
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1383
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	mov	ebx,eax
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_333
_1382:
	mov	eax,ebp
	push	eax
	push	_1863
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1386
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1388
	call	_brl_blitz_NullObjectError
_1388:
	cmp	dword [ebx+244],2
	jne	_1389
	mov	eax,ebp
	push	eax
	push	_1391
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1390
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_333
_1389:
	push	_1392
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	push	_1402
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_1403
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-60],eax
	push	4
	push	_1405
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-64],eax
	push	4
	push	_1407
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-68],eax
	push	4
	push	_1409
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-72],eax
	push	_1411
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-68]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-24]
	call	_GetClipBox@8
	push	_1412
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-72]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-8]
	call	_GetWindowRect@8
	push	_1413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-60]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-8]
	call	_GetClientRect@8
	push	_1414
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	mov	eax,dword [ebp-64]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-8]
	call	_GetUpdateRect@12
	cmp	eax,0
	jne	_1415
	mov	eax,ebp
	push	eax
	push	_1417
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1416
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-68]
	mov	dword [ebp-64],eax
	call	dword [_bbOnDebugLeaveScope]
_1415:
	push	_1418
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-64]
	lea	eax,dword [eax+24]
	push	eax
	call	_IsRectEmpty@4
	cmp	eax,0
	je	_1419
	mov	eax,ebp
	push	eax
	push	_1430
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1420
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	edi,eax
	mov	dword [edi+24],0
	mov	dword [edi+28],0
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1422
	call	_brl_blitz_ArrayBoundsError
_1422:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1424
	call	_brl_blitz_ArrayBoundsError
_1424:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [edi+32],edx
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1426
	call	_brl_blitz_ArrayBoundsError
_1426:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1428
	call	_brl_blitz_ArrayBoundsError
_1428:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [edi+36],edx
	mov	dword [ebp-64],edi
	call	dword [_bbOnDebugLeaveScope]
_1419:
	push	_1431
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1433
	call	_brl_blitz_NullObjectError
_1433:
	mov	eax,dword [ebp-8]
	cmp	eax,dword [ebx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_1448
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1435
	call	_brl_blitz_NullObjectError
_1435:
	mov	eax,dword [ebx+200]
	cmp	eax,0
	je	_1438
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1437
	call	_brl_blitz_NullObjectError
_1437:
	mov	eax,dword [ebx+252]
_1438:
	cmp	eax,0
	je	_1442
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1441
	call	_brl_blitz_NullObjectError
_1441:
	mov	eax,dword [ebx+256]
_1442:
	cmp	eax,0
	jne	_1446
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1445
	call	_brl_blitz_NullObjectError
_1445:
	fld	dword [ebx+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_1446:
_1448:
	cmp	eax,0
	je	_1450
	mov	eax,ebp
	push	eax
	push	_1462
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1451
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-24],eax
	push	_1452
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-140],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-140],eax
	jb	_1454
	call	_brl_blitz_ArrayBoundsError
_1454:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1456
	call	_brl_blitz_ArrayBoundsError
_1456:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1458
	call	_brl_blitz_ArrayBoundsError
_1458:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1460
	call	_brl_blitz_ArrayBoundsError
_1460:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-140]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	dword [ebp-16]
	call	_CreateCompatibleBitmap@12
	mov	dword [ebp-28],eax
	push	_1461
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	push	dword [ebp-24]
	call	_SelectObject@8
	call	dword [_bbOnDebugLeaveScope]
_1450:
	push	_1463
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-76],0
	push	_1465
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-80],0
	push	_1467
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-84],0
	push	_1469
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-88],0
	push	_1471
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_62
	call	_bbArrayNew1D
	add	esp,8
	mov	ebx,eax
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1473
	call	_brl_blitz_ArrayBoundsError
_1473:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+24],eax
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1475
	call	_brl_blitz_ArrayBoundsError
_1475:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+28],eax
	mov	esi,2
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1477
	call	_brl_blitz_ArrayBoundsError
_1477:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+32],eax
	mov	esi,3
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1479
	call	_brl_blitz_ArrayBoundsError
_1479:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebx+36],eax
	mov	dword [ebp-92],ebx
	push	_1482
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-96]
	push	_1484
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-100]
	push	_1486
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-104]
	push	_1488
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-108]
	push	_1490
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-112]
	push	_1492
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-116]
	push	_1494
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1496
	call	_brl_blitz_NullObjectError
_1496:
	cmp	dword [ebx+276],0
	je	_1497
	mov	eax,ebp
	push	eax
	push	_1563
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1498
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-60]
	cmp	esi,dword [eax+20]
	jb	_1500
	call	_brl_blitz_ArrayBoundsError
_1500:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1502
	call	_brl_blitz_ArrayBoundsError
_1502:
	mov	eax,dword [ebp-60]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	dword [ebp-80],eax
	push	_1503
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1505
	call	_brl_blitz_NullObjectError
_1505:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1507
	call	_brl_blitz_NullObjectError
_1507:
	mov	eax,dword [esi+292]
	sub	eax,dword [ebx+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-108]
	push	_1508
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1510
	call	_brl_blitz_NullObjectError
_1510:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1512
	call	_brl_blitz_NullObjectError
_1512:
	mov	eax,dword [esi+296]
	sub	eax,dword [ebx+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-112]
	push	_1513
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1515
	call	_brl_blitz_NullObjectError
_1515:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1517
	call	_brl_blitz_NullObjectError
_1517:
	mov	eax,dword [esi+300]
	sub	eax,dword [ebx+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-116]
	push	_1518
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1520
	call	_brl_blitz_NullObjectError
_1520:
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1522
	call	_brl_blitz_ArrayBoundsError
_1522:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1524
	call	_brl_blitz_ArrayBoundsError
_1524:
	mov	eax,dword [edi+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-108]
	faddp	st1,st0
	fstp	dword [ebp-96]
	push	_1525
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1527
	call	_brl_blitz_NullObjectError
_1527:
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1529
	call	_brl_blitz_ArrayBoundsError
_1529:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1531
	call	_brl_blitz_ArrayBoundsError
_1531:
	mov	eax,dword [edi+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-112]
	faddp	st1,st0
	fstp	dword [ebp-100]
	push	_1532
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1534
	call	_brl_blitz_NullObjectError
_1534:
	mov	esi,1
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1536
	call	_brl_blitz_ArrayBoundsError
_1536:
	mov	ebx,1
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1538
	call	_brl_blitz_ArrayBoundsError
_1538:
	mov	eax,dword [edi+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-116]
	faddp	st1,st0
	fstp	dword [ebp-104]
	push	_1539
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1541
	call	_brl_blitz_ArrayBoundsError
_1541:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp-84],eax
	mov	ebx,3
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1543
	call	_brl_blitz_ArrayBoundsError
_1543:
	mov	eax,dword [ebp-64]
	mov	esi,dword [eax+ebx*4+24]
	jmp	_1544
_48:
	mov	eax,ebp
	push	eax
	push	_1562
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1546
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-100]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-96]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	dword [ebp-88],eax
	push	_1547
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	push	dword [ebp-24]
	call	_SelectObject@8
	mov	dword [ebp-76],eax
	push	_1548
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1550
	call	_brl_blitz_ArrayBoundsError
_1550:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	mov	dword [eax+24],edx
	push	_1552
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,3
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1554
	call	_brl_blitz_ArrayBoundsError
_1554:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	add	edx,1
	mov	dword [eax+24],edx
	push	_1556
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	mov	eax,dword [ebp-92]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-24]
	call	_FillRect@12
	push	_1557
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-76]
	push	dword [ebp-24]
	call	_SelectObject@8
	push	_1558
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	call	_DeleteObject@4
	push	_1559
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-96]
	fadd	dword [ebp-108]
	fstp	dword [ebp-96]
	push	_1560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-100]
	fadd	dword [ebp-112]
	fstp	dword [ebp-100]
	push	_1561
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	fadd	dword [ebp-116]
	fstp	dword [ebp-104]
	call	dword [_bbOnDebugLeaveScope]
_46:
	add	dword [ebp-84],1
_1544:
	cmp	dword [ebp-84],esi
	jl	_48
_47:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1564
_1497:
	mov	eax,ebp
	push	eax
	push	_1630
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1565
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1567
	call	_brl_blitz_ArrayBoundsError
_1567:
	mov	ebx,0
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1569
	call	_brl_blitz_ArrayBoundsError
_1569:
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-64]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	call	_bbIntAbs
	add	esp,4
	mov	dword [ebp-80],eax
	push	_1570
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1572
	call	_brl_blitz_NullObjectError
_1572:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1574
	call	_brl_blitz_NullObjectError
_1574:
	mov	eax,dword [esi+292]
	sub	eax,dword [ebx+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-108]
	push	_1575
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1577
	call	_brl_blitz_NullObjectError
_1577:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1579
	call	_brl_blitz_NullObjectError
_1579:
	mov	eax,dword [esi+296]
	sub	eax,dword [ebx+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-112]
	push	_1580
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1582
	call	_brl_blitz_NullObjectError
_1582:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1584
	call	_brl_blitz_NullObjectError
_1584:
	mov	eax,dword [esi+300]
	sub	eax,dword [ebx+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-80]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-116]
	push	_1585
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1587
	call	_brl_blitz_NullObjectError
_1587:
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1589
	call	_brl_blitz_ArrayBoundsError
_1589:
	mov	ebx,0
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1591
	call	_brl_blitz_ArrayBoundsError
_1591:
	mov	eax,dword [edi+280]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-108]
	faddp	st1,st0
	fstp	dword [ebp-96]
	push	_1592
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1594
	call	_brl_blitz_NullObjectError
_1594:
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1596
	call	_brl_blitz_ArrayBoundsError
_1596:
	mov	ebx,0
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1598
	call	_brl_blitz_ArrayBoundsError
_1598:
	mov	eax,dword [edi+284]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-112]
	faddp	st1,st0
	fstp	dword [ebp-100]
	push	_1599
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1601
	call	_brl_blitz_NullObjectError
_1601:
	mov	esi,0
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1603
	call	_brl_blitz_ArrayBoundsError
_1603:
	mov	ebx,0
	mov	eax,dword [ebp-60]
	cmp	ebx,dword [eax+20]
	jb	_1605
	call	_brl_blitz_ArrayBoundsError
_1605:
	mov	eax,dword [edi+288]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-60]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	fmul	dword [ebp-116]
	faddp	st1,st0
	fstp	dword [ebp-104]
	push	_1606
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1608
	call	_brl_blitz_ArrayBoundsError
_1608:
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp-84],eax
	mov	ebx,2
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1610
	call	_brl_blitz_ArrayBoundsError
_1610:
	mov	eax,dword [ebp-64]
	mov	esi,dword [eax+ebx*4+24]
	jmp	_1611
_51:
	mov	eax,ebp
	push	eax
	push	_1629
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1613
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	ebx,eax
	shl	ebx,16
	fld	dword [ebp-100]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	shl	eax,8
	or	ebx,eax
	fld	dword [ebp-96]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	or	ebx,eax
	push	ebx
	call	_CreateSolidBrush@4
	mov	dword [ebp-88],eax
	push	_1614
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	push	dword [ebp-24]
	call	_SelectObject@8
	mov	dword [ebp-76],eax
	push	_1615
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1617
	call	_brl_blitz_ArrayBoundsError
_1617:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	mov	dword [eax+24],edx
	push	_1619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,2
	mov	eax,dword [ebp-92]
	cmp	ebx,dword [eax+20]
	jb	_1621
	call	_brl_blitz_ArrayBoundsError
_1621:
	mov	eax,dword [ebp-92]
	shl	ebx,2
	add	eax,ebx
	mov	edx,dword [ebp-84]
	add	edx,1
	mov	dword [eax+24],edx
	push	_1623
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	mov	eax,dword [ebp-92]
	lea	eax,dword [eax+24]
	push	eax
	push	dword [ebp-24]
	call	_FillRect@12
	push	_1624
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-76]
	push	dword [ebp-24]
	call	_SelectObject@8
	push	_1625
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-88]
	call	_DeleteObject@4
	push	_1626
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-96]
	fadd	dword [ebp-108]
	fstp	dword [ebp-96]
	push	_1627
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-100]
	fadd	dword [ebp-112]
	fstp	dword [ebp-100]
	push	_1628
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-104]
	fadd	dword [ebp-116]
	fstp	dword [ebp-104]
	call	dword [_bbOnDebugLeaveScope]
_49:
	add	dword [ebp-84],1
_1611:
	cmp	dword [ebp-84],esi
	jl	_51
_50:
	call	dword [_bbOnDebugLeaveScope]
_1564:
	push	_1631
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1633
	call	_brl_blitz_NullObjectError
_1633:
	mov	eax,dword [ebp-8]
	cmp	eax,dword [ebx+148]
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_1648
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1635
	call	_brl_blitz_NullObjectError
_1635:
	mov	eax,dword [ebx+200]
	cmp	eax,0
	je	_1638
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1637
	call	_brl_blitz_NullObjectError
_1637:
	mov	eax,dword [ebx+252]
_1638:
	cmp	eax,0
	je	_1642
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1641
	call	_brl_blitz_NullObjectError
_1641:
	mov	eax,dword [ebx+256]
_1642:
	cmp	eax,0
	jne	_1646
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1645
	call	_brl_blitz_NullObjectError
_1645:
	fld	dword [ebx+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setb	al
	movzx	eax,al
_1646:
_1648:
	cmp	eax,0
	jne	_1650
	mov	eax,ebp
	push	eax
	push	_1652
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1651
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_333
_1650:
	push	_1653
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1655
	call	_brl_blitz_NullObjectError
_1655:
	mov	eax,dword [ebx+200]
	cmp	eax,0
	je	_1658
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1657
	call	_brl_blitz_NullObjectError
_1657:
	mov	eax,dword [ebx+252]
_1658:
	cmp	eax,0
	je	_1662
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1661
	call	_brl_blitz_NullObjectError
_1661:
	mov	eax,dword [ebx+256]
_1662:
	cmp	eax,0
	je	_1664
	mov	eax,ebp
	push	eax
	push	_1810
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1665
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_CreateCompatibleDC@4
	mov	dword [ebp-32],eax
	push	_1666
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1668
	call	_brl_blitz_NullObjectError
_1668:
	push	dword [ebx+200]
	push	dword [ebp-32]
	call	_SelectObject@8
	push	_1669
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1671
	call	_brl_blitz_NullObjectError
_1671:
	mov	eax,dword [ebx+252]
	mov	dword [ebp-36],eax
	push	_1672
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1674
	call	_brl_blitz_NullObjectError
_1674:
	mov	eax,dword [ebx+256]
	mov	dword [ebp-40],eax
	push	_1675
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1677
	call	_brl_blitz_NullObjectError
_1677:
	mov	eax,dword [ebx+260]
	and	eax,7
	cmp	eax,0
	je	_1680
	cmp	eax,1
	je	_1681
	cmp	eax,2
	je	_1682
	cmp	eax,4
	je	_1682
	cmp	eax,3
	je	_1683
	jmp	_1679
_1680:
	mov	eax,ebp
	push	eax
	push	_1708
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1684
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_52
_54:
	mov	eax,ebp
	push	eax
	push	_1707
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1689
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],0
	push	_1690
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_55
_57:
	mov	eax,ebp
	push	eax
	push	_1705
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1695
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1697
	call	_brl_blitz_NullObjectError
_1697:
	cmp	dword [ebx+268],0
	je	_1698
	mov	eax,ebp
	push	eax
	push	_1700
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1699
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1701
_1698:
	mov	eax,ebp
	push	eax
	push	_1703
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1702
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	13369376
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_BitBlt@36
	call	dword [_bbOnDebugLeaveScope]
_1701:
	push	_1704
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-36]
	add	dword [ebp-44],eax
	call	dword [_bbOnDebugLeaveScope]
_55:
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1692
	call	_brl_blitz_ArrayBoundsError
_1692:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1694
	call	_brl_blitz_ArrayBoundsError
_1694:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	cmp	dword [ebp-44],edx
	jl	_57
_56:
	push	_1706
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-40]
	add	dword [ebp-48],eax
	call	dword [_bbOnDebugLeaveScope]
_52:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1686
	call	_brl_blitz_ArrayBoundsError
_1686:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1688
	call	_brl_blitz_ArrayBoundsError
_1688:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	cmp	dword [ebp-48],edx
	jl	_54
_53:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1679
_1681:
	mov	eax,ebp
	push	eax
	push	_1728
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1709
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1711
	call	_brl_blitz_ArrayBoundsError
_1711:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1713
	call	_brl_blitz_ArrayBoundsError
_1713:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-36]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-44],eax
	push	_1714
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1716
	call	_brl_blitz_ArrayBoundsError
_1716:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1718
	call	_brl_blitz_ArrayBoundsError
_1718:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-40]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-48],eax
	push	_1719
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1721
	call	_brl_blitz_NullObjectError
_1721:
	cmp	dword [ebx+268],0
	je	_1722
	mov	eax,ebp
	push	eax
	push	_1724
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1723
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1725
_1722:
	mov	eax,ebp
	push	eax
	push	_1727
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1726
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	13369376
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_BitBlt@36
	call	dword [_bbOnDebugLeaveScope]
_1725:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1679
_1682:
	mov	eax,ebp
	push	eax
	push	_1777
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1729
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1731
	call	_brl_blitz_ArrayBoundsError
_1731:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1733
	call	_brl_blitz_ArrayBoundsError
_1733:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-36]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-120]
	push	_1735
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1737
	call	_brl_blitz_ArrayBoundsError
_1737:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1739
	call	_brl_blitz_ArrayBoundsError
_1739:
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	mov	dword [ebp+-192],edx
	fild	dword [ebp+-192]
	mov	eax,dword [ebp-40]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fdivp	st1,st0
	fstp	dword [ebp-124]
	push	_1741
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	fld	dword [ebp-124]
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setbe	al
	movzx	eax,al
	cmp	eax,0
	jne	_1742
	mov	eax,ebp
	push	eax
	push	_1752
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1743
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1745
	call	_brl_blitz_NullObjectError
_1745:
	mov	eax,dword [ebx+260]
	and	eax,7
	cmp	eax,2
	jne	_1746
	mov	eax,ebp
	push	eax
	push	_1748
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1747
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-124]
	fstp	dword [ebp-120]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1749
_1746:
	mov	eax,ebp
	push	eax
	push	_1751
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1750
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	fstp	dword [ebp-124]
	call	dword [_bbOnDebugLeaveScope]
_1749:
	call	dword [_bbOnDebugLeaveScope]
_1742:
	push	_1753
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	mov	eax,dword [ebp-36]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-128],eax
	push	_1755
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-120]
	mov	eax,dword [ebp-40]
	mov	dword [ebp+-192],eax
	fild	dword [ebp+-192]
	fmulp	st1,st0
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-132],eax
	push	_1757
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,2
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1759
	call	_brl_blitz_ArrayBoundsError
_1759:
	mov	ebx,0
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1761
	call	_brl_blitz_ArrayBoundsError
_1761:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-128]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-44],eax
	push	_1762
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1764
	call	_brl_blitz_ArrayBoundsError
_1764:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1766
	call	_brl_blitz_ArrayBoundsError
_1766:
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+esi*4+24]
	mov	edx,dword [ebp-72]
	sub	eax,dword [edx+ebx*4+24]
	sub	eax,dword [ebp-132]
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	mov	dword [ebp-48],eax
	push	_1767
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	3
	push	dword [ebp-24]
	call	_SetStretchBltMode@8
	push	_1768
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1770
	call	_brl_blitz_NullObjectError
_1770:
	cmp	dword [ebx+268],0
	je	_1771
	mov	eax,ebp
	push	eax
	push	_1773
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1772
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-132]
	push	dword [ebp-128]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1774
_1771:
	mov	eax,ebp
	push	eax
	push	_1776
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1775
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	13369376
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	push	dword [ebp-132]
	push	dword [ebp-128]
	push	dword [ebp-48]
	push	dword [ebp-44]
	push	dword [ebp-24]
	call	_StretchBlt@44
	call	dword [_bbOnDebugLeaveScope]
_1774:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1679
_1683:
	mov	eax,ebp
	push	eax
	push	_1808
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1782
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	3
	push	dword [ebp-24]
	call	_SetStretchBltMode@8
	push	_1783
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1785
	call	_brl_blitz_NullObjectError
_1785:
	cmp	dword [ebx+268],0
	je	_1786
	mov	eax,ebp
	push	eax
	push	_1796
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1787
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-144],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-144],eax
	jb	_1789
	call	_brl_blitz_ArrayBoundsError
_1789:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1791
	call	_brl_blitz_ArrayBoundsError
_1791:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1793
	call	_brl_blitz_ArrayBoundsError
_1793:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1795
	call	_brl_blitz_ArrayBoundsError
_1795:
	push	33488896
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-144]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-24]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1797
_1786:
	mov	eax,ebp
	push	eax
	push	_1807
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1798
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-148],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-148],eax
	jb	_1800
	call	_brl_blitz_ArrayBoundsError
_1800:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1802
	call	_brl_blitz_ArrayBoundsError
_1802:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1804
	call	_brl_blitz_ArrayBoundsError
_1804:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1806
	call	_brl_blitz_ArrayBoundsError
_1806:
	push	13369376
	push	dword [ebp-40]
	push	dword [ebp-36]
	push	0
	push	0
	push	dword [ebp-32]
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-148]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-24]
	call	_StretchBlt@44
	call	dword [_bbOnDebugLeaveScope]
_1797:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1679
_1679:
	push	_1809
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-32]
	call	_DeleteDC@4
	call	dword [_bbOnDebugLeaveScope]
_1664:
	push	_1811
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1813
	call	_brl_blitz_NullObjectError
_1813:
	fld	dword [ebx+248]
	fld1
	fxch	st1
	fucompp
	fnstsw	ax
	sahf
	setae	al
	movzx	eax,al
	cmp	eax,0
	jne	_1814
	mov	eax,ebp
	push	eax
	push	_1845
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1815
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-8]
	push	dword [ebp-16]
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax]
	call	dword [eax+544]
	add	esp,12
	push	_1816
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1818
	call	_brl_blitz_NullObjectError
_1818:
	fld	dword [ebx+248]
	fmul	dword [_2386]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	and	eax,255
	shl	eax,16
	mov	dword [ebp-136],eax
	push	_1820
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-188],0
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-188],eax
	jb	_1822
	call	_brl_blitz_ArrayBoundsError
_1822:
	mov	dword [ebp-184],1
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-184],eax
	jb	_1824
	call	_brl_blitz_ArrayBoundsError
_1824:
	mov	dword [ebp-180],2
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-180],eax
	jb	_1826
	call	_brl_blitz_ArrayBoundsError
_1826:
	mov	dword [ebp-176],0
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-176],eax
	jb	_1828
	call	_brl_blitz_ArrayBoundsError
_1828:
	mov	dword [ebp-172],3
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-172],eax
	jb	_1830
	call	_brl_blitz_ArrayBoundsError
_1830:
	mov	dword [ebp-168],1
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-168],eax
	jb	_1832
	call	_brl_blitz_ArrayBoundsError
_1832:
	mov	dword [ebp-164],0
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-164],eax
	jb	_1834
	call	_brl_blitz_ArrayBoundsError
_1834:
	mov	dword [ebp-160],1
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-160],eax
	jb	_1836
	call	_brl_blitz_ArrayBoundsError
_1836:
	mov	dword [ebp-152],2
	mov	eax,dword [ebp-64]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-152],eax
	jb	_1838
	call	_brl_blitz_ArrayBoundsError
_1838:
	mov	edi,0
	mov	eax,dword [ebp-64]
	cmp	edi,dword [eax+20]
	jb	_1840
	call	_brl_blitz_ArrayBoundsError
_1840:
	mov	esi,3
	mov	eax,dword [ebp-64]
	cmp	esi,dword [eax+20]
	jb	_1842
	call	_brl_blitz_ArrayBoundsError
_1842:
	mov	ebx,1
	mov	eax,dword [ebp-64]
	cmp	ebx,dword [eax+20]
	jb	_1844
	call	_brl_blitz_ArrayBoundsError
_1844:
	push	dword [ebp-136]
	mov	eax,dword [ebp-64]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-64]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-152]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-64]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-160]
	push	dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-164]
	push	dword [edx+eax*4+24]
	push	dword [ebp-24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-172]
	mov	ecx,dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-168]
	sub	ecx,dword [edx+eax*4+24]
	push	ecx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-180]
	mov	ecx,dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-176]
	sub	ecx,dword [edx+eax*4+24]
	push	ecx
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-184]
	push	dword [edx+eax*4+24]
	mov	edx,dword [ebp-64]
	mov	eax,dword [ebp-188]
	push	dword [edx+eax*4+24]
	push	dword [ebp-16]
	call	_AlphaBlend@44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1847
_1814:
	mov	eax,ebp
	push	eax
	push	_1857
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1848
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-156],2
	mov	eax,dword [ebp-72]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-156],eax
	jb	_1850
	call	_brl_blitz_ArrayBoundsError
_1850:
	mov	edi,0
	mov	eax,dword [ebp-72]
	cmp	edi,dword [eax+20]
	jb	_1852
	call	_brl_blitz_ArrayBoundsError
_1852:
	mov	esi,3
	mov	eax,dword [ebp-72]
	cmp	esi,dword [eax+20]
	jb	_1854
	call	_brl_blitz_ArrayBoundsError
_1854:
	mov	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1856
	call	_brl_blitz_ArrayBoundsError
_1856:
	push	13369376
	push	0
	push	0
	push	dword [ebp-24]
	mov	eax,dword [ebp-72]
	mov	edx,dword [eax+esi*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	mov	edx,dword [ebp-72]
	mov	eax,dword [ebp-156]
	mov	edx,dword [edx+eax*4+24]
	mov	eax,dword [ebp-72]
	sub	edx,dword [eax+edi*4+24]
	push	edx
	push	0
	push	0
	push	dword [ebp-16]
	call	_BitBlt@36
	call	dword [_bbOnDebugLeaveScope]
_1847:
	push	_1858
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	cmp	dword [ebp-24],eax
	jne	_1859
	push	_58
	call	_brl_blitz_RuntimeError
	add	esp,4
_1859:
	push	_1860
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	call	_DeleteObject@4
	push	_1861
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	call	_DeleteDC@4
	push	_1862
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_333
_1377:
	push	_1889
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	__maxgui_win32maxguiex_TWindowsPanel_WndProc
	add	esp,20
	mov	ebx,eax
	jmp	_333
_333:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__skn3_maxguiex_Skn3PanelEx_SetGradient:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp+36]
	mov	dword [ebp-32],eax
	mov	eax,dword [ebp+40]
	mov	dword [ebp-36],eax
	push	ebp
	push	_1991
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1894
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	jne	_1895
	push	ebp
	push	_1906
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1896
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1898
	call	_brl_blitz_NullObjectError
_1898:
	cmp	dword [ebx+272],0
	je	_1899
	push	ebp
	push	_1905
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1900
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1902
	call	_brl_blitz_NullObjectError
_1902:
	mov	dword [ebx+272],0
	push	_1904
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1899:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1907
_1895:
	push	ebp
	push	_1990
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1908
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,dword [ebp-24]
	sete	al
	movzx	eax,al
	cmp	eax,0
	je	_1909
	mov	eax,dword [ebp-20]
	cmp	eax,dword [ebp-32]
	sete	al
	movzx	eax,al
_1909:
	cmp	eax,0
	je	_1911
	mov	eax,dword [ebp-16]
	cmp	eax,dword [ebp-28]
	sete	al
	movzx	eax,al
_1911:
	cmp	eax,0
	je	_1913
	push	ebp
	push	_1921
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1914
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1916
	call	_brl_blitz_NullObjectError
_1916:
	mov	dword [ebx+272],0
	push	_1918
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1920
	call	_brl_blitz_NullObjectError
_1920:
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	dword [ebp-12]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+248]
	add	esp,16
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1922
_1913:
	push	ebp
	push	_1989
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1923
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1925
	call	_brl_blitz_NullObjectError
_1925:
	mov	eax,dword [ebx+272]
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_1928
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1927
	call	_brl_blitz_NullObjectError
_1927:
	mov	eax,dword [ebp-12]
	cmp	eax,dword [ebx+280]
	setne	al
	movzx	eax,al
_1928:
	cmp	eax,0
	jne	_1932
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1931
	call	_brl_blitz_NullObjectError
_1931:
	mov	eax,dword [ebp-20]
	cmp	eax,dword [ebx+284]
	setne	al
	movzx	eax,al
_1932:
	cmp	eax,0
	jne	_1936
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1935
	call	_brl_blitz_NullObjectError
_1935:
	mov	eax,dword [ebp-16]
	cmp	eax,dword [ebx+288]
	setne	al
	movzx	eax,al
_1936:
	cmp	eax,0
	jne	_1940
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1939
	call	_brl_blitz_NullObjectError
_1939:
	mov	eax,dword [ebp-24]
	cmp	eax,dword [ebx+292]
	setne	al
	movzx	eax,al
_1940:
	cmp	eax,0
	jne	_1944
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1943
	call	_brl_blitz_NullObjectError
_1943:
	mov	eax,dword [ebp-32]
	cmp	eax,dword [ebx+296]
	setne	al
	movzx	eax,al
_1944:
	cmp	eax,0
	jne	_1948
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1947
	call	_brl_blitz_NullObjectError
_1947:
	mov	eax,dword [ebp-28]
	cmp	eax,dword [ebx+300]
	setne	al
	movzx	eax,al
_1948:
	cmp	eax,0
	jne	_1952
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1951
	call	_brl_blitz_NullObjectError
_1951:
	mov	eax,dword [ebp-36]
	cmp	eax,dword [ebx+276]
	setne	al
	movzx	eax,al
_1952:
	cmp	eax,0
	je	_1954
	push	ebp
	push	_1988
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1955
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1957
	call	_brl_blitz_NullObjectError
_1957:
	mov	dword [ebx+272],1
	push	_1959
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1961
	call	_brl_blitz_NullObjectError
_1961:
	mov	eax,dword [ebp-12]
	mov	dword [ebx+280],eax
	push	_1963
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1965
	call	_brl_blitz_NullObjectError
_1965:
	mov	eax,dword [ebp-20]
	mov	dword [ebx+284],eax
	push	_1967
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1969
	call	_brl_blitz_NullObjectError
_1969:
	mov	eax,dword [ebp-16]
	mov	dword [ebx+288],eax
	push	_1971
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1973
	call	_brl_blitz_NullObjectError
_1973:
	mov	eax,dword [ebp-24]
	mov	dword [ebx+292],eax
	push	_1975
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1977
	call	_brl_blitz_NullObjectError
_1977:
	mov	eax,dword [ebp-32]
	mov	dword [ebx+296],eax
	push	_1979
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1981
	call	_brl_blitz_NullObjectError
_1981:
	mov	eax,dword [ebp-28]
	mov	dword [ebx+300],eax
	push	_1983
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1985
	call	_brl_blitz_NullObjectError
_1985:
	mov	eax,dword [ebp-36]
	mov	dword [ebx+276],eax
	push	_1987
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_maxgui_maxgui_RedrawGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1954:
	call	dword [_bbOnDebugLeaveScope]
_1922:
	call	dword [_bbOnDebugLeaveScope]
_1907:
	mov	ebx,0
	jmp	_344
_344:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_CreatePanelEx:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	dword [ebp-32],_bbNullObject
	push	ebp
	push	_2057
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2000
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_skn3_maxguiex_GetCreationGroup
	add	esp,4
	mov	dword [ebp-20],eax
	push	_2001
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_2003
	call	_brl_blitz_NullObjectError
_2003:
	push	_1
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+480]
	add	esp,16
	mov	dword [ebp-32],eax
	push	_2005
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_maxgui_localization_LocalizationMode
	and	eax,2
	cmp	eax,0
	je	_2006
	push	ebp
	push	_2008
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2007
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-28]
	push	dword [ebp-32]
	call	_maxgui_maxgui_LocalizeGadget
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2009
_2006:
	push	ebp
	push	_2013
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2010
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2012
	call	_brl_blitz_NullObjectError
_2012:
	push	dword [ebp-28]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+236]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2009:
	push	_2014
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],_bbNullObject
	je	_2015
	push	ebp
	push	_2019
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2016
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2018
	call	_brl_blitz_NullObjectError
_2018:
	push	-1
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
_2015:
	push	_2020
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2022
	call	_brl_blitz_NullObjectError
_2022:
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+88]
	add	esp,20
	push	_2023
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_2024
	push	ebp
	push	_2049
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2025
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2027
	call	_brl_blitz_NullObjectError
_2027:
	push	dword [__maxgui_win32maxguiex_TWindowsGUIDriver_GDIFont]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+244]
	add	esp,8
	push	_2028
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-20]
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_2029
	push	ebp
	push	_2045
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2030
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_2032
	call	_brl_blitz_NullObjectError
_2032:
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-20]
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	jne	_2035
	call	_brl_blitz_NullObjectError
_2035:
	mov	eax,dword [esi+232]
	cmp	eax,0
	je	_2038
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-20]
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	jne	_2037
	call	_brl_blitz_NullObjectError
_2037:
	mov	eax,dword [esi+236]
	cmp	eax,0
	sete	al
	movzx	eax,al
_2038:
	cmp	eax,0
	sete	al
	movzx	eax,al
	mov	dword [ebx+236],eax
	push	_2040
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2042
	call	_brl_blitz_NullObjectError
_2042:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_2044
	call	_brl_blitz_NullObjectError
_2044:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+232]
	add	esp,4
	and	eax,4
	cmp	eax,0
	sete	al
	movzx	eax,al
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+284]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2029:
	push	_2046
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2048
	call	_brl_blitz_NullObjectError
_2048:
	push	0
	push	1
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+280]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
_2024:
	push	_2050
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_2051
	push	ebp
	push	_2055
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2052
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_maxgui_win32maxguiex_TWindowsGadget
	push	dword [ebp-32]
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_2054
	call	_brl_blitz_NullObjectError
_2054:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+548]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_2051:
	push	_2056
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	jmp	_353
_353:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_maxguiex_SetPanelExGradient:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp+36]
	mov	dword [ebp-32],eax
	mov	eax,dword [ebp+40]
	mov	dword [ebp-36],eax
	mov	dword [ebp-40],_bbNullObject
	push	ebp
	push	_2070
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2062
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_skn3_maxguiex_Skn3PanelEx
	push	dword [ebp-4]
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-40],eax
	push	_2064
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-40],_bbNullObject
	je	_2065
	push	ebp
	push	_2069
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2066
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_2068
	call	_brl_blitz_NullObjectError
_2068:
	push	dword [ebp-36]
	push	dword [ebp-28]
	push	dword [ebp-32]
	push	dword [ebp-24]
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+580]
	add	esp,36
	call	dword [_bbOnDebugLeaveScope]
_2065:
	mov	ebx,0
	jmp	_364
_364:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_377:
	dd	0
_370:
	db	"maxguiex",0
_371:
	db	"BCM_GETIDEALSIZE",0
_62:
	db	"i",0
	align	4
_372:
	dd	_bbStringClass
	dd	2147483646
	dd	4
	dw	53,54,51,51
_373:
	db	"BCM_GETTEXTMARGIN",0
	align	4
_374:
	dd	_bbStringClass
	dd	2147483646
	dd	4
	dw	53,54,51,55
_375:
	db	"FR_PRIVATE",0
	align	4
_376:
	dd	_bbStringClass
	dd	2147483646
	dd	2
	dw	49,54
	align	4
_369:
	dd	1
	dd	_370
	dd	1
	dd	_371
	dd	_62
	dd	_372
	dd	1
	dd	_373
	dd	_62
	dd	_374
	dd	1
	dd	_375
	dd	_62
	dd	_376
	dd	0
_367:
	db	"$BMXPATH/mod/skn3.mod/maxguiex.mod/maxguiex.bmx",0
	align	4
_366:
	dd	_367
	dd	58
	dd	2
	align	4
__skn3_maxguiex_Skn3ListBatchLock_all:
	dd	_bbNullObject
_60:
	db	"Skn3ListBatchLock",0
_61:
	db	"refCount",0
_63:
	db	"listBox",0
_64:
	db	":TWindowsListBox",0
_65:
	db	"index",0
_66:
	db	"link",0
_67:
	db	":TLink",0
_68:
	db	"it",0
_69:
	db	":LVITEMW",0
_70:
	db	"hwnd",0
_71:
	db	"New",0
_72:
	db	"()i",0
_73:
	db	"Delete",0
_74:
	db	"Find",0
_75:
	db	"(:TGadget):Skn3ListBatchLock",0
_76:
	db	"add",0
_77:
	db	"(:Skn3ListBatchLock)i",0
_78:
	db	"remove",0
	align	4
_59:
	dd	2
	dd	_60
	dd	3
	dd	_61
	dd	_62
	dd	8
	dd	3
	dd	_63
	dd	_64
	dd	12
	dd	3
	dd	_65
	dd	_62
	dd	16
	dd	3
	dd	_66
	dd	_67
	dd	20
	dd	3
	dd	_68
	dd	_69
	dd	24
	dd	3
	dd	_70
	dd	_62
	dd	28
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	7
	dd	_74
	dd	_75
	dd	48
	dd	7
	dd	_76
	dd	_77
	dd	52
	dd	7
	dd	_78
	dd	_77
	dd	56
	dd	0
	align	4
_16:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_59
	dd	32
	dd	__skn3_maxguiex_Skn3ListBatchLock_New
	dd	__skn3_maxguiex_Skn3ListBatchLock_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__skn3_maxguiex_Skn3ListBatchLock_Find
	dd	__skn3_maxguiex_Skn3ListBatchLock_add
	dd	__skn3_maxguiex_Skn3ListBatchLock_remove
	align	4
_368:
	dd	_367
	dd	150
	dd	2
	align	4
__skn3_maxguiex_Skn3CustomPointer_all:
	dd	_bbNullObject
_80:
	db	"Skn3CustomPointer",0
_81:
	db	"path",0
_82:
	db	"$",0
_83:
	db	"pointer",0
	align	4
_79:
	dd	2
	dd	_80
	dd	3
	dd	_81
	dd	_82
	dd	8
	dd	3
	dd	_83
	dd	_62
	dd	12
	dd	3
	dd	_61
	dd	_62
	dd	16
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	0
	align	4
_skn3_maxguiex_Skn3CustomPointer:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_79
	dd	20
	dd	__skn3_maxguiex_Skn3CustomPointer_New
	dd	__skn3_maxguiex_Skn3CustomPointer_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_85:
	db	"PARAFORMAT2",0
_86:
	db	"cbSize",0
_87:
	db	"dwMask",0
_88:
	db	"wNumbering",0
_89:
	db	"s",0
_90:
	db	"wEffects",0
_91:
	db	"dxStartIndent",0
_92:
	db	"dxRightIndent",0
_93:
	db	"dxOffset",0
_94:
	db	"wAlignment",0
_95:
	db	"cTabCount",0
_96:
	db	"rgxTabs00",0
_97:
	db	"rgxTabs01",0
_98:
	db	"rgxTabs02",0
_99:
	db	"rgxTabs03",0
_100:
	db	"rgxTabs10",0
_101:
	db	"rgxTabs11",0
_102:
	db	"rgxTabs12",0
_103:
	db	"rgxTabs13",0
_104:
	db	"rgxTabs20",0
_105:
	db	"rgxTabs21",0
_106:
	db	"rgxTabs22",0
_107:
	db	"rgxTabs23",0
_108:
	db	"rgxTabs30",0
_109:
	db	"rgxTabs31",0
_110:
	db	"rgxTabs32",0
_111:
	db	"rgxTabs33",0
_112:
	db	"rgxTabs40",0
_113:
	db	"rgxTabs41",0
_114:
	db	"rgxTabs42",0
_115:
	db	"rgxTabs43",0
_116:
	db	"rgxTabs50",0
_117:
	db	"rgxTabs51",0
_118:
	db	"rgxTabs52",0
_119:
	db	"rgxTabs53",0
_120:
	db	"rgxTabs60",0
_121:
	db	"rgxTabs61",0
_122:
	db	"rgxTabs62",0
_123:
	db	"rgxTabs63",0
_124:
	db	"rgxTabs70",0
_125:
	db	"rgxTabs71",0
_126:
	db	"rgxTabs72",0
_127:
	db	"rgxTabs73",0
_128:
	db	"dySpaceBefore",0
_129:
	db	"dySpaceAfter",0
_130:
	db	"dyLineSpacing",0
_131:
	db	"sStyle",0
_132:
	db	"bLineSpacingRule",0
_133:
	db	"b",0
_134:
	db	"bOutlineLevel",0
_135:
	db	"wShadingWeight",0
_136:
	db	"wShadingStyle",0
_137:
	db	"wNumberingStart",0
_138:
	db	"wNumberingStyle",0
_139:
	db	"wNumberingTab",0
_140:
	db	"wBorderSpace",0
_141:
	db	"wBorderWidth",0
_142:
	db	"wBorders",0
	align	4
_84:
	dd	2
	dd	_85
	dd	3
	dd	_86
	dd	_62
	dd	8
	dd	3
	dd	_87
	dd	_62
	dd	12
	dd	3
	dd	_88
	dd	_89
	dd	16
	dd	3
	dd	_90
	dd	_89
	dd	18
	dd	3
	dd	_91
	dd	_62
	dd	20
	dd	3
	dd	_92
	dd	_62
	dd	24
	dd	3
	dd	_93
	dd	_62
	dd	28
	dd	3
	dd	_94
	dd	_89
	dd	32
	dd	3
	dd	_95
	dd	_89
	dd	34
	dd	3
	dd	_96
	dd	_62
	dd	36
	dd	3
	dd	_97
	dd	_62
	dd	40
	dd	3
	dd	_98
	dd	_62
	dd	44
	dd	3
	dd	_99
	dd	_62
	dd	48
	dd	3
	dd	_100
	dd	_62
	dd	52
	dd	3
	dd	_101
	dd	_62
	dd	56
	dd	3
	dd	_102
	dd	_62
	dd	60
	dd	3
	dd	_103
	dd	_62
	dd	64
	dd	3
	dd	_104
	dd	_62
	dd	68
	dd	3
	dd	_105
	dd	_62
	dd	72
	dd	3
	dd	_106
	dd	_62
	dd	76
	dd	3
	dd	_107
	dd	_62
	dd	80
	dd	3
	dd	_108
	dd	_62
	dd	84
	dd	3
	dd	_109
	dd	_62
	dd	88
	dd	3
	dd	_110
	dd	_62
	dd	92
	dd	3
	dd	_111
	dd	_62
	dd	96
	dd	3
	dd	_112
	dd	_62
	dd	100
	dd	3
	dd	_113
	dd	_62
	dd	104
	dd	3
	dd	_114
	dd	_62
	dd	108
	dd	3
	dd	_115
	dd	_62
	dd	112
	dd	3
	dd	_116
	dd	_62
	dd	116
	dd	3
	dd	_117
	dd	_62
	dd	120
	dd	3
	dd	_118
	dd	_62
	dd	124
	dd	3
	dd	_119
	dd	_62
	dd	128
	dd	3
	dd	_120
	dd	_62
	dd	132
	dd	3
	dd	_121
	dd	_62
	dd	136
	dd	3
	dd	_122
	dd	_62
	dd	140
	dd	3
	dd	_123
	dd	_62
	dd	144
	dd	3
	dd	_124
	dd	_62
	dd	148
	dd	3
	dd	_125
	dd	_62
	dd	152
	dd	3
	dd	_126
	dd	_62
	dd	156
	dd	3
	dd	_127
	dd	_62
	dd	160
	dd	3
	dd	_128
	dd	_62
	dd	164
	dd	3
	dd	_129
	dd	_62
	dd	168
	dd	3
	dd	_130
	dd	_62
	dd	172
	dd	3
	dd	_131
	dd	_89
	dd	176
	dd	3
	dd	_132
	dd	_133
	dd	178
	dd	3
	dd	_134
	dd	_133
	dd	179
	dd	3
	dd	_135
	dd	_89
	dd	180
	dd	3
	dd	_136
	dd	_89
	dd	182
	dd	3
	dd	_137
	dd	_89
	dd	184
	dd	3
	dd	_138
	dd	_89
	dd	186
	dd	3
	dd	_139
	dd	_89
	dd	188
	dd	3
	dd	_140
	dd	_89
	dd	190
	dd	3
	dd	_141
	dd	_89
	dd	192
	dd	3
	dd	_142
	dd	_89
	dd	194
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	0
	align	4
_20:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_84
	dd	196
	dd	__skn3_maxguiex_PARAFORMAT2_New
	dd	__skn3_maxguiex_PARAFORMAT2_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_144:
	db	"Skn3PanelEx",0
_145:
	db	"gradientOn",0
_146:
	db	"gradientVertical",0
_147:
	db	"gradientStartR",0
_148:
	db	"gradientStartG",0
_149:
	db	"gradientStartB",0
_150:
	db	"gradientEndR",0
_151:
	db	"gradientEndG",0
_152:
	db	"gradientEndB",0
_153:
	db	"WndProc",0
_154:
	db	"(i,i,i,i)i",0
_155:
	db	"SetGradient",0
_156:
	db	"(i,i,i,i,i,i,i,i)i",0
	align	4
_143:
	dd	2
	dd	_144
	dd	3
	dd	_145
	dd	_62
	dd	272
	dd	3
	dd	_146
	dd	_62
	dd	276
	dd	3
	dd	_147
	dd	_62
	dd	280
	dd	3
	dd	_148
	dd	_62
	dd	284
	dd	3
	dd	_149
	dd	_62
	dd	288
	dd	3
	dd	_150
	dd	_62
	dd	292
	dd	3
	dd	_151
	dd	_62
	dd	296
	dd	3
	dd	_152
	dd	_62
	dd	300
	dd	6
	dd	_71
	dd	_72
	dd	16
	dd	6
	dd	_73
	dd	_72
	dd	20
	dd	6
	dd	_153
	dd	_154
	dd	520
	dd	6
	dd	_155
	dd	_156
	dd	580
	dd	0
	align	4
_skn3_maxguiex_Skn3PanelEx:
	dd	_maxgui_win32maxguiex_TWindowsPanel
	dd	_bbObjectFree
	dd	_143
	dd	304
	dd	__skn3_maxguiex_Skn3PanelEx_New
	dd	__skn3_maxguiex_Skn3PanelEx_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__maxgui_maxgui_TGadget_SetFilter
	dd	__maxgui_maxgui_TGadget_HasDescendant
	dd	__maxgui_maxgui_TGadget__setparent
	dd	__maxgui_maxgui_TGadget_SelectionChanged
	dd	__maxgui_maxgui_TGadget_Handle
	dd	__maxgui_maxgui_TGadget_GetXPos
	dd	__maxgui_maxgui_TGadget_GetYPos
	dd	__maxgui_maxgui_TGadget_GetWidth
	dd	__maxgui_maxgui_TGadget_GetHeight
	dd	__maxgui_maxgui_TGadget_GetGroup
	dd	__maxgui_maxgui_TGadget_SetShape
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetArea
	dd	__maxgui_maxgui_TGadget_SetRect
	dd	__maxgui_maxgui_TGadget_LockLayout
	dd	__maxgui_maxgui_TGadget_SetLayout
	dd	__maxgui_win32maxguiex_TWindowsGadget_LayoutKids
	dd	__maxgui_maxgui_TGadget_DoLayout
	dd	__maxgui_maxgui_TGadget_SetDataSource
	dd	__maxgui_maxgui_TGadget_KeysFromList
	dd	__maxgui_maxgui_TGadget_KeysFromObjectArray
	dd	__maxgui_maxgui_TGadget_SyncDataSource
	dd	__maxgui_maxgui_TGadget_SyncData
	dd	__maxgui_maxgui_TGadget_InsertItemFromKey
	dd	__maxgui_maxgui_TGadget_Clear
	dd	__maxgui_maxgui_TGadget_InsertItem
	dd	__maxgui_maxgui_TGadget_SetItem
	dd	__maxgui_maxgui_TGadget_RemoveItem
	dd	__maxgui_maxgui_TGadget_ItemCount
	dd	__maxgui_maxgui_TGadget_ItemText
	dd	__maxgui_maxgui_TGadget_ItemTip
	dd	__maxgui_maxgui_TGadget_ItemFlags
	dd	__maxgui_maxgui_TGadget_ItemIcon
	dd	__maxgui_maxgui_TGadget_ItemExtra
	dd	__maxgui_maxgui_TGadget_SetItemState
	dd	__maxgui_maxgui_TGadget_ItemState
	dd	__maxgui_maxgui_TGadget_SelectItem
	dd	__maxgui_maxgui_TGadget_SelectedItem
	dd	__maxgui_maxgui_TGadget_SelectedItems
	dd	__maxgui_maxgui_TGadget_Insert
	dd	__maxgui_win32maxguiex_TWindowsGadget_Query
	dd	__maxgui_maxgui_TGadget_CleanUp
	dd	__maxgui_win32maxguiex_TWindowsPanel_Free
	dd	__maxgui_win32maxguiex_TWindowsGadget_Rethink
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientWidth
	dd	__maxgui_win32maxguiex_TWindowsPanel_ClientHeight
	dd	__maxgui_win32maxguiex_TWindowsPanel_Activate
	dd	__maxgui_win32maxguiex_TWindowsGadget_State
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetText
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetFont
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_RemoveColor
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetAlpha
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTextColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetTooltip
	dd	__maxgui_win32maxguiex_TWindowsGadget_GetTooltip
	dd	__maxgui_win32maxguiex_TWindowsPanel_SetPixmap
	dd	__maxgui_maxgui_TGadget_SetIconStrip
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetShow
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetEnabled
	dd	__maxgui_maxgui_TGadget_SetSelected
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetHotKey
	dd	__maxgui_maxgui_TGadget_SetSensitivity
	dd	__maxgui_maxgui_TGadget_GetSensitivity
	dd	__maxgui_maxgui_TGadget_SetWatch
	dd	__maxgui_maxgui_TGadget_GetWatch
	dd	__maxgui_win32maxguiex_TWindowsPanel_Class
	dd	__maxgui_maxgui_TGadget_GetStatusText
	dd	__maxgui_maxgui_TGadget_SetStatusText
	dd	__maxgui_maxgui_TGadget_GetMenu
	dd	__maxgui_maxgui_TGadget_PopupMenu
	dd	__maxgui_maxgui_TGadget_UpdateMenu
	dd	__maxgui_maxgui_TGadget_SetMinimumSize
	dd	__maxgui_maxgui_TGadget_SetMaximumSize
	dd	__maxgui_maxgui_TGadget_ClearListItems
	dd	__maxgui_maxgui_TGadget_InsertListItem
	dd	__maxgui_maxgui_TGadget_SetListItem
	dd	__maxgui_maxgui_TGadget_RemoveListItem
	dd	__maxgui_maxgui_TGadget_SetListItemState
	dd	__maxgui_maxgui_TGadget_ListItemState
	dd	__maxgui_maxgui_TGadget_RootNode
	dd	__maxgui_maxgui_TGadget_InsertNode
	dd	__maxgui_maxgui_TGadget_ModifyNode
	dd	__maxgui_maxgui_TGadget_SelectedNode
	dd	__maxgui_maxgui_TGadget_CountKids
	dd	__maxgui_maxgui_TGadget_ReplaceText
	dd	__maxgui_maxgui_TGadget_AddText
	dd	__maxgui_maxgui_TGadget_AreaText
	dd	__maxgui_maxgui_TGadget_AreaLen
	dd	__maxgui_maxgui_TGadget_LockText
	dd	__maxgui_maxgui_TGadget_UnlockText
	dd	__maxgui_maxgui_TGadget_SetTabs
	dd	__maxgui_maxgui_TGadget_GetCursorPos
	dd	__maxgui_maxgui_TGadget_GetSelectionLength
	dd	__maxgui_maxgui_TGadget_SetStyle
	dd	__maxgui_maxgui_TGadget_SetSelection
	dd	__maxgui_maxgui_TGadget_CharX
	dd	__maxgui_maxgui_TGadget_CharY
	dd	__maxgui_maxgui_TGadget_CharAt
	dd	__maxgui_maxgui_TGadget_LineAt
	dd	__maxgui_maxgui_TGadget_SetValue
	dd	__maxgui_maxgui_TGadget_SetRange
	dd	__maxgui_maxgui_TGadget_SetStep
	dd	__maxgui_maxgui_TGadget_SetProp
	dd	__maxgui_maxgui_TGadget_GetProp
	dd	__maxgui_win32maxguiex_TWindowsPanel_AttachGraphics
	dd	__maxgui_win32maxguiex_TWindowsPanel_CanvasGraphics
	dd	__maxgui_maxgui_TGadget_Run
	dd	__maxgui_win32maxguiex_TWindowsPanel_Create
	dd	__maxgui_win32maxguiex_TWindowsGadget_FgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgColor
	dd	__maxgui_win32maxguiex_TWindowsGadget_BgBrush
	dd	__maxgui_win32maxguiex_TWindowsGadget_Register
	dd	__maxgui_win32maxguiex_TWindowsGadget_SetupToolTips
	dd	__maxgui_win32maxguiex_TWindowsGadget_isTabbable
	dd	__maxgui_win32maxguiex_TWindowsGadget_isControl
	dd	__maxgui_win32maxguiex_TWindowsPanel_RethinkClient
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnNotify
	dd	__skn3_maxguiex_Skn3PanelEx_WndProc
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnCommand
	dd	__maxgui_win32maxguiex_TWindowsGadget_OnDrawItem
	dd	__maxgui_win32maxguiex_TWindowsGadget_CreateControlBrush
	dd	__maxgui_win32maxguiex_TWindowsPanel_FlushBrushes
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_DrawParentBackground
	dd	__maxgui_win32maxguiex_TWindowsGadget_Sensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_DeSensitize
	dd	__maxgui_win32maxguiex_TWindowsGadget_PostGuiEvent
	dd	__maxgui_win32maxguiex_TWindowsGadget_StartResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_QueueResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_EndResize
	dd	__maxgui_win32maxguiex_TWindowsGadget_HasResized
	dd	__maxgui_win32maxguiex_TWindowsGadget_RefreshLook
	dd	__skn3_maxguiex_Skn3PanelEx_SetGradient
_384:
	db	"Self",0
_385:
	db	":Skn3ListBatchLock",0
	align	4
_383:
	dd	1
	dd	_71
	dd	2
	dd	_384
	dd	_385
	dd	-4
	dd	0
	align	4
_382:
	dd	3
	dd	0
	dd	0
_418:
	db	"Gadget",0
_419:
	db	":TGadget",0
_420:
	db	"listBoxLock",0
_421:
	db	"listBoxLockLink",0
	align	4
_417:
	dd	1
	dd	_74
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_420
	dd	_385
	dd	-8
	dd	2
	dd	_421
	dd	_67
	dd	-12
	dd	0
	align	4
_393:
	dd	_367
	dd	69
	dd	3
	align	4
_396:
	dd	3
	dd	0
	dd	0
	align	4
_395:
	dd	_367
	dd	69
	dd	17
	align	4
_397:
	dd	_367
	dd	71
	dd	3
	align	4
_399:
	dd	_367
	dd	72
	dd	3
	align	4
_403:
	dd	_367
	dd	73
	dd	3
	align	4
_416:
	dd	3
	dd	0
	dd	0
	align	4
_404:
	dd	_367
	dd	75
	dd	4
	align	4
_407:
	dd	_367
	dd	78
	dd	4
	align	4
_412:
	dd	3
	dd	0
	dd	0
	align	4
_411:
	dd	_367
	dd	78
	dd	36
	align	4
_413:
	dd	_367
	dd	81
	dd	4
_441:
	db	"Lock",0
	align	4
_440:
	dd	1
	dd	_76
	dd	2
	dd	_441
	dd	_385
	dd	-4
	dd	0
	align	4
_422:
	dd	_367
	dd	87
	dd	3
	align	4
_429:
	dd	3
	dd	0
	dd	0
	align	4
_424:
	dd	_367
	dd	87
	dd	17
	align	4
_430:
	dd	_367
	dd	88
	dd	3
	align	4
_466:
	dd	1
	dd	_78
	dd	2
	dd	_441
	dd	_385
	dd	-4
	dd	0
	align	4
_442:
	dd	_367
	dd	93
	dd	3
	align	4
_465:
	dd	3
	dd	0
	dd	0
	align	4
_444:
	dd	_367
	dd	94
	dd	4
	align	4
_449:
	dd	_367
	dd	95
	dd	4
	align	4
_457:
	dd	_367
	dd	96
	dd	4
_470:
	db	":Skn3CustomPointer",0
	align	4
_469:
	dd	1
	dd	_71
	dd	2
	dd	_384
	dd	_470
	dd	-4
	dd	0
	align	4
_468:
	dd	3
	dd	0
	dd	0
_476:
	db	":PARAFORMAT2",0
	align	4
_475:
	dd	1
	dd	_71
	dd	2
	dd	_384
	dd	_476
	dd	-4
	dd	0
	align	4
_474:
	dd	3
	dd	0
	dd	0
_544:
	db	"TrimAndFixPath",0
_545:
	db	"slash",0
_546:
	db	"keepRootSlash",0
_547:
	db	"startIndex",0
_548:
	db	"hasRootSlash",0
_549:
	db	"slashAsc",0
_550:
	db	"length",0
	align	4
_543:
	dd	1
	dd	_544
	dd	2
	dd	_81
	dd	_82
	dd	-4
	dd	2
	dd	_545
	dd	_82
	dd	-8
	dd	2
	dd	_546
	dd	_62
	dd	-12
	dd	2
	dd	_65
	dd	_62
	dd	-16
	dd	2
	dd	_547
	dd	_62
	dd	-20
	dd	2
	dd	_548
	dd	_62
	dd	-24
	dd	2
	dd	_549
	dd	_62
	dd	-28
	dd	2
	dd	_550
	dd	_62
	dd	-32
	dd	0
	align	4
_478:
	dd	_367
	dd	207
	dd	2
	align	4
_481:
	dd	3
	dd	0
	dd	0
	align	4
_480:
	dd	_367
	dd	207
	dd	21
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_482:
	dd	_367
	dd	210
	dd	2
	align	4
_21:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_23:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	92
	align	4
_483:
	dd	_367
	dd	211
	dd	2
	align	4
_486:
	dd	3
	dd	0
	dd	0
	align	4
_485:
	dd	_367
	dd	211
	dd	18
	align	4
_487:
	dd	_367
	dd	214
	dd	2
	align	4
_489:
	dd	_367
	dd	215
	dd	2
	align	4
_491:
	dd	_367
	dd	216
	dd	2
	align	4
_493:
	dd	_367
	dd	217
	dd	2
	align	4
_495:
	dd	_367
	dd	219
	dd	2
	align	4
_512:
	dd	3
	dd	0
	dd	0
	align	4
_498:
	dd	_367
	dd	220
	dd	3
	align	4
_509:
	dd	3
	dd	0
	dd	0
	align	4
_508:
	dd	_367
	dd	220
	dd	52
	align	4
_510:
	dd	_367
	dd	221
	dd	3
	align	4
_511:
	dd	_367
	dd	222
	dd	3
	align	4
_513:
	dd	_367
	dd	225
	dd	2
	align	4
_515:
	dd	_367
	dd	226
	dd	2
	align	4
_530:
	dd	3
	dd	0
	dd	0
	align	4
_517:
	dd	_367
	dd	227
	dd	3
	align	4
_528:
	dd	3
	dd	0
	dd	0
	align	4
_527:
	dd	_367
	dd	227
	dd	52
	align	4
_529:
	dd	_367
	dd	228
	dd	3
	align	4
_531:
	dd	_367
	dd	230
	dd	2
	align	4
_534:
	dd	3
	dd	0
	dd	0
	align	4
_533:
	dd	_367
	dd	230
	dd	17
	align	4
_535:
	dd	_367
	dd	233
	dd	2
	align	4
_536:
	dd	_367
	dd	234
	dd	2
	align	4
_541:
	dd	3
	dd	0
	dd	0
	align	4
_540:
	dd	_367
	dd	234
	dd	36
	align	4
_542:
	dd	_367
	dd	237
	dd	2
_591:
	db	"IncBinToDisk",0
	align	4
_590:
	dd	1
	dd	_591
	dd	2
	dd	_81
	dd	_82
	dd	-4
	dd	0
	align	4
_551:
	dd	_367
	dd	242
	dd	2
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	105,110,99,98,105,110,58,58
_582:
	db	"pathBase",0
_583:
	db	"pathCount",0
_584:
	db	"pathFile",0
_585:
	db	"path2",0
_586:
	db	"in",0
_587:
	db	":TStream",0
_588:
	db	"out",0
	align	4
_581:
	dd	3
	dd	0
	dd	2
	dd	_582
	dd	_82
	dd	-8
	dd	2
	dd	_583
	dd	_82
	dd	-12
	dd	2
	dd	_584
	dd	_82
	dd	-16
	dd	2
	dd	_585
	dd	_82
	dd	-20
	dd	2
	dd	_586
	dd	_587
	dd	-24
	dd	2
	dd	_588
	dd	_587
	dd	-28
	dd	0
	align	4
_553:
	dd	_367
	dd	244
	dd	3
	align	4
_555:
	dd	_367
	dd	245
	dd	3
	align	4
_557:
	dd	_367
	dd	246
	dd	3
	align	4
_559:
	dd	_367
	dd	248
	dd	3
	align	4
_561:
	dd	3
	dd	0
	dd	0
	align	4
_560:
	dd	_367
	dd	249
	dd	4
	align	4
_562:
	dd	_367
	dd	253
	dd	3
	align	4
_564:
	dd	_367
	dd	256
	dd	3
	align	4
_566:
	dd	_367
	dd	257
	dd	3
	align	4
_569:
	dd	3
	dd	0
	dd	0
	align	4
_568:
	dd	_367
	dd	257
	dd	16
	align	4
_570:
	dd	_367
	dd	258
	dd	3
	align	4
_572:
	dd	_367
	dd	259
	dd	3
	align	4
_576:
	dd	3
	dd	0
	dd	0
	align	4
_574:
	dd	_367
	dd	260
	dd	4
	align	4
_575:
	dd	_367
	dd	261
	dd	4
	align	4
_577:
	dd	_367
	dd	263
	dd	3
	align	4
_578:
	dd	_367
	dd	264
	dd	3
	align	4
_579:
	dd	_367
	dd	265
	dd	3
	align	4
_580:
	dd	_367
	dd	268
	dd	3
	align	4
_589:
	dd	_367
	dd	272
	dd	2
_600:
	db	"PointInRect",0
_601:
	db	"pointX",0
_602:
	db	"f",0
_603:
	db	"pointY",0
_604:
	db	"rectX",0
_605:
	db	"rectY",0
_606:
	db	"rectWidth",0
_607:
	db	"rectHeight",0
	align	4
_599:
	dd	1
	dd	_600
	dd	2
	dd	_601
	dd	_602
	dd	-4
	dd	2
	dd	_603
	dd	_602
	dd	-8
	dd	2
	dd	_604
	dd	_602
	dd	-12
	dd	2
	dd	_605
	dd	_602
	dd	-16
	dd	2
	dd	_606
	dd	_602
	dd	-20
	dd	2
	dd	_607
	dd	_602
	dd	-24
	dd	0
	align	4
_592:
	dd	_367
	dd	277
	dd	2
_610:
	db	"RequestScrollbarSize",0
	align	4
_609:
	dd	1
	dd	_610
	dd	0
	align	4
_608:
	dd	_367
	dd	299
	dd	2
_614:
	db	"SetComboBoxHeight",0
_615:
	db	"comboBox",0
_616:
	db	"Height",0
	align	4
_613:
	dd	1
	dd	_614
	dd	2
	dd	_615
	dd	_419
	dd	-4
	dd	2
	dd	_616
	dd	_62
	dd	-8
	dd	0
	align	4
_611:
	dd	_367
	dd	316
	dd	3
	align	4
_612:
	dd	_367
	dd	317
	dd	3
_643:
	db	"GadgetScreenPosition",0
_644:
	db	"gadget",0
_645:
	db	"client",0
_646:
	db	"point",0
_647:
	db	"l",0
_648:
	db	"Position",0
_649:
	db	"[]i",0
	align	4
_642:
	dd	1
	dd	_643
	dd	2
	dd	_644
	dd	_419
	dd	-12
	dd	2
	dd	_645
	dd	_62
	dd	-16
	dd	2
	dd	_646
	dd	_647
	dd	-8
	dd	2
	dd	_648
	dd	_649
	dd	-20
	dd	0
	align	4
_617:
	dd	_367
	dd	336
	dd	3
	align	4
_619:
	dd	_367
	dd	337
	dd	3
	align	4
_624:
	dd	3
	dd	0
	dd	0
	align	4
_621:
	dd	_367
	dd	338
	dd	4
	align	4
_629:
	dd	3
	dd	0
	dd	0
	align	4
_626:
	dd	_367
	dd	340
	dd	4
	align	4
_630:
	dd	_367
	dd	343
	dd	3
_631:
	db	"i",0
	align	4
_633:
	dd	_367
	dd	344
	dd	3
	align	4
_637:
	dd	_367
	dd	345
	dd	3
	align	4
_641:
	dd	_367
	dd	349
	dd	2
_676:
	db	"PointOverGadget",0
_677:
	db	"targetGadget",0
_678:
	db	"sourceGadget",0
_679:
	db	"targetPosition",0
	align	4
_675:
	dd	1
	dd	_676
	dd	2
	dd	_601
	dd	_62
	dd	-4
	dd	2
	dd	_603
	dd	_62
	dd	-8
	dd	2
	dd	_677
	dd	_419
	dd	-12
	dd	2
	dd	_678
	dd	_419
	dd	-16
	dd	2
	dd	_679
	dd	_649
	dd	-20
	dd	0
	align	4
_650:
	dd	_367
	dd	368
	dd	2
	align	4
_655:
	dd	3
	dd	0
	dd	0
	align	4
_654:
	dd	_367
	dd	368
	dd	55
	align	4
_656:
	dd	_367
	dd	371
	dd	2
_667:
	db	"sourcePosition",0
	align	4
_666:
	dd	3
	dd	0
	dd	2
	dd	_667
	dd	_649
	dd	-24
	dd	0
	align	4
_658:
	dd	_367
	dd	372
	dd	3
	align	4
_660:
	dd	_367
	dd	373
	dd	3
	align	4
_663:
	dd	_367
	dd	374
	dd	3
	align	4
_668:
	dd	_367
	dd	378
	dd	2
	align	4
_670:
	dd	_367
	dd	380
	dd	2
_683:
	db	"DisableGadgetRedraw",0
	align	4
_682:
	dd	1
	dd	_683
	dd	2
	dd	_644
	dd	_419
	dd	-4
	dd	0
	align	4
_680:
	dd	_367
	dd	399
	dd	3
	align	4
_681:
	dd	_367
	dd	400
	dd	3
_687:
	db	"EnableGadgetRedraw",0
	align	4
_686:
	dd	1
	dd	_687
	dd	2
	dd	_644
	dd	_419
	dd	-4
	dd	0
	align	4
_684:
	dd	_367
	dd	419
	dd	3
	align	4
_685:
	dd	_367
	dd	420
	dd	3
_702:
	db	"MessageBox",0
_703:
	db	"title",0
_704:
	db	"message",0
_705:
	db	"parent",0
_706:
	db	"oldTitle",0
	align	4
_701:
	dd	1
	dd	_702
	dd	2
	dd	_703
	dd	_82
	dd	-4
	dd	2
	dd	_704
	dd	_82
	dd	-8
	dd	2
	dd	_705
	dd	_419
	dd	-12
	dd	2
	dd	_706
	dd	_82
	dd	-16
	dd	0
	align	4
_688:
	dd	_367
	dd	439
	dd	2
	align	4
_690:
	dd	_367
	dd	440
	dd	2
	align	4
_695:
	dd	_367
	dd	441
	dd	2
	align	4
_696:
	dd	_367
	dd	442
	dd	2
_757:
	db	"GadgetSizeForString",0
_758:
	db	"text",0
_759:
	db	"maxWidth",0
	align	4
_756:
	dd	1
	dd	_757
	dd	2
	dd	_644
	dd	_419
	dd	-4
	dd	2
	dd	_758
	dd	_82
	dd	-8
	dd	2
	dd	_759
	dd	_62
	dd	-12
	dd	2
	dd	_70
	dd	_62
	dd	-16
	dd	0
	align	4
_707:
	dd	_367
	dd	462
	dd	3
	align	4
_709:
	dd	_367
	dd	465
	dd	3
_738:
	db	"dc",0
_739:
	db	"font",0
_740:
	db	"rect",0
_741:
	db	"flags",0
	align	4
_737:
	dd	3
	dd	0
	dd	2
	dd	_738
	dd	_62
	dd	-20
	dd	2
	dd	_739
	dd	_62
	dd	-24
	dd	2
	dd	_740
	dd	_649
	dd	-28
	dd	2
	dd	_741
	dd	_62
	dd	-32
	dd	0
	align	4
_713:
	dd	_367
	dd	488
	dd	5
	align	4
_715:
	dd	_367
	dd	491
	dd	5
	align	4
_717:
	dd	_367
	dd	492
	dd	5
	align	4
_718:
	dd	_367
	dd	495
	dd	5
	align	4
_721:
	dd	_367
	dd	497
	dd	5
	align	4
_723:
	dd	_367
	dd	498
	dd	5
	align	4
_726:
	dd	3
	dd	0
	dd	0
	align	4
_725:
	dd	_367
	dd	498
	dd	21
	align	4
_727:
	dd	_367
	dd	501
	dd	5
	align	4
_730:
	dd	_367
	dd	504
	dd	5
	align	4
_731:
	dd	_367
	dd	507
	dd	5
_754:
	db	"size",0
_755:
	db	"oldText",0
	align	4
_753:
	dd	3
	dd	0
	dd	2
	dd	_754
	dd	_649
	dd	-36
	dd	2
	dd	_755
	dd	_82
	dd	-40
	dd	0
	align	4
_742:
	dd	_367
	dd	468
	dd	5
	align	4
_745:
	dd	_367
	dd	471
	dd	5
	align	4
_746:
	dd	_367
	dd	474
	dd	5
	align	4
_748:
	dd	_367
	dd	475
	dd	5
	align	4
_749:
	dd	_367
	dd	478
	dd	5
	align	4
_750:
	dd	_367
	dd	481
	dd	5
	align	4
_751:
	dd	_367
	dd	482
	dd	5
	align	4
_752:
	dd	_367
	dd	485
	dd	5
_770:
	db	"GetCreationGroup",0
_771:
	db	"tmpProxy",0
_772:
	db	":TProxyGadget",0
	align	4
_769:
	dd	1
	dd	_770
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_771
	dd	_772
	dd	-8
	dd	0
	align	4
_760:
	dd	_367
	dd	530
	dd	2
	align	4
_762:
	dd	_367
	dd	531
	dd	2
	align	4
_767:
	dd	3
	dd	0
	dd	0
	align	4
_764:
	dd	_367
	dd	531
	dd	19
	align	4
_768:
	dd	_367
	dd	532
	dd	2
_782:
	db	"SetGadgetReadOnly",0
_783:
	db	"yes",0
	align	4
_781:
	dd	1
	dd	_782
	dd	2
	dd	_644
	dd	_419
	dd	-4
	dd	2
	dd	_783
	dd	_62
	dd	-8
	dd	0
	align	4
_773:
	dd	_367
	dd	549
	dd	2
	align	4
_780:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-12
	dd	0
	align	4
_777:
	dd	_367
	dd	552
	dd	5
	align	4
_779:
	dd	_367
	dd	553
	dd	5
_795:
	db	"SetGadgetMaxLength",0
	align	4
_794:
	dd	1
	dd	_795
	dd	2
	dd	_644
	dd	_419
	dd	-4
	dd	2
	dd	_550
	dd	_62
	dd	-8
	dd	0
	align	4
_784:
	dd	_367
	dd	573
	dd	2
	align	4
_793:
	dd	3
	dd	0
	dd	0
	align	4
_788:
	dd	_367
	dd	576
	dd	5
	align	4
_791:
	dd	3
	dd	0
	dd	0
	align	4
_790:
	dd	_367
	dd	576
	dd	19
	align	4
_792:
	dd	_367
	dd	577
	dd	5
_805:
	db	"GetGadgetMaxLength",0
	align	4
_804:
	dd	1
	dd	_805
	dd	2
	dd	_644
	dd	_419
	dd	-4
	dd	0
	align	4
_796:
	dd	_367
	dd	599
	dd	2
	align	4
_801:
	dd	3
	dd	0
	dd	0
	align	4
_800:
	dd	_367
	dd	607
	dd	4
	align	4
_803:
	dd	3
	dd	0
	dd	0
	align	4
_802:
	dd	_367
	dd	602
	dd	5
_868:
	db	"LoadCustomPointer",0
_869:
	db	"deletePath2",0
	align	4
_867:
	dd	1
	dd	_868
	dd	2
	dd	_81
	dd	_82
	dd	-4
	dd	2
	dd	_83
	dd	_470
	dd	-8
	dd	2
	dd	_585
	dd	_82
	dd	-12
	dd	2
	dd	_869
	dd	_62
	dd	-16
	dd	0
	align	4
_806:
	dd	_367
	dd	625
	dd	2
	align	4
_808:
	dd	_367
	dd	626
	dd	2
	align	4
_810:
	dd	_367
	dd	627
	dd	2
	align	4
_812:
	dd	_367
	dd	628
	dd	2
	align	4
_815:
	dd	3
	dd	0
	dd	0
	align	4
_814:
	dd	_367
	dd	629
	dd	3
	align	4
_818:
	dd	3
	dd	0
	dd	0
	align	4
_817:
	dd	_367
	dd	631
	dd	3
	align	4
_819:
	dd	_367
	dd	635
	dd	2
	align	4
_826:
	dd	3
	dd	0
	dd	0
	align	4
_821:
	dd	_367
	dd	637
	dd	3
	align	4
_831:
	dd	3
	dd	0
	dd	0
	align	4
_828:
	dd	_367
	dd	640
	dd	3
	align	4
_832:
	dd	_367
	dd	644
	dd	2
	align	4
_852:
	dd	3
	dd	0
	dd	0
	align	4
_834:
	dd	_367
	dd	646
	dd	3
	align	4
_835:
	dd	_367
	dd	647
	dd	3
	align	4
_843:
	dd	_367
	dd	648
	dd	3
	align	4
_846:
	dd	_367
	dd	652
	dd	3
	align	4
_853:
	dd	_367
	dd	662
	dd	2
	align	4
_856:
	dd	3
	dd	0
	dd	0
	align	4
_855:
	dd	_367
	dd	662
	dd	17
	align	4
_857:
	dd	_367
	dd	665
	dd	2
	align	4
_866:
	dd	3
	dd	0
	dd	0
	align	4
_861:
	dd	_367
	dd	667
	dd	3
	align	4
_865:
	dd	_367
	dd	668
	dd	3
_887:
	db	"SetCustomPointer",0
	align	4
_886:
	dd	1
	dd	_887
	dd	2
	dd	_83
	dd	_470
	dd	-4
	dd	0
	align	4
_870:
	dd	_367
	dd	685
	dd	2
	align	4
_885:
	dd	3
	dd	0
	dd	0
	align	4
_872:
	dd	_367
	dd	686
	dd	3
	align	4
_873:
	dd	_367
	dd	688
	dd	3
	align	4
_876:
	dd	_367
	dd	689
	dd	3
	align	4
_879:
	dd	_367
	dd	690
	dd	3
	align	4
_884:
	dd	3
	dd	0
	dd	0
	align	4
_881:
	dd	_367
	dd	690
	dd	39
_919:
	db	"FreeCustomPointer",0
	align	4
_918:
	dd	1
	dd	_919
	dd	2
	dd	_83
	dd	_470
	dd	-4
	dd	0
	align	4
_888:
	dd	_367
	dd	710
	dd	2
	align	4
_917:
	dd	3
	dd	0
	dd	0
	align	4
_890:
	dd	_367
	dd	712
	dd	3
	align	4
_894:
	dd	_367
	dd	713
	dd	3
	align	4
_916:
	dd	3
	dd	0
	dd	0
	align	4
_898:
	dd	_367
	dd	715
	dd	4
	align	4
_903:
	dd	_367
	dd	720
	dd	4
	align	4
_908:
	dd	3
	dd	0
	dd	0
	align	4
_907:
	dd	_367
	dd	720
	dd	51
	align	4
_909:
	dd	_367
	dd	722
	dd	4
	align	4
_912:
	dd	_367
	dd	723
	dd	4
_971:
	db	"ExtractCursorHotspot",0
_972:
	db	"result",0
_973:
	db	"file",0
	align	4
_970:
	dd	1
	dd	_971
	dd	2
	dd	_81
	dd	_82
	dd	-4
	dd	2
	dd	_65
	dd	_62
	dd	-8
	dd	2
	dd	_972
	dd	_649
	dd	-12
	dd	2
	dd	_973
	dd	_587
	dd	-16
	dd	0
	align	4
_920:
	dd	_367
	dd	747
	dd	2
_921:
	db	"i",0
	align	4
_923:
	dd	_367
	dd	749
	dd	2
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	108,105,116,116,108,101,101,110,100,105,97,110,58,58
	align	4
_925:
	dd	_367
	dd	750
	dd	2
_968:
	db	"temp",0
	align	4
_967:
	dd	3
	dd	0
	dd	2
	dd	_968
	dd	_62
	dd	-20
	dd	0
	align	4
_927:
	dd	_367
	dd	752
	dd	3
	align	4
_930:
	dd	_367
	dd	755
	dd	3
	align	4
_934:
	dd	_367
	dd	756
	dd	3
	align	4
_963:
	dd	3
	dd	0
	dd	0
	align	4
_936:
	dd	_367
	dd	758
	dd	4
	align	4
_939:
	dd	_367
	dd	759
	dd	4
	align	4
_962:
	dd	3
	dd	0
	dd	0
	align	4
_941:
	dd	_367
	dd	761
	dd	5
	align	4
_942:
	dd	_367
	dd	762
	dd	5
	align	4
_961:
	dd	3
	dd	0
	dd	0
	align	4
_946:
	dd	_367
	dd	763
	dd	6
	align	4
_949:
	dd	_367
	dd	764
	dd	6
	align	4
_955:
	dd	_367
	dd	765
	dd	6
	align	4
_964:
	dd	_367
	dd	771
	dd	3
	align	4
_969:
	dd	_367
	dd	775
	dd	2
_1026:
	db	"ListBatchLock",0
	align	4
_1025:
	dd	1
	dd	_1026
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_441
	dd	_385
	dd	-8
	dd	2
	dd	_63
	dd	_64
	dd	-12
	dd	0
	align	4
_974:
	dd	_367
	dd	792
	dd	3
	align	4
_976:
	dd	_367
	dd	793
	dd	3
	align	4
_983:
	dd	3
	dd	0
	dd	0
	align	4
_978:
	dd	_367
	dd	795
	dd	4
	align	4
_982:
	dd	_367
	dd	796
	dd	4
	align	4
_984:
	dd	_367
	dd	800
	dd	3
	align	4
_986:
	dd	_367
	dd	801
	dd	3
	align	4
_989:
	dd	3
	dd	0
	dd	0
	align	4
_988:
	dd	_367
	dd	801
	dd	21
	align	4
_990:
	dd	_367
	dd	804
	dd	3
	align	4
_991:
	dd	_367
	dd	805
	dd	3
	align	4
_995:
	dd	_367
	dd	806
	dd	3
	align	4
_1003:
	dd	_367
	dd	807
	dd	3
	align	4
_1009:
	dd	_367
	dd	808
	dd	3
	align	4
_1017:
	dd	_367
	dd	809
	dd	3
	align	4
_1021:
	dd	_367
	dd	812
	dd	3
	align	4
_1024:
	dd	_367
	dd	815
	dd	3
_1117:
	db	"ListBatchAdd",0
_1118:
	db	"icon",0
_1119:
	db	"tip",0
_1120:
	db	"extra",0
_1121:
	db	":Object",0
_1122:
	db	"item",0
_1047:
	db	":TGadgetItem",0
	align	4
_1116:
	dd	1
	dd	_1117
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_758
	dd	_82
	dd	-8
	dd	2
	dd	_741
	dd	_62
	dd	-12
	dd	2
	dd	_1118
	dd	_62
	dd	-16
	dd	2
	dd	_1119
	dd	_82
	dd	-20
	dd	2
	dd	_1120
	dd	_1121
	dd	-24
	dd	2
	dd	_441
	dd	_385
	dd	-28
	dd	2
	dd	_1122
	dd	_1047
	dd	-32
	dd	0
	align	4
_1027:
	dd	_367
	dd	834
	dd	3
	align	4
_1029:
	dd	_367
	dd	837
	dd	3
	align	4
_1033:
	dd	3
	dd	0
	dd	0
	align	4
_1031:
	dd	_367
	dd	838
	dd	4
	align	4
_1032:
	dd	_367
	dd	839
	dd	4
	align	4
_1034:
	dd	_367
	dd	844
	dd	3
	align	4
_1036:
	dd	_367
	dd	845
	dd	3
	align	4
_1039:
	dd	_367
	dd	848
	dd	3
	align	4
_1052:
	dd	_367
	dd	849
	dd	3
	align	4
_1065:
	dd	_367
	dd	852
	dd	3
	align	4
_1071:
	dd	_367
	dd	853
	dd	3
	align	4
_1079:
	dd	_367
	dd	854
	dd	3
	align	4
_1087:
	dd	_367
	dd	857
	dd	4
	align	4
_1093:
	dd	_367
	dd	858
	dd	4
	align	4
_1101:
	dd	_367
	dd	861
	dd	3
	align	4
_1107:
	dd	_367
	dd	862
	dd	3
	align	4
_1112:
	dd	_367
	dd	865
	dd	3
_1160:
	db	"ListBatchUnlock",0
	align	4
_1159:
	dd	1
	dd	_1160
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_441
	dd	_385
	dd	-8
	dd	0
	align	4
_1123:
	dd	_367
	dd	887
	dd	3
	align	4
_1125:
	dd	_367
	dd	888
	dd	3
	align	4
_1128:
	dd	3
	dd	0
	dd	0
	align	4
_1127:
	dd	_367
	dd	888
	dd	18
	align	4
_1129:
	dd	_367
	dd	891
	dd	3
	align	4
_1133:
	dd	_367
	dd	894
	dd	3
	align	4
_1158:
	dd	3
	dd	0
	dd	0
	align	4
_1137:
	dd	_367
	dd	896
	dd	4
	align	4
_1140:
	dd	_367
	dd	897
	dd	4
	align	4
_1151:
	dd	3
	dd	0
	dd	0
	align	4
_1146:
	dd	_367
	dd	897
	dd	46
	align	4
_1152:
	dd	_367
	dd	898
	dd	4
	align	4
_1157:
	dd	_367
	dd	901
	dd	4
_1171:
	db	"GadgetWindow",0
	align	4
_1170:
	dd	1
	dd	_1171
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_705
	dd	_419
	dd	-8
	dd	0
	align	4
_1161:
	dd	_367
	dd	921
	dd	2
	align	4
_1163:
	dd	_367
	dd	922
	dd	2
	align	4
_1169:
	dd	3
	dd	0
	dd	0
	align	4
_1164:
	dd	_367
	dd	923
	dd	3
	align	4
_1167:
	dd	3
	dd	0
	dd	0
	align	4
_1166:
	dd	_367
	dd	923
	dd	42
	align	4
_1168:
	dd	_367
	dd	924
	dd	3
_1185:
	db	"SetWindowAlwaysOnTop",0
_1186:
	db	"Window",0
_1187:
	db	"State",0
	align	4
_1184:
	dd	1
	dd	_1185
	dd	2
	dd	_1186
	dd	_419
	dd	-4
	dd	2
	dd	_1187
	dd	_62
	dd	-8
	dd	2
	dd	_70
	dd	_62
	dd	-12
	dd	0
	align	4
_1172:
	dd	_367
	dd	942
	dd	3
	align	4
_1174:
	dd	_367
	dd	943
	dd	3
	align	4
_1183:
	dd	3
	dd	0
	dd	0
	align	4
_1176:
	dd	_367
	dd	944
	dd	4
	align	4
_1179:
	dd	3
	dd	0
	dd	0
	align	4
_1178:
	dd	_367
	dd	946
	dd	5
	align	4
_1182:
	dd	3
	dd	0
	dd	0
	align	4
_1181:
	dd	_367
	dd	948
	dd	5
_1195:
	db	"BringWindowToTop",0
	align	4
_1194:
	dd	1
	dd	_1195
	dd	2
	dd	_1186
	dd	_419
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1188:
	dd	_367
	dd	970
	dd	3
	align	4
_1190:
	dd	_367
	dd	971
	dd	3
	align	4
_1193:
	dd	3
	dd	0
	dd	0
	align	4
_1192:
	dd	_367
	dd	971
	dd	11
_1203:
	db	"FocusWindow",0
	align	4
_1202:
	dd	1
	dd	_1203
	dd	2
	dd	_1186
	dd	_419
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1196:
	dd	_367
	dd	991
	dd	3
	align	4
_1198:
	dd	_367
	dd	992
	dd	3
	align	4
_1201:
	dd	3
	dd	0
	dd	0
	align	4
_1200:
	dd	_367
	dd	992
	dd	11
_1206:
	db	"GadgetToInt",0
	align	4
_1205:
	dd	1
	dd	_1206
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	0
	align	4
_1204:
	dd	_367
	dd	1015
	dd	3
_1226:
	db	"SetColorPickerCustomColors",0
_1227:
	db	"colors",0
	align	4
_1225:
	dd	1
	dd	_1226
	dd	2
	dd	_1227
	dd	_649
	dd	-4
	dd	0
	align	4
_1207:
	dd	_367
	dd	1040
	dd	3
	align	4
_1219:
	dd	3
	dd	0
	dd	2
	dd	_65
	dd	_62
	dd	-8
	dd	0
	align	4
_1209:
	dd	_367
	dd	1041
	dd	4
	align	4
_1211:
	dd	_367
	dd	1042
	dd	4
	align	4
_1212:
	dd	_367
	dd	1043
	dd	4
	align	4
_1218:
	dd	3
	dd	0
	dd	0
	align	4
_1214:
	dd	_367
	dd	1044
	dd	5
	align	4
_1220:
	dd	_367
	dd	1048
	dd	3
_1237:
	db	"ClearColorPickerCustomColors",0
	align	4
_1236:
	dd	1
	dd	_1237
	dd	0
	align	4
_1228:
	dd	_367
	dd	1078
	dd	3
	align	4
_1235:
	dd	3
	dd	0
	dd	2
	dd	_65
	dd	_62
	dd	-4
	dd	0
	align	4
_1231:
	dd	_367
	dd	1079
	dd	4
_1246:
	db	"RedrawGadgetFrame",0
	align	4
_1245:
	dd	1
	dd	_1246
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1238:
	dd	_367
	dd	1098
	dd	3
	align	4
_1240:
	dd	_367
	dd	1099
	dd	3
	align	4
_1244:
	dd	3
	dd	0
	dd	0
	align	4
_1242:
	dd	_367
	dd	1100
	dd	4
	align	4
_1243:
	dd	_367
	dd	1101
	dd	4
_1281:
	db	"HideGadgetBorder",0
	align	4
_1280:
	dd	1
	dd	_1281
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	0
	align	4
_1247:
	dd	_367
	dd	1120
	dd	3
	align	4
_1279:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1251:
	dd	_367
	dd	1122
	dd	5
	align	4
_1253:
	dd	_367
	dd	1123
	dd	5
_1276:
	db	"Style",0
_1277:
	db	"styleEx",0
_1278:
	db	"changed",0
	align	4
_1275:
	dd	3
	dd	0
	dd	2
	dd	_1276
	dd	_62
	dd	-12
	dd	2
	dd	_1277
	dd	_62
	dd	-16
	dd	2
	dd	_1278
	dd	_62
	dd	-20
	dd	0
	align	4
_1255:
	dd	_367
	dd	1124
	dd	6
	align	4
_1257:
	dd	_367
	dd	1125
	dd	6
	align	4
_1259:
	dd	_367
	dd	1128
	dd	6
	align	4
_1261:
	dd	_367
	dd	1129
	dd	6
	align	4
_1265:
	dd	3
	dd	0
	dd	0
	align	4
_1263:
	dd	_367
	dd	1130
	dd	7
	align	4
_1264:
	dd	_367
	dd	1131
	dd	7
	align	4
_1266:
	dd	_367
	dd	1133
	dd	6
	align	4
_1270:
	dd	3
	dd	0
	dd	0
	align	4
_1268:
	dd	_367
	dd	1134
	dd	7
	align	4
_1269:
	dd	_367
	dd	1135
	dd	7
	align	4
_1271:
	dd	_367
	dd	1139
	dd	6
	align	4
_1274:
	dd	3
	dd	0
	dd	0
	align	4
_1273:
	dd	_367
	dd	1139
	dd	17
_1296:
	db	"InstallGuiFont",0
	align	4
_1295:
	dd	1
	dd	_1296
	dd	2
	dd	_81
	dd	_82
	dd	-4
	dd	0
	align	4
_1282:
	dd	_367
	dd	1168
	dd	3
_1289:
	db	"installed",0
	align	4
_1288:
	dd	3
	dd	0
	dd	2
	dd	_1289
	dd	_62
	dd	-8
	dd	0
	align	4
_1284:
	dd	_367
	dd	1170
	dd	4
	align	4
_1285:
	dd	_367
	dd	1171
	dd	4
	align	4
_1287:
	dd	_367
	dd	1172
	dd	4
	align	4
_1294:
	dd	3
	dd	0
	dd	0
	align	4
_1291:
	dd	_367
	dd	1175
	dd	4
_1326:
	db	"SetTextareaLineSpacing",0
_1327:
	db	"lineSpacing",0
	align	4
_1325:
	dd	1
	dd	_1326
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_1327
	dd	_602
	dd	-8
	dd	0
	align	4
_1297:
	dd	_367
	dd	1227
	dd	2
	align	4
_1324:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-12
	dd	0
	align	4
_1299:
	dd	_367
	dd	1230
	dd	4
	align	4
_1301:
	dd	_367
	dd	1231
	dd	4
_1323:
	db	"P",0
	align	4
_1322:
	dd	3
	dd	0
	dd	2
	dd	_1323
	dd	_476
	dd	-16
	dd	0
	align	4
_1303:
	dd	_367
	dd	1232
	dd	5
	align	4
_1305:
	dd	_367
	dd	1233
	dd	5
	align	4
_1309:
	dd	_367
	dd	1234
	dd	5
	align	4
_1313:
	dd	_367
	dd	1235
	dd	5
	align	4
_1317:
	dd	_367
	dd	1236
	dd	5
	align	4
_2354:
	dd	0x41a00000
	align	4
_1321:
	dd	_367
	dd	1237
	dd	5
_1338:
	db	"ScrollTextAreaToTop",0
	align	4
_1337:
	dd	1
	dd	_1338
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	0
	align	4
_1328:
	dd	_367
	dd	1264
	dd	2
	align	4
_1336:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1330:
	dd	_367
	dd	1266
	dd	4
	align	4
_1332:
	dd	_367
	dd	1267
	dd	4
	align	4
_1335:
	dd	3
	dd	0
	dd	0
	align	4
_1334:
	dd	_367
	dd	1267
	dd	12
_1349:
	db	"ScrollTextAreaToBottom",0
	align	4
_1348:
	dd	1
	dd	_1349
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	0
	align	4
_1339:
	dd	_367
	dd	1288
	dd	2
	align	4
_1347:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1341:
	dd	_367
	dd	1290
	dd	4
	align	4
_1343:
	dd	_367
	dd	1291
	dd	4
	align	4
_1346:
	dd	3
	dd	0
	dd	0
	align	4
_1345:
	dd	_367
	dd	1291
	dd	12
_1360:
	db	"ScrollTextAreaToCursor",0
	align	4
_1359:
	dd	1
	dd	_1360
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	0
	align	4
_1350:
	dd	_367
	dd	1312
	dd	2
	align	4
_1358:
	dd	3
	dd	0
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	0
	align	4
_1352:
	dd	_367
	dd	1314
	dd	4
	align	4
_1354:
	dd	_367
	dd	1315
	dd	4
	align	4
_1357:
	dd	3
	dd	0
	dd	0
	align	4
_1356:
	dd	_367
	dd	1315
	dd	12
_1363:
	db	"GetAppResourcesPath",0
	align	4
_1362:
	dd	1
	dd	_1363
	dd	0
	align	4
_1361:
	dd	_367
	dd	1339
	dd	3
_1369:
	db	"AssignGadgetClassId",0
_1370:
	db	"idCount",0
	align	4
_1365:
	dd	0
	align	4
_1368:
	dd	1
	dd	_1369
	dd	4
	dd	_1370
	dd	_62
	dd	_1365
	dd	0
	align	4
_1364:
	dd	_367
	dd	1360
	dd	2
	align	4
_1366:
	dd	_367
	dd	1361
	dd	2
	align	4
_1367:
	dd	_367
	dd	1362
	dd	2
_1373:
	db	":Skn3PanelEx",0
	align	4
_1372:
	dd	1
	dd	_71
	dd	2
	dd	_384
	dd	_1373
	dd	-4
	dd	0
	align	4
_1371:
	dd	3
	dd	0
	dd	0
_1891:
	db	"MSG",0
_1892:
	db	"wp",0
_1893:
	db	"lp",0
	align	4
_1890:
	dd	1
	dd	_153
	dd	2
	dd	_384
	dd	_1373
	dd	-4
	dd	2
	dd	_70
	dd	_62
	dd	-8
	dd	2
	dd	_1891
	dd	_62
	dd	-12
	dd	2
	dd	_1892
	dd	_62
	dd	-16
	dd	2
	dd	_1893
	dd	_62
	dd	-20
	dd	0
	align	4
_1375:
	dd	_367
	dd	1391
	dd	3
	align	4
_1888:
	dd	3
	dd	0
	dd	0
	align	4
_1379:
	dd	_367
	dd	1393
	dd	5
	align	4
_1384:
	dd	3
	dd	0
	dd	0
	align	4
_1383:
	dd	_367
	dd	1395
	dd	6
_1864:
	db	"hdc",0
_1865:
	db	"hdcCanvas",0
_1866:
	db	"hdcBitmap",0
_1867:
	db	"srcw",0
_1868:
	db	"srch",0
_1869:
	db	"x",0
_1870:
	db	"y",0
_1871:
	db	"xoffset",0
_1872:
	db	"yoffset",0
_1873:
	db	"clientRect",0
_1874:
	db	"UpdateRect",0
_1875:
	db	"clipRect",0
_1876:
	db	"windowRect",0
_1877:
	db	"previousBrush",0
_1878:
	db	"gradientSize",0
_1879:
	db	"gradientPosition",0
_1880:
	db	"gradientBrush",0
_1881:
	db	"gradientRect",0
_1882:
	db	"gradientR",0
_1883:
	db	"gradientG",0
_1884:
	db	"gradientB",0
_1885:
	db	"gradientStepR",0
_1886:
	db	"gradientStepG",0
_1887:
	db	"gradientStepB",0
	align	4
_1863:
	dd	3
	dd	0
	dd	2
	dd	_1864
	dd	_62
	dd	-24
	dd	2
	dd	_1865
	dd	_62
	dd	-28
	dd	2
	dd	_1866
	dd	_62
	dd	-32
	dd	2
	dd	_1867
	dd	_62
	dd	-36
	dd	2
	dd	_1868
	dd	_62
	dd	-40
	dd	2
	dd	_1869
	dd	_62
	dd	-44
	dd	2
	dd	_1870
	dd	_62
	dd	-48
	dd	2
	dd	_1871
	dd	_62
	dd	-52
	dd	2
	dd	_1872
	dd	_62
	dd	-56
	dd	2
	dd	_1873
	dd	_649
	dd	-60
	dd	2
	dd	_1874
	dd	_649
	dd	-64
	dd	2
	dd	_1875
	dd	_649
	dd	-68
	dd	2
	dd	_1876
	dd	_649
	dd	-72
	dd	2
	dd	_1877
	dd	_62
	dd	-76
	dd	2
	dd	_1878
	dd	_62
	dd	-80
	dd	2
	dd	_1879
	dd	_62
	dd	-84
	dd	2
	dd	_1880
	dd	_62
	dd	-88
	dd	2
	dd	_1881
	dd	_649
	dd	-92
	dd	2
	dd	_1882
	dd	_602
	dd	-96
	dd	2
	dd	_1883
	dd	_602
	dd	-100
	dd	2
	dd	_1884
	dd	_602
	dd	-104
	dd	2
	dd	_1885
	dd	_602
	dd	-108
	dd	2
	dd	_1886
	dd	_602
	dd	-112
	dd	2
	dd	_1887
	dd	_602
	dd	-116
	dd	0
	align	4
_1386:
	dd	_367
	dd	1398
	dd	6
	align	4
_1391:
	dd	3
	dd	0
	dd	0
	align	4
_1390:
	dd	_367
	dd	1398
	dd	34
	align	4
_1392:
	dd	_367
	dd	1399
	dd	6
	align	4
_1402:
	dd	_367
	dd	1400
	dd	6
_1403:
	db	"i",0
_1405:
	db	"i",0
_1407:
	db	"i",0
_1409:
	db	"i",0
	align	4
_1411:
	dd	_367
	dd	1401
	dd	6
	align	4
_1412:
	dd	_367
	dd	1402
	dd	6
	align	4
_1413:
	dd	_367
	dd	1403
	dd	6
	align	4
_1414:
	dd	_367
	dd	1404
	dd	6
	align	4
_1417:
	dd	3
	dd	0
	dd	0
	align	4
_1416:
	dd	_367
	dd	1404
	dd	58
	align	4
_1418:
	dd	_367
	dd	1405
	dd	6
	align	4
_1430:
	dd	3
	dd	0
	dd	0
	align	4
_1420:
	dd	_367
	dd	1405
	dd	38
	align	4
_1431:
	dd	_367
	dd	1408
	dd	6
	align	4
_1462:
	dd	3
	dd	0
	dd	0
	align	4
_1451:
	dd	_367
	dd	1409
	dd	7
	align	4
_1452:
	dd	_367
	dd	1410
	dd	7
	align	4
_1461:
	dd	_367
	dd	1411
	dd	7
	align	4
_1463:
	dd	_367
	dd	1415
	dd	6
	align	4
_1465:
	dd	_367
	dd	1416
	dd	6
	align	4
_1467:
	dd	_367
	dd	1417
	dd	6
	align	4
_1469:
	dd	_367
	dd	1418
	dd	6
	align	4
_1471:
	dd	_367
	dd	1419
	dd	6
	align	4
_1482:
	dd	_367
	dd	1420
	dd	6
	align	4
_1484:
	dd	_367
	dd	1421
	dd	6
	align	4
_1486:
	dd	_367
	dd	1422
	dd	6
	align	4
_1488:
	dd	_367
	dd	1423
	dd	6
	align	4
_1490:
	dd	_367
	dd	1424
	dd	6
	align	4
_1492:
	dd	_367
	dd	1425
	dd	6
	align	4
_1494:
	dd	_367
	dd	1428
	dd	6
	align	4
_1563:
	dd	3
	dd	0
	dd	0
	align	4
_1498:
	dd	_367
	dd	1429
	dd	7
	align	4
_1503:
	dd	_367
	dd	1430
	dd	7
	align	4
_1508:
	dd	_367
	dd	1431
	dd	7
	align	4
_1513:
	dd	_367
	dd	1432
	dd	7
	align	4
_1518:
	dd	_367
	dd	1436
	dd	7
	align	4
_1525:
	dd	_367
	dd	1437
	dd	7
	align	4
_1532:
	dd	_367
	dd	1438
	dd	7
	align	4
_1539:
	dd	_367
	dd	1441
	dd	7
	align	4
_1562:
	dd	3
	dd	0
	dd	0
	align	4
_1546:
	dd	_367
	dd	1443
	dd	8
	align	4
_1547:
	dd	_367
	dd	1444
	dd	8
	align	4
_1548:
	dd	_367
	dd	1447
	dd	8
	align	4
_1552:
	dd	_367
	dd	1448
	dd	8
	align	4
_1556:
	dd	_367
	dd	1451
	dd	8
	align	4
_1557:
	dd	_367
	dd	1454
	dd	8
	align	4
_1558:
	dd	_367
	dd	1455
	dd	8
	align	4
_1559:
	dd	_367
	dd	1458
	dd	8
	align	4
_1560:
	dd	_367
	dd	1459
	dd	8
	align	4
_1561:
	dd	_367
	dd	1460
	dd	8
	align	4
_1630:
	dd	3
	dd	0
	dd	0
	align	4
_1565:
	dd	_367
	dd	1463
	dd	7
	align	4
_1570:
	dd	_367
	dd	1464
	dd	7
	align	4
_1575:
	dd	_367
	dd	1465
	dd	7
	align	4
_1580:
	dd	_367
	dd	1466
	dd	7
	align	4
_1585:
	dd	_367
	dd	1470
	dd	7
	align	4
_1592:
	dd	_367
	dd	1471
	dd	7
	align	4
_1599:
	dd	_367
	dd	1472
	dd	7
	align	4
_1606:
	dd	_367
	dd	1474
	dd	7
	align	4
_1629:
	dd	3
	dd	0
	dd	0
	align	4
_1613:
	dd	_367
	dd	1476
	dd	8
	align	4
_1614:
	dd	_367
	dd	1477
	dd	8
	align	4
_1615:
	dd	_367
	dd	1480
	dd	8
	align	4
_1619:
	dd	_367
	dd	1481
	dd	8
	align	4
_1623:
	dd	_367
	dd	1484
	dd	8
	align	4
_1624:
	dd	_367
	dd	1487
	dd	8
	align	4
_1625:
	dd	_367
	dd	1488
	dd	8
	align	4
_1626:
	dd	_367
	dd	1491
	dd	8
	align	4
_1627:
	dd	_367
	dd	1492
	dd	8
	align	4
_1628:
	dd	_367
	dd	1493
	dd	8
	align	4
_1631:
	dd	_367
	dd	1498
	dd	6
	align	4
_1652:
	dd	3
	dd	0
	dd	0
	align	4
_1651:
	dd	_367
	dd	1498
	dd	107
	align	4
_1653:
	dd	_367
	dd	1501
	dd	6
	align	4
_1810:
	dd	3
	dd	0
	dd	0
	align	4
_1665:
	dd	_367
	dd	1502
	dd	7
	align	4
_1666:
	dd	_367
	dd	1503
	dd	7
	align	4
_1669:
	dd	_367
	dd	1504
	dd	7
	align	4
_1672:
	dd	_367
	dd	1505
	dd	7
	align	4
_1675:
	dd	_367
	dd	1506
	dd	7
	align	4
_1708:
	dd	3
	dd	0
	dd	0
	align	4
_1684:
	dd	_367
	dd	1508
	dd	9
	align	4
_1707:
	dd	3
	dd	0
	dd	0
	align	4
_1689:
	dd	_367
	dd	1509
	dd	10
	align	4
_1690:
	dd	_367
	dd	1510
	dd	10
	align	4
_1705:
	dd	3
	dd	0
	dd	0
	align	4
_1695:
	dd	_367
	dd	1511
	dd	11
	align	4
_1700:
	dd	3
	dd	0
	dd	0
	align	4
_1699:
	dd	_367
	dd	1512
	dd	12
	align	4
_1703:
	dd	3
	dd	0
	dd	0
	align	4
_1702:
	dd	_367
	dd	1514
	dd	12
	align	4
_1704:
	dd	_367
	dd	1516
	dd	11
	align	4
_1706:
	dd	_367
	dd	1518
	dd	10
	align	4
_1728:
	dd	3
	dd	0
	dd	0
	align	4
_1709:
	dd	_367
	dd	1521
	dd	9
	align	4
_1714:
	dd	_367
	dd	1522
	dd	9
	align	4
_1719:
	dd	_367
	dd	1523
	dd	9
	align	4
_1724:
	dd	3
	dd	0
	dd	0
	align	4
_1723:
	dd	_367
	dd	1524
	dd	10
	align	4
_1727:
	dd	3
	dd	0
	dd	0
	align	4
_1726:
	dd	_367
	dd	1526
	dd	10
_1778:
	db	"mx",0
_1779:
	db	"my",0
_1780:
	db	"w",0
_1781:
	db	"h",0
	align	4
_1777:
	dd	3
	dd	0
	dd	2
	dd	_1778
	dd	_602
	dd	-120
	dd	2
	dd	_1779
	dd	_602
	dd	-124
	dd	2
	dd	_1780
	dd	_62
	dd	-128
	dd	2
	dd	_1781
	dd	_62
	dd	-132
	dd	0
	align	4
_1729:
	dd	_367
	dd	1531
	dd	9
	align	4
_1735:
	dd	_367
	dd	1532
	dd	9
	align	4
_1741:
	dd	_367
	dd	1534
	dd	9
	align	4
_1752:
	dd	3
	dd	0
	dd	0
	align	4
_1743:
	dd	_367
	dd	1535
	dd	10
	align	4
_1748:
	dd	3
	dd	0
	dd	0
	align	4
_1747:
	dd	_367
	dd	1535
	dd	73
	align	4
_1751:
	dd	3
	dd	0
	dd	0
	align	4
_1750:
	dd	_367
	dd	1535
	dd	84
	align	4
_1753:
	dd	_367
	dd	1537
	dd	9
	align	4
_1755:
	dd	_367
	dd	1538
	dd	9
	align	4
_1757:
	dd	_367
	dd	1539
	dd	9
	align	4
_1762:
	dd	_367
	dd	1540
	dd	9
	align	4
_1767:
	dd	_367
	dd	1541
	dd	9
	align	4
_1768:
	dd	_367
	dd	1543
	dd	9
	align	4
_1773:
	dd	3
	dd	0
	dd	0
	align	4
_1772:
	dd	_367
	dd	1544
	dd	10
	align	4
_1776:
	dd	3
	dd	0
	dd	0
	align	4
_1775:
	dd	_367
	dd	1546
	dd	10
	align	4
_1808:
	dd	3
	dd	0
	dd	0
	align	4
_1782:
	dd	_367
	dd	1550
	dd	9
	align	4
_1783:
	dd	_367
	dd	1552
	dd	9
	align	4
_1796:
	dd	3
	dd	0
	dd	0
	align	4
_1787:
	dd	_367
	dd	1553
	dd	10
	align	4
_1807:
	dd	3
	dd	0
	dd	0
	align	4
_1798:
	dd	_367
	dd	1555
	dd	10
	align	4
_1809:
	dd	_367
	dd	1560
	dd	7
	align	4
_1811:
	dd	_367
	dd	1564
	dd	6
_1846:
	db	"blendfunction",0
	align	4
_1845:
	dd	3
	dd	0
	dd	2
	dd	_1846
	dd	_62
	dd	-136
	dd	0
	align	4
_1815:
	dd	_367
	dd	1565
	dd	7
	align	4
_1816:
	dd	_367
	dd	1566
	dd	7
	align	4
_2386:
	dd	0x437f0000
	align	4
_1820:
	dd	_367
	dd	1567
	dd	7
	align	4
_1857:
	dd	3
	dd	0
	dd	0
	align	4
_1848:
	dd	_367
	dd	1569
	dd	7
	align	4
_1858:
	dd	_367
	dd	1572
	dd	6
	align	4
_58:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	104,100,99,32,61,61,32,119,112,33,32,80,108,101,97,115
	dw	101,32,112,111,115,116,32,97,32,77,97,120,71,85,73,32
	dw	98,117,103,32,114,101,112,111,114,116,46
	align	4
_1860:
	dd	_367
	dd	1574
	dd	6
	align	4
_1861:
	dd	_367
	dd	1575
	dd	6
	align	4
_1862:
	dd	_367
	dd	1577
	dd	6
	align	4
_1889:
	dd	_367
	dd	1582
	dd	3
_1992:
	db	"on",0
_1993:
	db	"r1",0
_1994:
	db	"b1",0
_1995:
	db	"g1",0
_1996:
	db	"r2",0
_1997:
	db	"b2",0
_1998:
	db	"g2",0
_1999:
	db	"vertical",0
	align	4
_1991:
	dd	1
	dd	_155
	dd	2
	dd	_384
	dd	_1373
	dd	-4
	dd	2
	dd	_1992
	dd	_62
	dd	-8
	dd	2
	dd	_1993
	dd	_62
	dd	-12
	dd	2
	dd	_1994
	dd	_62
	dd	-16
	dd	2
	dd	_1995
	dd	_62
	dd	-20
	dd	2
	dd	_1996
	dd	_62
	dd	-24
	dd	2
	dd	_1997
	dd	_62
	dd	-28
	dd	2
	dd	_1998
	dd	_62
	dd	-32
	dd	2
	dd	_1999
	dd	_62
	dd	-36
	dd	0
	align	4
_1894:
	dd	_367
	dd	1588
	dd	3
	align	4
_1906:
	dd	3
	dd	0
	dd	0
	align	4
_1896:
	dd	_367
	dd	1589
	dd	4
	align	4
_1905:
	dd	3
	dd	0
	dd	0
	align	4
_1900:
	dd	_367
	dd	1590
	dd	5
	align	4
_1904:
	dd	_367
	dd	1591
	dd	5
	align	4
_1990:
	dd	3
	dd	0
	dd	0
	align	4
_1908:
	dd	_367
	dd	1594
	dd	4
	align	4
_1921:
	dd	3
	dd	0
	dd	0
	align	4
_1914:
	dd	_367
	dd	1596
	dd	5
	align	4
_1918:
	dd	_367
	dd	1597
	dd	5
	align	4
_1989:
	dd	3
	dd	0
	dd	0
	align	4
_1923:
	dd	_367
	dd	1600
	dd	5
	align	4
_1988:
	dd	3
	dd	0
	dd	0
	align	4
_1955:
	dd	_367
	dd	1601
	dd	6
	align	4
_1959:
	dd	_367
	dd	1602
	dd	6
	align	4
_1963:
	dd	_367
	dd	1603
	dd	6
	align	4
_1967:
	dd	_367
	dd	1604
	dd	6
	align	4
_1971:
	dd	_367
	dd	1605
	dd	6
	align	4
_1975:
	dd	_367
	dd	1606
	dd	6
	align	4
_1979:
	dd	_367
	dd	1607
	dd	6
	align	4
_1983:
	dd	_367
	dd	1608
	dd	6
	align	4
_1987:
	dd	_367
	dd	1609
	dd	6
_2058:
	db	"CreatePanelEx",0
_2059:
	db	"Width",0
_2060:
	db	"group",0
_2061:
	db	"panel",0
	align	4
_2057:
	dd	1
	dd	_2058
	dd	2
	dd	_1869
	dd	_62
	dd	-4
	dd	2
	dd	_1870
	dd	_62
	dd	-8
	dd	2
	dd	_2059
	dd	_62
	dd	-12
	dd	2
	dd	_616
	dd	_62
	dd	-16
	dd	2
	dd	_2060
	dd	_419
	dd	-20
	dd	2
	dd	_1276
	dd	_62
	dd	-24
	dd	2
	dd	_758
	dd	_82
	dd	-28
	dd	2
	dd	_2061
	dd	_419
	dd	-32
	dd	0
	align	4
_2000:
	dd	_367
	dd	1640
	dd	2
	align	4
_2001:
	dd	_367
	dd	1646
	dd	3
	align	4
_2005:
	dd	_367
	dd	1648
	dd	3
	align	4
_2008:
	dd	3
	dd	0
	dd	0
	align	4
_2007:
	dd	_367
	dd	1649
	dd	4
	align	4
_2013:
	dd	3
	dd	0
	dd	0
	align	4
_2010:
	dd	_367
	dd	1651
	dd	4
	align	4
_2014:
	dd	_367
	dd	1654
	dd	3
	align	4
_2019:
	dd	3
	dd	0
	dd	0
	align	4
_2016:
	dd	_367
	dd	1654
	dd	17
	align	4
_2020:
	dd	_367
	dd	1655
	dd	3
	align	4
_2023:
	dd	_367
	dd	1658
	dd	3
	align	4
_2049:
	dd	3
	dd	0
	dd	0
	align	4
_2025:
	dd	_367
	dd	1659
	dd	4
	align	4
_2028:
	dd	_367
	dd	1660
	dd	4
	align	4
_2045:
	dd	3
	dd	0
	dd	0
	align	4
_2030:
	dd	_367
	dd	1661
	dd	5
	align	4
_2040:
	dd	_367
	dd	1662
	dd	5
	align	4
_2046:
	dd	_367
	dd	1664
	dd	4
	align	4
_2050:
	dd	_367
	dd	1667
	dd	3
	align	4
_2055:
	dd	3
	dd	0
	dd	0
	align	4
_2052:
	dd	_367
	dd	1667
	dd	33
	align	4
_2056:
	dd	_367
	dd	1670
	dd	3
_2071:
	db	"SetPanelExGradient",0
	align	4
_2070:
	dd	1
	dd	_2071
	dd	2
	dd	_418
	dd	_419
	dd	-4
	dd	2
	dd	_1992
	dd	_62
	dd	-8
	dd	2
	dd	_1993
	dd	_62
	dd	-12
	dd	2
	dd	_1994
	dd	_62
	dd	-16
	dd	2
	dd	_1995
	dd	_62
	dd	-20
	dd	2
	dd	_1996
	dd	_62
	dd	-24
	dd	2
	dd	_1997
	dd	_62
	dd	-28
	dd	2
	dd	_1998
	dd	_62
	dd	-32
	dd	2
	dd	_1999
	dd	_62
	dd	-36
	dd	2
	dd	_2061
	dd	_1373
	dd	-40
	dd	0
	align	4
_2062:
	dd	_367
	dd	1732
	dd	2
	align	4
_2064:
	dd	_367
	dd	1733
	dd	2
	align	4
_2069:
	dd	3
	dd	0
	dd	0
	align	4
_2066:
	dd	_367
	dd	1733
	dd	11
